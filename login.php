<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Finalizar pedido</title>        
        <link rel="icon" href="./admin/assets/img/favicon.ico">
        <link href="./admin/assets/css/bootstrap.css" rel="stylesheet">
        <link href="./admin/assets/css/datatables.css" rel="stylesheet">        
        <link href="./admin/assets/css/main.css" rel="stylesheet">        
    </head>
    <body>
        <?php include './admin/partials/topo-site.php'; ?>
        <div class="vertical-center">
            <div class="container align-items-center">
                <div class="row justify-content-center">
                    <div class="col-lg-6 align-self-center">
                        <div class="card border">
                            <div class="card-header text-center bg-dark">
                                <img src="admin/assets/img/logo.png" alt="">
                            </div>
                            <div id="cdLogin" class="card-body">
                                <div class="text-center w-75 m-auto">
                                    <h7 class="text-dark-50 text-center mt-0 font-weight-bold">Acesso ao painel</h7>
                                    <p class="text-muted mb-4">Insira o e-mail e senha para entrar no painel!</p>
                                </div>
                                <?php include './admin/includes/formLogin.php'; ?>
                                <button class="btn btn-success w-100 mt-3" id="btnLogin" type="button"> Entrar </button>                                
                                <button class="btn btn-success w-100 mt-3 hide" id="btnRecuperar" type="button"> Enviar </button>                                                                
                                <button class="btn btn-success w-100 mt-3 hide" id="btnAlterarSenha" type="button"> Alterar </button>                                
                                <div id="cdRecuperar" class="col-12 mt-3 text-center">
                                    <p><a href="javascript:void(0)" onclick="paginaRecuperar();" class="text-dark">Esqueceu sua senha?</a></p>
                                </div>                                                                                                                            
                            </div>
                            <div id="cdCadastrar" class="card-body hide">
                                <div class="text-center w-75 m-auto">
                                    <h7 class="text-dark-50 text-center mt-0 font-weight-bold">Nova conta</h7>
                                    <p class="text-muted mb-4">Inserir os dados abaixo para cadastrar sua conta!</p>
                                </div>
                                <form id="formCadastro" novalidate>                                                                   
                                    <?php include './admin/includes/formPessoa.php'; ?>
                                    <?php include './admin/includes/formPessoaEndereco.php'; ?>
                                    <br>
                                    <div class="form-check">
                                        <input id="txtConfirmar" name="txtConfirmar" type="checkbox" class="form-check-input" value="1">
                                        <label class="form-check-label" for="txtConfirmar">Li e concordo com os <a href="./admin/assets/docs/TermosLojista.pdf" target="_blank"><b>termos de serviço</b></a></label>
                                    </div>                                       
                                    <button class="btn btn-success w-100 mt-3" id="btnConfirmar" type="button"> Confirmar </button>                            
                                </form>
                            </div>
                            <div class="card-footer text-muted">
                                <div id="cdLoginFt" class="col-12 p-3 text-center">
                                    Deseja criar uma conta? <a href="javascript:void(0)" onclick="paginaCadastrar();" class="text-dark ml-1"><b>Clique aqui</b></a>
                                </div>
                                <div id="cdCadastrarFt" class="col-12 p-3 text-center hide">
                                    Para voltar a página de login <a href="javascript:void(0)" onclick="paginaLogin();" class="text-dark ml-1"><b>Clique aqui</b></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="admin/assets/js/lib/jquery-3.5.1.js" type="text/javascript"></script>
        <script src="admin/assets/js/lib/datatables.js" type="text/javascript"></script>
        <script src="admin/assets/js/lib/dataTables.bootstrap4.js" type="text/javascript"></script>
        <script src="admin/assets/js/lib/bootstrap.bundle.min.js" type="text/javascript"></script>
        <script src="admin/assets/js/lib/feather.min.js" type="text/javascript"></script>
        <script src="admin/assets/js/lib/bootbox.min.js" type="text/javascript"></script>
        <script src="admin/assets/js/lib/fnReloadAjax.js" type="text/javascript"></script>
        <script src="admin/assets/js/lib/jquery.mask.min.js" type="text/javascript"></script>
        <script src="admin/assets/js/lib/moment.js" type="text/javascript"></script>
        <script src="admin/assets/js/lib/jquery.validate.min.js" type="text/javascript"></script>
        <script src="admin/assets/js/lib/priceformat.min.js" type="text/javascript"></script>        
        <script src="admin/assets/js/main.js" type="text/javascript"></script>
        <script src="admin/assets/js/formLogin.js" type="text/javascript"></script>
        <script src="admin/assets/js/formPessoa.js" type="text/javascript"></script>
        <script src="admin/assets/js/formPessoaEndereco.js" type="text/javascript"></script>
        <script src="admin/assets/js/login.js" type="text/javascript"></script>        
    </body>
</html>