<div class="boxLoader">
    <div id="loader"></div>        
</div>
<div class="topbar">
    <button class="button-menu-mobile ml-2 border rounded">
        <span class="icon-menu"></span>
    </button>
    <div class="dropdown ml-auto">
        <button class="btn btn-outline-light d-flex flex-row mr-2 account border rounded" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <input type="hidden" id="sessionId" name="sessionId" value="<?php echo $pessoa_session["id"]; ?>">
            <img class="avatar rounded-circle" src="<?php echo $pessoa_session["img"]; ?>" alt="avatar">
            <span class="d-flex flex-column ml-2">
                <span class="account-name w-100 text-left"><?php echo $pessoa_session["empresa"]; ?></span> 
                <span class="account-description w-100 text-left"><?php echo $pessoa_session["nome"]; ?></span>                
            </span>           
        </button>
        <div class="dropdown-menu dropdown-menu-right">
            <a class="dropdown-item" href="meu-perfil.php"><span data-feather="user"></span> Meu Perfil</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="javascript:void(0)" onclick="sysLogout();"><span data-feather="log-out"></span> Sair</a>
        </div>
    </div>
</div>