<nav id="menu">
    <a class="header-menu navbar-brand" href="index.php">
        <img src="assets/img/logo.png" alt="Logo"/>
    </a>
    <div class="content-menu sidebar">    
    <?php foreach ($pessoa_menu_lateral as $value) { ?>
        <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1">
            <span><?php echo $value["text"]; ?></span>
        </h6>
        <ul class="nav flex-column">
            <?php foreach ($value["menu"] as $menu) { 
                if (!isset($menu["menu"])) { ?>
                <li class="nav-item">
                    <a class="nav-link <?php echo ($pageSel == $menu["link"] ? "active" : ""); ?>" href="<?php echo $menu["link"]; ?>">
                        <span data-feather="<?php echo $menu["icon"]; ?>"></span> <?php echo $menu["text"]; ?>                        
                        <span class="badge badge-pill badge-menu <?php echo ($menu["info"] > 0 ? "" : "hide"); ?>"><?php echo $menu["info"]; ?></span>
                    </a>
                </li>
            <?php }} ?>
        </ul>
    <?php } ?>                
    </div>          
</nav>