<?php include './Connections/configini.php'; ?>
<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title><?php echo $name_page; ?></title>
        <link rel="icon" href="./assets/img/favicon.ico">
        <link href="./assets/css/bootstrap.css" rel="stylesheet">
        <link href="./assets/css/bootstrap-toggle.min.css" rel="stylesheet"/>
        <link href="./assets/css/datatables.css" rel="stylesheet">        
        <link href="./assets/css/radio.css" rel="stylesheet"/>
        <link href="./assets/css/main.css" rel="stylesheet">
        <link href="./assets/css/cropper.css" rel="stylesheet" type="text/css"/>        
    </head>
    <body>
        <div class="wraper">
            <aside><?php include './partials/menu-lateral.php'; ?></aside>
            <main>
                <header><?php include './partials/topo.php'; ?></header>              
                <section>
                    <div class="container-fluid">
                        <div class="p-3 pt-4">
                            <h1 class="h5"><span class="align-baseline" data-feather="<?php echo $icon_page; ?>"></span> <?php echo $name_page; ?></h1>
                        </div>                    
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card bg-primary mb-3">
                                    <div class="card-body profile-user-box">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="row align-items-center">
                                                    <div class="col-auto">
                                                        <div class="avatar-lg">
                                                            <button id="imageDirAdd" class="btn btn-sm rounded-circle position-absolute btn-success" data-toggle="modal" data-target="#FormCropper" data-type="produto"><span data-feather="edit"></span></button>
                                                            <button id="imageDirDel" class="btn btn-sm rounded-circle position-absolute btn-danger hide" data-type="produto"><span data-feather="x"></span></button>
                                                            <img id="imageDirView" src="assets/img/no_image.png" alt="" class="img-thumbnail">                                                            
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <h4 class="mt-1 mb-1 text-white"></h4>
                                                        <p class="font-13 text-white-50 mb-1"></p>
                                                        <p class="font-13 text-white-50 mb-1"></p>
                                                        <div class="rating text-warning" data-rate-value=0></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>                            
                        <div class="row">
                            <div class="col-md-4">
                                <div class="card h-100">
                                    <div class="card-body">
                                        <form id="formCadastro" novalidate>                                        
                                            <div class="form-row">                                            
                                                <input id="txtId" name="txtId" type="hidden" value="">
                                                <input id="imageDir" name="imageDir" type="hidden" value=""/>
                                                <input id="imageNow" name="imageNow" type="hidden" value=""/>                                                
                                                <div class="col-md-12">
                                                    <label for="txtNome">Título:</label>
                                                    <input id="txtNome" name="txtNome" type="text" class="form-control" placeholder="Título do Anúncio" value="">
                                                </div>
                                                <div class="col-md-12">
                                                    <label for="txtTipo">Tipo:</label>
                                                    <select id="txtTipo" name="txtTipo" class="form-control" value="">
                                                        <option value="0">Serviço</option>                                                    
                                                        <option value="1">Produto</option>
                                                        <option value="2">Aluguel de Espaços</option>
                                                    </select>
                                                </div>                                                                                        
                                                <div class="col-md-12">
                                                    <label for="txtGrupo">Grupo:</label>
                                                    <select id="txtGrupo" name="txtGrupo" class="form-control" value=""></select>
                                                </div>
                                                <div class="col-md-12">
                                                    <label for="txtCategoria">Minhas Categorias:</label>
                                                    <select id="txtCategoria" name="txtCategoria" class="form-control" value=""></select>
                                                </div>                                                   
                                                <div class="form-group col-md-12">
                                                    <div class="form-check">
                                                        <input id="txtInativo" name="txtInativo" type="checkbox" class="form-check-input" value="1">
                                                        <label class="form-check-label" for="txtInativo">Inativo</label>
                                                    </div>
                                                </div>                                            
                                            </div>
                                        </form>                                            
                                    </div>
                                </div>
                            </div>                            
                            <div class="col-md-8">
                                <div class="card h-100">
                                    <div class="card-body">
                                        <ul class="nav nav-pills nav-fill mb-3" id="v-pills-tab" role="tablist">
                                            <li class="nav-item">
                                                <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Anúncios</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" id="descricao-tab" data-toggle="tab" href="#descricao" role="tab" aria-controls="descricao" aria-selected="false">Descrição</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" id="divulgacao-tab" data-toggle="tab" href="#divulgacao" role="tab" aria-controls="divulgacao" aria-selected="false">Tipo de Anúncio</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" id="financeiro-tab" data-toggle="tab" href="#financeiro" role="tab" aria-controls="financeiro" aria-selected="false">Financeiro</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" id="imagens-tab" data-toggle="tab" href="#imagens" role="tab" aria-controls="imagens" aria-selected="false">Mídia</a>
                                            </li>
                                        </ul>                                        
                                        <div class="tab-content">
                                            <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                                                <form id="formAnuncio" novalidate>
                                                    <div class="form-row">
                                                        <input id="txtIdAnuncio" name="txtIdAnuncio" type="hidden" value="">
                                                        <div class="col-md-12">
                                                            <label for="txtNomeSub">Subtítulo:</label>
                                                            <input id="txtNomeSub" name="txtNomeSub" type="text" class="form-control" placeholder="Subtítulo do Anúncio" value="">
                                                        </div>
                                                        <div class="form-group col-md-3 col-6">
                                                            <label for="txtQuantidade">Quantidade</label>
                                                            <input id="txtQuantidade" name="txtQuantidade" type="text" class="form-control" value="" placeholder="1">
                                                        </div>                                            
                                                        <div class="form-group col-md-3 col-6">
                                                            <label for="txtValor">Valor R$</label>
                                                            <input id="txtValor" name="txtValor" type="text" class="form-control" value="" placeholder="0,00">
                                                        </div>                                            
                                                        <div class="form-group col-md-3 col-6">
                                                            <label for="txtCashback">Cashback %</label>
                                                            <input id="txtCashback" name="txtCashback" type="text" class="form-control" value="" placeholder="10,00">
                                                        </div>
                                                        <div class="form-group col-md-3 col-6 text-right align-self-end">
                                                            <button id="btnAnuncio" type="button" class="btn btn-success btn-sm mb-1 hide"><span data-feather="save"></span><span class="d-none d-sm-inline"> Salvar</span></button>                                            
                                                        </div>
                                                    </div>
                                                </form>
                                                <div id="tableAnuncio" class="hide">
                                                    <table id="tbAnuncio" class="table table-striped table-bordered dt-responsive w-100 mt-3">
                                                        <thead class="thead-light">
                                                            <tr>                                                                                                        
                                                                <th style="width: 40%;">Sub Título</th>
                                                                <th style="width: 20%;">Valor R$</th>
                                                                <th style="width: 20%;">Cashback %</th>
                                                                <th style="width: 20%;">Ação</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody></tbody>
                                                    </table>
                                                </div>
                                            </div>                                            
                                            <div class="tab-pane fade show" id="descricao" role="tabpanel" aria-labelledby="descricao-tab">                                               
                                                <textarea id="txtDescricao" name="txtDescricao" class="w-100"></textarea>
                                            </div>                                                
                                            <div class="tab-pane fade show" id="divulgacao" role="tabpanel" aria-labelledby="divulgacao-tab">
                                                <form id="formDivulgacao" novalidate>
                                                    <div class="row">
                                                        <div class="col-sm-6 mb-3 mb-md-0">
                                                            <div class="card border h-100">
                                                                <div class="card-body">
                                                                    <h6 class="card-title mb-4">Selecione uma forma de divulgação:</h6>
                                                                    <div class="row">
                                                                        <div class="col-4">
                                                                            <label class="text-center" data-toggle="tooltip" data-placement="top" data-html="true">
                                                                                <input type="radio" name="txtDivulgacao" value="1" checked>
                                                                                <img src="assets/img/type/1.png" alt="" height="50"/>                                        
                                                                                <p class="mb-0">Bronze</p>
                                                                                <div class="star ml-3 text-warning" data-rate-value=1></div>
                                                                            </label>
                                                                        </div>
                                                                        <div class="col-4">                                                                        
                                                                            <label class="text-center" data-toggle="tooltip" data-placement="top" data-html="true">
                                                                                <input type="radio" name="txtDivulgacao" value="2">
                                                                                <img src="assets/img/type/2.png" alt="" height="50"/>                                        
                                                                                <p class="mb-0">Prata</p>
                                                                                <div class="star ml-3 text-warning" data-rate-value=2></div>
                                                                            </label>
                                                                        </div>
                                                                        <div class="col-4">                                                                                                                                            
                                                                            <label class="text-center" data-toggle="tooltip" data-placement="top" data-html="true">
                                                                                <input type="radio" name="txtDivulgacao" value="3">
                                                                                <img src="assets/img/type/3.png" alt="" height="50"/>                                        
                                                                                <p class="mb-0">Ouro</p>
                                                                                <div class="star ml-3 text-warning" data-rate-value=3></div>
                                                                            </label>
                                                                        </div>
                                                                    </div>
                                                                    <br>
                                                                    <p class="mb-0">Quando o seu anúncio expirar, você irá receber uma notificação em Meus Anúncios.</p>
                                                                    <p class="mb-0">Para  renovar publique novamente.</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <div class="card border h-100">
                                                                <div id="lblAnuncio" class="card-body"></div>
                                                            </div>
                                                        </div>
                                                    </div>                                                    
                                                </form>
                                            </div>                                                                                                
                                            <div class="tab-pane fade show" id="financeiro" role="tabpanel" aria-labelledby="financeiro-tab">
                                                <form id="formFinanceiro" novalidate>
                                                    <div class="form-row">
                                                        <div class="col-sm-6 mb-3">
                                                            <div class="card border">
                                                                <div class="card-body">
                                                                    <h5 class="card-title"><img src="assets/img/payments/cartao.png" alt="" height="50"/> Cartão de Crédito</h5>                                                                
                                                                    <select id="txtCartao" name="txtCartao" class="form-control" value="">
                                                                        <?php for ($i = 1; $i <= 12; $i++) { ?>
                                                                        <option value="<?php echo $i; ?>" selected>Pagamento em até <?php echo $i; ?>x</option>
                                                                        <?php } ?>
                                                                        <option value="0">Desativar Cartão</option>
                                                                    </select>                                                                
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6 mb-3">
                                                            <div class="card border">
                                                                <div class="card-body">
                                                                    <h5 class="card-title"><img src="assets/img/payments/pix.png" alt="" height="50"/> Pagamento PIX</h5>                                                                
                                                                    <select id="txtPix" name="txtPix" class="form-control" value="">
                                                                        <option value="1" selected>Ativar Pix</option>
                                                                        <option value="0">Desativar Pix</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-6 mb-3">
                                                            <div class="card border">
                                                                <div class="card-body">
                                                                    <h5 class="card-title"><img src="assets/img/payments/boleto.png" alt="" height="50"/> Boleto bancário</h5>                                                                
                                                                    <select id="txtBoleto" name="txtBoleto" class="form-control" value="">
                                                                        <option value="1" selected>Ativar Boleto</option>
                                                                        <option value="0">Desativar Boleto</option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>                                                        
                                                    </div>
                                                </form>
                                            </div>
                                            <div class="tab-pane fade show" id="imagens" role="tabpanel" aria-labelledby="imagens-tab">
                                                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                                                    <strong>Atenção!</strong> Crie vídeos curtos com o telefone na vertical para uma melhor experiência no anuncio!
                                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                      <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>                                                
                                                <button id="btnAdicionarImgs" type="button" class="btn btn-primary mb-3 btn-sm" data-toggle="modal" data-target="#FormArquivo" data-type="pFotos" disabled>
                                                    <span data-feather="camera"></span><p class="mb-0 h6">Incluir Fotos</p><p class="mb-0">Limite de 5</p></button>
                                                <button id="btnAdicionarVideos" type="button" class="btn btn-warning mb-3 btn-sm" data-toggle="modal" data-target="#FormArquivo" data-type="videos" disabled>
                                                    <span data-feather="video"></span><p class="mb-0 h6">Incluir Vídeos</p><p class="mb-0">Max. 10Mb</p></button>
                                                <div class="alert alert-warning" role="alert">
                                                    <div class="row justify-content-center">
                                                        <img src="assets/img/type/3.png" alt="" height="25" class="mr-3">
                                                        <div>Para adicionar vídeo, selecione tipo de anúncio ouro</div>
                                                        <div class="star ml-3 text-warning" data-rate-value=3></div>
                                                    </div>                                                     
                                                </div>
                                                <div id="dvImagens" class="row"></div>
                                            </div>                                                
                                        </div>
                                    </div>
                                </div>                                                                                
                            </div>                            
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card mt-3">
                                    <div class="card-body text-right">
                                        <button type="button" id="btnAlterar" class="btn btn-success hide">Alterar Anúncio</button>                                        
                                        <button type="button" id="btnProduto" class="btn btn-success">Próximo</button>
                                    </div>
                                </div>
                            </div>
                        </div>                        
                        <?php include './includes/modal/mdArquivo.php'; ?>
                        <?php include './includes/modal/mdCropper.php'; ?>                        
                    </div>
                </section>
            </main>
        </div>
        <?php include './partials/lib-js.php'; ?>
        <script src="assets/js/lib/cropper.js" type="text/javascript"></script>        
        <script src="assets/js/lib/bootstrap-toggle.min.js" type="text/javascript"></script>
        <script src="assets/js/arquivos.js" type="text/javascript"></script>        
        <script src="assets/js/produto.js" type="text/javascript"></script>
    </body>
</html>