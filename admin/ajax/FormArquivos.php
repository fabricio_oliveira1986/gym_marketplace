<?php

include '../Connections/configini.php';

$dados = filter_input_array(INPUT_POST);

if (isset($_POST["btnArquivo"])) {
    $toReturn = [];
    for ($i = 0; $i < count($_FILES['txtArquivo']['name']); $i++) {
        $dir_save = getPathFile($dados);        
        $extensao = strtolower(strrchr($_FILES['txtArquivo']['name'][$i], '.'));
        $error = validateFiles($dados, $extensao, $dir_save, $_FILES['txtArquivo'], $i);
        if ($error == "") {
            $newFile = ($dados["txtType"] == "documentos" ? $dados["txtIdItem"] . ".png" : md5(microtime()) . $extensao);
            if (!file_exists($dir_save)) {
                mkdir($dir_save, 0777, true);
            }
            if (move_uploaded_file($_FILES['txtArquivo']['tmp_name'][$i], $dir_save . $newFile)) {
                $toReturn[] = $newFile;
                saveFile($conn, $dados, $newFile);
            }
        } else {
            echo $error; exit;
        }
    }
    echo json_encode($toReturn);
    exit;
}

function validateFiles($dados, $extensao, $dir_save, $arquivo, $i) {
    if ($arquivo['tmp_name'][$i] == "" || $arquivo["error"][$i] != 0) {
        return "Erro ao salvar arquivo!";
    } else if ($arquivo['size'][$i] > 100000000) {
        return "Tamanho de arquivo maior que o permitido!";
    } else if ($dados["txtType"] == "videos" && !strstr('.mp4;', $extensao)) {
        return "Vídeo suportado .mp4;";
    } else if ($dados["txtType"] != "videos" && !strstr('.jpg;.jpeg;.gif;.png;.mp4;', $extensao)) {
        return "Arquivos suportados .jpg;.jpeg;.gif;.png;.mp4;";
    } else if (file_exists($dir_save) && count(scandir($dir_save)) > 6) {
        return "O limite de arquivos são 5!";
    }
    return "";
}

if (isset($_POST["delArquivo"])) {
    $path = getPathFile($dados);
    if ($dados["txtCapa"] == "S") {
        saveFile($conn, $dados, null);        
    }
    if (file_exists($path . $dados["txtFile"])) {
        if (unlink($path . $dados["txtFile"])) {
            echo json_encode("YES");
        }
    }
}

if (isset($_POST["lerArquivo"])) {
    $data = [];
    $path = getPathFile($dados);
    if (is_dir($path)) {
        foreach (scandir($path) as $key => $value) {
            if (!in_array($value, array(".", "..")) && !is_dir($path . $value)) {
                $data[] = (object) ["id" => $value];
            }
        }    
    }
    echo json_encode($data);
}