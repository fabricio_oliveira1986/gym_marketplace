<?php

include '../Connections/configini.php';
include './includes/mdlPessoa.php';
include './includes/mdlEmail.php';

$dados = filter_input_array(INPUT_POST);

if (isset($_POST['btnLogin'])) {
    $login = getLogin($conn, $dados);
    if ($login) {
        echo json_encode($login);
    } else {
        echo 'Usuário ou senha inválidos!';
    }
    exit;
}

if (isset($_POST['btnLogout'])) {
    session_destroy();
    echo json_encode(['exit' => "YES"]);
    exit;
}

if (isset($_POST['btnRecuperar'])) {
    if (getCadastroEmail($conn, $dados["txtEmail"])) {
        $return = sendMsg($dados["txtEmail"], $dados["txtEmail"], 
        "Recuperar Senha", "Para recuperar sua senha entre no link abaixo:", 
        "", "", "http://gymbrother.com.br/login.php?conta=" . encrypt($dados["txtEmail"], "gymBrother7034", true));
        echo json_encode(['send' => $return]);
    } else {
        echo "E-mail inválido!";
    }
    exit;
}

if (isset($_POST['btnAlterarSenha'])) {    
    $usuario = getCadastroEmail($conn, encrypt($dados["txtConta"], "gymBrother7034", false));
    if ($usuario) {
        echo json_encode(['id_pessoa' => salvarSenha($conn, $dados["txtSenha"], $usuario->id)]);
    } else {
        echo "E-mail inválido!";
    }
}

if (isset($_POST["btnSalvar"])) {
    if (!isset($_SESSION['id']) || !is_numeric($_SESSION['id'])) {
        $id = salvarPessoa($conn, $dados, "");        
        if (!is_numeric($id)) {
            echo "ocorreu um erro ao salvar o usuário";
            exit;
        }        
        salvarEndereco($conn, $dados, $id);
        $_SESSION['id'] = $id;        
    } else {
        $id = salvarPessoa($conn, $dados, $_SESSION['id']);
    }
    if (isset($dados["imageDir"]) && strlen($dados["imageDir"]) > 0) {
        $dados["txtType"] = "perfil";
        saveFile($conn, $dados, saveImgDirectory($dados));
    }
    echo json_encode(['id_usuario' => $id]);
    exit;
}

if (isset($_POST["btnPerfil"])) {
    echo json_encode(getPerfil($conn, $_SESSION['id']));
    exit;
}

if (isset($_POST["btnSalvarEndereco"])) {
    $id_endereco = salvarEndereco($conn, $dados, $_SESSION['id']);
    echo json_encode(['id_endereco' => $id_endereco]);
    exit;
}

if (isset($_POST["btnDeleteEndereco"])) {
    if (is_numeric($_POST["txtIdEndereco"])) {
        $id_endereco = inativarEndereco($conn, $_POST["txtIdEndereco"], $_SESSION['id'], 1);
        echo json_encode((is_numeric($id_endereco) ? "YES" : "NO"));
    }
}

if (isset($_POST["btnDeleteCartao"])) {
    if (is_numeric($_POST["txtIdCartao"])) {
        $id_cartao = inativarCartao($conn, $_POST["txtIdCartao"], $_SESSION['id'], 1);
        echo json_encode((is_numeric($id_cartao) ? "YES" : "NO"));
    }
}

if (isset($_POST["btnBuscarEstados"])) {
    echo json_encode(getEstados($conn, false));
}

if (isset($_POST["btnBuscarCidades"])) {
    echo json_encode(getCidades($conn, $_POST["txtEstado"], false));
}

if (isset($_POST["btnCashback"])) {
    echo json_encode(getCashback($conn, $_SESSION['id']));
}

function getLogin($conn, $dados) {
    $prepareSQL = $conn->prepare("SELECT * FROM sf_pessoa WHERE inativo = 0 and email = :login AND senha = :password limit 1");
    $prepareSQL->bindValue(':login', $dados['txtEmailLogin']);
    $prepareSQL->bindValue(':password', md5($dados['txtSenhaLogin']));
    $prepareSQL->execute();
    $res = $prepareSQL->fetch(PDO::FETCH_OBJ);
    if ($res) {
        $_SESSION['id'] = $res->id;
        return ['id' => $res->id, 'nome' => $res->nome];
    }
    return false;
}

function existeCadastro($conn, $dados) {
    $prepareSQL = $conn->prepare("SELECT * FROM sf_pessoa WHERE cpf = :cpf or email = :email limit 1");
    $prepareSQL->bindValue(':cpf', $dados['txtCPF']);
    $prepareSQL->bindValue(':email', $dados['txtEmail']);
    $prepareSQL->execute();
    $res = $prepareSQL->fetch(PDO::FETCH_OBJ);
    if ($res) {
        return true;
    }
    return false;
}