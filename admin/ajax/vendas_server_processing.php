<?php

include '../Connections/configini.php';

$loja = getLojaSession($conn);
$sTable = "sf_vendas inner join sf_vendas_parcelas on sf_vendas.id = sf_vendas_parcelas.id_venda
inner join sf_pessoa on sf_vendas.id_pessoa = sf_pessoa.id";
$aColumns = ["sf_pessoa.nome"];
$sLimit = "";
$sWhere = "";
$sWhere2 = "tipo_documento in ('C', 'B', 'P', 'T') AND id_empresa = " . (isset($loja->id) ? $loja->id : "-1");
$sOrder = "ORDER BY data desc ";

if (isset($loja->id)) {
    $prepareSQL = $conn->prepare("update sf_vendas v inner join sf_vendas_parcelas vp on v.id = vp.id_venda
    set v.status = 'C' where vp.id_empresa = :id and v.status = 'A' and DATE_ADD(v.data, INTERVAL 4 DAY) < now()");
    $prepareSQL->bindValue(':id', $loja->id);
    $prepareSQL->execute();
}

if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
    $sLimit = "LIMIT " . $_GET['iDisplayStart'] . ", " . $_GET['iDisplayLength'];
}

if (isset($_GET['status']) && in_array($_GET['status'], array("A", "P", "C", "E", "N"))) {
    $sWhere2 .= " AND sf_vendas.status = '" . $_GET['status'] . "'";
}

if (isset($_GET['tipo']) && in_array($_GET['tipo'], array("C", "B", "P", "T"))) {
    $sWhere2 .= " AND sf_vendas.tipo_documento = '" . $_GET['tipo'] . "'";
}

if (isset($_GET['entrega']) && in_array($_GET['entrega'], array("E", "P"))) {
    $sWhere2 .= " AND sf_vendas.entrega = '" . $_GET['entrega'] . "'";
}

if (isset($_GET['sSearch']) && strlen($_GET['sSearch'])) {
    $sWhere = " AND (";
    for ($i = 0; $i < count($aColumns); $i++) {
        $sWhere .= $aColumns[$i] . " LIKE '%" . $_GET['sSearch'] . "%' OR ";
    }
    $sWhere = substr_replace($sWhere, "", -3) . ')';
}

$totalRecords = $conn->query("SELECT count(*) from $sTable WHERE $sWhere2")->fetchColumn();
$totalRecordwithFilter = $conn->query("SELECT count(*) from $sTable WHERE $sWhere2 $sWhere")->fetchColumn();
$stmt = $conn->prepare("SELECT sf_vendas.*, sf_pessoa.nome, sf_pessoa.id_foto, sf_vendas_parcelas.valor,
(select sum(valor) from sf_vendas_parcelas px where px.id_empresa in (1,2) and px.id_venda = sf_vendas.id) valor_custo    
FROM $sTable WHERE $sWhere2 $sWhere $sOrder $sLimit");
$stmt->execute();
$return = $stmt->fetchAll(PDO::FETCH_OBJ);

$output = [
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => $totalRecords,
    "iTotalDisplayRecords" => $totalRecordwithFilter,
    "aaData" => $return
];

echo json_encode($output);