<?php

include '../Connections/configini.php';

$sTable = "sf_pessoa_endereco inner join tb_cidades on id_cidade = cidade_codigo 
inner join tb_estados on estado_codigo = cidade_codigoEstado";
$aColumns = ["id", "cep", "endereco", "numero", "bairro", "complemento", 
"estado_codigo", "estado_sigla", "id_cidade", "cidade_nome"];
$sLimit = "";
$sWhere = "";
$sWhere2 = "id_pessoa = " . $_SESSION['id'];
$sOrder = "ORDER BY " . $aColumns[0];
$data = [];

if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
    $sLimit = "LIMIT " . $_GET['iDisplayStart'] . ", " . $_GET['iDisplayLength'];
}

if (isset($_GET['sSearch']) && strlen($_GET['sSearch'])) {
    $sWhere = " AND (";
    for ($i = 0; $i < count($aColumns); $i++) {
        $sWhere .= $aColumns[$i] . " LIKE '%" . $_GET['sSearch'] . "%' OR ";
    }
    $sWhere = substr_replace($sWhere, "", -3) . ')';
}

$totalRecords = $conn->query("SELECT count(*) from $sTable WHERE $sWhere2")->fetchColumn();
$totalRecordwithFilter = $conn->query("SELECT count(*) from $sTable WHERE $sWhere2 $sWhere")->fetchColumn();

$stmt = $conn->prepare("SELECT " . implode(",", $aColumns) . " FROM $sTable WHERE $sWhere2 $sWhere $sOrder $sLimit");
$stmt->execute();
$return = $stmt->fetchAll(PDO::FETCH_OBJ);

$output = [
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => $totalRecords,
    "iTotalDisplayRecords" => $totalRecordwithFilter,
    "aaData" => $return
];

echo json_encode($output);