<?php

include '../Connections/configini.php';

$sTable = "sf_empresa_produto ep inner join sf_empresa_produto_anuncio epa on ep.id = epa.id_empresa_produto";
$aColumns = ["ep.id", "nome", "tipo", "categoria", "categoria_empresa", "ep.descricao", "divulgacao", "boleto", 
"cartao_credito", "id_foto", "epa.inativo", "quantidade", "valor", "cashback", "epa.descricao"];
$sLimit = "";
$sWhere = "";
$loja = getLojaSession($conn);
$sWhere2 = "id_empresa = " . (isset($loja->id) && is_numeric($loja->id) ? $loja->id : 0);
$sOrder = "ORDER BY " . $aColumns[0];
$data = [];

if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
    $sLimit = "LIMIT " . $_GET['iDisplayStart'] . ", " . $_GET['iDisplayLength'];
}

if (isset($_GET['tipo']) && is_numeric($_GET['tipo'])) {
    $sWhere2 .= " AND epa.inativo = " . $_GET['tipo'];
}

if (isset($_GET['sSearch']) && strlen($_GET['sSearch'])) {
    $sWhere = " AND (";
    for ($i = 0; $i < count($aColumns); $i++) {
        $sWhere .= $aColumns[$i] . " LIKE '%" . $_GET['sSearch'] . "%' OR ";
    }
    $sWhere = substr_replace($sWhere, "", -3) . ')';
}

$totalRecords = $conn->query("SELECT count(*) from $sTable WHERE $sWhere2")->fetchColumn();
$totalRecordwithFilter = $conn->query("SELECT count(*) from $sTable WHERE $sWhere2 $sWhere")->fetchColumn();

$stmt = $conn->prepare("SELECT " . implode(",", $aColumns) . " subtitulo, fn_vendas_anuncio(epa.id) vendas FROM $sTable WHERE $sWhere2 $sWhere $sOrder $sLimit");
$stmt->execute();
$return = $stmt->fetchAll(PDO::FETCH_OBJ);

$output = [
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => $totalRecords,
    "iTotalDisplayRecords" => $totalRecordwithFilter,
    "aaData" => $return
];

echo json_encode($output);
