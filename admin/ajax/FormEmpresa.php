<?php

include '../Connections/configini.php';
include './includes/mdlPessoa.php'; 
include './includes/mdlSafe.php'; 

$dados = filter_input_array(INPUT_POST);

if (isset($_POST["btnSalvar"])) { 
    try {
        $conn->beginTransaction();
        $id = salvarEmpresa($conn, $dados);
        if (!is_numeric($dados["txtId"])) {
            $dados["txtId"] = $id;
            salvarPermissao($conn, $dados);
        }
        $id_endereco = salvarEnderecoEmpresa($conn, $dados);
        $id_banco = salvarBanco($conn, $dados);
        if (isset($dados["imageDir"]) && strlen($dados["imageDir"]) > 0) {
            $dados["txtType"] = "empresa";
            saveFile($conn, $dados, saveImgDirectory($dados));
        }
        $conn->commit();
        SaveSafe($conn);
        echo json_encode(['id_empresa' => $id, 'id_endereco' => $id_endereco, 'id_banco' => $id_banco]);
        exit;
    } catch (Exception $e) {
        echo $e->getMessage();
        $conn->rollBack();
        exit;
    }
}

if (isset($_POST["btnLoja"])) {
    echo json_encode(getLoja($conn));
    exit;
}

function salvarEmpresa($conn, $dados) {
    $valores = ['txtRazaoSocial', 'txtNomeFantasia', 'txtCnpj'];
    if (is_numeric($dados["txtId"])) {
        $prepareSQL = $conn->prepare("UPDATE sf_empresa SET razao_social = :txtRazaoSocial, 
        nome_fantasia = :txtNomeFantasia, cnpj = :txtCnpj WHERE id = :id");
        $prepareSQL->bindValue(':id', $dados["txtId"]);
    } else if (existeCadastro($conn, $dados)) {
        echo "E-mail ou CNPJ já pertence a um cadastro no sistema, não é possível usar o mesmo!";
        exit;
    } else {
        $prepareSQL = $conn->prepare("INSERT INTO sf_empresa (razao_social, nome_fantasia, cnpj) values 
        (:txtRazaoSocial, :txtNomeFantasia, :txtCnpj)");
    }
    foreach ($valores as $item) {
        $prepareSQL->bindValue(":$item", $dados[$item]);
    }
    $prepareSQL->execute();
    return is_numeric($dados["txtId"]) ? $dados["txtId"] : $conn->lastInsertId();
}

function salvarPermissao($conn, $dados) {
    if (is_numeric($dados["txtId"])) {
        $prepareSQL = $conn->prepare("INSERT INTO sf_pessoa_empresa (id_pessoa, id_empresa) values (:id_pessoa, :id_empresa)");
        $prepareSQL->bindValue(':id_pessoa', $_SESSION['id']);
        $prepareSQL->bindValue(':id_empresa', $dados["txtId"]);
        $prepareSQL->execute();
        return true;
    }
    return false;
}

function salvarEnderecoEmpresa($conn, $dados) {
    $valores = ['txtId', 'txtCep', 'txtEndereco', 'txtBairro', 'txtComplemento', 'txtNumero', 'txtCidade'];
    if (is_numeric($dados["txtIdEndereco"])) {
        $prepareSQL = $conn->prepare("UPDATE sf_empresa_endereco SET
        id_empresa = :txtId, descricao = 'Sede', cep = :txtCep, endereco = :txtEndereco, 
        numero = :txtNumero, bairro = :txtBairro, id_cidade = :txtCidade, complemento = :txtComplemento WHERE id = :id");
        $prepareSQL->bindValue(':id', $dados["txtIdEndereco"]);
    } else {
        $prepareSQL = $conn->prepare("INSERT INTO sf_empresa_endereco
        (id_empresa, descricao, cep, endereco, numero, bairro, id_cidade, complemento) values 
        (:txtId, 'Sede', :txtCep, :txtEndereco, :txtNumero, :txtBairro, :txtCidade, :txtComplemento)");
    }
    foreach ($valores as $item) {
        $prepareSQL->bindValue(":$item", $dados[$item]);
    }
    $prepareSQL->execute();
    return is_numeric($dados["txtIdEndereco"]) ? $dados["txtIdEndereco"] : $conn->lastInsertId();
}

function salvarBanco($conn, $dados) {
    $valores = ['txtId', 'txtBanco', 'txtAgencia', 'txtAgenciaDv', 'txtConta', 'txtContaDv', 'txtTipoConta'];
    if (is_numeric($dados["txtIdBanco"])) {
        $prepareSQL = $conn->prepare("UPDATE sf_empresa_banco SET
        id_empresa = :txtId, banco = :txtBanco, agencia = :txtAgencia, agencia_dv = :txtAgenciaDv, conta = :txtConta, conta_dv = :txtContaDv, 
        tipo = :txtTipoConta WHERE id = :id");
        $prepareSQL->bindValue(':id', $dados["txtIdBanco"]);
    } else {
        $prepareSQL = $conn->prepare("INSERT INTO sf_empresa_banco
        (id_empresa, banco, agencia, agencia_dv, conta, conta_dv, tipo) values 
        (:txtId, :txtBanco, :txtAgencia, :txtAgenciaDv, :txtConta, :txtContaDv, :txtTipoConta)");
    }
    foreach ($valores as $item) {
        $prepareSQL->bindValue(":$item", $dados[$item]);
    }
    $prepareSQL->execute();
    return is_numeric($dados["txtIdBanco"]) ? $dados["txtIdBanco"] : $conn->lastInsertId();
}

function getLoja($conn) {
    $prepareSQL = $conn->prepare("SELECT * FROM sf_empresa 
    WHERE id in (select id_empresa from sf_pessoa_empresa where id_pessoa = :id) limit 1");
    $prepareSQL->bindValue(':id', $_SESSION['id']);
    $prepareSQL->execute();
    $return = $prepareSQL->fetch(PDO::FETCH_OBJ);
    if (isset($return->id)) {
        $return->endereco = getLojaEndereco($conn, $return->id);
        $return->banco = getLojaBanco($conn, $return->id);
    }
    return $return;
}

function getLojaEndereco($conn, $id) {
    $prepareSQL = $conn->prepare("SELECT * FROM sf_empresa_endereco WHERE id_empresa = :id limit 1");
    $prepareSQL->bindValue(':id', $id);
    $prepareSQL->execute();
    $return = $prepareSQL->fetch(PDO::FETCH_OBJ);
    $return->id_cidade = getCidade($conn, $return->id_cidade);
    return $return;
}

function existeCadastro($conn, $dados) {
    $prepareSQL = $conn->prepare("SELECT * FROM sf_empresa WHERE cnpj = :cnpj limit 1");
    $prepareSQL->bindValue(':cnpj', $dados['txtCnpj']);
    $prepareSQL->execute();
    $res = $prepareSQL->fetch(PDO::FETCH_OBJ);
    if ($res) {
        return true;
    }
    return false;
}