<?php

include '../Connections/configini.php';
include './includes/mdlAvaliacao.php';
include './includes/mdlEmail.php';

$dados = filter_input_array(INPUT_POST);

if (isset($_POST["btnSalvarCategoria"])) {
    $id = salvarCategoria($conn, $dados);
    echo json_encode(['id' => $id]);
    exit;
}

if (isset($_POST["btnSalvarFrete"])) {
    $id = salvarFrete($conn, $dados);
    echo json_encode(['id' => $id]);
    exit;
}

if (isset($_POST["btnSalvarPergunta"])) {
    $id = salvarPergunta($conn, $dados);
    echo json_encode(['id' => $id]);
    exit;
}

if (isset($_POST["btnSalvarAvaliacao"])) {
    $id = salvarAvaliacao($conn, $dados);
    echo json_encode(['id' => $id]);
    exit;
}

if (isset($_POST["btnSalvar"])) {
    $id = salvarProduto($conn, $dados);
    if (!is_numeric($_POST["txtId"])) {
        $dados["txtId"] = $id;
        salvarAnuncio($conn, $dados);
    }
    if (isset($dados["imageDir"]) && strlen($dados["imageDir"]) > 0) {
        $dados["txtType"] = "produto";
        saveFile($conn, $dados, saveImgDirectory($dados));
    }
    echo json_encode(['id' => $id]);
    exit;
}

if (isset($_POST["btnSalvarAnuncio"])) {
    $id = salvarAnuncio($conn, $dados);
    echo json_encode(['id' => $id]);
    exit;
}

if (isset($_POST["btnAtivarAnuncio"])) {
    $id = ativarAnuncio($conn, $dados);
    echo json_encode(['id' => $id]);
    exit;
}

if (isset($_POST["btnDeleteCategoria"])) {
    if (is_numeric($_POST["txtIdCategoria"])) {
        $loja = getLojaSession($conn);
        $prepareSQL = $conn->prepare("DELETE FROM sf_empresa_categoria WHERE id_empresa = :txtEmpresa and id = :id");
        $prepareSQL->bindValue(':txtEmpresa', $loja->id);
        $prepareSQL->bindValue(':id', $_POST["txtIdCategoria"]);
        $prepareSQL->execute();
        echo json_encode("YES");
    }
}

if (isset($_POST["btnDeleteFrete"])) {
    if (is_numeric($_POST["txtIdFrete"])) {
        $loja = getLojaSession($conn);
        $prepareSQL = $conn->prepare("DELETE FROM sf_empresa_frete WHERE id_empresa = :txtEmpresa and id = :id");
        $prepareSQL->bindValue(':txtEmpresa', $loja->id);
        $prepareSQL->bindValue(':id', $_POST["txtIdFrete"]);
        $prepareSQL->execute();
        echo json_encode("YES");
    }
}

if (isset($_POST["btnDeletePergunta"])) {
    if (is_numeric($_POST["txtIdPergunta"])) {
        $prepareSQL = $conn->prepare("UPDATE sf_pergunta_resposta SET inativo = 1 WHERE id = :id");
        $prepareSQL->bindValue(':id', $_POST["txtIdPergunta"]);
        $prepareSQL->execute();
        echo json_encode("YES");
    }
}

if (isset($_POST["btnDeleteProduto"])) {
    try {
        if (is_numeric($_POST["txtId"])) {
            $loja = getLojaSession($conn);
            $prepareSQL = $conn->prepare("DELETE FROM sf_empresa_produto WHERE id_empresa = :txtEmpresa and id = :id");
            $prepareSQL->bindValue(':txtEmpresa', $loja->id);
            $prepareSQL->bindValue(':id', $_POST["txtId"]);
            $prepareSQL->execute();
            echo json_encode("YES");
        }
    } catch (Exception $e) {
        echo "Não é possível efetuar esta operação, em alguns casos este registro pode estar associado a um cadastro que não pode ser excluído, use a opção de inativar!";
    }
}

if (isset($_POST["btnDeleteAnuncio"])) {
    try {    
        if (is_numeric($_POST["txtIdAnuncio"])) {
            $prepareSQL = $conn->prepare("DELETE FROM sf_empresa_produto_anuncio WHERE id = :id");
            $prepareSQL->bindValue(':id', $_POST["txtIdAnuncio"]);
            $prepareSQL->execute();
            echo json_encode("YES");
        }
    } catch (Exception $e) {
        echo "Não é possível efetuar esta operação, em alguns casos este registro pode estar associado a um cadastro que não pode ser excluído, use a opção de inativar!";
    }    
}

if (isset($_POST["btnProduto"])) {
    $loja = getLojaSession($conn);
    echo json_encode(getProduto($conn, $_POST["txtId"], (isset($loja->id) ? $loja->id : 0)));
}

if (isset($_POST["btnProdutoPendentes"])) {
    echo json_encode(getProdutoTotal($conn, 1));
}

if (isset($_POST["btnBuscarGrupos"])) {
    echo json_encode(getCategoria($conn));
}

if (isset($_POST["btnBuscarCategorias"])) {
    $loja = getLojaSession($conn);
    echo json_encode(getCategoriaEmpresa($conn, (isset($loja->id) ? $loja->id : 0)));
}

if (isset($_POST["btnProdutosAtivos"])) {
    echo json_encode(getProdutosAtivos($conn));
}

if (isset($_POST["btnTotRespostas"])) {
    $total = getTotalTypeItem($conn, $_SESSION['id'], ($_POST["txtTipo"] == 0 ? "sf_pergunta_resposta" : "sf_vendas_avaliacao"));
    echo json_encode(['total' => $total]);
    exit;
}

function salvarCategoria($conn, $dados) {
    $loja = getLojaSession($conn);
    $valores = ['txtNome', 'txtDescricao', 'txtCorFonte', 'txtCorFundo'];
    if (is_numeric($dados["txtId"])) {
        $prepareSQL = $conn->prepare("UPDATE sf_empresa_categoria SET nome = :txtNome, 
        descricao = :txtDescricao, inativo = :txtInativo, cor_fonte = :txtCorFonte, cor_fundo = :txtCorFundo WHERE id = :id");
        $prepareSQL->bindValue(':id', $dados["txtId"]);
    } else if (!isset($loja->id)) {
        echo "É necessário cadastrar a loja antes de realizar esta operação!";
        exit;
    } else {
        $prepareSQL = $conn->prepare("INSERT INTO sf_empresa_categoria (id_empresa, nome, descricao, inativo, cor_fonte, cor_fundo) values 
        (:txtEmpresa, :txtNome, :txtDescricao, :txtInativo, :txtCorFonte, :txtCorFundo)");
        $prepareSQL->bindValue(':txtEmpresa', $loja->id);
    }
    $prepareSQL->bindValue(':txtInativo', (isset($dados['txtInativo']) ? "1" : "0"));
    foreach ($valores as $item) {
        $prepareSQL->bindValue(":$item", $dados[$item]);
    }
    $prepareSQL->execute();
    return is_numeric($dados["txtId"]) ? $dados["txtId"] : $conn->lastInsertId();
}

function salvarFrete($conn, $dados) {
    $loja = getLojaSession($conn);
    $valores = ['txtNome', 'txtDescricao'];
    if (is_numeric($dados["txtId"])) {
        $prepareSQL = $conn->prepare("UPDATE sf_empresa_frete SET nome = :txtNome, 
        descricao = :txtDescricao, valor = :txtValor, inativo = :txtInativo WHERE id = :id");
        $prepareSQL->bindValue(':id', $dados["txtId"]);
    } else if (!isset($loja->id)) {
        echo "É necessário cadastrar a loja antes de realizar esta operação!";
        exit;
    } else {
        $prepareSQL = $conn->prepare("INSERT INTO sf_empresa_frete (id_empresa, nome, descricao, valor, inativo) values 
        (:txtEmpresa, :txtNome, :txtDescricao, :txtValor, :txtInativo)");
        $prepareSQL->bindValue(':txtEmpresa', $loja->id);
    }
    $prepareSQL->bindValue(':txtValor', valoresNumericos($dados["txtValor"]));
    $prepareSQL->bindValue(':txtInativo', (isset($dados['txtInativo']) ? "1" : "0"));
    foreach ($valores as $item) {
        $prepareSQL->bindValue(":$item", $dados[$item]);
    }
    $prepareSQL->execute();
    return is_numeric($dados["txtId"]) ? $dados["txtId"] : $conn->lastInsertId();
}

function salvarProduto($conn, $dados) {
    $loja = getLojaSession($conn);
    $valores = ['txtNome', 'txtTipo', 'txtGrupo', 'txtDivulgacao', 'txtBoleto', 'txtCartao', 'txtPix'];
    if (is_numeric($dados["txtId"])) {
        $prepareSQL = $conn->prepare("UPDATE sf_empresa_produto SET nome = :txtNome, tipo = :txtTipo, categoria = :txtGrupo, 
        categoria_empresa = :txtCategoria, descricao = :txtDescricao, divulgacao = :txtDivulgacao, boleto = :txtBoleto, 
        cartao_credito = :txtCartao, pix = :txtPix, inativo = :txtInativo WHERE id = :id");
        $prepareSQL->bindValue(':id', $dados["txtId"]);
    } else if (!isset($loja->id)) {
        echo "É necessário cadastrar a loja antes de realizar esta operação!";
        exit;
    } else {
        $prepareSQL = $conn->prepare("INSERT INTO sf_empresa_produto 
        (id_empresa, nome, tipo, categoria, categoria_empresa, descricao, divulgacao, boleto, cartao_credito, pix, inativo) values 
        (:txtEmpresa, :txtNome, :txtTipo, :txtGrupo, :txtCategoria, :txtDescricao, :txtDivulgacao, :txtBoleto, :txtCartao, :txtPix, :txtInativo)");
        $prepareSQL->bindValue(':txtEmpresa', $loja->id);
    }
    $prepareSQL->bindValue(':txtDescricao',  removeEmoji($dados["txtDescricao"]));    
    $prepareSQL->bindValue(':txtCategoria', (isset($dados['txtCategoria']) ? $dados['txtCategoria'] : null));
    $prepareSQL->bindValue(':txtInativo', (isset($dados['txtInativo']) ? "1" : "0"));
    foreach ($valores as $item) {
        $prepareSQL->bindValue(":$item", $dados[$item]);
    }
    $prepareSQL->execute();
    return is_numeric($dados["txtId"]) ? $dados["txtId"] : $conn->lastInsertId();
}

function removeEmoji($string) {
    $string = str_replace( "?", "{%}", $string );
    $string = mb_convert_encoding( $string, "ISO-8859-1", "UTF-8" );
    $string = mb_convert_encoding( $string, "UTF-8", "ISO-8859-1" );
    $string = str_replace( array( "?", "? ", " ?" ), array(""), $string );
    $string = str_replace( "{%}", "?", $string );
    return trim($string);
}

function salvarAnuncio($conn, $dados) {
    $valores = ['txtNomeSub'];
    if (is_numeric($dados["txtIdAnuncio"])) {
        $prepareSQL = $conn->prepare("UPDATE sf_empresa_produto_anuncio SET 
        descricao = :txtNomeSub, quantidade = :txtQuantidade, valor = :txtValor, cashback = :txtCashback, inativo = 1 WHERE id = :id");
        $prepareSQL->bindValue(':id', $dados["txtIdAnuncio"]);
    } else {
        $prepareSQL = $conn->prepare("INSERT INTO sf_empresa_produto_anuncio
        (id_empresa_produto, dt_cadastro, descricao, quantidade, valor, cashback, inativo) values 
        (:txtId, now(), :txtNomeSub, :txtQuantidade, :txtValor, :txtCashback, 1)");
        $prepareSQL->bindValue(':txtId', $dados["txtId"]);
    }
    $prepareSQL->bindValue(':txtQuantidade', valoresNumericos($dados["txtQuantidade"]));
    $prepareSQL->bindValue(':txtValor', valoresNumericos($dados["txtValor"]));
    $prepareSQL->bindValue(':txtCashback', valoresNumericos($dados["txtCashback"]));
    foreach ($valores as $item) {
        $prepareSQL->bindValue(":$item", $dados[$item]);
    }
    $prepareSQL->execute();
    return is_numeric($dados["txtIdAnuncio"]) ? $dados["txtIdAnuncio"] : $conn->lastInsertId();
}

function ativarAnuncio($conn, $dados) {
    if (is_numeric($dados["txtIdAnuncio"])) {
        $prepareSQL = $conn->prepare("UPDATE sf_empresa_produto_anuncio SET inativo = :tipo WHERE id = :id");
        $prepareSQL->bindValue(':id', $dados["txtIdAnuncio"]);
        $prepareSQL->bindValue(':tipo', $dados["txtTipo"]);
        $prepareSQL->execute();
        return $dados["txtIdAnuncio"];
    }
}

function getProduto($conn, $id, $loja) {
    $prepareSQL = $conn->prepare("SELECT * FROM sf_empresa_produto WHERE id_empresa = :id_empresa AND id = :id");
    $prepareSQL->bindValue(':id_empresa', $loja);
    $prepareSQL->bindValue(':id', $id);
    $prepareSQL->execute();
    $return = $prepareSQL->fetch(PDO::FETCH_OBJ);
    return $return;
}

function getProdutoTotal($conn, $id) {
    $loja = getLojaSession($conn);
    if (isset($loja->id)) {
        $prepareSQL = $conn->prepare("SELECT count(*) FROM sf_empresa_produto_anuncio epa 
        inner join sf_empresa_produto ep on epa.id_empresa_produto = ep.id 
        WHERE id_empresa = :id_empresa AND epa.inativo = :id");        
        $prepareSQL->bindValue(':id_empresa', $loja->id);
        $prepareSQL->bindValue(':id', $id);        
        $prepareSQL->execute();
        $return = $prepareSQL->fetchColumn();
        return $return;    
    } else {            
        echo "É necessário cadastrar a loja antes de realizar esta operação!";
        exit;
    }
}

function getCategoria($conn) {
    $prepareSQL = $conn->prepare("SELECT * FROM sf_categoria WHERE inativo = 0");
    $prepareSQL->execute();
    $return = $prepareSQL->fetchAll(PDO::FETCH_OBJ);
    return $return;
}

function getCategoriaEmpresa($conn, $id) {
    $prepareSQL = $conn->prepare("SELECT * FROM sf_empresa_categoria WHERE id_empresa = :id AND inativo = 0");
    $prepareSQL->bindValue(':id', $id);
    $prepareSQL->execute();
    $return = $prepareSQL->fetchAll(PDO::FETCH_OBJ);
    return $return;
}

function getProdutosAtivos($conn) {
    $loja = getLojaSession($conn);
    if (isset($loja->id)) {
        $prepareSQL = $conn->prepare("select tipo, count(epa.id) total from sf_empresa_produto_anuncio epa
        inner join sf_empresa_produto ep on epa.id_empresa_produto = ep.id
        where ep.inativo = 0 and epa.inativo = 0 and ep.id_empresa = :id
        group by tipo");
        $prepareSQL->bindValue(':id', $loja->id);
        $prepareSQL->execute();
        return $prepareSQL->fetchAll(PDO::FETCH_OBJ);
    } else {
        return [];
    }
}