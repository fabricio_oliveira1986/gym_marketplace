<?php

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: Content-Type");

include '../Connections/configini.php';
include './includes/mdlAvaliacao.php';
include './includes/mdlEmail.php';
include './includes/mdlPessoa.php';
include './includes/mdlVenda.php';
include './includes/mdlSafe.php'; 

require_once __DIR__ . '/includes/functions.php';
require __DIR__ . '/../utils/jwt/vendor/autoload.php';

use Firebase\JWT\JWT;

$key = "adkalsdlkasdklasdadkladj21123123nasdalksdjkl";
$payload = array(
    "iss" => $_SERVER['SERVER_NAME'],
    "aud" => $_SERVER['SERVER_NAME'],
    'exp' => time()+(600 * 24 * 300),
);
$url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https' : 'https')
."://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];

$dados = filter_input_array(INPUT_POST, FILTER_DEFAULT);
if (!$dados) {
    $dados = file_get_contents('php://input');
    if ($dados) {
        $dados = json_decode($dados, 1);
    }
}
if (!$dados) {
    $dados = filter_input_array(INPUT_GET, FILTER_DEFAULT);
}

$headers = apache_request_headers();
if (isset($headers['Authorization']) || isset($headers['authorization'])) {
    $authorization = isset($headers['Authorization']) 
    ? $headers['Authorization'] : $headers['authorization'];
    try {
        $partes = explode(' ', $authorization);
        $resultado = JWT::decode($partes[1], $key, ['HS256']);
        $dados["id_usuario"] = $resultado->sub;
    } catch (Exception $e) {
        http_response_code(401);
        echo json_encode(['erro' => $e->getMessage()]);
        exit;
    }
}
//var_dump($dados);exit;

if (isset($dados["login"])) {
    try {
        $validaDados = array_filter($dados, function($d) {
            return empty($d);
        });  
        if ($validaDados) {
            http_response_code(422);
            $campos = array_map(function($c) {
                return 'Valor inválido';
            }, $validaDados);
            echo json_encode($campos);
            exit;
        }
        $dadosUsuario = getLogin($conn, $dados);
        if(!$dadosUsuario) {
            throw new Exception('Usuário ou senha inválidos!', 400);
        }
        $payload['sub'] = $dadosUsuario->id;
        $token = JWT::encode($payload, $key);
        $dados = new stdClass();
        $dados->user = $dadosUsuario;
        $dados->token = $token;
        echo json_encode($dados);
        exit;
    } catch (Exception $e) {
        $code = $e->getCode() ? $e->getCode() : 500;
        http_response_code($code);
        echo json_encode(['erro' => $e->getMessage()]);
    }
} else if(isset($dados['cadastrar'])) {
    try {
        $validaDados = array_filter($dados, function($d) {
            return empty($d);
        });  
        if ($validaDados) {
            http_response_code(422);
            $campos = array_map(function($c) {
                return 'Valor inválido';
            }, $validaDados);
            echo json_encode($campos);
            exit;
        }
        if (existeEmail($conn, $dados['email'])) {
            throw new Exception('Email já utilizado', 400);
        }   
        $id = criarUsuario($conn, $dados);
        if (!$id) {
            throw new Exception('Erro ao criar o usuário', 500);
        } 
        $dadosUsuario = getUsuarioPorId($conn, $id);
        $payload['sub'] = $id;
        $token = JWT::encode($payload, $key);
        $dados = new stdClass();
        $dados->user = $dadosUsuario;
        $dados->token = $token;
        echo json_encode($dados);
        exit;
    } catch (Exception $e) {
        $code = $e->getCode() ? $e->getCode() : 500;
        http_response_code($code);
        echo json_encode(['erro' => $e->getMessage()]);
    }
}else if (isset($dados["getMenuCategorias"])) {
    try {
        echo json_encode(getCategorias($conn));
    } catch (Exception $e) {
        http_response_code(500);
        echo json_encode(['erro' => $e->getMessage()]);
    }
    exit;    
}else if (isset($dados["getMenuRegioes"])) {
    try {
        echo json_encode(getEmpresasRegioes($conn));
    } catch (Exception $e) {
        http_response_code(500);
        echo json_encode(['erro' => $e->getMessage()]);
    }
    exit;    
}else if (isset($dados["getMenuCidades"])) {
    try {
        echo json_encode(getEmpresasCidades($conn));
    } catch (Exception $e) {
        http_response_code(500);
        echo json_encode(['erro' => $e->getMessage()]);
    }
    exit;    
}else if (isset($dados["getMenuValores"])) {
    try {
        echo json_encode(getAnunciosValores($conn));
    } catch (Exception $e) {
        http_response_code(500);
        echo json_encode(['erro' => $e->getMessage()]);
    }
    exit;    
}else if (isset($dados["getMenuCashback"])) {    
    try {
        echo json_encode(getCashbackValores($conn));
    } catch (Exception $e) {
        http_response_code(500);
        echo json_encode(['erro' => $e->getMessage()]);
    }
    exit;    
}else if (isset($dados["getAnuncios"])) {
    try {
        if (!isset($dados["id_usuario"])) {
            http_response_code(401);
            echo json_encode(["erro" => "Usuário não logado"]);
            exit;
        }        
        if (isset($dados['order']) && $dados['order'] == "3") {
            $usuario = getPerfil($conn, $dados["id_usuario"], true);
            $dados['regiao'] = substr($usuario->celular, 1, 2);
        }
        echo json_encode(getAnuncios($conn, $dados));
    } catch (Exception $e) {
        http_response_code(500);
        echo json_encode(['erro' => $e->getMessage()]);
    }
    exit;    
} else if (isset($dados["getAnuncio"])) {
    try {
        echo json_encode(getAnuncio($conn, $dados));
    } catch (Exception $e) {
        http_response_code(500);
        echo json_encode(['erro' => $e->getMessage()]);
    }
    exit;
} else if (isset($dados['setAvaliacao'])) {
    try {
        if (!isset($dados["id_usuario"])) {
            http_response_code(401);
            echo json_encode(["erro" => "Usuário não logado"]);
            exit;
        }
        $dados["txtPessoa"] = $dados["id_usuario"];       
        $dados["txtComentario"] = "";       
        $id = salvarAvaliacaoVenda($conn, $dados);
        $itens = getVendaItens($conn, $dados["txtVenda"]);        
        foreach ($itens as $item) {
            $dados["txtAnuncio"] = $item->id_anuncio;
            $id = salvarAvaliacao($conn, $dados);
        }   
        echo json_encode(['id' => $id]);            
    } catch (Exception $e) {
        http_response_code(500);
        echo json_encode(['erro' => $e->getMessage()]);
    }
    exit;   
} else if (isset($dados['getAvaliacoes'])) {
    try {
        echo json_encode(getAnuncioAvaliacoes($conn, $dados));
    } catch (Exception $e) {
        http_response_code(500);
        echo json_encode(['erro' => $e->getMessage()]);
    }
    exit;
} else if (isset($dados['setPergunta'])) {
    try {
        if (!isset($dados["id_usuario"])) {
            http_response_code(401);
            echo json_encode(["erro" => "Usuário não logado"]);
            exit;
        }
        $dados["txtPessoa"] = $dados["id_usuario"];
        $id = salvarPergunta($conn, $dados);
        echo json_encode(['id' => $id]);
    } catch (Exception $e) {
        http_response_code(500);
        echo json_encode(['erro' => $e->getMessage()]);
    }
    exit;    
} else if (isset($dados['getPerguntas'])) {
    try {
        echo json_encode(getAnuncioPerguntas($conn, $dados));
    } catch (Exception $e) {
        http_response_code(500);
        echo json_encode(['erro' => $e->getMessage()]);
    }
    exit;
} else if (isset($dados["getEmpresas"])) {
    try {
        echo json_encode(getEmpresas($conn, $dados));
    } catch (Exception $e) {
        http_response_code(500);
        echo json_encode(['erro' => $e->getMessage()]);
    }
    exit;
} else if (isset($dados["getEmpresa"])) {
    try {
        echo json_encode(getEmpresa($conn, $dados));
    } catch (Exception $e) {
        http_response_code(500);
        echo json_encode(['erro' => $e->getMessage()]);
    }
    exit;
} else if (isset($dados["setEndereco"])) {
    try {
        if (!isset($dados["id_usuario"])) {
            http_response_code(401);
            echo json_encode(["erro" => "Usuário não logado"]);
            exit;
        }
        $id_endereco = salvarEndereco($conn, $dados, $dados['id_usuario']);
        echo json_encode(['id_endereco' => $id_endereco]);
    } catch (Exception $e) {
        http_response_code(500);
        echo json_encode(['erro' => $e->getMessage()]);
    }
    exit;            
} else if (isset($dados["getEnderecos"])) {
    try {
        if (!isset($dados["id_usuario"])) {
            http_response_code(401);
            echo json_encode(["erro" => "Usuário não logado"]);
            exit;
        }
        echo json_encode(getEnderecos($conn, $dados));
    } catch (Exception $e) {
        http_response_code(500);
        echo json_encode(['erro' => $e->getMessage()]);
    }
    exit;
} else if (isset($dados["getEstados"])) {
    try {
        echo json_encode(getEstados($conn, isset($dados["onlyActived"])));
    } catch (Exception $e) {
        http_response_code(500);
        echo json_encode(['erro' => $e->getMessage()]);
    }
    exit;    
} else if (isset($dados["getCidades"])) {    
    try {
        echo json_encode(getCidades($conn, $dados["txtEstado"], isset($dados["onlyActived"])));
    } catch (Exception $e) {
        http_response_code(500);
        echo json_encode(['erro' => $e->getMessage()]);
    }
    exit;
} else if (isset($dados["deleteAccount"])) {
    try {
        if (!isset($dados["id_usuario"])) {
            http_response_code(401);
            echo json_encode(["erro" => "Usuário não logado"]);
            exit;
        }
        $id_usuario = inativarUsuario($conn, $dados['id_usuario'], 1);
        echo json_encode(['id_usuario' => $id_usuario]);
    } catch (Exception $e) {
        http_response_code(500);
        echo json_encode(['erro' => $e->getMessage()]);
    }
    exit;
} else if (isset($dados["delEndereco"])) {
    try {
        if (!isset($dados["id_usuario"])) {
            http_response_code(401);
            echo json_encode(["erro" => "Usuário não logado"]);
            exit;
        }
        $id_endereco = inativarEndereco($conn, $dados['id_endereco'], $dados['id_usuario'], 1);
        echo json_encode(['id_endereco' => $id_endereco]);
    } catch (Exception $e) {
        http_response_code(500);
        echo json_encode(['erro' => $e->getMessage()]);
    }
    exit;
} else if (isset($dados["getCartoes"])) {
    try {
        if (!isset($dados["id_usuario"])) {
            http_response_code(401);
            echo json_encode(["erro" => "Usuário não logado"]);
            exit;
        }
        echo json_encode(getCartoes($conn, $dados["id_usuario"]));
    } catch (Exception $e) {
        http_response_code(500);
        echo json_encode(['erro' => $e->getMessage()]);
    }
    exit;
} else if (isset($dados["delCartao"])) {
    try {
        if (!isset($dados["id_usuario"])) {
            http_response_code(401);
            echo json_encode(["erro" => "Usuário não logado"]);
            exit;
        }
        $id_cartao = inativarCartao($conn, $dados['id_cartao'], $dados['id_usuario'], 1);
        echo json_encode(['id_cartao' => $id_cartao]);
    } catch (Exception $e) {
        http_response_code(500);
        echo json_encode(['erro' => $e->getMessage()]);
    }
    exit;
} else if (isset($dados["getFretes"])) {
    try {
        $return = countProdutoAnuncios($conn, $dados);
        echo json_encode($return->total == 1 ? getFretes($conn, $return->id_empresa) : []);
    } catch (Exception $e) {
        http_response_code(500);
        echo json_encode(['erro' => $e->getMessage()]);
    }
    exit;
} else if (isset($dados["getFormasPagamento"])) {
    try {
        echo json_encode(getFormasPagamento($conn, $dados));
    } catch (Exception $e) {
        http_response_code(500);
        echo json_encode(['erro' => $e->getMessage()]);
    }
    exit;
} else if (isset($dados["getCashback"])) {
    try {
        if (!isset($dados["id_usuario"])) {
            http_response_code(401);
            echo json_encode(["erro" => "Usuário não logado"]);
            exit;
        }
        echo json_encode(getCashback($conn, $dados["id_usuario"]));
    } catch (Exception $e) {
        http_response_code(500);
        echo json_encode(['erro' => $e->getMessage()]);
    }
    exit;    
} else if (isset($dados["getPerfil"])) {
    try {
        if (!isset($dados["id_usuario"])) {
            http_response_code(401);
            echo json_encode(["erro" => "Usuário não logado"]);
            exit;
        }
        echo json_encode(getPerfil($conn, $dados["id_usuario"]));
    } catch (Exception $e) {
        http_response_code(500);
        echo json_encode(['erro' => $e->getMessage()]);
    }
    exit;
} else if (isset($dados["setPerfil"])) {
    try {
        if (!isset($dados["id_usuario"])) {
            http_response_code(401);
            echo json_encode(["erro" => "Usuário não logado"]);
            exit;
        }
        $id_usuario = salvarPessoa($conn, $dados, $dados['id_usuario']);
        echo json_encode(['id_usuario' => $id_usuario]);
    } catch (Exception $e) {
        http_response_code(500);
        echo json_encode(['erro' => $e->getMessage()]);
    }
    exit;
} else if (isset($dados["setAlterarSenha"])) {
    try {
        if (!isset($dados["id_usuario"])) {
            http_response_code(401);
            echo json_encode(["erro" => "Usuário não logado"]);
            exit;
        } else if (strlen($dados["txtNovaSenha"]) < 5 || ($dados["txtNovaSenha"] != $dados["txtConfirmSenha"])) {
            http_response_code(401);
            echo json_encode(["erro" => "Senhas novas inválidas!"]);
            exit;
        }
        $usuario = getPerfil($conn, $dados["id_usuario"], false);
        if ($usuario->senha != md5($dados["txtSenhaAtual"])) {        
            http_response_code(401);
            echo json_encode(["erro" => "Senha atual inválida!"]);
            exit;            
        } else {
            $id_usuario = salvarSenha($conn, $dados["txtNovaSenha"], $dados["id_usuario"]);
            echo json_encode(['id_usuario' => $id_usuario]);
        }
    } catch (Exception $e) {
        http_response_code(500);
        echo json_encode(['erro' => $e->getMessage()]);
    }
    exit;
} else if (isset($dados["setRecuperarSenha"])) {
    try {
        if (!getCadastroEmail($conn, $dados["txtEmail"])) {
            http_response_code(401);
            echo json_encode(["erro" => "E-mail não cadastrado!"]);
            exit;
        }
        $return = sendMsg($dados["txtEmail"], $dados["txtEmail"], 
        "Recuperar Senha", "Para recuperar sua senha entre no link abaixo:", 
        "", "", "http://gymbrother.com.br/login.php?conta=" . encrypt($dados["txtEmail"], "gymBrother7034", true));
        echo json_encode(['send' => $return]);
    } catch (Exception $e) {
        http_response_code(500);
        echo json_encode(['erro' => $e->getMessage()]);
    }
    exit;    
} else if (isset($dados["getExtrato"])) {
    try {
        if (!isset($dados["id_usuario"])) {
            http_response_code(401);
            echo json_encode(["erro" => "Usuário não logado"]);
            exit;
        }
        $dataValor = getCashback($conn, $dados["id_usuario"]);
        $data = getExtratoCashback($conn, $dados["id_usuario"]);
        $item = new stdClass();
        $item->cashback = $dataValor->total;
        $item->cashbackDisponible = $dataValor->total_liquido;
        $item->data = $data;
        echo json_encode($item);
    } catch (Exception $e) {
        http_response_code(500);
        echo json_encode(['erro' => $e->getMessage()]);
    }
    exit;  
} else if (isset($dados["getVendas"])) {
    try {
        if (!isset($dados["id_usuario"])) {
            http_response_code(401);
            echo json_encode(["erro" => "Usuário não logado"]);
            exit;
        }
        echo json_encode(getVendas($conn, $dados["id_usuario"], true));
    } catch (Exception $e) {
        http_response_code(500);
        echo json_encode(['erro' => $e->getMessage()]);
    }
    exit;
} else if (isset($dados["getVenda"])) {
    try {
        if (!isset($dados["id_usuario"])) {
            http_response_code(401);
            echo json_encode(["erro" => "Usuário não logado"]);
            exit;
        }
        echo json_encode(getVenda($conn, $dados["id_venda"]));
    } catch (Exception $e) {
        http_response_code(500);
        echo json_encode(['erro' => $e->getMessage()]);
    }
    exit;
} else if (isset($dados["setVenda"])) {
    try {
        if (!isset($dados["id_usuario"])) {
            http_response_code(401);
            echo json_encode(["erro" => "Usuário não logado"]);
            exit;
        }        
        $dados["txtIdPessoa"] = $dados["id_usuario"];
        $id = makeVenda($conn, $dados);
        $venda = getVenda($conn, $id);
        if (in_array($venda->status, array("A", "P"))) {
            echo json_encode(['id_venda' => $id, 'status' => $venda->status]);
        } else {
            http_response_code(402);
            echo json_encode(['erro' => "Transação negada!"]);            
        }
    } catch (Exception $e) {
        http_response_code(500);
        echo json_encode(['erro' => $e->getMessage()]);
    }
    exit;
} else if (isset($dados["setEstornarVendas"])) {
    try {
        if (!isset($dados["id_usuario"])) {
            http_response_code(401);
            echo json_encode(["erro" => "Usuário não logado"]);
            exit;
        }
        if (is_numeric($dados["txtId"]) && makeEstorno($conn, $dados["txtId"], 0)) {
            echo json_encode("YES");
        } else {
            http_response_code(500);            
            echo json_encode(["erro" => "Erro ao realizar o estorno!"]);
        }
    } catch (Exception $e) {
        http_response_code(500);
        echo json_encode(['erro' => $e->getMessage()]);
    }
    exit; 
} else if (isset($dados["getTermos"])) {
    try {
        echo file_get_contents('./includes/termos.html');
    } catch (Exception $e) {
        http_response_code(500);
        echo json_encode(['erro' => $e->getMessage()]);
    }
}