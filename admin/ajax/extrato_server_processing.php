<?php

include '../Connections/configini.php';

$loja = getLojaSession($conn);
$sTable = "sf_vendas v inner join sf_vendas_parcelas vp on v.id = vp.id_venda 
inner join sf_pessoa p on v.id_pessoa = p.id";
$sLimit = "";
$sWhere = "";
$sWhere2 = "id_empresa = " . (isset($loja->id) ? $loja->id : "-1");
$sOrder = "ORDER BY data desc ";

if (isset($loja->id)) {
    $prepareSQL = $conn->prepare("update sf_vendas v inner join sf_vendas_parcelas vp on v.id = vp.id_venda
    set v.status = 'C' where vp.id_empresa = :id and v.status = 'A' and DATE_ADD(v.data, INTERVAL 4 DAY) < now()");
    $prepareSQL->bindValue(':id', $loja->id);
    $prepareSQL->execute();
}

if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
    $sLimit = "LIMIT " . $_GET['iDisplayStart'] . ", " . $_GET['iDisplayLength'];
}

$totalRecords = $conn->query("SELECT count(*) from $sTable WHERE $sWhere2")->fetchColumn();
$totalRecordwithFilter = $conn->query("SELECT count(*) from $sTable WHERE $sWhere2 $sWhere")->fetchColumn();
$stmt = $conn->prepare("SELECT v.*, p.nome, p.id_foto, vp.valor, (select sum(valor) from sf_vendas_parcelas where id_venda = v.id) valor_total    
FROM $sTable WHERE $sWhere2 $sWhere $sOrder $sLimit");
$stmt->execute();
$return = $stmt->fetchAll(PDO::FETCH_OBJ);

$output = [
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => $totalRecords,
    "iTotalDisplayRecords" => $totalRecordwithFilter,
    "aaData" => $return
];

echo json_encode($output);