<?php

include '../Connections/configini.php';
include './includes/mdlPessoa.php';
include './includes/mdlVenda.php';
include './includes/mdlEmail.php';
include './includes/mdlSafe.php'; 

$dados = filter_input_array(INPUT_POST);

if (isset($_POST["getVenda"])) {
    if (is_numeric($dados["txtId"])) {
        echo json_encode(getVenda($conn, $dados["txtId"]));
    }
}

if (isset($_POST["btnSalvar"])) {
    $id = makeVenda($conn, $dados);
    echo json_encode(['id_venda' => $id]);
    exit;
}

if (isset($_POST["btnSalvarTransf"])) {
    $dados["txtTipoDocumento"] = "S";
    $dados["txtIdPessoa"] = $_SESSION['id'];
    $dados["txtParcelas"] = "1";
    $dados["txtValorSaque"] = $dados["txtValor"];
    $id = makeVenda($conn, $dados);
    echo json_encode(['id_venda' => $id]);
    exit;        
}

if (isset($_POST["btnEstornar"])) {
    if (is_numeric($_POST["txtId"])) {
        if (makeEstorno($conn, $_POST["txtId"], (isset($_POST["txtValor"]) ? valoresNumericos($_POST["txtValor"]) : 0))) {
            echo json_encode("YES");
        }
    }
}

if (isset($_POST["btnSalvarSafe"])) {
    if (is_numeric($_POST["txtId"])) {
        echo json_encode(processarVendaSafe($conn, $_POST["txtId"]));
    }
}

if (isset($_POST["btnBaixarEntrega"])) {
    if (is_numeric($_POST["txtId"])) {
        $prepareSQL = $conn->prepare("UPDATE sf_vendas SET entrega = 'E', data_entrega = now() WHERE id = :id");
        $prepareSQL->bindValue(':id', $_POST["txtId"]);
        $prepareSQL->execute();
        sendMsgVenda($conn, $_POST["txtId"]);
        echo json_encode("YES");
    }
}

//------------------------------------------------------------------------------

if (isset($_POST["btnTotalVendas"])) {
    echo json_encode(getVendasTotal($conn));
}

if (isset($_POST["btnVendasTipo"])) {
    echo json_encode(getVendasTipo($conn));
}

if (isset($_POST["btnVendasRecebidas"])) {
    echo json_encode(getVendasRecebidas($conn));
}

if (isset($_POST["btnVendasReceber"])) {
    echo json_encode(getVendasReceber($conn));
}

//------------------------------------------------------------------------------

function getVendasTotal($conn) {
    $loja = getLojaSession($conn);
    if (isset($loja->id)) {
        $prepareSQL = $conn->prepare("select sum(vi.quantidade * vi.valor) total, count(v.id) itens 
        from sf_vendas v inner join sf_vendas_parcelas vp on vp.id_venda = v.id
        inner join sf_vendas_itens vi on vi.id_venda = v.id
        where id_empresa = :id and v.status = 'P' and v.tipo_documento in ('C', 'B', 'P', 'T') limit 1");
        $prepareSQL->bindValue(':id', $loja->id);
        $prepareSQL->execute();
        return $prepareSQL->fetch(PDO::FETCH_OBJ);
    }
}

function getVendasTipo($conn) {
    $loja = getLojaSession($conn);
    if (isset($loja->id)) {
        $prepareSQL = $conn->prepare("select sum(valor) total, descricao, color 
        from sf_vendas_parcelas vp inner join sf_vendas v on v.id = vp.id_venda
        inner join sf_tipo_documento td on td.id = v.tipo_documento
        where vp.id_empresa = :id and v.status in ('P', 'T') and v.tipo_documento in ('C', 'B', 'P', 'T')
        group by descricao, color");
        $prepareSQL->bindValue(':id', $loja->id);
        $prepareSQL->execute();
        return $prepareSQL->fetchAll(PDO::FETCH_OBJ);
    }
}

function getVendasRecebidas($conn) {
    $loja = getLojaSession($conn);
    if (isset($loja->id)) {
        $saldo = getSaldoSafe($loja);        
        $prepareSQL = $conn->prepare("select sum(valor) total, 0 liberado 
        from sf_vendas v inner join sf_vendas_parcelas vp on vp.id_venda = v.id
        where id_empresa = :id and v.status in ('P', 'T') and v.tipo_documento in ('C', 'B', 'P', 'T', 'S') limit 1");
        $prepareSQL->bindValue(':id', $loja->id);
        $prepareSQL->execute();
        $return = $prepareSQL->fetch(PDO::FETCH_OBJ);
        if (isset($saldo["AmountReceived"])) {
            $return->liberado = $saldo["AmountReceived"];
        }
        return $return;
    }
}

function getVendasReceber($conn) {
    $loja = getLojaSession($conn);
    if (isset($loja->id)) {
        $prepareSQL = $conn->prepare("select sum(valor) total, count(v.id) itens 
        from sf_vendas v inner join sf_vendas_parcelas vp on vp.id_venda = v.id
        where id_empresa = :id and v.status = 'A' and v.tipo_documento in ('C', 'B', 'P', 'T') limit 1");
        $prepareSQL->bindValue(':id', $loja->id);
        $prepareSQL->execute();
        return $prepareSQL->fetch(PDO::FETCH_OBJ);
    }
}