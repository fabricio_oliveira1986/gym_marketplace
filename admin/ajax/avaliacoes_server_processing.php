<?php

include '../Connections/configini.php';

$loja = getLojaSession($conn);
$sTable = "sf_vendas_avaliacao pr
inner join sf_empresa_produto_anuncio epa on pr.id_anuncio = epa.id
inner join sf_empresa_produto ep on ep.id = epa.id_empresa_produto
inner join sf_pessoa p on p.id = pr.id_pessoa";
$aColumns = ["p.nome", "ep.nome", "epa.descricao"];
$sLimit = "";
$sWhere = "";
$sWhere2 = "ep.id_empresa = " . (isset($loja->id) ? $loja->id : "-1");
$sOrder = "ORDER BY data desc ";

if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
    $sLimit = "LIMIT " . $_GET['iDisplayStart'] . ", " . $_GET['iDisplayLength'];
}

if (isset($_GET['avaliacoes']) && in_array($_GET['avaliacoes'], array("0", "1"))) {
    if ($_GET['avaliacoes'] == "0") {
        $sWhere2 .= " AND pr.avaliacao >= 3 ";    
    } else if ($_GET['avaliacoes'] == "1") {
        $sWhere2 .= " AND pr.avaliacao < 3 ";        
    } 
}

if (isset($_GET['tipo']) && in_array($_GET['tipo'], array("0", "1", "2"))) {
    if ($_GET['tipo'] == "0") {
        $sWhere2 .= " AND pr.data_resposta is null AND pr.inativo = 0 ";    
    } else if ($_GET['tipo'] == "1") {
        $sWhere2 .= " AND pr.data_resposta is not null AND pr.inativo = 0 ";
    } else if ($_GET['tipo'] == "2") {
        $sWhere2 .= " AND pr.inativo = 1 ";        
    } 
}

if (isset($_GET['sSearch']) && strlen($_GET['sSearch'])) {
    $sWhere = " AND (";
    for ($i = 0; $i < count($aColumns); $i++) {
        $sWhere .= $aColumns[$i] . " LIKE '%" . $_GET['sSearch'] . "%' OR ";
    }
    $sWhere = substr_replace($sWhere, "", -3) . ')';
}

$totalRecords = $conn->query("SELECT count(*) from $sTable WHERE $sWhere2")->fetchColumn();
$totalRecordwithFilter = $conn->query("SELECT count(*) from $sTable WHERE $sWhere2 $sWhere")->fetchColumn();
$stmt = $conn->prepare("SELECT pr.*, p.nome, p.id_foto, ep.nome nome_produto, ep.id id_produto, ep.id_foto foto_produto, epa.descricao FROM $sTable WHERE $sWhere2 $sWhere $sOrder $sLimit");
$stmt->execute();
$return = $stmt->fetchAll(PDO::FETCH_OBJ);

$output = [
    "sEcho" => intval($_GET['sEcho']),
    "iTotalRecords" => $totalRecords,
    "iTotalDisplayRecords" => $totalRecordwithFilter,
    "aaData" => $return
];

echo json_encode($output);