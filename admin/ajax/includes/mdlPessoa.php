<?php

function salvarPessoa($conn, $dados, $id_pessoa) {
    $valores = ['txtNome', 'txtSexo', 'txtCPF', 'txtEmail', 'txtCelular'];
    if (is_numeric($id_pessoa)) {
        $prepareSQL = $conn->prepare("UPDATE sf_pessoa SET nome = :txtNome, dt_nasc= :txtDtNasc, sexo = :txtSexo, 
        cpf = :txtCPF, email = :txtEmail, " . (isset($dados['txtSenha']) ? "senha = :txtSenha," : "") . 
        " celular = :txtCelular WHERE id = :id");
        $prepareSQL->bindValue(':id', $id_pessoa);
    } else if (existeCadastro($conn, $dados)) {
        echo "E-mail ou CPF já pertence a um cadastro no sistema, não é possível usar o mesmo!";
        exit;
    } else {
        $prepareSQL = $conn->prepare("INSERT INTO sf_pessoa(nome, dt_nasc, sexo, cpf, email, senha, celular) values 
        (:txtNome, :txtDtNasc, :txtSexo, :txtCPF, :txtEmail, :txtSenha, :txtCelular)");
    }
    foreach ($valores as $item) {
        $prepareSQL->bindValue(":$item", $dados[$item]);
    }
    $prepareSQL->bindValue(':txtDtNasc', valoresData($dados['txtDtNasc']));
    if (isset($dados['txtSenha'])) {
        $prepareSQL->bindValue(':txtSenha', md5($dados['txtSenha']));
    }
    $prepareSQL->execute();
    return (is_numeric($id_pessoa) ? $id_pessoa : $conn->lastInsertId());
}

function salvarSenha($conn, $senha, $id_pessoa) {
    $prepareSQL = $conn->prepare("UPDATE sf_pessoa SET senha = :txtSenha WHERE id = :id");
    $prepareSQL->bindValue(':id', $id_pessoa);    
    $prepareSQL->bindValue(':txtSenha', md5($senha));
    $prepareSQL->execute();
    return $id_pessoa;
}

function salvarEndereco($conn, $dados, $id_pessoa) {
    $valores = ['txtCep', 'txtEndereco', 'txtBairro', 'txtComplemento', 'txtNumero', 'txtCidade'];
    if (is_numeric($dados["txtIdEndereco"])) {
        $prepareSQL = $conn->prepare("UPDATE sf_pessoa_endereco SET
        id_pessoa = :txtPessoa, descricao = 'Residencial', cep = :txtCep, endereco = :txtEndereco, 
        numero = :txtNumero, bairro = :txtBairro, id_cidade = :txtCidade, complemento = :txtComplemento WHERE id = :id");
        $prepareSQL->bindValue(':id', $dados["txtIdEndereco"]);
    } else {
        $prepareSQL = $conn->prepare("INSERT INTO sf_pessoa_endereco
        (id_pessoa, descricao, cep, endereco, numero, bairro, id_cidade, complemento) values 
        (:txtPessoa, 'Residencial', :txtCep, :txtEndereco, :txtNumero, :txtBairro, :txtCidade, :txtComplemento)");
    }
    $prepareSQL->bindValue(':txtPessoa', $id_pessoa);
    foreach ($valores as $item) {
        $prepareSQL->bindValue(":$item", $dados[$item]);
    }
    $prepareSQL->execute();
    return is_numeric($dados["txtIdEndereco"]) ? $dados["txtIdEndereco"] : $conn->lastInsertId();
}

function inativarUsuario($conn, $id_pessoa, $tipo) {
    $prepareSQL = $conn->prepare("UPDATE sf_pessoa SET inativo = :txtTipo WHERE id = :id");
    $prepareSQL->bindValue(':txtTipo', $tipo);  
    $prepareSQL->bindValue(':id', $id_pessoa);
    $prepareSQL->execute();
    return $id_pessoa;
}

function inativarEndereco($conn, $id_endereco, $id_pessoa, $tipo) {
    $prepareSQL = $conn->prepare("UPDATE sf_pessoa_endereco SET inativo = :txtTipo WHERE id_pessoa = :txtPessoa AND id = :id");
    $prepareSQL->bindValue(':txtTipo', $tipo);
    $prepareSQL->bindValue(':txtPessoa', $id_pessoa);    
    $prepareSQL->bindValue(':id', $id_endereco);
    $prepareSQL->execute();
    return $id_endereco;
}

function inativarCartao($conn, $id_cartao, $id_pessoa, $tipo) {
    $prepareSQL = $conn->prepare("UPDATE sf_pessoa_cartao SET inativo = :txtTipo WHERE id_pessoa = :txtPessoa AND id = :id");
    $prepareSQL->bindValue(':txtTipo', $tipo);
    $prepareSQL->bindValue(':txtPessoa', $id_pessoa);    
    $prepareSQL->bindValue(':id', $id_cartao);
    $prepareSQL->execute();
    return $id_cartao;
}

function getPerfil($conn, $id, $psw = true) {
    $prepareSQL = $conn->prepare("SELECT * FROM sf_pessoa WHERE id = :id limit 1");
    $prepareSQL->bindValue(':id', $id);
    $prepareSQL->execute();
    $return = $prepareSQL->fetch(PDO::FETCH_OBJ);
    if ($psw) {    
        unset($return->senha);
    }
    return $return;
}

function getLojaBanco($conn, $id) {
    $prepareSQL = $conn->prepare("SELECT * FROM sf_empresa_banco WHERE id_empresa = :id limit 1");
    $prepareSQL->bindValue(':id', $id);
    $prepareSQL->execute();
    $return = $prepareSQL->fetch(PDO::FETCH_OBJ);
    return $return;
}

function getCadastroEmail($conn, $email) {
    $prepareSQL = $conn->prepare("SELECT * FROM sf_pessoa WHERE email = :email limit 1");
    $prepareSQL->bindValue(':email', $email);
    $prepareSQL->execute();
    return $prepareSQL->fetch(PDO::FETCH_OBJ);
}

function getCashback($conn, $id_pessoa) {
    $prepareSQL = $conn->prepare("select fn_cashback(:id) total, fn_cashback_liquido(:id) total_liquido;");
    $prepareSQL->bindValue(':id', $id_pessoa);
    $prepareSQL->execute();
    return $prepareSQL->fetch(PDO::FETCH_OBJ);
}

function getEstado($conn, $id) {
    $prepareSQL = $conn->prepare("SELECT * FROM tb_estados WHERE estado_codigo = :id limit 1");
    $prepareSQL->bindValue(':id', $id);
    $prepareSQL->execute();
    return $prepareSQL->fetch(PDO::FETCH_OBJ);
}

function getCidade($conn, $id) {
    $prepareSQL = $conn->prepare("SELECT * FROM tb_cidades WHERE cidade_codigo = :id limit 1");
    $prepareSQL->bindValue(':id', $id);
    $prepareSQL->execute();
    $return = $prepareSQL->fetch(PDO::FETCH_OBJ);
    $return->cidade_codigoEstado = getEstado($conn, $return->cidade_codigoEstado);
    return $return;
}

function getEstados($conn, $filter) {
    $prepareSQL = $conn->prepare("SELECT * FROM tb_estados " . 
    ($filter ? "WHERE estado_codigo in (select c.cidade_codigoEstado from sf_empresa_endereco ee
    inner join tb_cidades c on c.cidade_codigo = ee.id_cidade)" : "") .
    "ORDER BY estado_sigla");
    $prepareSQL->execute();
    return $prepareSQL->fetchAll(PDO::FETCH_OBJ);
}

function getCidades($conn, $id, $filter) {
    $prepareSQL = $conn->prepare("SELECT * FROM tb_cidades WHERE cidade_codigoEstado = :uf " . 
    ($filter ? "AND cidade_codigo in (select ee.id_cidade from sf_empresa_endereco ee)" : "") .            
    "ORDER BY cidade_nome");
    $prepareSQL->bindValue(':uf', $id);
    $prepareSQL->execute();        
    return $prepareSQL->fetchAll(PDO::FETCH_OBJ);    
}