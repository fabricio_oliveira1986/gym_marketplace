<?php

function salvarPergunta($conn, $dados) {
    if (is_numeric($dados["txtId"])) {
        $prepareSQL = $conn->prepare("UPDATE sf_pergunta_resposta SET
        resposta = :txtResposta, data_resposta = now() WHERE id = :id");
        $prepareSQL->bindValue(':txtResposta', $dados["txtResposta"]);
        $prepareSQL->bindValue(':id', $dados["txtId"]);
        $prepareSQL->execute();    
        sendResposta($conn, $dados["txtId"]);
        return $dados["txtId"];
    } else {
        $prepareSQL = $conn->prepare("INSERT INTO sf_pergunta_resposta 
        (id_anuncio, id_pessoa, data, pergunta, inativo) values 
        (:txtAnuncio, :txtPessoa, now(), :txtPergunta, 0)");
        $prepareSQL->bindValue(':txtAnuncio', $dados["txtAnuncio"]);
        $prepareSQL->bindValue(':txtPessoa', $dados["txtPessoa"]);
        $prepareSQL->bindValue(':txtPergunta', $dados["txtPergunta"]);
        $prepareSQL->execute();
        return $conn->lastInsertId();
    }
}

function salvarAvaliacao($conn, $dados) {
    if (is_numeric($dados["txtId"])) {
        $prepareSQL = $conn->prepare("UPDATE sf_vendas_avaliacao SET
        resposta = :txtResposta, data_resposta = now() WHERE id = :id");
        $prepareSQL->bindValue(':txtResposta', $dados["txtResposta"]);
        $prepareSQL->bindValue(':id', $dados["txtId"]);
        $prepareSQL->execute();       
        return $dados["txtId"];
    } else {
        $prepareSQL = $conn->prepare("INSERT INTO sf_vendas_avaliacao 
        (id_anuncio, id_pessoa, data, avaliacao, comentario, inativo) values 
        (:txtAnuncio, :txtPessoa, now(), :txtAvaliacao, :txtComentario, 0)");
        $prepareSQL->bindValue(':txtAnuncio', $dados["txtAnuncio"]);
        $prepareSQL->bindValue(':txtPessoa', $dados["txtPessoa"]);        
        $prepareSQL->bindValue(':txtAvaliacao', $dados["txtAvaliacao"]);
        $prepareSQL->bindValue(':txtComentario', $dados["txtComentario"]);
        $prepareSQL->execute();        
        $id = $conn->lastInsertId();
        $prepareSQLAv = $conn->prepare("UPDATE sf_empresa_produto_anuncio SET 
        avaliacao = fn_avaliacao_anuncio(id), avaliacao_total = fn_numero_avaliacao_anuncio(id) 
        WHERE id = :txtAnuncio");
        $prepareSQLAv->bindValue(':txtAnuncio', $dados["txtAnuncio"]);
        $prepareSQLAv->execute();
        return $id;
    }
}

function getPergunta($conn, $id) {
    $prepareSQL = $conn->prepare("SELECT ep.id id_anuncio, ep.id_foto, ep.nome, epa.descricao, pergunta, resposta, p.nome nome_pessoa, p.email 
    from sf_pergunta_resposta pr inner join sf_empresa_produto_anuncio epa on pr.id_anuncio = epa.id
    inner join sf_empresa_produto ep on ep.id = epa.id_empresa_produto
    inner join sf_pessoa p on p.id = pr.id_pessoa
    where pr.id = :id limit 1;");
    $prepareSQL->bindValue(':id', $id);
    $prepareSQL->execute();
    $dados = $prepareSQL->fetch(PDO::FETCH_OBJ);
    $dados->image = $dados->id_foto ? getUrlFile('produto', $dados->id_anuncio, $dados->id_foto) : 
        'https://gymbrother.com.br/admin/assets/img/no_image.png';
    return $dados;
}