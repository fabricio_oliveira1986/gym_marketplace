<?php

function makeVenda($conn, $dados) {
    $itens = validarItens($conn, $dados);
    if (count($itens) > 0) {
        try {
            $conn->beginTransaction();
            if ($dados["txtTipoDocumento"] == "C" && (!isset($dados["txtIdCartao"]) || !is_numeric($dados["txtIdCartao"]))) {
                $dados["txtIdCartao"] = salvarCartao($conn, $dados);
            }
            $id = salvarVenda($conn, $dados);
            if ($dados["txtTipoDocumento"] == "X") {
                salvarConta($conn, $id, $dados);
            }
            foreach ($itens as $item) {
                salvarVendaItem($conn, $id, $item);
            }
            distribuirParcelas($conn, $id, $itens, $dados["txtTipoDocumento"]);
            $conn->commit();
            $id_return = processarVendaSafe($conn, $id);
            if (is_numeric($id_return) && in_array($dados["txtTipoDocumento"], array("C", "B", "P", "T"))) {
                sendMsgVenda($conn, $id);            
            } else if (!is_numeric($id_return)) {
                print_r($id_return);
            }
            return $id;
        } catch (Exception $e) {
            echo $e->getMessage();
            $conn->rollBack();
            exit;
        }
    } else {
        echo "Dados inválidos";
        exit;
    }
}

function makeEstorno($conn, $id, $total) {
    return setEstornarSafe($conn, $id, $total);    
}

function validarItens($conn, $dados) {
    $itens = [];
    $id_empresa = "";
    $valor_tot = 0;
    if ($dados["txtTipoDocumento"] == "S") {
        $loja = getLojaSession($conn);        
        $saldo = getSaldoSafe($loja);        
        $row = getAnuncioVenda($conn, 2);        
        $row->taxas = getTaxas($conn, $dados["txtTipoDocumento"], 1);
        if (isset($dados["txtValorSaque"]) && isset($saldo["AmountAvailableToday"]) && ($saldo["AmountAvailableToday"] + $row->taxas->valor_real) >= valoresNumericos($dados["txtValorSaque"]) && valoresNumericos($dados["txtValorSaque"]) > 0) {
            $row->id_empresa = $loja->id;
            $row->safe_token = $loja->safe_token;
            $row->valor = valoresNumericos($dados["txtValorSaque"]);
            $row->quantidade_compra = 1;
            $itens[] = $row;
        } else {
            echo "Valor indisponível para Saque!";
            exit;
        }
    } else if ($dados["txtTipoDocumento"] == "X") {
        $cashback = getCashback($conn, $dados["txtIdPessoa"]);
        $row = getAnuncioVenda($conn, 2);        
        $row->taxas = getTaxas($conn, $dados["txtTipoDocumento"], 1);        
        if (isset($dados["txtValorSaque"]) && $cashback->total_liquido > (valoresNumericos($dados["txtValorSaque"]) + $row->taxas->valor_real) && valoresNumericos($dados["txtValorSaque"]) > 0) {            
            $row->valor = valoresNumericos($dados["txtValorSaque"]);
            $row->quantidade_compra = 1;            
            $itens[] = $row;            
        } else {
            echo "Valor indisponível para Saque!";
            exit;
        }
    } else {
        for ($i = 0; $i < count($dados["txtIdAnuncio"]); $i++) {
            $row = getAnuncioVenda($conn, $dados["txtIdAnuncio"][$i]);
            $row->taxas = getTaxas($conn, $dados["txtTipoDocumento"], $row->divulgacao);
            if ($row->quantidade < ($row->quantidade_venda + $dados["txtQuantidade"][$i])) {
                echo "Quantidade indisponível para venda!";
                exit;
            } else if ($row->cashback > 100) {
                echo "Índice de cashback indisponível para venda!";
                exit;
            } else if ($row->inativo == 1 || $row->inativo_p == 1) {
                echo "Anuncio inativo!";
                exit;
            } else if ($dados["txtTipoDocumento"] == "C" && $row->cartao_credito == 0) {
                echo "Forma de pagamento inválida!";
                exit;
            } else if ($dados["txtTipoDocumento"] == "B" && $row->boleto == 0) {
                echo "Forma de pagamento inválida!";
                exit;
            } else if ($dados["txtTipoDocumento"] == "P" && $row->pix == 0) {
                echo "Forma de pagamento inválida!";
                exit;
            } else if (is_numeric($id_empresa) && $id_empresa != $row->id_empresa) {
                echo "Produtos de empresas diferentes!";
                exit;
            } else {
                $row->quantidade_compra = $dados["txtQuantidade"][$i];
                $valor_tot += ($row->valor * $row->quantidade_compra);
                $id_empresa = $row->id_empresa;
                $itens[] = $row;
            }
        }
        if (isset($dados["txtIdFrete"]) && is_numeric($dados["txtIdFrete"])) {
            $frete = getFrete($conn, $dados["txtIdFrete"]);                        
            if ($frete->valor > 0 && $frete->inativo == 0) {
                $row = getAnuncioVenda($conn, 1);
                $row->taxas = getTaxas($conn, $dados["txtTipoDocumento"], 1);                
                $row->valor = $frete->valor;
                $row->quantidade_compra = 1;
                $valor_tot += $row->valor;
                $itens[] = $row;
            }
        }
        if ($dados["txtTipoDocumento"] == "T") {
            $cashback = getCashback($conn, $dados["txtIdPessoa"]);
            if ($cashback->total_liquido < $valor_tot) {
                echo "Saldo em cashback insuficiente para pagamento!";
                exit;
            }
        }
    }
    return $itens;
}

function distribuirParcelas($conn, $id, $itens, $tipo_documento) {
    $valor_tot = 0;    
    $valor_gym = 0;
    $taxas_gym = 0;    
    $valor_cbk = 0;
    $valor_cli = 0;
    foreach ($itens as $item) {
        $valor = $item->valor * $item->quantidade_compra;
        $valor_tot += $valor;        
        $valor_gym += (($valor * $item->taxas->valor_porcentagem) / 100);
        if ($taxas_gym == 0 && $item->taxas->valor_real > 0) {
            $taxas_gym = $item->taxas->valor_real;
        }        
        $valor_cbk += (($valor * $item->cashback) / 100);
        $valor_cli += (($valor * (100 - ($item->taxas->valor_porcentagem + $item->cashback))) / 100);
    }
    if ($tipo_documento == "S") { //Loja Sacando Dinheiro
        salvarVendaParcela($conn, $id, $itens[0]->id_empresa, (-1 * $valor_tot));
        salvarVendaParcela($conn, $id, 2, (-1 * $taxas_gym));        
    } else if ($tipo_documento == "T") { //Pagamento em CashBack
        salvarVendaParcela($conn, $id, $itens[0]->id_empresa, ($valor_cli - $taxas_gym));
        salvarVendaParcela($conn, $id, 1, (-1 * $valor_tot));
        salvarVendaParcela($conn, $id, 2, (-1 * ($valor_cli - $taxas_gym)));
    } else if ($tipo_documento == "X") { //Sacando CashBack
        salvarVendaParcela($conn, $id, 1, (-1 * ($valor_tot + $taxas_gym)));
        salvarVendaParcela($conn, $id, 2, (-1 * $valor_tot));
    } else { //Pagamento normal (Cartão, Boleto ou PIX)
        $diff = $valor_tot - (round(($valor_cli - $taxas_gym),2) + round($valor_cbk,2) + round(($valor_gym + $taxas_gym),2));
        salvarVendaParcela($conn, $id, $itens[0]->id_empresa, ($valor_cli - $taxas_gym + $diff));
        salvarVendaParcela($conn, $id, 1, $valor_cbk);
        salvarVendaParcela($conn, $id, 2, ($valor_gym + $taxas_gym));
    }
}

function salvarVenda($conn, $dados) {
    $prepareSQL = $conn->prepare("INSERT INTO sf_vendas (id_pessoa, data, tipo_documento, id_cartao, n_parcela, id_endereco, id_frete, status, entrega) values 
    (:txtIdPessoa, now(), :txtTipoDocumento, :txtIdCartao, :txtParcelas, :txtIdEndereco, :txtIdFrete, 'A', 'P')");
    $prepareSQL->bindValue(':txtIdPessoa', $dados["txtIdPessoa"]);
    $prepareSQL->bindValue(':txtTipoDocumento', $dados["txtTipoDocumento"]);
    $prepareSQL->bindValue(':txtParcelas', $dados["txtParcelas"]);
    $valores = ['txtIdCartao', 'txtIdEndereco', 'txtIdFrete'];
    foreach ($valores as $item) {
        $prepareSQL->bindValue(":$item", (isset($dados[$item]) && is_numeric($dados[$item]) ? $dados[$item] : null));
    }
    $prepareSQL->execute();
    return $conn->lastInsertId();
}

function salvarVendaItem($conn, $id, $item) {
    $prepareSQL = $conn->prepare("INSERT INTO sf_vendas_itens (id_venda, id_anuncio, quantidade, valor, cashback, divulgacao) values 
    (:txtId, :txtIdAnuncio, :txtQuantidade, :txtValor, :txtCashback, :txtDivulgacao)");
    $prepareSQL->bindValue(':txtId', $id);
    $prepareSQL->bindValue(':txtIdAnuncio', $item->id);
    $prepareSQL->bindValue(':txtQuantidade', $item->quantidade_compra);
    $prepareSQL->bindValue(':txtValor', $item->valor);
    $prepareSQL->bindValue(':txtCashback', $item->cashback);
    $prepareSQL->bindValue(':txtDivulgacao', $item->divulgacao);
    $prepareSQL->execute();
    return $conn->lastInsertId();
}

function salvarVendaParcela($conn, $id, $id_empresa, $valor) {
    $prepareSQL = $conn->prepare("INSERT INTO sf_vendas_parcelas (id_venda, id_empresa, valor) values 
    (:txtId, :txtIdEmpresa, :txtValor)");
    $prepareSQL->bindValue(':txtId', $id);
    $prepareSQL->bindValue(':txtIdEmpresa', $id_empresa);
    $prepareSQL->bindValue(':txtValor', round($valor, 2));
    $prepareSQL->execute();
    return $conn->lastInsertId();
}

function salvarConta($conn, $id, $dados) {
    $valores = ['txtBanco', 'txtAgencia', 'txtAgenciaDv', 'txtConta', 'txtContaDv', 'txtTitularNome', 'txtTitularCpf', 'txtTipoConta', 'txtCompConta'];    
    $prepareSQL = $conn->prepare("INSERT INTO sf_vendas_banco (id_venda, banco, agencia, agencia_dv, conta, conta_dv, titular_nome, titular_cpf, tipo, complemento) values 
    (:txtId, :txtBanco, :txtAgencia, :txtAgenciaDv, :txtConta, :txtContaDv, :txtTitularNome, :txtTitularCpf, :txtTipoConta, :txtCompConta)");
    foreach ($valores as $item) {
        $prepareSQL->bindValue(":$item", $dados[$item]);
    }    
    $prepareSQL->bindValue(':txtId', $id);    
    $prepareSQL->execute();
    return $conn->lastInsertId();   
}

function salvarCartao($conn, $dados) {
    $valores = ['txtIdPessoa', 'txtCardNome', 'txtCardCvv'];
    $prepareSQL = $conn->prepare("INSERT INTO sf_pessoa_cartao (id_pessoa, nome_cartao, cartao, data, bandeira, cvv, token, safe_id, inativo) values 
    (:txtIdPessoa, :txtCardNome, :txtCardNumero, :txtCardData, :txtCardBandeira, :txtCardCvv, :txtCardHash, :txtSafeId, :txtCardSalvar)");
    foreach ($valores as $item) {
        $prepareSQL->bindValue(":$item", $dados[$item]);
    }
    $prepareSQL->bindValue(':txtCardNumero', substr($dados['txtCardNumero'], 0, 4) . "..." . substr($dados['txtCardNumero'], -4));     
    $prepareSQL->bindValue(':txtCardData', valoresData($dados['txtCardData']));     
    $prepareSQL->bindValue(':txtCardBandeira', getBandeira($dados['txtCardNumero']));       
    $prepareSQL->bindValue(':txtCardHash', encrypt($dados['txtCardNumero'], "gymBrother7034", true));         
    if ($dados['txtCardSalvar'] == "1") {
        //$safeCard = setCartaoSafe($dados['txtCardNome'], $dados['txtCardNumero'], $dados['txtCardData'], $dados['txtCardCvv']);
        $prepareSQL->bindValue(':txtSafeId', "");
        $prepareSQL->bindValue(':txtCardSalvar', 0);
    } else {
        $prepareSQL->bindValue(':txtSafeId', null);
        $prepareSQL->bindValue(':txtCardSalvar', 1);
    }
    $prepareSQL->execute();
    return $conn->lastInsertId();
}

//------------------------------------------------------------------------------

function getAnuncioVenda($conn, $id) {
    $prepareSQL = $conn->prepare("select epa.id, epa.quantidade, fn_vendas_anuncio(epa.id) quantidade_venda, 
    epa.valor, epa.cashback, epa.inativo, ep.nome, ep.divulgacao, ep.boleto, ep.cartao_credito, ep.pix, 
    ep.inativo inativo_p, e.id id_empresa, e.safe_token 
    from sf_empresa_produto_anuncio epa 
    inner join sf_empresa_produto ep on epa.id_empresa_produto = ep.id
    inner join sf_empresa e on e.id = ep.id_empresa
    where epa.id = :id limit 1");
    $prepareSQL->bindValue(':id', $id);
    $prepareSQL->execute();
    $return = $prepareSQL->fetch(PDO::FETCH_OBJ);
    return $return;
}

function getVendas($conn, $id_pessoa, $credito) { 
    $query = "select v.*, descricao from sf_vendas v 
    inner join sf_tipo_documento td on td.id = v.tipo_documento        
    where v.id_pessoa = :id_pessoa" . ($credito ? " and v.tipo_documento in ('C', 'B', 'P', 'T')" : "") . " order by id";
    $prepareSQL = $conn->prepare($query);
    $prepareSQL->bindValue(':id_pessoa', $id_pessoa);
    $prepareSQL->execute();
    $itens = $prepareSQL->fetchAll(PDO::FETCH_OBJ);
    foreach ($itens as $dados) {
        $dados->id_pessoa = getPessoa($conn, $dados->id_pessoa);
        $dados->id_endereco = getPessoaEndereco($conn, $dados->id_endereco);
        $dados->id_cartao = getCartao($conn, $dados->id_cartao);
        $dados->itens = getVendaItens($conn, $dados->id);
        $dados->parcelas = getVendaParcelas($conn, $dados->id);
        $dados->bancos = getVendaBancos($conn, $dados->id);
        $dados->id_frete = getFrete($conn, $dados->id_frete);
        $dados->charge_pix = $dados->charge_pix;
    }
    return $itens;    
}

function getVenda($conn, $id) {
    $prepareSQL = $conn->prepare("select v.*, descricao from sf_vendas v 
    inner join sf_tipo_documento td on td.id = v.tipo_documento        
    where v.id = :id limit 1");
    $prepareSQL->bindValue(':id', $id);
    $prepareSQL->execute();
    $dados = $prepareSQL->fetch(PDO::FETCH_OBJ);
    $dados->id_pessoa = getPessoa($conn, $dados->id_pessoa);
    $dados->id_endereco = getPessoaEndereco($conn, $dados->id_endereco);
    $dados->id_cartao = getCartao($conn, $dados->id_cartao);
    $dados->itens = getVendaItens($conn, $id);
    $dados->parcelas = getVendaParcelas($conn, $id);
    $dados->bancos = getVendaBancos($conn, $id);    
    $dados->id_frete = getFrete($conn, $dados->id_frete);
    $dados->charge_pix = $dados->charge_pix;    
    return $dados;
}

function getVendaItens($conn, $id) {
    $query = "select vi.*, ep.nome, epa.descricao, ep.id_foto, epa.avaliacao, 
    epa.avaliacao_total, e.nome_fantasia from sf_vendas_itens vi 
    inner join sf_empresa_produto_anuncio epa on epa.id = vi.id_anuncio
    inner join sf_empresa_produto ep on ep.id = epa.id_empresa_produto 
    inner join sf_empresa e on e.id = ep.id_empresa    
    where vi.id_venda = :id_venda";
    $prepareSQL = $conn->prepare($query);
    $prepareSQL->bindValue(':id_venda', $id);
    $prepareSQL->execute();
    $itens = $prepareSQL->fetchAll(PDO::FETCH_OBJ);
    foreach ($itens as $dados) {
        $dados->image = $dados->id_foto ? getUrlFile('produto', $dados->id_anuncio, $dados->id_foto) : 
        'https://gymbrother.com.br/admin/assets/img/no_image.png';
    }    
    return $itens;
}

function getVendaParcelas($conn, $id) {
    $query = "select vp.*, e.nome_fantasia razao_social, e.safe_token, e.safe_id,
    e.cnpj, e.nome_fantasia razao_social 
    from sf_vendas_parcelas vp
    inner join sf_empresa e on vp.id_empresa = e.id    
    where vp.id_venda = :id_venda";
    $prepareSQL = $conn->prepare($query);
    $prepareSQL->bindValue(':id_venda', $id);
    $prepareSQL->execute();
    $dados = $prepareSQL->fetchAll(PDO::FETCH_OBJ);
    return $dados;
}

function getVendaBancos($conn, $id) {
    $query = "SELECT vb.* FROM sf_vendas_banco vb WHERE vb.id_venda = :id_venda limit 1";
    $prepareSQL = $conn->prepare($query);
    $prepareSQL->bindValue(':id_venda', $id);
    $prepareSQL->execute();
    $return = $prepareSQL->fetch(PDO::FETCH_OBJ);
    return $return;
}

function getPessoa($conn, $id) {
    $prepareSQL = $conn->prepare("SELECT * FROM sf_pessoa WHERE id = :id limit 1");
    $prepareSQL->bindValue(':id', $id);
    $prepareSQL->execute();
    $return = $prepareSQL->fetch(PDO::FETCH_OBJ);
    unset($return->senha);
    return $return;
}

function getPessoaEndereco($conn, $id) {
    $prepareSQL = $conn->prepare("SELECT * FROM sf_pessoa_endereco WHERE id = :id limit 1");
    $prepareSQL->bindValue(':id', $id);
    $prepareSQL->execute();
    $return = $prepareSQL->fetch(PDO::FETCH_OBJ);
    if (isset($return->complemento)) {  
        $return->complemento = ($return->complemento == null ? "" : $return->complemento);
    }
    if (isset($return->id_cidade)) {
        $return->id_cidade = getCidade($conn, $return->id_cidade);
    }
    return $return;
}

function getCartao($conn, $id) {
    if (is_numeric($id)) {
        $prepareSQL = $conn->prepare("SELECT * FROM sf_pessoa_cartao WHERE id = :id limit 1");
        $prepareSQL->bindValue(':id', $id);
        $prepareSQL->execute();
        $return = $prepareSQL->fetch(PDO::FETCH_OBJ);
        return $return;
    } else {
        return null;
    }
}

function getFrete($conn, $id) {
    if (is_numeric($id)) {
        $prepareSQL = $conn->prepare("SELECT * FROM sf_empresa_frete WHERE id = :id limit 1");
        $prepareSQL->bindValue(':id', $id);
        $prepareSQL->execute();
        $return = $prepareSQL->fetch(PDO::FETCH_OBJ);
        return $return;
    } else {
        return null;
    }
}

function getTaxas($conn, $tipo_documento, $tipo_divulgacao) {
    $prepareSQL = $conn->prepare("SELECT * FROM sf_taxas WHERE id_tipo_documento = :tipo_documento AND tipo_divulgacao = :tipo_divulgacao limit 1");
    $prepareSQL->bindValue(':tipo_documento', $tipo_documento);
    $prepareSQL->bindValue(':tipo_divulgacao', $tipo_divulgacao);
    $prepareSQL->execute();
    $return = $prepareSQL->fetch(PDO::FETCH_OBJ);
    return $return;
}

//------------------------------------------------------------------------------

function salvarAvaliacaoVenda($conn, $dados) {
    $prepareSQL = $conn->prepare("UPDATE sf_vendas SET avaliacao = :txtAvaliacao 
    WHERE id = :txtVenda");
    $prepareSQL->bindValue(':txtAvaliacao', $dados["txtAvaliacao"]);
    $prepareSQL->bindValue(':txtVenda', $dados["txtVenda"]);
    $prepareSQL->execute();
    return $dados["txtVenda"];
}