<?php

function criarUsuario($conn, $data) {
    $query = "insert into sf_pessoa(nome, dt_nasc, cpf, email, senha, sexo, celular) 
    values (:nome, :dt_nasc, :cpf, :email, :senha, :sexo, :celular)";
    $prepareSQL = $conn->prepare($query);
    $prepareSQL->bindValue(':nome', $data['name']);
    $prepareSQL->bindValue(':dt_nasc', valoresData($data['birthDate']));
    $prepareSQL->bindValue(':cpf', $data['cpf']);
    $prepareSQL->bindValue(':email', $data['email']);
    $prepareSQL->bindValue(':senha', md5($data['password']));
    $prepareSQL->bindValue(':sexo', $data['genre']);
    $prepareSQL->bindValue(':celular', $data['celphone']);
    $prepareSQL->execute();
    return $conn->lastInsertId();
}

function getUsuarioPorId($conn, $id, $completo = 0) {
    $query = "select * from sf_pessoa where id = :id limit 1";
    $prepareSQL = $conn->prepare($query);
    $prepareSQL->bindValue(':id', $id);
    $prepareSQL->execute();
    $dados = $prepareSQL->fetch(PDO::FETCH_OBJ);
    $usuario = transformUsuario($dados, $completo);
    return $usuario;
}

function getEmpresasRegioes($conn) {
    $query = "select distinct SUBSTRING(celular, 2, 2) ddd
    from sf_empresa ep where ep.id > 2;
    order by ddd";
    $prepareSQL = $conn->prepare($query);
    $prepareSQL->execute();
    $dados = $prepareSQL->fetchAll(PDO::FETCH_OBJ);
    $empresas = array_map(function($item) {
        return transformRegioes($item);
    }, $dados);
    return $empresas;
}

function getEmpresasCidades($conn) {
    $query = "select distinct cidade_codigo, cidade_nome, estado_sigla from sf_empresa ep 
    inner join sf_empresa_endereco ee on ee.id_empresa = ep.id
    inner join tb_cidades c on c.cidade_codigo = ee.id_cidade
    inner join tb_estados e on c.cidade_codigoEstado = e.estado_codigo
    where ep.id > 2 order by c.cidade_nome;";
    $prepareSQL = $conn->prepare($query);
    $prepareSQL->execute();
    $dados = $prepareSQL->fetchAll(PDO::FETCH_OBJ);
    $empresas = array_map(function($item) {
        return transformCidade($item);
    }, $dados);
    return $empresas;
}

function getAnunciosValores($conn) {
    $query = "select min(valor) min_val, max(valor) max_val 
    from sf_empresa_produto_anuncio where inativo = 0 and id > 1 limit 1";
    $prepareSQL = $conn->prepare($query);
    $prepareSQL->execute();
    $dados = $prepareSQL->fetch(PDO::FETCH_OBJ);
    $return = [];    
    $faixas = 4;
    $valor = ($dados->max_val / $faixas);
    for ($i = 0; $i < $faixas; $i++) {
        $row = new stdClass();
        $row->min_val = ($valor * $i);
        $row->max_val = ($valor * ($i + 1));
        $return[] = $row;
    }
    return $return;
}

function getCashbackValores($conn) {
    $return[] = getCashbackItem(0, "De 0 a 15%");    
    $return[] = getCashbackItem(1, "De 16 a 25%");    
    $return[] = getCashbackItem(2, "De 26 a 35%");    
    $return[] = getCashbackItem(3, "Acima de 35%");    
    return $return;    
}

function getCashbackItem($id, $descricao) {
    $row = new stdClass();
    $row->id = $id;     
    $row->descricao = $descricao;
    return $row;
}

function getEmpresas($conn, $data) {
    $condicao = "";
    if (isset($data['id_cidade'])) {
        $condicao .= " and ee.id_cidade = :id_cidade";
    }
    $query = "select e.id as id_empresa, nome_fantasia as razao_social, 
    id_foto as id_foto_empresa, ee.id as id_endereco, bairro, c.cidade_nome as cidade
    from sf_empresa e inner join sf_empresa_endereco ee on ee.id_empresa = e.id
    inner join tb_cidades c on c.cidade_codigo = ee.id_cidade 
    " . $condicao . " order by razao_social asc;";
    $prepareSQL = $conn->prepare($query);
    if (isset($data['id_cidade'])) {
        $prepareSQL->bindValue(':id_cidade', $data['id_cidade']);
    }
    $prepareSQL->execute();
    $dados = $prepareSQL->fetchAll(PDO::FETCH_OBJ);
    $empresas = array_map(function($item) {
        return transformEmpresa($item);
    }, $dados);
    return $empresas;
}

function getEmpresa($conn, $data) {
    $query = "select e.id as id_empresa, nome_fantasia as razao_social, id_foto as id_foto_empresa,
    ee.id as id_endereco, bairro, c.cidade_nome as cidade
    from sf_empresa e 
    inner join sf_empresa_endereco ee on ee.id_empresa = e.id
    inner join tb_cidades c on c.cidade_codigo = ee.id_cidade
    where e.id = :id limit 1;";
    $prepareSQL = $conn->prepare($query);
    $prepareSQL->bindValue(':id', $data['id']);
    $prepareSQL->execute();
    $dados = $prepareSQL->fetch(PDO::FETCH_OBJ);
    $empresa = transformEmpresa($dados);
    $empresa->images = getUrlImagens('empresas', $dados->id_empresa);    
    $empresa->videos = getUrlVideos('empresas', $dados->id_empresa);    
    $empresa->categories = getCategoriaEmpresa($conn, ['id' => $dados->id_empresa]);
    $empresa->products = getAnuncios($conn, ['id_empresa' => $dados->id_empresa]);
    return $empresa;
}

function getAnuncios($conn, $data) {     
    //http://localhost/gym_marketplace/admin/ajax/FormApp.php?getAnuncios=S&id_usuario=&order=0&cidade=3655
    $condicao = "";    
    if (isset($data['texto']) && strlen($data['texto']) > 0) {
        $condicao .= " and (ep.nome like :texto or a.descricao like :texto)";
    }
    if (isset($data['id_empresa']) && is_numeric($data['id_empresa'])) {
        $condicao .= " and ep.id_empresa = :id_empresa";
    }
    if (isset($data['categoria']) && is_numeric($data['categoria'])) {
        $condicao .= " and ep.categoria = :categoria";
    }
    if (isset($data['categoria_empresa']) && is_numeric($data['categoria_empresa'])) {
        $condicao .= " and ep.categoria_empresa = :categoria_empresa";
    }
    if (isset($data['cidade']) && is_numeric($data['cidade'])) {
        $condicao .= " and ee.id_cidade = :cidade";
    }        
    if (isset($data['valor_min']) && is_numeric($data['valor_min']) && isset($data['valor_max']) && is_numeric($data['valor_max'])) {
        $condicao .= " and a.valor between :valor_min and :valor_max";
    }    
    if (isset($data['regiao']) && is_numeric($data['regiao'])) {
        //$condicao .= " and e.celular like CONCAT('(', :regiao, ')%')";
    } 
    if (isset($data['cashback']) && $data['cashback'] == "0") {    
        $condicao .= " and a.cashback between 0 and 15";
    } else if (isset($data['cashback']) && $data['cashback'] == "1") {    
        $condicao .= " and a.cashback between 16 and 25";
    } else if (isset($data['cashback']) && $data['cashback'] == "2") {    
        $condicao .= " and a.cashback between 26 and 35";
    } else if (isset($data['cashback']) && $data['cashback'] == "3") {    
        $condicao .= " and a.cashback > 35";
    }
    $query = "select a.*, ep.tipo, ep.id_empresa, ep.nome nome_produto, 
    ep.descricao descricao_produto, ep.id_foto id_foto_produto, ep.cartao_credito,
    e.id_foto id_foto_empresa, e.nome_fantasia razao_social 
    from sf_empresa_produto_anuncio a
    inner join sf_empresa_produto ep on a.id_empresa_produto = ep.id
    inner join sf_empresa e on ep.id_empresa = e.id
    inner join sf_empresa_endereco ee on ee.id_empresa = e.id
    where a.inativo = 0 and ep.inativo = 0 and ((ep.divulgacao = 1 and DATEDIFF(now(), a.dt_cadastro) < 45) or
    (ep.divulgacao = 2 and DATEDIFF(now(), a.dt_cadastro) < 60) or (ep.divulgacao = 3)) " . $condicao;    
    if (isset($data['order']) && $data['order'] == "0") { //0 - Melhores Ofertas
        $query .= " and ep.divulgacao in (2,3) order by ep.divulgacao desc, a.avaliacao desc";     
    } else if (isset($data['order']) && $data['order'] == "1") { //1 - Planos Desconto > (Destaques da Semana)
        $query .= " and ep.divulgacao in (3) order by a.dt_cadastro desc";             
    } else if (isset($data['order']) && $data['order'] == "2") { //2 - Maior Cashback
        $query .= " and ep.divulgacao in (1,2,3) order by a.cashback desc, ep.divulgacao desc";
    } else if (isset($data['order']) && $data['order'] == "3") { //3 - Produtos e Serviços Perto de Você
        $query .= " and ep.divulgacao in (1,2,3) order by ep.divulgacao desc, a.avaliacao desc";        
    }
    $prepareSQL = $conn->prepare($query);
    if (isset($data['texto']) && strlen($data['texto']) > 0) {
        $prepareSQL->bindValue(":texto", $data["texto"].'%', PDO::PARAM_STR);
    }
    $valores = ['id_empresa', 'categoria', 'categoria_empresa', 'cidade', 'valor_min', 'valor_max', 'regiao'];    
    foreach ($valores as $item) {
        if (isset($data[$item]) && is_numeric($data[$item])) {
            $prepareSQL->bindValue(":$item", $data[$item]);
        }
    }
    $prepareSQL->execute();
    $dados = $prepareSQL->fetchAll(PDO::FETCH_OBJ);
    $produtos = array_map(function($item) use($data) {
        return transformProduto($item, $data);
    }, $dados);
    return $produtos;
}

function getAnuncio($conn, $data) {
    $query = "select a.*, ep.tipo, ep.id_empresa, ep.nome nome_produto, 
    ep.descricao descricao_produto, ep.id_foto id_foto_produto, ep.cartao_credito,
    e.id_foto id_foto_empresa, e.nome_fantasia razao_social 
    from sf_empresa_produto_anuncio a
    inner join sf_empresa_produto ep on a.id_empresa_produto = ep.id
    inner join sf_empresa e on ep.id_empresa = e.id
    where a.id = :id limit 1";
    $prepareSQL = $conn->prepare($query);
    $prepareSQL->bindValue(':id', $data['id']);
    $prepareSQL->execute();
    $dados = $prepareSQL->fetch(PDO::FETCH_OBJ);
    $produto = transformProduto($dados);    
    return $produto;
}

function getAnuncioAvaliacoes($conn, $data) {
    $query = "select va.*, p.nome, p.id_foto from sf_vendas_avaliacao va
    inner join sf_pessoa p on p.id = va.id_pessoa where va.id_anuncio = :id_anuncio
    and va.inativo = 0";
    $prepareSQL = $conn->prepare($query);
    $prepareSQL->bindValue(':id_anuncio', $data['id_anuncio']);
    $prepareSQL->execute();
    $dados = $prepareSQL->fetchAll(PDO::FETCH_OBJ);
    $avaliacoes = array_map(function($item) {
        return transformAvaliacoes($item);
    }, $dados);
    return $avaliacoes;
}

function getAnuncioPerguntas($conn, $data) {
    $query = "select pr.*, p.nome, p.id_foto from sf_pergunta_resposta pr
    inner join sf_pessoa p on p.id = pr.id_pessoa where pr.id_anuncio = :id_anuncio
    and pr.inativo = 0";
    $prepareSQL = $conn->prepare($query);
    $prepareSQL->bindValue(':id_anuncio', $data['id_anuncio']);
    $prepareSQL->execute();
    $dados = $prepareSQL->fetchAll(PDO::FETCH_OBJ);
    $avaliacoes = array_map(function($item) {
        return transformPerguntas($item);
    }, $dados);
    return $avaliacoes;
}

function getCategorias($conn) {
    $query = "select * from sf_categoria where inativo = 0 order by id;";
    $prepareSQL = $conn->prepare($query);
    $prepareSQL->execute();
    $dados = $prepareSQL->fetchAll(PDO::FETCH_OBJ);
    $empresas = array_map(function($item) {
        return transformCategoria($item);
    }, $dados);
    return $empresas;
}

function getCategoriaEmpresa($conn, $data) {
    $query = "select * from sf_empresa_categoria where id_empresa = :id_empresa";
    $prepareSQL = $conn->prepare($query);
    $prepareSQL->bindValue(':id_empresa', $data['id']);
    $prepareSQL->execute();
    $dados = $prepareSQL->fetchAll(PDO::FETCH_OBJ);
    $categorias = array_map(function($item) {
        return transformCategoriaEmpresa($item);
    }, $dados);
    return $categorias;
}

function getEnderecos($conn, $data) {
    $query = "select * from sf_pessoa_endereco 
    inner join tb_cidades on id_cidade = cidade_codigo
    inner join tb_estados on cidade_codigoEstado = estado_codigo        
    where id_pessoa = :id_pessoa and inativo = 0";
    $prepareSQL = $conn->prepare($query);
    $prepareSQL->bindValue(':id_pessoa', $data['id_usuario']);
    $prepareSQL->execute();
    $dados = $prepareSQL->fetchAll(PDO::FETCH_OBJ);
    $enderecos = array_map(function($item) {
        return transformEndereco($item);
    }, $dados);
    return $enderecos;
}

function getCartoes($conn, $id_pessoa) {
    $query = "select * from sf_pessoa_cartao
    where id_pessoa = :id_pessoa 
    and safe_id is not null and inativo = 0";
    $prepareSQL = $conn->prepare($query);
    $prepareSQL->bindValue(':id_pessoa', $id_pessoa);
    $prepareSQL->execute();
    $dados = $prepareSQL->fetchAll(PDO::FETCH_OBJ);
    $enderecos = array_map(function($item) {
        return transformCartoes($item);
    }, $dados);
    return $enderecos;
}

function getLogin($conn, $dados) {
    $prepareSQL = $conn->prepare("SELECT id as id_pessoa, nome, id_foto FROM sf_pessoa WHERE inativo = 0 and email = :login AND senha = :password limit 1");
    $prepareSQL->bindValue(':login', $dados['email']);
    $prepareSQL->bindValue(':password', md5($dados['password']));
    $prepareSQL->execute();
    $res = $prepareSQL->fetch(PDO::FETCH_OBJ);
    if ($res) {
        return transformUsuario($res);
    }
    return false;
}

function existeEmail($conn, $email) {
    $prepareSQL = $conn->prepare("SELECT * FROM sf_pessoa WHERE email = :email limit 1");
    $prepareSQL->bindValue(':email', $email);
    $prepareSQL->execute();
    $res = $prepareSQL->fetch(PDO::FETCH_OBJ);
    if ($res) {
        return true;
    }
    return false;
}

function countProdutoAnuncios($conn, $data) {
    $inQuery = implode(',', array_fill(0, count($data["IdAnuncio"]), '?'));
    $query = "select count(id_empresa) total, max(id_empresa) id_empresa 
    from sf_empresa_produto_anuncio epa
    inner join sf_empresa_produto ep on epa.id_empresa_produto = ep.id
    where epa.id in (".$inQuery.") and ep.tipo = 1 limit 1;";
    $prepareSQL = $conn->prepare($query);
    foreach ($data["IdAnuncio"] as $k => $id) $prepareSQL->bindValue(($k+1), $id);
    $prepareSQL->execute();
    $dados = $prepareSQL->fetch(PDO::FETCH_OBJ);
    return $dados;    
}

function getFretes($conn, $id_empresa) { 
    $query = "select * from sf_empresa_frete where id_empresa = :id_empresa and inativo = 0";
    $prepareSQL = $conn->prepare($query);
    $prepareSQL->bindValue(':id_empresa', $id_empresa);
    $prepareSQL->execute();
    $dados = $prepareSQL->fetchAll(PDO::FETCH_OBJ);
    $fretes = array_map(function($item) {
        return transformFrete($item);
    }, $dados);
    return $fretes;    
}

function getExtratoCashback($conn, $id_pessoa) {
    $query = "select v.id, v.data, vp.valor, td.descricao,
    case when (v.tipo_documento in ('C', 'B', 'P') and v.status in ('P') and DATE_ADD(v.data, INTERVAL td.dias_cashback DAY) < now()) then 'C' 
    when (v.tipo_documento in ('C', 'B', 'P') and v.status in ('P')) then 'P'
    else 'B' end as status from sf_vendas_parcelas vp
    inner join sf_vendas v on v.id = vp.id_venda
    inner join sf_tipo_documento td on td.id = v.tipo_documento
    inner join sf_vendas_itens vi on vi.id_venda = v.id and cashback > 0
    where vp.id_empresa = 1 
    and ((v.tipo_documento in ('C', 'B', 'P') and v.status in ('P')) 
    or (v.tipo_documento in ('T', 'S') and v.status in ('P', 'A')))
    and v.id_pessoa = :id_pessoa order by v.data desc;";
    $prepareSQL = $conn->prepare($query);
    $prepareSQL->bindValue(':id_pessoa', $id_pessoa);
    $prepareSQL->execute();
    $dados = $prepareSQL->fetchAll(PDO::FETCH_OBJ);
    $cashbacks = array_map(function($item) {
        return transformExtratoCashback($item);
    }, $dados);
    return $cashbacks; 
}

function getFormasPagamento($conn, $data) {
    $inQuery = implode(',', array_fill(0, count($data["IdAnuncio"]), '?'));
    $query = "select min(boleto) boleto, min(cartao_credito) credit_card, min(pix) pix 
    from sf_empresa_produto_anuncio epa
    inner join sf_empresa_produto ep on epa.id_empresa_produto = ep.id
    where epa.id in (".$inQuery.") limit 1;";
    $prepareSQL = $conn->prepare($query);
    foreach ($data["IdAnuncio"] as $k => $id) $prepareSQL->bindValue(($k+1), $id);
    $prepareSQL->execute();
    $dados = $prepareSQL->fetch(PDO::FETCH_OBJ);
    return $dados;
}

//------------------------------------------------------------------------------

function transformEmpresa($item) {
    $empresa = new stdClass();
    $empresa->id = $item->id_empresa;
    $empresa->name = $item->razao_social;
    $empresa->image = $item->id_foto_empresa ? getUrlFile('empresa', $item->id_empresa, $item->id_foto_empresa) : 'https://gymbrother.com.br/admin/assets/img/no_image.png';
    if (isset($item->id_endereco)) {
        $empresa->id_address = $item->id_endereco;
        $empresa->city = $item->cidade;
        $empresa->neighborhood = $item->bairro;
    }
    return $empresa;
}

function transformProduto($item, $data = []) {
    $produto = new stdClass();
    $produto->id = $item->id;
    $produto->title = $item->nome_produto;
    $produto->type = $item->tipo;
    $produto->subTitle = $item->descricao;
    $produto->description = $item->descricao_produto;
    $produto->stars = $item->avaliacao;
    $produto->starsTotal = $item->avaliacao_total;
    $produto->image = $item->id_foto_produto ? getUrlFile('produto', $item->id_empresa_produto, $item->id_foto_produto) : 'https://gymbrother.com.br/admin/assets/img/no_image.png';
    $produto->images = getUrlImagens('produtos', $item->id_empresa_produto);    
    $produto->videos = getUrlVideos('produtos', $item->id_empresa_produto);    
    $produto->value = $item->valor;
    $produto->cashBack = $item->cashback;    
    $produto->bussiness = transformEmpresa($item);
    if ($item->cartao_credito > 1) {
        $produto->plots = new stdClass();
        $produto->plots->qtd = intval($item->cartao_credito);
        $produto->plots->value = floatval($item->valor) / intval($item->cartao_credito);
    }
    return $produto;
}

function transformUsuario($item, $completo = 0) {
    $pessoa = new stdClass();
    $pessoa->id = $item->id_pessoa ? $item->id_pessoa : $item->id;
    $pessoa->name = $item->nome;
    $pessoa->image = $item->id_foto ? getUrlFile('perfil', $pessoa->id, $item->id_foto) : 'https://gymbrother.com.br/admin/assets/img/no_image.png';
    if ($completo) {
        $pessoa->cpf = $item->cpf;
        $pessoa->birthDate = $item->dt_nasc;
        $pessoa->sexo = $item->sexo;
        $pessoa->cellphone = $item->celular;
    }
    return $pessoa;
}

function transformAvaliacoes($item) {
    $avaliacao = new stdClass();
    $avaliacao->id = $item->id;
    $avaliacao->commentary = $item->comentario;
    $avaliacao->stars = $item->avaliacao;
    $avaliacao->date = $item->data;
    $avaliacao->dateReply = $item->data_resposta;
    $avaliacao->reply = $item->resposta;
    $avaliacao->user = transformUsuario($item);
    return $avaliacao;
}

function transformPerguntas($item) {
    $pergunta = new stdClass();
    $pergunta->id = $item->id;
    $pergunta->ask = $item->pergunta;
    $pergunta->date = $item->data;
    $pergunta->dateReply = $item->data_resposta;
    $pergunta->reply = $item->resposta;
    $pergunta->user = transformUsuario($item);
    return $pergunta;
}

function transformCategoria($item) {
    $categoria = new stdClass();
    $categoria->id = $item->id;
    $categoria->name = $item->nome;
    $categoria->image = getUrlFile('categoria', "", $item->imagem);
    return $categoria;
}

function transformCategoriaEmpresa($item) {
    $categoria = new stdClass();
    $categoria->id = $item->id;
    $categoria->title = $item->nome;
    $categoria->subtitle = $item->descricao;
    $categoria->background = $item->cor_fundo;
    $categoria->color = $item->cor_fonte;
    return $categoria;
}

function transformRegioes($item) {
    $endereco = new stdClass();
    $endereco->id = $item->ddd;
    $endereco->name = "DDD " . $item->ddd;
    return $endereco;
}

function transformCidade($item) {
    $endereco = new stdClass();
    $endereco->id = $item->cidade_codigo;
    $endereco->city = $item->cidade_nome;
    $endereco->state = $item->estado_sigla;
    return $endereco;
}

function transformEndereco($item) {
    $endereco = new stdClass();
    $endereco->id = $item->id;
    $endereco->postCode = $item->cep;
    $endereco->street = $item->endereco;
    $endereco->number = $item->numero;
    $endereco->neighborhood = $item->bairro;
    $endereco->complement = $item->complemento;
    $endereco->city = $item->cidade_nome;
    $endereco->state = $item->estado_sigla;
    return $endereco;
}

function transformCartoes($item) {
    $endereco = new stdClass();
    $endereco->id = $item->id;    
    $endereco->cardNumber = $item->cartao;
    $endereco->expiration = $item->data;
    $endereco->banner = $item->bandeira;
    $endereco->image = 'https://gymbrother.com.br/admin/assets/img/payments/' . $item->bandeira .'.png';
    return $endereco;
}

function transformFrete($item) {
    $frete = new stdClass();
    $frete->id = $item->id;
    $frete->title = $item->nome;
    $frete->description = $item->descricao;
    $frete->value = floatval($item->valor);
    return $frete;
}

function transformExtratoCashback($item) {
    $cashback = new stdClass();
    $cashback->id = $item->id;
    $cashback->description = $item->descricao;
    $cashback->date = $item->data;
    $cashback->value = $item->valor;
    $cashback->status = $item->status;
    return $cashback;
}
