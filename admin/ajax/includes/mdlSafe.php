<?php

require_once '../utils/safe/vendor/autoload.php';

use Safe2Pay\API\AccountDepositRequest;
use Safe2Pay\API\PaymentRequest;
use Safe2Pay\API\RefundType;
use Safe2Pay\API\TokenizationRequest as TokenizationRequest;
use Safe2Pay\API\TransferenceRequest;
use Safe2Pay\API\MarketplaceRequest;
use Safe2Pay\Models\Payment\BankSlip;
use Safe2Pay\Models\Payment\CreditCard;
use Safe2Pay\Models\Payment\Pix;
use Safe2Pay\Models\Transactions\Splits;
use Safe2Pay\Models\Transactions\Transaction;
use Safe2Pay\Models\General\Customer;
use Safe2Pay\Models\General\Product;
use Safe2Pay\Models\General\Address;
use Safe2Pay\Models\Merchant\Merchant;
use Safe2Pay\Models\Merchant\MerchantSplit;
use Safe2Pay\Models\Merchant\MerchantSplitTax;
use Safe2Pay\Models\Transference\Transfer;
use Safe2Pay\Models\Transference\TransferRegister;
use Safe2Pay\Models\Bank\AccountType;
use Safe2Pay\Models\Bank\BankData;
use Safe2Pay\Models\Bank\Bank;
use Safe2Pay\Models\Core\Config as Enviroment;

function SaveSafe($conn) {
    $loja = getLoja($conn);        
    $pessoa = getPerfil($conn, $_SESSION['id']);
    
    $enviroment = new Enviroment();
    $enviroment->setAPIKEY(APIKEY);
    $Merchant = new Merchant();
    if (is_numeric($loja->safe_id)) {
        $Merchant->setId($loja->safe_id);
    }    
    $Merchant->setIdentity(valoresTexto($loja->cnpj));
    $Merchant->setName($loja->razao_social);
    $Merchant->setCommercialName($loja->nome_fantasia);
    $Merchant->setIsPanelRestricted(false);    
    //Dados do responsável 
    $Merchant->setResponsibleIdentity(valoresTexto($pessoa->cpf));
    $Merchant->setResponsibleName($pessoa->nome);
    $Merchant->setEmail($pessoa->email);
    //Dados do responsável técnico
    $Merchant->setTechIdentity("11595130748");
    $Merchant->setTechName("Cleiton Cirino Furtado");
    $Merchant->setTechEmail("gymbrother" . $loja->id . "@gmail.com");
    //Endereço
    $Merchant->Address = new Address();
    $Merchant->Address->setZipCode(valoresTexto($loja->endereco->cep));
    $Merchant->Address->setStreet($loja->endereco->endereco);
    $Merchant->Address->setDistrict($loja->endereco->bairro);        
    $Merchant->Address->setComplement($loja->endereco->complemento);        
    $Merchant->Address->setNumber($loja->endereco->numero);
    $Merchant->Address->setStateInitials($loja->endereco->id_cidade->cidade_codigoEstado->estado_sigla);
    $Merchant->Address->setCityName($loja->endereco->id_cidade->cidade_nome);
    $Merchant->Address->setCountryName("Brasil");
    //Conta Bancária
    $bankData = new BankData();
    $bankData->setBank(new Bank($loja->banco->banco));
    $bankData->setBankAgency($loja->banco->agencia);
    $bankData->setBankAgencyDigit($loja->banco->agencia_dv);    
    $bankData->setBankAccount($loja->banco->conta);
    $bankData->setBankAccountDigit($loja->banco->conta_dv);
    $bankData->setAccountType(new AccountType($loja->banco->tipo));        
    $merchantSplit = array(
        new MerchantSplit(false, "1", array(new MerchantSplitTax("1", "1.00"))), // 1 - Boleto bancário
        new MerchantSplit(false, "2", array(new MerchantSplitTax("1", "1.75"))), // 2 - Cartão de crédito
        new MerchantSplit(false, "3", array(new MerchantSplitTax("1", "1.50"))), // 3 - Criptomoeda
        new MerchantSplit(false, "4", array(new MerchantSplitTax("1", "1.50"))), // 4 - Cartão de débito 
    );
    $Merchant->setBankData($bankData);
    $Merchant->setMerchantSplit($merchantSplit);    
    if (is_numeric($loja->safe_id)) {
        $response = MarketplaceRequest::Update($Merchant);
    } else {
        $response = MarketplaceRequest::Add($Merchant);
    }
    if (!$response->HasError) {
        $prepareSQL = $conn->prepare("UPDATE sf_empresa set safe_id = :txtsafe_id, 
        safe_token = :txtsafe_token, safe_secretkey = :txtsafe_secretkey, safe_tokensandbox = :txtsafe_tokensandbox, 
        safe_secretkeysandbox = :txtsafe_secretkeysandbox WHERE id = :id");
        $prepareSQL->bindValue(':txtsafe_id', $response->ResponseDetail["Id"]);
        $prepareSQL->bindValue(':txtsafe_token', $response->ResponseDetail["Token"]);
        $prepareSQL->bindValue(':txtsafe_secretkey', $response->ResponseDetail["SecretKey"]);
        $prepareSQL->bindValue(':txtsafe_tokensandbox', $response->ResponseDetail["TokenSandbox"]);
        $prepareSQL->bindValue(':txtsafe_secretkeysandbox', $response->ResponseDetail["SecretKeySandbox"]);
        $prepareSQL->bindValue(':id', $loja->id);
        $prepareSQL->execute();
    } else {
        echo $response->Error; exit;
    }
}

function getSaldoSafe($loja) {
    if (!$loja->safe_token) {
        echo "Verifique o cadastro da sua loja, é necessário completar o cadastro"; exit;
    }    
    $enviroment = new Enviroment();
    $enviroment->setAPIKEY($loja->safe_token);
    $response = AccountDepositRequest::GetBalance("2022-06-01", date('Y-m-d'));
    //print_r($response); exit;
    if (!$response->HasError) {
        return $response->ResponseDetail;
    } else {
        echo $response->Error; exit;
    }
}

function processarVendaSafe($conn, $id) {
    $id_empresa = null;
    $id_loja = "";
    $token_loja = "";
    $razao_loja = "";
    $cnpj_loja = "";
    $total_loja = 0;
    $total_gb = 0;
    $total = 0;
    $venda = getVenda($conn, $id);
    //print_r($venda); exit;
    foreach ($venda->parcelas as $item) {
        if (!in_array($item->id_empresa, array(1, 2))) {
            $id_empresa = $item->id_empresa;
            $id_loja = $item->safe_id;
            $token_loja = $item->safe_token;
            $razao_loja = $item->razao_social;
            $cnpj_loja = $item->cnpj;
            $total_loja += ($item->valor > 0 ? $item->valor : ($item->valor * -1));
        } else {
            $total_gb += ($item->valor > 0 ? $item->valor : ($item->valor * -1));
        }
        $total += ($item->valor > 0 ? $item->valor : ($item->valor * -1));
    }
    //--------------------------------------------------------------------------    
    if (in_array($venda->tipo_documento, array("C", "B", "P"))) {    
        $enviroment = new Enviroment();
        $enviroment->setAPIKEY(APIKEY);        
        
        $payload = new Transaction();
        $payload->setIsSandbox(!PRODUCAO);
        $payload->setApplication(APIRAZAO);
        $payload->setVendor($venda->itens[0]->nome_fantasia);
        $payload->setCallbackUrl(CALLBACKURL);        
        
        $Customer = new Customer();
        $Customer->setName($venda->id_pessoa->nome);
        $Customer->setIdentity(valoresTexto($venda->id_pessoa->cpf));
        $Customer->setEmail($venda->id_pessoa->email);
        $Customer->setPhone(valoresTexto($venda->id_pessoa->celular));
        $Customer->Address = new Address();
        $Customer->Address->setZipCode(valoresTexto($venda->id_endereco->cep));
        $Customer->Address->setStreet($venda->id_endereco->endereco);
        $Customer->Address->setNumber($venda->id_endereco->numero);
        $Customer->Address->setComplement($venda->id_endereco->complemento);
        $Customer->Address->setDistrict($venda->id_endereco->bairro);
        $Customer->Address->setStateInitials($venda->id_endereco->id_cidade->cidade_codigoEstado->estado_sigla);
        $Customer->Address->setCityName($venda->id_endereco->id_cidade->cidade_nome);
        $Customer->Address->setCountryName("Brasil");
        $payload->setCustomer($Customer);               
        $payload->setPaymentMethod(getPaymentType($venda->tipo_documento));          
        if ($venda->tipo_documento == "C") {
            $CreditCard = new CreditCard($venda->id_cartao->nome_cartao, 
            encrypt($venda->id_cartao->token, "gymBrother7034", false), 
            substr(escreverData($venda->id_cartao->data), -7), $venda->id_cartao->cvv, null, $venda->n_parcela);
            $payload->setPaymentObject($CreditCard);
        } else if ($venda->tipo_documento == "B") {
            $BankSlip = new BankSlip();
            $BankSlip->setDueDate(date("d/m/Y", strtotime("+ 2 day")));
            $BankSlip->setInstruction("Pagar até o vencimento");
            $BankSlip->setCancelAfterDue(true);
            $BankSlip->setIsEnablePartialPayment(false);
            $payload->setPaymentObject($BankSlip);            
        } else if ($venda->tipo_documento == "P") {
            $payload->setReference("Pagamento em Pix");
            $pix = new Pix();
            $pix->setExpiration(86400);
            $payload->setPaymentObject($pix);            
        }
        $Products = array();
        foreach ($venda->itens as $item) {
            $payloadProduct = new Product();
            $payloadProduct->setCode($item->id_anuncio);
            $payloadProduct->setDescription($item->nome);
            $payloadProduct->setUnitPrice($item->valor);
            $payloadProduct->setQuantity($item->quantidade);
            array_push($Products, $payloadProduct);
        }
        $payload->setProducts($Products);
        $payload->setSplits(makeSplit($id_loja, $total_loja, $razao_loja, $total_gb));
        //print_r(json_encode($payload)); exit;
        $response = PaymentRequest::CreatePayment($payload);
        //echo(json_encode($response)); exit;                     
        if (!$response->HasError) {
            $prepareSQL = $conn->prepare("UPDATE sf_vendas SET charge_id = :charge_id WHERE id = :id");            
            $prepareSQL->bindValue(':charge_id', $response->ResponseDetail["IdTransaction"]);
            $prepareSQL->bindValue(':id', $id);
            $prepareSQL->execute();
        } else {
            echo $response->Error; exit;  
        }
        if ($venda->tipo_documento == "C") { //Pagamento em Cartão de Crédito
            $prepareSQL = $conn->prepare("UPDATE sf_vendas SET status = :status, data_pagamento = now() WHERE id = :id");
            $prepareSQL->bindValue(':status', $response->ResponseDetail["Status"] == "3" ? "P" : "N");
            $prepareSQL->bindValue(':id', $id);
            $prepareSQL->execute();
            return $id;
        } else if ($venda->tipo_documento == "B") { //Pagamento em Boleto
            $prepareSQL = $conn->prepare("UPDATE sf_vendas SET charge_link = :charge_link, charge_linha = :charge_linha WHERE id = :id");
            $prepareSQL->bindValue(':charge_linha', $response->ResponseDetail["DigitableLine"]);
            $prepareSQL->bindValue(':charge_link', $response->ResponseDetail["BankSlipUrl"]);
            $prepareSQL->bindValue(':id', $id);
            $prepareSQL->execute();
            return $id;
        } else if ($venda->tipo_documento == "P") { //Pagamento em PIX
            $prepareSQL = $conn->prepare("UPDATE sf_vendas SET charge_pix = :charge_pix, charge_linha = :charge_linha WHERE id = :id");
            $prepareSQL->bindValue(':charge_pix', $response->ResponseDetail["QrCode"]);
            $prepareSQL->bindValue(':charge_linha', $response->ResponseDetail["Key"]);
            $prepareSQL->bindValue(':id', $id);
            $prepareSQL->execute();
            return $id;
        } 
    } else if (in_array($venda->tipo_documento, array("T", "S", "X"))) {
        if ($venda->tipo_documento == "T") { //Pagamento em Cashback
            $banco = getLojaBanco($conn, $id_empresa);
            $response = setTransferenciaSafe($banco, $total_loja, $razao_loja, $cnpj_loja, APIKEY);            
        } else if ($venda->tipo_documento == "S") { //Saque Loja
            $banco = getLojaBanco($conn, $id_empresa);
            $response = setTransferenciaSafe($banco, $total_loja, $razao_loja, $cnpj_loja, $token_loja);            
        } else if ($venda->tipo_documento == "X") { //Saque CashBack      
            $response = setTransferenciaSafe($venda->bancos, $total_loja, $venda->bancos->titular_nome, $venda->bancos->titular_cpf, APIKEY);            
        }        
        $resultStatus = (!$response->HasError ? "REQUESTED" : null);
        $prepareSQL = $conn->prepare("UPDATE sf_vendas SET status = :status, data_pagamento = now(), charge_id = :charge_id WHERE id = :id");
        $prepareSQL->bindValue(':status', ($resultStatus == "REQUESTED" ? "P" : "N"));
        $prepareSQL->bindValue(':charge_id', ($resultStatus == "REQUESTED" ? $response->ResponseDetail[""] : null));
        $prepareSQL->bindValue(':id', $id);
        $prepareSQL->execute();        
        if (!$response->HasError) {
            return $id;            
        } else {
            echo $response->Error; exit;  
        }
    }          
}

function setTransferenciaSafe($banco, $total_loja, $nome_loja, $doc_loja, $chave) {
    $enviroment = new Enviroment();
    $enviroment->setAPIKEY($chave);    
        $bankData = new BankData();    
        $bankData->setBank(new Bank($banco->banco));
        $bankData->setAccountType(new AccountType($banco->tipo));
        $bankData->setBankAgency($banco->agencia);
        $bankData->setBankAgencyDigit($banco->agencia_dv);    
        $bankData->setBankAccount($banco->conta);
        $bankData->setBankAccountDigit($banco->conta_dv);
    $transferRegisters = array(new TransferRegister($nome_loja, strval(round($total_loja, 2)), valoresTexto($doc_loja), "Saque", date("Y-m-d", strtotime("+1 day")), CALLBACKURL, $bankData));
    $payload = new Transfer(false, $transferRegisters);
    $response = TransferenceRequest::CreateTransference($payload);
    return $response;
}

function setEstornarSafe($conn, $id, $tot_sol) {
    $venda = getVenda($conn, $id);    
    $enviroment = new Enviroment();
    $enviroment->setAPIKEY(APIKEY);
    if ($venda->tipo_documento == "C") { 
        $type = RefundType::CREDIT;    
    }
    $response = PaymentRequest::Refund($venda->charge_id, $type);
    if (!$response->HasError) {
        $prepareSQL = $conn->prepare("UPDATE sf_vendas SET status = 'E', data_estorno = now(), refund_code = :refund_code WHERE id = :id");
        $prepareSQL->bindValue(':refund_code', $venda->charge_id);            
        $prepareSQL->bindValue(':id', $id);
        $prepareSQL->execute();
        sendMsgVenda($conn, $id);        
        return true;
    } else {
        echo $response->Error; exit;
    }
}

function setCartaoSafe($Holder, $CardNumber, $ExpirationDate, $SecurityCode) {
    $enviroment = new Enviroment();
    $enviroment->setAPIKEY(APIKEY);    
    $CreditCard = new CreditCard($Holder, $CardNumber, substr($ExpirationDate, -7), $SecurityCode, null, false);
    $response = TokenizationRequest::Create($CreditCard);    
    if (!$response->HasError) {
        return $response->ResponseDetail["Token"];
    } else {
        echo $response->Error; exit;
    }     
}

function makeSplit($id_loja, $total_loja, $razao_loja, $total_gb) {
    $split1 = new Splits();
        $split1->setIdReceiver($id_loja);
        $split1->setName($razao_loja);
        $split1->setCodeReceiverType('2');  //1 para 'Empresa' ou 2 'Subconta
        $split1->setCodeTaxType('2'); //1 para Percentual ou 2 para Valor
        $split1->setAmount(strval(round($total_loja, 2)));
        $split1->setIsPayTax(false);    
    $split2 = new Splits();
        $split2->setIdentity(APICNPJ);
        $split2->setName(APIRAZAO);
        $split2->setCodeReceiverType('1');
        $split2->setCodeTaxType('2');
        $split2->setAmount(strval(round($total_gb, 2)));
        $split2->setIsPayTax(true);    
    return array($split1, $split2);
}

function getPaymentType($type) {
    if ($type == "B") {
        return 1;
    } else if ($type == "C") {
        return 2;
    } else if ($type == "P") {
        return 6;
    } else {
        return null;
    }
}