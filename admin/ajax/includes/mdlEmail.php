<?php

require(__DIR__ . "./../../utils/phpmailer/class.phpmailer.php");

function sendMsgVenda($conn, $id) {
    $link = "";
    $venda = getVenda($conn, $id);
    $num_venda = "Pedido #" . str_pad($venda->id, 7, "0", STR_PAD_LEFT);
    $mensagem = "Olá <b>" . $venda->id_pessoa->nome . "</b>,<br>
    Recebemos seu <b>" . $num_venda . "</b>, efetuado em <b>" . escreverDataHora($venda->data) . "</b>,<br><br>
    Com o(s) seguinte(s) Item(ns):<br><br>".
    "<table style='border: 1px solid #6A6A6A; color:#6A6A6A; font-size: 11px; width:100%;'><tbody><tr style='background-color: darkgray; color:white;'>";
    $mensagem .= "<th colspan='2'>Produto</th>";
    $mensagem .= "<th>Quant.</th>";
    $mensagem .= "<th>Valor</th>";
    $mensagem .= "<th>Loja</th></tr>";
    foreach ($venda->itens as $item) {
        $mensagem .= "<tr><td><img height='45' src='" . $item->image . "' alt=''></td>";    
        $mensagem .= "<td>" . $item->nome . "<br>" . $item->descricao . "</td>";
        $mensagem .= "<td>" . $item->quantidade . "</td>";
        $mensagem .= "<td>" . escreverNumero($item->valor, 1) . "</td>";
        $mensagem .= "<td>" . $item->nome_fantasia . "</td></tr>";
    }
    $mensagem .= "</tbody></table><br>
    Caso tenha alguma dúvida, você poderá entrar em contato com a loja<br> pelo telefone ou e-mail informando acima.<br><br>";
    $mensagem .= "Forma de pagamento escolhida foi <b>" . $venda->descricao . ".</b><br><br>";
    if ($venda->status == "A" && in_array($venda->tipo_documento, array("B", "P"))) {
        $mensagem .= "Para efetuar o <b>pagamento</b> acesse o link abaixo:";
        $link = $venda->charge_link;
    } else if ($venda->status == "A") {
        $mensagem .= "Porém o pagamento ainda encontra-se <b>Aguardando</b>,<br> acompanhe o processamento pelo aplicativo.";
    } else if ($venda->status == "C") {
        $mensagem .= "Porém o pagamento foi <b>Cancelado</b>,<br> devido à falta de pagamento até o vencimento,<br> é necessário realizar um novo pedido.";
    } else if ($venda->status == "N") {
        $mensagem .= "Porém o pagamento foi <b>Negado</b>, é necessário realizar um novo pedido.";        
    } else if ($venda->status == "E") {        
        $mensagem .= "Porém o pagamento foi <b>Estornado</b>. Cancelando esta venda!";
    } else if ($venda->status == "P" && $venda->entrega == "P") {
        $mensagem .= "O pagamento foi realizado com <b>Sucesso</b>,<br> e a entrega está <b>pendente</b> de acordo com a loja.";
    } else if ($venda->status == "P" && $venda->entrega == "E") {
        $mensagem .= "O pagamento foi realizado com <b>Sucesso</b>,<br> e a entrega está <b>finalizada</b> de acordo com a loja.";
    }
    return sendMsg($venda->id_pessoa->email, $venda->id_pessoa->nome, $num_venda, $mensagem, "", "", $link);
}

function sendResposta($conn, $id) {
    $dados = getPergunta($conn, $id);
    $num_venda = "Sua pergunta foi respondida.";
    $mensagem = "Olá <b>" . $dados->nome_pessoa . "</b>,<br>
    Sua pergunta foi respondida. Sobre o seguinte item:<br><br>
    <table style='border: 1px solid #6A6A6A; color:#6A6A6A; font-size: 11px; width:100%;'>
    <tbody><tr><td><img height='45' src='" . $dados->image . "' alt=''></td>
    <td>" . $dados->nome . "<br>" . $dados->descricao . "</td></tr></tbody></table><br>
    <b>" . $dados->pergunta . "</b><br>";
    $mensagem .= "R: " . $dados->resposta;
    return sendMsg($dados->email, $dados->nome_pessoa, $num_venda, $mensagem, "", "", "");
}

function sendMsg($mailDestino, $nomeDestino, $assunto, $mensagem, $responsavel, $empresa, $link) {
    $mail = new PHPMailer();
    $mail->Charset = 'utf-8';
    $mail->IsSMTP();
    $mail->Host = "smtplw.com.br";
    $mail->SMTPAuth = true;
    $mail->Username = 'sivis';
    $mail->Password = 'Sivis33485953';
    $mail->Port = '587';
    $mail->From = "financeiro@amdbrasil.com.br";
    $mail->FromName = "GYM Brother";
    $mail->IsHTML(true);
    $mail->AddAddress($mailDestino, $nomeDestino);
    $mail->Subject = utf8_decode($assunto);
    $mail->Body = getMessage($assunto, $mensagem, $responsavel, $empresa, $link);
    $mail->AltBody = "";
    return $mail->Send();
}

function getMessage($assunto, $mensagem, $responsavel, $empresa, $link) {
    $content = '<body bgcolor="#f7f7f7" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="background: #f7f7f7;">
        <table width="550" border="0" align="center" cellpadding="0" cellspacing="0" style="background:#f7f7f7;"><tbody>
                <tr><td width="550" height="10" align="center" valign="middle"><h3 style="font-family:Segoe, &#39;Segoe UI&#39;, &#39;DejaVu Sans&#39;, &#39;Trebuchet MS&#39;, Verdana, sans-serif; font-size:10px; text-transform:uppercase; color:#727272;">&nbsp;</h3></td></tr></tbody>
        </table>
        <table width="550" border="0" align="center" cellpadding="0" cellspacing="0" style="background:#FFF;">
            <tbody>
                <tr><td width="500" height="120" align="center" valign="top" bgcolor="#43525a"><br><br><img src="http://gymbrother.com.br/admin/assets/img/logo.png"  alt=""></td></tr>
                <tr><td align="center">&nbsp;</td></tr>';    
                if (strlen($assunto) > 0) {
                    $content .= '<tr><td align="center"><h4 style="font-family:Verdana; font-size: 20px; color:#6A6A6A"><br>' . $assunto . '<br></h4></td></tr>';        
                } if (strlen($responsavel) > 0 && strlen($empresa) > 0) {
                    $content .= '<tr>
                        <td><div class="margin" style="margin: 0px 20px;"><span style="font-family:Verdana; font-size: 14px; color:#6A6A6A"><strong>' . $responsavel . ' (' . $empresa . ')<br><br></strong></span></div></td>
                    </tr>';
                } if (strlen($mensagem) > 0) {
                    $content .= '<tr>
                        <td><div class="margin" style="margin: 0px 20px;"><span style="font-family:Verdana; font-size: 12px; color:#6A6A6A">' . $mensagem . '<br><br></span></div></td>
                    </tr>';
                } if (strlen($link) > 0) {
                $content .= '<tr>
                        <td align="center"><a href="'. $link . '"><div class="margin" style="margin: 10px 20px;"><span style="font-family:Verdana; font-size: 14px; color:#6A6A6A"><strong> Clique para visualizar </strong></span></div></a></td>
                    </tr>';
                }
                $content .= '<tr><td>&nbsp;</td></tr>
                <tr style="background-color: darkgray;"><td align="center"><span style="font-family:Verdana; font-size: 11px; color:#FFF">Favor não responder este e-mail.<br><br></span></td></tr>
            </tbody>
        </table>
        <table width="550" border="0" align="center" cellpadding="0" cellspacing="0" style="background:#f7f7f7;">
            <tbody>
                <tr>
                    <td width="550" height="10" align="center" valign="middle"><h3 style="font-family:Segoe, &#39;Segoe UI&#39;, &#39;DejaVu Sans&#39;, &#39;Trebuchet MS&#39;, Verdana, sans-serif; font-size:10px; text-transform:uppercase; color:#727272;">&nbsp;</h3></td>
                </tr>
            </tbody>
        </table>
    </body>';
    return utf8_decode($content);
}