<?php

include '../Connections/configini.php';
include './includes/mdlVenda.php';

$result = "";
$dados = filter_input_array(INPUT_POST, FILTER_DEFAULT);
if (!$dados) {
    $dados = file_get_contents('php://input');
    if ($dados) {
        $dados = json_decode($dados, 1);
    }
}
if (!$dados) {
    $dados = filter_input_array(INPUT_GET, FILTER_DEFAULT);
}

$result .= "IdTransaction: " . $dados["IdTransaction"] . "\n";
$result .= "TransactionStatus: " . $dados["TransactionStatus"]["Name"] . "\n";
$result .= "PaymentMethod: " . $dados["PaymentMethod"]["Name"] . "\n";
$result .= "Application: " . $dados["Application"] . "\n";
$result .= "Vendor: " . $dados["Vendor"] . "\n";
$result .= "Amount: " . $dados["Amount"] . "\n";
$result .= "IncluedDate: " . $dados["IncluedDate"] . "\n";
$result .= "InstallmentQuantity: " . $dados["InstallmentQuantity"] . "\n";
$result .= "SecretKey: " . $dados["SecretKey"] . "\n";
$result .= "Reference: " . $dados["Reference"] . "\n";

if (is_numeric($dados["IdTransaction"]) && (in_array($dados["TransactionStatus"]["Name"], array("Liberado", "Autorizado")))) {    
    $prepareSQL = $conn->prepare("UPDATE sf_vendas SET status = :status, data_pagamento = now() WHERE charge_id = :charge_id");
    $prepareSQL->bindValue(':status', ((in_array($dados["TransactionStatus"]["Name"], array("Liberado", "Autorizado"))) ? "P" : "N"));
    $prepareSQL->bindValue(':charge_id', $dados["IdTransaction"]);
    $prepareSQL->execute();
    
}

$url = __DIR__ . "/webhooks/" . date("Y/m/d") . "/";
if (!file_exists($url)) {
    mkdir($url, 0777, true);
}
$handle = fopen($url . date("d_m_Y__H_i_s") . ".txt", "x+");
fwrite($handle, $result);
fclose($handle);