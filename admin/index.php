<?php include './Connections/configini.php'; ?>
<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title><?php echo $name_page; ?></title>
        <link rel="icon" href="./assets/img/favicon.ico">
        <link href="./assets/css/bootstrap.css" rel="stylesheet">
        <link href="./assets/css/datatables.css" rel="stylesheet">        
        <link href="./assets/css/main.css" rel="stylesheet">        
    </head>
    <body>
        <div class="wraper">
            <aside><?php include './partials/menu-lateral.php'; ?></aside>
            <main>
                <header><?php include './partials/topo.php'; ?></header>
                <section>
                    <div class="container-fluid">
                        <div class="p-3 pt-4">
                            <h1 class="h5"><span class="align-baseline" data-feather="<?php echo $icon_page; ?>"></span> <?php echo $name_page; ?></h1>
                        </div>                    
                        <div class="row">
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="col-md-6 mb-3">
                                        <div class="card h-100">
                                            <div class="card-body">                     
                                                <h5 class="text-muted fw-normal mt-0" title="Total em Vendas">Total em Vendas</h5>
                                                <h3 id="txtTotalVendas" class="text-info mt-3 mb-3">R$ 0,00</h3>
                                                <p class="mb-0 text-muted">
                                                    <span id="txtTotalVendasNum" class="text-success me-2">0</span>
                                                    <span class="text-nowrap">Número de vendas</span>  
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 mb-3">
                                        <div class="card h-100">
                                            <div class="card-body">
                                                <h5 class="text-muted fw-normal mt-0 d-flex justify-content-between">Anúncios Ativos<button id="btnAnuncio" title="Anúncios Ativos" type="button" class="btn btn-success btn-sm mb-1"><span data-feather="plus"></span></button></h5>
                                                <p class="mb-0 h6"><span id="txtAnuncioAtivo0" class="text-success">0</span> Serviços</p>
                                                <p class="mb-0 h6"><span id="txtAnuncioAtivo1" class="text-success">0</span> Produtos</p>
                                                <p class="h6"><span id="txtAnuncioAtivo2" class="text-success">0</span> Aluguel de Espaços</p>                                          
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 mb-3">
                                        <div class="card h-100">
                                            <div class="card-body">
                                                <h5 class="text-muted fw-normal mt-0 d-flex justify-content-between">Saldo Atual</h5>
                                                <h3 id="txtRecebidas" class="mt-3 mb-3">R$ 0,00</h3>
                                                <p class="mb-0 text-muted">
                                                    <span id="txtRecebidasSaque" class="text-success me-2">R$ 0,00</span>
                                                    <span class="text-nowrap">Valor Recebido</span>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 mb-3">
                                        <div class="card h-100">
                                            <div class="card-body">
                                                <h5 class="text-muted fw-normal mt-0" title="A receber">A receber de Vendas</h5>
                                                <h3 id="txtReceber" class="text-warning mt-3 mb-3">R$ 0,00</h3>
                                                <p class="mb-0 text-muted">
                                                    <span id="txtReceberTotal" class="text-success me-2">0</span>
                                                    <span class="text-nowrap">transações</span>
                                                    <span class="text-nowrap">Por todo período</span>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <div class="card h-100">
                                    <div class="card-body h-100">
                                        <h5 class="text-muted fw-normal mt-0" title="A receber">Demonstrativo de Vendas</h5>
                                        <div id="chartdiv" class="pb-4 h-100"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-body">
                                        <h5 class="text-muted fw-normal mt-0" title="Extrato de Transações">Extrato de Transações</h5>                                        
                                        <table id="tbExtrato" class="table table-striped table-bordered dt-responsive w-100 mt-3">
                                            <thead class="thead-light">
                                                <tr>
                                                    <th style="width: 10%;">Id</th>
                                                    <th style="width: 15%;">Data</th>
                                                    <th style="width: 35%;">Cliente</th>
                                                    <th style="width: 5%;">Status</th>
                                                    <th style="width: 5%;">Tipo</th>
                                                    <th style="width: 10%;">Entradas</th>
                                                    <th style="width: 10%;">Saídas</th>                                                    
                                                    <th style="width: 10%;">Valor Liq.</th>
                                                </tr>
                                            </thead>
                                            <tbody></tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php include './includes/modal/mdTransferencia.php'; ?>
                    </div>
                </section>
            </main>
        </div>
        <?php include './partials/lib-js.php'; ?>
        <script src="assets/js/lib/amcharts/core.js" type="text/javascript"></script>
        <script src="assets/js/lib/amcharts/charts.js" type="text/javascript"></script>
        <script src="assets/js/lib/amcharts/themes/animated.js" type="text/javascript"></script>
        <script src="assets/js/paginaInicial.js" type="text/javascript"></script>
    </body>
</html>