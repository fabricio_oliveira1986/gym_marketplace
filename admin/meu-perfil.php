<?php include './Connections/configini.php'; ?>
<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title><?php echo $name_page; ?></title>
        <link rel="icon" href="./assets/img/favicon.ico">
        <link href="./assets/css/bootstrap.css" rel="stylesheet">
        <link href="./assets/css/datatables.css" rel="stylesheet">        
        <link href="./assets/css/main.css" rel="stylesheet">
        <link href="./assets/css/cropper.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div class="wraper">
            <aside><?php include './partials/menu-lateral.php'; ?></aside>
            <main>
                <header><?php include './partials/topo.php'; ?></header>
                <section>
                    <div class="container-fluid">
                        <div class="p-3 pt-4">
                            <h1 class="h5"><span class="align-baseline" data-feather="<?php echo $icon_page; ?>"></span> <?php echo $name_page; ?></h1>
                        </div>                    
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card bg-primary mb-3">
                                    <div class="card-body profile-user-box">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="row align-items-center">
                                                    <div class="col-auto">
                                                        <div class="avatar-lg">
                                                            <button id="imageDirAdd" class="btn btn-sm rounded-circle position-absolute btn-success hide" data-toggle="modal" data-target="#FormCropper" data-type="perfil"><span data-feather="edit"></span></button>
                                                            <button id="imageDirDel" class="btn btn-sm rounded-circle position-absolute btn-danger hide" data-type="perfil"><span data-feather="x"></span></button>
                                                            <img id="imageDirView" src="assets/img/no_person.png" alt="" class="rounded-circle img-thumbnail">                                                            
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <h4 class="mt-1 mb-1 text-white"></h4>
                                                        <p class="font-13 text-white-50"></p>
                                                        <p class="font-13 text-white-50"></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="card h-100">
                                    <div class="card-body">
                                        <form id="formCadastro" novalidate>
                                            <input id="imageDir" name="imageDir" type="hidden" value=""/>
                                            <input id="imageNow" name="imageNow" type="hidden" value=""/>
                                            <?php include './includes/formPessoa.php'; ?>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="card h-100">
                                    <div class="card-body">
                                        <ul class="nav nav-pills nav-fill mb-3" id="v-pills-tab" role="tablist">
                                            <li class="nav-item">
                                                <a class="nav-link active" id="endereco-tab" data-toggle="tab" href="#endereco" role="tab" aria-controls="endereco" aria-selected="true">Meus Endereços</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" id="cartao-tab" data-toggle="tab" href="#cartao" role="tab" aria-controls="cartao" aria-selected="false">Meus Cartões</a>
                                            </li>
                                        </ul>                                        
                                        <div class="tab-content">
                                            <div class="tab-pane fade show active" id="endereco" role="tabpanel" aria-labelledby="endereco-tab">
                                                <button class="btn btn-success btn-sm" data-toggle="modal" data-target="#FormEndereco"><span data-feather="plus"></span><span class="d-none d-sm-inline"> Criar Cadastro</span></button>                                            
                                                <table id="tbEndereco" class="table table-striped table-bordered dt-responsive w-100 mt-3">
                                                    <thead class="thead-light">
                                                        <tr>                                                                                                        
                                                            <th style="width: 80%;">Endereço</th>
                                                            <th style="width: 20%;">Ação</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody></tbody>
                                                </table>          
                                            </div>
                                            <div class="tab-pane fade show" id="cartao" role="tabpanel" aria-labelledby="cartao-tab">                                               
                                                <table id="tbCartao" class="table table-striped table-bordered dt-responsive w-100 mt-3">
                                                    <thead class="thead-light">
                                                        <tr>                                                                                                        
                                                            <th style="width: 80%;">Cartão</th>
                                                            <th style="width: 20%;">Ação</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody></tbody>
                                                </table>
                                            </div>                                                                    
                                        </div>
                                    </div>
                                </div>                                                
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card mt-3">
                                    <div class="card-body text-right">
                                        <button type="button" id="btnAlterar" class="btn btn-success">Alterar Perfil</button>                                        
                                        <button type="button" id="btnPerfil" class="btn btn-success hide">Salvar</button>
                                        <button type="button" id="btnCancelar" class="btn btn-secondary hide">Cancelar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php include './includes/modal/mdPessoaEndereco.php'; ?>
                        <?php include './includes/modal/mdCropper.php'; ?>                        
                    </div>
                </section>
            </main>
        </div>
        <?php include './partials/lib-js.php'; ?>
        <script src="assets/js/lib/cropper.js" type="text/javascript"></script>
        <script src="assets/js/formPessoa.js" type="text/javascript"></script>
        <script src="assets/js/formPessoaEndereco.js" type="text/javascript"></script>        
        <script src="assets/js/meuPerfil.js" type="text/javascript"></script>
    </body>
</html>