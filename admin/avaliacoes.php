<?php include './Connections/configini.php'; ?>
<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title><?php echo $name_page; ?></title>
        <link rel="icon" href="./assets/img/favicon.ico">
        <link href="./assets/css/bootstrap.css" rel="stylesheet">
        <link href="./assets/css/datatables.css" rel="stylesheet">        
        <link href="./assets/css/main.css" rel="stylesheet">        
    </head>
    <body>
        <div class="wraper">
            <aside><?php include './partials/menu-lateral.php'; ?></aside>
            <main>
                <header><?php include './partials/topo.php'; ?></header>
                <section>
                    <div class="container-fluid">
                        <div class="p-3 pt-4">
                            <h1 class="h5"><span class="align-baseline" data-feather="<?php echo $icon_page; ?>"></span> <?php echo $name_page; ?></h1>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row justify-content-md-end">
                                            <div class="col-auto pl-0">
                                                <label for="txtAvaliacoes">Avaliações</label>
                                                <select id="txtAvaliacoes" class="form-control form-control-sm">
                                                    <option value="null">Todas</option>
                                                    <option value="0">Positivas [5-3]</option>
                                                    <option value="1">Negativas [2-0]</option>
                                                </select>
                                            </div>                                            
                                            <div class="col-auto pl-0">
                                                <label for="txtTipo">Tipo</label>
                                                <select id="txtTipo" class="form-control form-control-sm">
                                                    <option value="null">Todas</option>
                                                    <option value="0">Aguardando</option>
                                                    <option value="1">Respondidas</option>
                                                </select>
                                            </div>                                                                                        
                                            <div class="col-auto pl-0">
                                                <label for="txtPesquisar">Pesquisar</label>
                                                <input id="txtPesquisar" type="text" class="form-control form-control-sm" value="">
                                            </div>                                            
                                            <div class="col-auto align-self-end pl-0">
                                                <button id="btnBuscar" type="button" class="btn btn-success btn-sm" title="Buscar"><span data-feather="search"></span></button>
                                            </div>
                                        </div>                                        
                                        <hr>
                                        <table id="tbAvaliacoes" class="table table-striped table-bordered dt-responsive w-100 mt-3">
                                            <thead class="thead-light">
                                                <tr>
                                                    <th style="width: 13%;">Data</th>                                                    
                                                    <th style="width: 29%;">Anuncio</th>
                                                    <th style="width: 18%;">Cliente</th>
                                                    <th style="width: 8%;">Avaliação</th>                                                    
                                                    <th style="width: 17%;">Comentário</th>
                                                    <th style="width: 9%;">Status</th>                                                    
                                                    <th style="width: 6%;">Ação</th>
                                                </tr>
                                            </thead>
                                            <tbody></tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php include './includes/modal/mdPergunta.php'; ?>   
                    </div>
                </section>
            </main>
        </div>
        <?php include './partials/lib-js.php'; ?>        
        <script src="assets/js/avaliacoes.js" type="text/javascript"></script>
    </body>
</html>