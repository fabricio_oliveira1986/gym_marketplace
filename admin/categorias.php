<?php include './Connections/configini.php'; ?>
<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title><?php echo $name_page; ?></title>
        <link rel="icon" href="./assets/img/favicon.ico">
        <link href="./assets/css/bootstrap.css" rel="stylesheet">
        <link href="./assets/css/datatables.css" rel="stylesheet">        
        <link href="./assets/css/main.css" rel="stylesheet">        
    </head>
    <body>
        <div class="wraper">
            <aside><?php include './partials/menu-lateral.php'; ?></aside>
            <main>
                <header><?php include './partials/topo.php'; ?></header>
                <section>
                    <div class="container-fluid">
                        <div class="p-3 pt-4">
                            <h1 class="h5"><span class="align-baseline" data-feather="<?php echo $icon_page; ?>"></span> <?php echo $name_page; ?></h1>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row justify-content-between">
                                            <div class="pl-2 align-self-end ml-2">
                                                <button class="btn btn-success btn-sm" data-toggle="modal" data-target="#FormCategoria"><span data-feather="plus"></span><span class="d-none d-sm-inline"> Criar Cadastro</span></button>
                                            </div>
                                            <div class="pr-2 mr-2">                                               
                                                <div class="form-row justify-content-sm-end">
                                                    <div class="col-auto align-self-end">
                                                        <input id="text_Filter" name="text_Filter" type="text" class="form-control form-control-sm" placeholder="Buscar">
                                                    </div>                                                                                     
                                                    <div class="col-auto align-self-end">
                                                        <button id="btnBuscar" type="button" title="Buscar" class="btn btn-success btn-sm"><span data-feather="search"></span></button>
                                                    </div>
                                                </div>                                                
                                            </div>
                                        </div>                                        
                                        <hr>
                                        <table id="tbCategorias" class="table table-striped table-bordered dt-responsive w-100 mt-3">
                                            <thead class="thead-light">
                                                <tr>
                                                    <th style="width: 80%;">Nome da Categoria</th>
                                                    <th style="width: 10%;">Inativo</th>
                                                    <th style="width: 10%;">Ação</th>
                                                </tr>
                                            </thead>
                                            <tbody></tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php include './includes/modal/mdCategoria.php'; ?>   
                    </div>
                </section>
            </main>
        </div>
        <?php include './partials/lib-js.php'; ?>        
        <script src="assets/js/categorias.js" type="text/javascript"></script>
    </body>
</html>