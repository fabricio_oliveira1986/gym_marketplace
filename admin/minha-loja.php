<?php include './Connections/configini.php'; ?>
<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title><?php echo $name_page; ?></title>        
        <link rel="icon" href="./assets/img/favicon.ico">
        <link href="./assets/css/bootstrap.css" rel="stylesheet">
        <link href="./assets/css/datatables.css" rel="stylesheet">        
        <link href="./assets/css/main.css" rel="stylesheet">        
        <link href="./assets/css/cropper.css" rel="stylesheet" type="text/css"/>        
    </head>
    <body>
        <div class="wraper">
            <aside><?php include './partials/menu-lateral.php'; ?></aside>
            <main>
                <header><?php include './partials/topo.php'; ?></header>
                <section>
                    <div class="container-fluid">
                        <div class="p-3 pt-4">
                            <h1 class="h5"><span class="align-baseline" data-feather="<?php echo $icon_page; ?>"></span> <?php echo $name_page; ?></h1>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card bg-primary mb-3">
                                    <div class="card-body profile-user-box">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="row align-items-center">
                                                    <div class="col-auto">
                                                        <div class="avatar-lg">
                                                            <button id="imageDirAdd" class="btn btn-sm rounded-circle position-absolute btn-success hide" data-toggle="modal" data-target="#FormCropper" data-type="empresa"><span data-feather="edit"></span></button>
                                                            <button id="imageDirDel" class="btn btn-sm rounded-circle position-absolute btn-danger hide" data-type="empresa"><span data-feather="x"></span></button>
                                                            <img id="imageDirView" src="assets/img/no_image.png" alt="" class="img-thumbnail">
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <h4 class="mt-1 mb-1 text-white"></h4>
                                                        <p class="font-13 text-white-50"></p>
                                                        <p class="font-13 text-white-50"></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row"> 
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-body">                                        
                                        <div class="row"> 
                                            <div class="col-sm-12 col-md-2">
                                                <div class="nav flex-row nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                                                    <a class="nav-link active" id="tab1-tab" data-toggle="pill" href="#tab1" role="tab" aria-controls="tab1">Principal</a>
                                                    <a class="nav-link" id="tab2-tab" data-toggle="pill" href="#tab2" role="tab" aria-controls="tab2">Mídias</a>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-10">
                                                <form id="formEmpresa" novalidate>                                                    
                                                    <div class="tab-content" id="v-pills-tabContent">                                                    
                                                        <div class="tab-pane fade show active" id="tab1" role="tabpanel" aria-labelledby="tab1-tab">
                                                            <input id="txtId" name="txtId" type="hidden" value="">
                                                            <input id="imageDir" name="imageDir" type="hidden" value=""/>
                                                            <input id="imageNow" name="imageNow" type="hidden" value=""/>                                                            
                                                            <div class="form-row">                                                
                                                                <div class="form-group col-md-4">
                                                                    <label for="txtCnpj">CNPJ</label>
                                                                    <input id="txtCnpj" name="txtCnpj" type="text" class="form-control" value="" placeholder="00.000.000/0000-00" pattern="(\d{3}\.?\d{3}\.?\d{3}-?\d{2})|(\d{2}\.?\d{3}\.?\d{3}/?\d{4}-?\d{2})">
                                                                </div>                                                                
                                                                <div class="form-group col-md-8">
                                                                    <label for="txtRazaoSocial">Razão Social</label>
                                                                    <input id="txtRazaoSocial" name="txtRazaoSocial" type="text" class="form-control" value="" placeholder="Razão Social">
                                                                </div>
                                                            </div>
                                                            <div class="form-row">                                                                
                                                                <div class="form-group col-md-12">
                                                                    <label for="txtNomeFantasia">Nome Fantasia</label>
                                                                    <input id="txtNomeFantasia" name="txtNomeFantasia" type="text" class="form-control" value="" placeholder="Nome Fantasia">
                                                                </div>                                                                
                                                            </div>
                                                            <?php include './includes/formPessoaEndereco.php'; ?>
                                                            <input id="txtIdBanco" name="txtIdBanco" type="hidden" value="">
                                                            <div class="form-row">
                                                                <div class="form-group col-md-2">
                                                                    <label for="txtBanco">Banco</label>
                                                                    <input id="txtBanco" name="txtBanco" type="text" class="form-control" value="" placeholder="000">
                                                                </div>                                                
                                                                <div class="form-group col-md-2 col-8">
                                                                    <label for="txtAgencia">Agência</label>
                                                                    <input id="txtAgencia" name="txtAgencia" type="text" class="form-control" value="" placeholder="0000000">
                                                                </div>                                                                                           
                                                                <div class="form-group col-md-1 col-4">
                                                                    <label for="txtAgenciaDv">Dv</label>
                                                                    <input id="txtAgenciaDv" name="txtAgenciaDv" type="text" class="form-control" value="" placeholder="0">
                                                                </div>                                                                                                                                       
                                                                <div class="form-group col-md-2 col-8">
                                                                    <label for="txtConta">Conta</label>
                                                                    <input id="txtConta" name="txtConta" type="text" class="form-control" value="" placeholder="0000000000">
                                                                </div>                                                                                           
                                                                <div class="form-group col-md-1 col-4">
                                                                    <label for="txtContaDv">Dv</label>
                                                                    <input id="txtContaDv" name="txtContaDv" type="text" class="form-control" value="" placeholder="0">
                                                                </div>            
                                                                <div class="form-group col-md-4">
                                                                    <label for="txtTipoConta">Tipo</label>
                                                                    <select id="txtTipoConta" name="txtTipoConta" class="form-control">
                                                                        <option value="CC">Conta Corrente</option>
                                                                        <option value="PP">Conta Poupança</option>
                                                                    </select>
                                                                </div>                                                                                                                
                                                            </div>
                                                            <div class="form-row">
                                                                <label>(Se não tiver dígito, ou dígito for X, preencher com 0)</label>
                                                            </div>
                                                        </div>
                                                        <div class="tab-pane fade" id="tab2" role="tabpanel" aria-labelledby="tab4-tab">
                                                            <button id="btnAdicionarImgs" type="button" class="btn btn-success mb-3 btn-sm" data-toggle="modal" data-target="#FormArquivo" data-type="lFotos" disabled><span data-feather="plus"></span> Adicionar Fotos ou Vídeos</button>
                                                            <div id="dvImagens" class="row"></div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>                            
                                        </div>
                                    </div>
                                </div>                            
                            </div>                            
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card mt-3">
                                    <div class="card-body text-right">
                                        <button type="button" id="btnAlterar" class="btn btn-success">Alterar Loja</button>                                        
                                        <button type="button" id="btnProximo" class="btn btn-success hide">Próximo</button>
                                        <button type="button" id="btnCancelar" class="btn btn-secondary hide">Cancelar</button>
                                    </div>
                                </div>
                            </div>
                        </div>                        
                        <?php include './includes/modal/mdArquivo.php'; ?>
                        <?php include './includes/modal/mdCropper.php'; ?>                        
                    </div>
                </section>
            </main>
        </div>
        <?php include './partials/lib-js.php'; ?>
        <script src="assets/js/lib/cropper.js" type="text/javascript"></script>        
        <script src="assets/js/formPessoaEndereco.js" type="text/javascript"></script>
        <script src="assets/js/arquivos.js" type="text/javascript"></script>        
        <script src="assets/js/minhaLoja.js" type="text/javascript"></script>
    </body>
</html>