<?php

require_once __DIR__ . '/vendor/autoload.php';
require_once __DIR__ . '/configJuno.php';

try {
    $auth = new \ODJuno\ODJunoAuth($config);
    $authorization = $auth->authService()->authenticate();
    $juno = new \ODJuno\ODJuno($authorization->getAccessToken(), $config['privateToken'], $config['sandbox'], false);    
    $filename = "./img/foto2.jpg";
    $handle = fopen ($filename, "r");        
    $result = $juno->DigitalAccounts()->setDocument("doc_A1ECB8ED5323817E", $handle);
    echo json_encode($result);
} catch (\GuzzleHttp\Exception\RequestException $e) {
    if ($e->hasResponse()) {
        $response = $e->getResponse();
        var_dump($response->getStatusCode());
        var_dump($response->getReasonPhrase());
        var_dump(json_decode((string) $response->getBody()));
    }
}
