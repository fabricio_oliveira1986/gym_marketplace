<?php

require_once __DIR__ . '/vendor/autoload.php';
require_once __DIR__ . '/configJuno.php';

try {
    $auth = new \ODJuno\ODJunoAuth($config);
    $authorization = $auth->authService()->authenticate();
    $juno = new \ODJuno\ODJuno($authorization->getAccessToken(), $config['privateToken'], $config['sandbox'], false);
    $result = $juno->DigitalAccounts()->listBanks();
    echo json_encode($result);
} catch (\GuzzleHttp\Exception\RequestException $e) {
    if ($e->hasResponse()) {
        $response = $e->getResponse();
        var_dump($response->getStatusCode());
        var_dump($response->getReasonPhrase());
        var_dump(json_decode((string) $response->getBody()));
    }
}