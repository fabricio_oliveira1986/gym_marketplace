<?php 

require_once __DIR__ . '/vendor/autoload.php';
require_once __DIR__ . '/configJuno.php';

try {
    $auth = new \ODJuno\ODJunoAuth($config);
    $authorization = $auth->authService()->authenticate();
    $juno = new \ODJuno\ODJuno($authorization->getAccessToken(), $config['privateToken'], $config['sandbox'], false);
    //--------------------------------------------------------------------------
    $address = new \ODJuno\Entities\Address();
    $address->setStreet("Rua 42")
        ->setNumber("54")
        ->setNeighborhood("Vila Santa Cecília")
        ->setCity("Volta Redonda")
        ->setState("RJ")
        ->setComplement("")            
        ->setPostCode("27351140");
    //--------------------------------------------------------------------------
    $charge = new \ODJuno\Entities\Charge();
    $charge->getChargeDetail()
        ->setDescription("Venda Direta")
        ->setPaymentTypes([\ODJuno\Entities\ChargeDetail::PAYMENT_TYPE_CREDIT_CARD])
        ->setAmount(49.99);
    $charge->getBilling()
        ->setName("Fabricio Oliveira")
        ->setDocument("56202888032")
        ->setEmail("fabricio2237@gmail.com")
        ->setAddress($address);
    $result = $juno->charges()->create($charge);
    //--------------------------------------------------------------------------
    $payments = new \ODJuno\Entities\Payment();
    $payments->setChargeId("chr_9D269451DFD287797676B99799648099"); //$result->_embedded->charges[0]->id
    $payments->getBilling()
        ->setDelayed(false)
        ->setEmail("fabricio2237@gmail.com")
        ->setAddress($address);
    $payments->getCreditCardDetails()->setCreditCardHash("c08e1d91-33d0-499c-9efe-6ecb2a467465");
    $resultpay = $juno->Financial()->create($payments);
    print_r($resultpay);
    //--------------------------------------------------------------------------    
} catch (\GuzzleHttp\Exception\RequestException $e) {
    if ($e->hasResponse()) {
        $response = $e->getResponse();
        var_dump($response->getStatusCode());
        var_dump($response->getReasonPhrase());
        var_dump(json_decode((string) $response->getBody()));
    }
}