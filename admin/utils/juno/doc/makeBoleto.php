<?php 

require_once __DIR__ . '/vendor/autoload.php';
require_once __DIR__ . '/configJuno.php';

try {
    $auth = new \ODJuno\ODJunoAuth($config);
    $authorization = $auth->authService()->authenticate();
    $juno = new \ODJuno\ODJuno($authorization->getAccessToken(), $config['privateToken'], $config['sandbox'], false);
    //--------------------------------------------------------------------------
    $address = new \ODJuno\Entities\Address();
    $address->setStreet("Rua 42")
        ->setNumber("54")
        ->setNeighborhood("Vila Santa Cecília")
        ->setCity("Volta Redonda")
        ->setState("RJ")
        ->setComplement("")            
        ->setPostCode("27351140");
    //--------------------------------------------------------------------------    
    $charge = new \ODJuno\Entities\Charge();
    $charge->getChargeDetail()
        ->setDescription("Venda Direta")
        ->setPaymentTypes([\ODJuno\Entities\ChargeDetail::PAYMENT_TYPE_BOLETO])
        ->setAmount(49.99);
    $charge->getBilling()
        ->setName("Fabricio Oliveira")
        ->setDocument("56202888032")
        ->setEmail("fabricio2237@gmail.com")
        ->setAddress($address);
    $result = $juno->charges()->create($charge);
    print_r($result);
} catch (\GuzzleHttp\Exception\RequestException $e) {
    if ($e->hasResponse()) {
        $response = $e->getResponse();
        var_dump($response->getStatusCode());
        var_dump($response->getReasonPhrase());
        var_dump(json_decode((string) $response->getBody()));
    }
}
//--------------------------------------------------------------------------
// Consultar uma cobrança
/*try {
    $auth = new \ODJuno\ODJunoAuth($config);
    $authorization = $auth->authService()->authenticate();
    $juno = new \ODJuno\ODJuno($authorization->getAccessToken(), $config['privateToken'], $config['sandbox'], false);
    $result = $juno->charges()->show('charge_id');
    print_r($result);
} catch (\GuzzleHttp\Exception\RequestException $e) {
    if ($e->hasResponse()) {
        $response = $e->getResponse();
        var_dump($response->getStatusCode()); // HTTP status code;
        var_dump($response->getReasonPhrase()); // Response message;
        var_dump(json_decode((string) $response->getBody())); // Body as the decoded JSON;
    }
}*/