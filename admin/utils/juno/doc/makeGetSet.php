<?php

$lista = [
    ["type", "string"],
    ["name", "string"],
    ["document", "string"],
    ["amount", "float"],
    ["bankAccount", "BankAccount"],
];

foreach ($lista as $value) {
    
echo "/**<br>
     * Get the value of $value[0]<br>
     *<br>
     * @return  $value[1]<br>
     */<br> 
    public function get" . ucfirst ($value[0]) . "()<br>
    {<br>
        return \$this->$value[0];<br>
    }<br><br>";
}

echo "<br>";

foreach ($lista as $value) {
    
echo "/**<br>
     * Set the value of $value[0]<br>
     *<br>
     * @param  $value[1]  \$$value[0]<br>
     *<br>
     * @return  self<br>
     */<br> 
    public function set" . ucfirst ($value[0]) . "($value[1] \$$value[0])<br>
    {<br>
        \$this->$value[0] = \$$value[0];<br><br>
        return \$this;<br>
    }<br><br>";

}
exit;
