<?php 

require_once __DIR__ . '/vendor/autoload.php';
require_once __DIR__ . '/configJuno.php';

try {
    $auth = new \ODJuno\ODJunoAuth($config);
    $authorization = $auth->authService()->authenticate();
    $juno = new \ODJuno\ODJuno($authorization->getAccessToken(), $config['privateToken'], $config['sandbox'], false);
    //--------------------------------------------------------------------------
    $bankAccount = new \ODJuno\Entities\BankAccount();
    $bankAccount->setBankNumber("001")
        ->setAgencyNumber("22221")
        ->setAccountNumber("55555")
        ->setAccountComplementNumber("6")
        ->setAccountType("CHECKING");  
    //--------------------------------------------------------------------------        
    $transfer = new \ODJuno\Entities\Transfer();
    $transfer->setType("BANK_ACCOUNT")
            ->setName("Douglas Serafim")
            ->setDocument("03953044079")
            ->setAmount(50.00)
            ->setBankAccount($bankAccount);
    $result = $juno->Financial()->transfer($transfer);
    print_r($result);
} catch (\GuzzleHttp\Exception\RequestException $e) {
    if ($e->hasResponse()) {
        $response = $e->getResponse();
        var_dump($response->getStatusCode());
        var_dump($response->getReasonPhrase());
        var_dump(json_decode((string) $response->getBody()));
    }
}