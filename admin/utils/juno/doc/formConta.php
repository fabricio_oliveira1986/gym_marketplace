<?php 

require_once __DIR__ . '/../vendor/autoload.php';
require_once __DIR__ . '/configJuno.php';

try {
    $auth = new \ODJuno\ODJunoAuth($config);
    $authorization = $auth->authService()->authenticate();
    $juno = new \ODJuno\ODJuno($authorization->getAccessToken(), $config['privateToken'], $config['sandbox'], false);
    //--------------------------------------------------------------------------   
    $address = new \ODJuno\Entities\Address();
    $address->setStreet("Rua 42")
        ->setNumber("54")
        ->setNeighborhood("Vila Santa Cecília")
        ->setCity("Volta Redonda")
        ->setState("RJ")
        ->setComplement("")            
        ->setPostCode("27351140");    
    //--------------------------------------------------------------------------    
    $accountHolder = new \ODJuno\Entities\AccountHolder();
    $accountHolder->setName("Saulo Rodrigues")
        ->setDocument("40129730025");
    $bankAccount = new \ODJuno\Entities\BankAccount();
    $bankAccount->setBankNumber("001")
        ->setAgencyNumber("22221")
        ->setAccountNumber("55555")
        ->setAccountComplementNumber("6")
        ->setAccountType("CHECKING")
        ->setAccountHolder($accountHolder);            
    //--------------------------------------------------------------------------    
    $digitalAccount = new \ODJuno\Entities\DigitalAccount();
    $digitalAccount->setType("PAYMENT")
        ->setName("Conta Academia SRO")
        ->setDocument("40129730025")
        ->setEmail("saulo22@gmail.com")
        ->setPhone("24988186405")
        ->setBusinessArea(1003)
        ->setLinesOfBusiness("Academia SRO")
        ->setAddress($address)
        ->setBankAccount($bankAccount)
        ->setEmailOptOut(false)
        ->setAutoTransfer(false)
        ->setMonthlyIncomeOrRevenue(12000.00)
        ->setPep(false);
    //PF------------------------------------------------------------------------
    $digitalAccount->setBirthDate("1979-12-11")
        ->setMotherName("Maria Graça")            
        ->setSocialName(false);
    //PJ------------------------------------------------------------------------
    /*$legalRepresentative = new \ODJuno\Entities\LegalRepresentative();    
    $legalRepresentative->setName("Saulo Rodrigues")
        ->setDocument("40129730025")
        ->setBirthDate("1979-12-11")
        ->setMotherName("Maria Graça")
        ->setType("INDIVIDUAL");
    //--------------------------------------------------------------------------    
    $members = array();
    $member = new \ODJuno\Entities\CompanyMember();
    $member->setName("Saulo Rodrigues")
        ->setDocument("40129730025")
        ->setBirthDate("1996-12-11");
    $members[] = $member;    
    //--------------------------------------------------------------------------        
    $digitalAccount->setLegalRepresentative($legalRepresentative)
        ->setCnae("7777777")
        ->setCompanyType("MEI")
        ->setEstablishmentDate("1990-12-11")
        ->setCompanyMembers($members);*/
    $result = $juno->DigitalAccounts()->create($digitalAccount);
    print_r($result);
} catch (\GuzzleHttp\Exception\RequestException $e) {
    if ($e->hasResponse()) {
        $response = $e->getResponse();
        var_dump($response->getStatusCode()); // HTTP status code;
        var_dump($response->getReasonPhrase()); // Response message;
        var_dump(json_decode((string) $response->getBody())); // Body as the decoded JSON;
    }
}
//--------------------------------------------------------------------------
// Consultar contas
//try {
//    $auth = new \ODJuno\ODJunoAuth($config);
//    $authorization = $auth->authService()->authenticate();
//    $juno = new \ODJuno\ODJuno($authorization->getAccessToken(), $config['privateToken'], $config['sandbox'], false);
//    $result = $juno->DigitalAccounts()->listAll();
//    echo json_encode($result);
//} catch (\GuzzleHttp\Exception\RequestException $e) {
//    if ($e->hasResponse()) {
//        $response = $e->getResponse();
//        var_dump($response->getStatusCode()); // HTTP status code;
//        var_dump($response->getReasonPhrase()); // Response message;
//        var_dump(json_decode((string) $response->getBody())); // Body as the decoded JSON;
//    }
//}
//--------------------------------------------------------------------------
//try {
//    $auth = new \ODJuno\ODJunoAuth($config);
//    $authorization = $auth->authService()->authenticate();
//    $juno = new \ODJuno\ODJuno($authorization->getAccessToken(), $config['privateToken'], $config['sandbox'], true);    
//    $result = $juno->DigitalAccounts()->createPixKey("GYM_BROTHER");
//    echo json_encode($result);
//} catch (\GuzzleHttp\Exception\RequestException $e) {
//    if ($e->hasResponse()) {
//        $response = $e->getResponse();
//        var_dump($response->getStatusCode()); // HTTP status code;
//        var_dump($response->getReasonPhrase()); // Response message;
//        var_dump(json_decode((string) $response->getBody())); // Body as the decoded JSON;
//    }
//}
