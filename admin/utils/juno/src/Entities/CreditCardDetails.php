<?php

namespace ODJuno\Entities;

class CreditCardDetails extends BaseEntity
{
    /**
     * @var string $creditCardId
     */
    protected $creditCardId;    
    /**
     * @var string $creditCardHash
     */
    protected $creditCardHash;

    /**
     * Get $creditCardId
     *
     * @return  string
     */ 
    public function getCreditCardId()
    {
        return $this->creditCardId;
    }
    
    /**
     * Get $creditCardHash
     *
     * @return  string
     */ 
    public function getCreditCardHash()
    {
        return $this->creditCardHash;
    }

    /**
     * Set $creditCardId
     *
     * @param  string  $creditCardId  $creditCardId
     *
     * @return  self
     */ 
    public function setCreditCardId(string $creditCardId)
    {
        $this->creditCardId = $creditCardId;

        return $this;
    }
    
    /**
     * Set $creditCardHash
     *
     * @param  string  $creditCardHash  $creditCardHash
     *
     * @return  self
     */ 
    public function setCreditCardHash(string $creditCardHash)
    {
        $this->creditCardHash = $creditCardHash;

        return $this;
    }

}