<?php

namespace ODJuno\Entities;

class Payment extends BaseEntity
{
    /**
     * @var string $chargeId
     */
    protected $chargeId;    
    /**
     * @var BillingForPayment $billing
     */
    protected $billing;    
    /**
     * @var CreditCardDetails $creditCardDetails
     */
    protected $creditCardDetails;

    /**
     * Get $chargeId
     *
     * @return  string
     */ 
    public function getChargeId()
    {
        return $this->chargeId;
    }    

    /**
     * Get $billing
     *
     * @return  BillingForPayment
     */ 
    public function getBilling()
    {
        $this->billing = empty($this->billing) ? new BillingForPayment() : $this->billing;
        return $this->billing;
    }    
    
    /**
     * Get $creditCardDetails
     *
     * @return  CreditCardDetails
     */ 
    public function getCreditCardDetails()
    {
        $this->creditCardDetails = empty($this->creditCardDetails) ? new CreditCardDetails() : $this->creditCardDetails;
        return $this->creditCardDetails;
    }

    /**
     * Set $chargeId
     *
     * @param  string  $chargeId  $chargeId
     *
     * @return  self
     */ 
    public function setChargeId(string $chargeId)
    {
        $this->chargeId = $chargeId;

        return $this;
    }    
    
    /**
     * Set $billing
     *
     * @param  BillingForPayment  $billing  $billing
     *
     * @return  self
     */ 
    public function setBilling(Billing $billing)
    {
        $this->billing = $billing;

        return $this;
    }
    
    /**
     * Set $creditCardDetails
     *
     * @param  CreditCardDetails  $creditCardDetails  $creditCardDetails
     *
     * @return  self
     */ 
    public function setChargeDetail(CreditCardDetails $creditCardDetails)
    {
        $this->creditCardDetails = $creditCardDetails;

        return $this;
    }
}