<?php

namespace ODJuno\Entities;

class Transfer extends BaseEntity {

    /**
     * @var string $type
     */
    protected $type;

    /**
     * @var string $name
     */
    protected $name;

    /**
     * @var string $document
     */
    protected $document;

    /**
     * @var string $amount
     */
    protected $amount;

    /**
     * @var BankAccount $bankAccount
     */
    protected $bankAccount;

    /**
     * Get the value of type
     *
     * @return string
     */
    public function getType() {
        return $this->type;
    }

    /**
     * Get the value of name
     *
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Get the value of document
     *
     * @return string
     */
    public function getDocument() {
        return $this->document;
    }

    /**
     * Get the value of amount
     *
     * @return string
     */
    public function getAmount() {
        return $this->amount;
    }

    /**
     * Get the value of bankAccount
     *
     * @return BankAccount
     */
    public function getBankAccount() {
        return $this->bankAccount;
    }

    /**
     * Set the value of type
     *
     * @param string $type
     *
     * @return self
     */
    public function setType(string $type) {
        $this->type = $type;

        return $this;
    }

    /**
     * Set the value of name
     *
     * @param string $name
     *
     * @return self
     */
    public function setName(string $name) {
        $this->name = $name;

        return $this;
    }

    /**
     * Set the value of document
     *
     * @param string $document
     *
     * @return self
     */
    public function setDocument(string $document) {
        $this->document = $document;

        return $this;
    }

    /**
     * Set the value of amount
     *
     * @param string $amount
     *
     * @return self
     */
    public function setAmount(string $amount) {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Set the value of bankAccount
     *
     * @param BankAccount $bankAccount
     *
     * @return self
     */
    public function setBankAccount(BankAccount $bankAccount) {
        $this->bankAccount = $bankAccount;

        return $this;
    }

}
