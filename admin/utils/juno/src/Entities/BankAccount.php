<?php

namespace ODJuno\Entities;

class BankAccount extends BaseEntity {

    /**
     * @var string $bankNumber
     */
    protected $bankNumber;

    /**
     * @var string $agencyNumber
     */
    protected $agencyNumber;

    /**
     * @var string $accountNumber
     */
    protected $accountNumber;

    /**
     * @var string $accountComplementNumber
     */
    protected $accountComplementNumber;

    /**
     * @var string $accountType
     */
    protected $accountType;

    /**
     * @var AccountHolder $accountHolder
     */
    protected $accountHolder;

    /**
     * Get the value of bankNumber
     *
     * @return string
     */
    public function getBankNumber() {
        return $this->bankNumber;
    }

    /**
     * Get the value of agencyNumber
     *
     * @return string
     */
    public function getAgencyNumber() {
        return $this->agencyNumber;
    }

    /**
     * Get the value of accountNumber
     *
     * @return string
     */
    public function getAccountNumber() {
        return $this->accountNumber;
    }

    /**
     * Get the value of accountComplementNumber
     *
     * @return string
     */
    public function getAccountComplementNumber() {
        return $this->accountComplementNumber;
    }

    /**
     * Get the value of accountType
     *
     * @return string
     */
    public function getAccountType() {
        return $this->accountType;
    }

    /**
     * Get the value of accountHolder
     *
     * @return AccountHolder
     */
    public function getAccountHolder() {
        return empty($this->accountHolder) ? new AccountHolder() : $this->accountHolder;
    }

    /**
     * Set the value of bankNumber
     *
     * @param string $bankNumber
     *
     * @return self
     */
    public function setBankNumber(string $bankNumber) {
        $this->bankNumber = $bankNumber;

        return $this;
    }

    /**
     * Set the value of agencyNumber
     *
     * @param string $agencyNumber
     *
     * @return self
     */
    public function setAgencyNumber(string $agencyNumber) {
        $this->agencyNumber = $agencyNumber;

        return $this;
    }

    /**
     * Set the value of accountNumber
     *
     * @param string $accountNumber
     *
     * @return self
     */
    public function setAccountNumber(string $accountNumber) {
        $this->accountNumber = $accountNumber;

        return $this;
    }

    /**
     * Set the value of accountComplementNumber
     *
     * @param string $accountComplementNumber
     *
     * @return self
     */
    public function setAccountComplementNumber(string $accountComplementNumber) {
        $this->accountComplementNumber = $accountComplementNumber;

        return $this;
    }

    /**
     * Set the value of accountType
     *
     * @param string $accountType
     *
     * @return self
     */
    public function setAccountType(string $accountType) {
        $this->accountType = $accountType;

        return $this;
    }

    /**
     * Set the value of accountHolder
     *
     * @param AccountHolder $accountHolder
     *
     * @return self
     */
    public function setAccountHolder(AccountHolder $accountHolder) {
        $this->accountHolder = $accountHolder;

        return $this;
    }

}
