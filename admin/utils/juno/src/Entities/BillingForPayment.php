<?php

namespace ODJuno\Entities;

class BillingForPayment extends BaseEntity
{
    /**
     * @var string $email
     */
    protected $email;
    /**
     * @var Address $address
     */
    protected $address;   
    /**
     * @var bool $delayed
     */
    protected $delayed;    

    /**
     * Get $email
     *
     * @return  string
     */ 
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Get $address
     *
     * @return  Address
     */ 
    public function getAddress()
    {
        return empty($this->address) ? new Address() : $this->address;
    }
    
    /**
     * Get $delayed
     *
     * @return  bool
     */ 
    public function getDelayed()
    {
        return $this->delayed;
    }


    /**
     * Set $email
     *
     * @param  string  $email  $email
     *
     * @return  self
     */ 
    public function setEmail(string $email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Set $address
     *
     * @param  Address  $address  $address
     *
     * @return  self
     */ 
    public function setAddress(Address $address)
    {
        $this->address = $address;

        return $this;
    }    

    /**
     * Set $delayed
     *
     * @param  bool  $delayed  $delayed
     *
     * @return  self
     */ 
    public function setDelayed(bool $delayed)
    {
        $this->delayed = $delayed;

        return $this;
    }
}