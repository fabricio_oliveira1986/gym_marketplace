<?php

namespace ODJuno\Entities;

class AccountHolder extends BaseEntity {

    /**
     * @var string $name
     */
    protected $name;

    /**
     * @var string $document
     */
    protected $document;

    /**
     * Get the value of name
     *
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Get the value of document
     *
     * @return string
     */
    public function getDocument() {
        return $this->document;
    }

    /**
     * Set the value of name
     *
     * @param string $name
     *
     * @return self
     */
    public function setName(string $name) {
        $this->name = $name;

        return $this;
    }

    /**
     * Set the value of document
     *
     * @param string $document
     *
     * @return self
     */
    public function setDocument(string $document) {
        $this->document = $document;

        return $this;
    }

}
