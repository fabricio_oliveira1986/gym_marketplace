<?php

namespace ODJuno\Entities;

class DigitalAccount extends BaseEntity {

    /**
     * @var string $type
     */
    protected $type;

    /**
     * @var string $name
     */
    protected $name;

    /**
     * @var string $tradingName
     */
    protected $tradingName;    

    /**
     * @var string $document
     */
    protected $document;

    /**
     * @var string $email
     */
    protected $email;

    /**
     * @var string $birthDate
     */
    protected $birthDate;

    /**
     * @var string $motherName
     */
    protected $motherName;

    /**
     * @var string $phone
     */
    protected $phone;

    /**
     * @var int $businessArea
     */
    protected $businessArea;

    /**
     * @var string $linesOfBusiness
     */
    protected $linesOfBusiness;

    /**
     * @var string $companyType
     */
    protected $companyType;

    /**
     * @var LegalRepresentative $legalRepresentative
     */
    protected $legalRepresentative;

    /**
     * @var Address $address
     */
    protected $address;

    /**
     * @var BankAccount $bankAccount
     */
    protected $bankAccount;

    /**
     * @var bool $emailOptOut
     */
    protected $emailOptOut;

    /**
     * @var bool $autoTransfer
     */
    protected $autoTransfer;

    /**
     * @var bool $socialName
     */
    protected $socialName;

    /**
     * @var string $monthlyIncomeOrRevenue
     */
    protected $monthlyIncomeOrRevenue;

    /**
     * @var string $cnae
     */
    protected $cnae;

    /**
     * @var string $establishmentDate
     */
    protected $establishmentDate;

    /**
     * @var bool $pep
     */
    protected $pep;

    /**
     * @var array $companyMembers
     */
    protected $companyMembers;

    /**
     * Get the value of type
     *
     * @return string
     */
    public function getType() {
        return $this->type;
    }

    /**
     * Get the value of name
     *
     * @return string
     */
    public function getName() {
        return $this->name;
    }
    
    /**
     * Get the value of tradingName
     *
     * @return string
     */
    public function getTradingName() {
        return $this->tradingName;
    }
    
    /**
     * Get the value of document
     *
     * @return string
     */
    public function getDocument() {
        return $this->document;
    }

    /**
     * Get the value of email
     *
     * @return string
     */
    public function getEmail() {
        return $this->email;
    }

    /**
     * Get the value of birthDate
     *
     * @return string
     */
    public function getBirthDate() {
        return $this->birthDate;
    }
    
    /**
     * Get the value of motherName
     *
     * @return string
     */
    public function getMotherName() {
        return $this->motherName;
    }

    /**
     * Get the value of phone
     *
     * @return string
     */
    public function getPhone() {
        return $this->phone;
    }

    /**
     * Get the value of businessArea
     *
     * @return int
     */
    public function getBusinessArea() {
        return $this->businessArea;
    }

    /**
     * Get the value of linesOfBusiness
     *
     * @return string
     */
    public function getLinesOfBusiness() {
        return $this->linesOfBusiness;
    }

    /**
     * Get the value of companyType
     *
     * @return string
     */
    public function getCompanyType() {
        return $this->companyType;
    }

    /**
     * Get the value of legalRepresentative
     *
     * @return LegalRepresentative
     */
    public function getLegalRepresentative() {
        return empty($this->legalRepresentative) ? new LegalRepresentative() : $this->legalRepresentative;
    }

    /**
     * Get the value of address
     *
     * @return Address
     */
    public function getAddress() {
        return empty($this->address) ? new Address() : $this->address;
    }

    /**
     * Get the value of bankAccount
     *
     * @return BankAccount
     */
    public function getBankAccount() {
        return empty($this->bankAccount) ? new BankAccount() : $this->bankAccount;
    }

    /**
     * Get the value of emailOptOut
     *
     * @return bool
     */
    public function getEmailOptOut() {
        return $this->emailOptOut;
    }

    /**
     * Get the value of autoTransfer
     *
     * @return bool
     */
    public function getAutoTransfer() {
        return $this->autoTransfer;
    }

    /**
     * Get the value of socialName
     *
     * @return bool
     */
    public function getSocialName() {
        return $this->socialName;
    }

    /**
     * Get the value of monthlyIncomeOrRevenue
     *
     * @return string
     */
    public function getMonthlyIncomeOrRevenue() {
        return $this->monthlyIncomeOrRevenue;
    }

    /**
     * Get the value of cnae
     *
     * @return string
     */
    public function getCnae() {
        return $this->cnae;
    }

    /**
     * Get the value of establishmentDate
     *
     * @return string
     */
    public function getEstablishmentDate() {
        return $this->establishmentDate;
    }

    /**
     * Get the value of pep
     *
     * @return bool
     */
    public function getPep() {
        return $this->pep;
    }

    /**
     * Get the value of companyMembers
     *
     * @return array
     */
    public function getCompanyMembers() {
        return $this->companyMembers;
    }

    /**
     * Set the value of type
     *
     * @param string $type
     *
     * @return self
     */
    public function setType(string $type) {
        $this->type = $type;

        return $this;
    }

    /**
     * Set the value of name
     *
     * @param string $name
     *
     * @return self
     */
    public function setName(string $name) {
        $this->name = $name;

        return $this;
    }
    
    /**
     * Set the value of tradingName
     *
     * @param string $tradingName
     *
     * @return self
     */
    public function setTradingName(string $tradingName) {
        $this->tradingName = $tradingName;

        return $this;
    }    

    /**
     * Set the value of document
     *
     * @param string $document
     *
     * @return self
     */
    public function setDocument(string $document) {
        $this->document = $document;

        return $this;
    }

    /**
     * Set the value of email
     *
     * @param string $email
     *
     * @return self
     */
    public function setEmail(string $email) {
        $this->email = $email;

        return $this;
    }

    /**
     * Set the value of birthDate
     *
     * @param string $birthDate
     *
     * @return self
     */
    public function setBirthDate(string $birthDate) {
        $this->birthDate = $birthDate;

        return $this;
    }
    
    /**
     * Set the value of motherName
     *
     * @param string $motherName
     *
     * @return self
     */
    public function setMotherName(string $motherName) {
        $this->motherName = $motherName;

        return $this;
    }

    /**
     * Set the value of phone
     *
     * @param string $phone
     *
     * @return self
     */
    public function setPhone(string $phone) {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Set the value of businessArea
     *
     * @param int $businessArea
     *
     * @return self
     */
    public function setBusinessArea(int $businessArea) {
        $this->businessArea = $businessArea;

        return $this;
    }

    /**
     * Set the value of linesOfBusiness
     *
     * @param string $linesOfBusiness
     *
     * @return self
     */
    public function setLinesOfBusiness(string $linesOfBusiness) {
        $this->linesOfBusiness = $linesOfBusiness;

        return $this;
    }

    /**
     * Set the value of companyType
     *
     * @param string $companyType
     *
     * @return self
     */
    public function setCompanyType(string $companyType) {
        $this->companyType = $companyType;

        return $this;
    }

    /**
     * Set the value of legalRepresentative
     *
     * @param LegalRepresentative $legalRepresentative
     *
     * @return self
     */
    public function setLegalRepresentative(LegalRepresentative $legalRepresentative) {
        $this->legalRepresentative = $legalRepresentative;

        return $this;
    }

    /**
     * Set the value of address
     *
     * @param Address $address
     *
     * @return self
     */
    public function setAddress(Address $address) {
        $this->address = $address;

        return $this;
    }

    /**
     * Set the value of bankAccount
     *
     * @param BankAccount $bankAccount
     *
     * @return self
     */
    public function setBankAccount(BankAccount $bankAccount) {
        $this->bankAccount = $bankAccount;

        return $this;
    }

    /**
     * Set the value of emailOptOut
     *
     * @param bool $emailOptOut
     *
     * @return self
     */
    public function setEmailOptOut(bool $emailOptOut) {
        $this->emailOptOut = $emailOptOut;

        return $this;
    }

    /**
     * Set the value of autoTransfer
     *
     * @param bool $autoTransfer
     *
     * @return self
     */
    public function setAutoTransfer(bool $autoTransfer) {
        $this->autoTransfer = $autoTransfer;

        return $this;
    }

    /**
     * Set the value of socialName
     *
     * @param bool $socialName
     *
     * @return self
     */
    public function setSocialName(bool $socialName) {
        $this->socialName = $socialName;

        return $this;
    }

    /**
     * Set the value of monthlyIncomeOrRevenue
     *
     * @param string $monthlyIncomeOrRevenue
     *
     * @return self
     */
    public function setMonthlyIncomeOrRevenue(string $monthlyIncomeOrRevenue) {
        $this->monthlyIncomeOrRevenue = $monthlyIncomeOrRevenue;

        return $this;
    }

    /**
     * Set the value of cnae
     *
     * @param string $cnae
     *
     * @return self
     */
    public function setCnae(string $cnae) {
        $this->cnae = $cnae;

        return $this;
    }

    /**
     * Set the value of establishmentDate
     *
     * @param string $establishmentDate
     *
     * @return self
     */
    public function setEstablishmentDate(string $establishmentDate) {
        $this->establishmentDate = $establishmentDate;

        return $this;
    }

    /**
     * Set the value of pep
     *
     * @param bool $pep
     *
     * @return self
     */
    public function setPep(bool $pep) {
        $this->pep = $pep;

        return $this;
    }

    /**
     * Set the value of companyMembers
     *
     * @param array $companyMembers
     *
     * @return self
     */
    public function setCompanyMembers(array $companyMembers) {
        $this->companyMembers = $companyMembers;

        return $this;
    }

}
