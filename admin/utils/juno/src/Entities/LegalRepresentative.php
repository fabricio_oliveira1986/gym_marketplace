<?php

namespace ODJuno\Entities;

class LegalRepresentative extends BaseEntity {

    /**
     * @var string $name
     */
    protected $name;

    /**
     * @var string $document
     */
    protected $document;

    /**
     * @var string $birthDate
     */
    protected $birthDate;

    /**
     * @var string $motherName
     */
    protected $motherName;

    /**
     * @var string $type
     */
    protected $type;

    /**
     * Get the value of name
     *
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Get the value of document
     *
     * @return string
     */
    public function getDocument() {
        return $this->document;
    }

    /**
     * Get the value of birthDate
     *
     * @return string
     */
    public function getBirthDate() {
        return $this->birthDate;
    }

    /**
     * Get the value of motherName
     *
     * @return string
     */
    public function getMotherName() {
        return $this->motherName;
    }

    /**
     * Get the value of type
     *
     * @return string
     */
    public function getType() {
        return $this->type;
    }

    /**
     * Set the value of name
     *
     * @param string $name
     *
     * @return self
     */
    public function setName(string $name) {
        $this->name = $name;

        return $this;
    }

    /**
     * Set the value of document
     *
     * @param string $document
     *
     * @return self
     */
    public function setDocument(string $document) {
        $this->document = $document;

        return $this;
    }

    /**
     * Set the value of birthDate
     *
     * @param string $birthDate
     *
     * @return self
     */
    public function setBirthDate(string $birthDate) {
        $this->birthDate = $birthDate;

        return $this;
    }

    /**
     * Set the value of motherName
     *
     * @param string $motherName
     *
     * @return self
     */
    public function setMotherName(string $motherName) {
        $this->motherName = $motherName;

        return $this;
    }

    /**
     * Set the value of type
     *
     * @param string $type
     *
     * @return self
     */
    public function setType(string $type) {
        $this->type = $type;

        return $this;
    }

}
