<?php

namespace ODJuno\Services;

use ODJuno\Entities\DigitalAccount;
use ODJuno\Response\Response;

class DigitalAccountsService extends BaseService {

    public function create(DigitalAccount $params): Response {
        //print_r($params); exit;
        $response = $this->client->post('digital-accounts', [
            'json' => $params
        ]);
        return $this->response->fromJson($response);
    }
    
    public function update(DigitalAccount $params): Response {
        //print_r($params); exit;
        $response = $this->client->patch('digital-accounts', [
            'json' => $params
        ]);
        return $this->response->fromJson($response);
    }
    
    public function setCard($param): Response {
        $response = $this->client->post('credit-cards/tokenization', [
            'json' => [
                'creditCardHash' => $param
            ]
        ]);
        return $this->response->fromJson($response);
    }
    
    public function createPixKey($param): Response {        
        $response = $this->client->post('pix/keys', [
            'json' => [
                'type' => 'RANDOM_KEY',
                'key' => $param
            ]
        ]);
        return $this->response->fromJson($response);
    }

    public function listAll(): Response {
        $response = $this->client->get('digital-accounts');
        return $this->response->fromJson($response);
    }

    public function listBanks(): Response {
        $response = $this->client->get('data/banks');
        return $this->response->fromJson($response);
    }

    public function listCompanyTypes(): Response {
        $response = $this->client->get('data/company-types');
        return $this->response->fromJson($response);
    }

    public function listBusinessAreas(): Response {
        $response = $this->client->get('data/business-areas');
        return $this->response->fromJson($response);
    }

    public function listDocuments(): Response {
        $response = $this->client->get('documents');
        return $this->response->fromJson($response);
    }

    public function setDocument($id, $params): Response {
        $response = $this->client->post("documents/{$id}/files", [
            'multipart' => [
                [
                    'name' => 'files',
                    'contents' => $params
                ]
            ]
        ]);
        return $this->response->fromJson($response);
    }

    public function getDocument($id): Response {
        $response = $this->client->get("documents/{$id}");
        return $this->response->fromJson($response);
    }

}
