<?php

namespace ODJuno\Services;

use ODJuno\Entities\Payment;
use ODJuno\Entities\Transfer;
use ODJuno\Response\Response;

class FinancialService extends BaseService {

    public function payment(Payment $params): Response {
        //echo json_encode($params); exit;
        $response = $this->client->post('payments', [
            'json' => $params
        ]);
        return $this->response->fromJson($response);
    }
        
    public function refund($id, $split, $total): Response {
        //echo json_encode($split); exit;
        $response = $this->client->post("payments/{$id}/refunds", [
            'json' => [
                'amount' => $total,
                'split' => $split,
            ]
        ]);
        return $this->response->fromJson($response);
    }    
    
    public function transfer(Transfer $params): Response {
        //echo json_encode($params); exit;
        $response = $this->client->post('transfers', [
            'json' => $params
        ]);
        return $this->response->fromJson($response);
    }
    
    public function balance(): Response {
        $response = $this->client->get('balance');
        return $this->response->fromJson($response);
    }    

}
