<?php

// autoload_classmap.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'Composer\\InstalledVersions' => $vendorDir . '/composer/InstalledVersions.php',
    'Safe2Pay\\API\\AccountDepositRequest' => $vendorDir . '/safe2pay/sdk/lib/API/AccountDepositRequest.php',
    'Safe2Pay\\API\\AdvancedPaymentRequest' => $vendorDir . '/safe2pay/sdk/lib/API/AdvancedPaymentRequest.php',
    'Safe2Pay\\API\\BankTransferRequest' => $vendorDir . '/safe2pay/sdk/lib/API/BankTransferRequest.php',
    'Safe2Pay\\API\\InvoiceRequest' => $vendorDir . '/safe2pay/sdk/lib/API/InvoiceRequest.php',
    'Safe2Pay\\API\\MarketplaceRequest' => $vendorDir . '/safe2pay/sdk/lib/API/MarketplaceRequest.php',
    'Safe2Pay\\API\\PaymentRequest' => $vendorDir . '/safe2pay/sdk/lib/API/PaymentRequest.php',
    'Safe2Pay\\API\\PlanRequest' => $vendorDir . '/safe2pay/sdk/lib/API/PlanRequest.php',
    'Safe2Pay\\API\\RefundType' => $vendorDir . '/safe2pay/sdk/lib/API/PaymentRequest.php',
    'Safe2Pay\\API\\SubscriptionRequests' => $vendorDir . '/safe2pay/sdk/lib/API/SubscriptionRequests.php',
    'Safe2Pay\\API\\TokenizationRequest' => $vendorDir . '/safe2pay/sdk/lib/API/TokenizationRequest.php',
    'Safe2Pay\\API\\TransactionRequest' => $vendorDir . '/safe2pay/sdk/lib/API/TransactionRequest.php',
    'Safe2Pay\\API\\TransferenceRequest' => $vendorDir . '/safe2pay/sdk/lib/API/TransferenceRequest.php',
    'Safe2Pay\\Models\\Bank\\AccountType' => $vendorDir . '/safe2pay/sdk/lib/Models/Bank/AccountType.php',
    'Safe2Pay\\Models\\Bank\\Bank' => $vendorDir . '/safe2pay/sdk/lib/Models/Bank/Bank.php',
    'Safe2Pay\\Models\\Bank\\BankData' => $vendorDir . '/safe2pay/sdk/lib/Models/Bank/BankData.php',
    'Safe2Pay\\Models\\Core\\Client' => $vendorDir . '/safe2pay/sdk/lib/Models/Core/Client.php',
    'Safe2Pay\\Models\\Core\\Config' => $vendorDir . '/safe2pay/sdk/lib/Models/Core/Config.php',
    'Safe2Pay\\Models\\General\\Address' => $vendorDir . '/safe2pay/sdk/lib/Models/General/Address.php',
    'Safe2Pay\\Models\\General\\Customer' => $vendorDir . '/safe2pay/sdk/lib/Models/General/Customer.php',
    'Safe2Pay\\Models\\General\\Product' => $vendorDir . '/safe2pay/sdk/lib/Models/General/Product.php',
    'Safe2Pay\\Models\\Merchant\\Configuration' => $vendorDir . '/safe2pay/sdk/lib/Models/Merchant/Configuration.php',
    'Safe2Pay\\Models\\Merchant\\ListTax' => $vendorDir . '/safe2pay/sdk/lib/Models/Merchant/ListTax.php',
    'Safe2Pay\\Models\\Merchant\\Merchant' => $vendorDir . '/safe2pay/sdk/lib/Models/Merchant/Merchant.php',
    'Safe2Pay\\Models\\Merchant\\MerchantConfiguration' => $vendorDir . '/safe2pay/sdk/lib/Models/Merchant/MerchantConfiguration.php',
    'Safe2Pay\\Models\\Merchant\\MerchantNotify' => $vendorDir . '/safe2pay/sdk/lib/Models/Merchant/MerchantNotify.php',
    'Safe2Pay\\Models\\Merchant\\MerchantPaymentDate' => $vendorDir . '/safe2pay/sdk/lib/Models/Merchant/MerchantPaymentDate.php',
    'Safe2Pay\\Models\\Merchant\\MerchantPaymentMethod' => $vendorDir . '/safe2pay/sdk/lib/Models/Merchant/MerchantPaymentMethod.php',
    'Safe2Pay\\Models\\Merchant\\MerchantSplit' => $vendorDir . '/safe2pay/sdk/lib/Models/Merchant/MerchantSplit.php',
    'Safe2Pay\\Models\\Merchant\\MerchantSplitTax' => $vendorDir . '/safe2pay/sdk/lib/Models/Merchant/MerchantSplitTax.php',
    'Safe2Pay\\Models\\Merchant\\MerchantType' => $vendorDir . '/safe2pay/sdk/lib/Models/Merchant/MerchantType.php',
    'Safe2Pay\\Models\\Merchant\\Plan' => $vendorDir . '/safe2pay/sdk/lib/Models/Merchant/Plan.php',
    'Safe2Pay\\Models\\Merchant\\PlanFrequence' => $vendorDir . '/safe2pay/sdk/lib/Models/Merchant/PlanFrequence.php',
    'Safe2Pay\\Models\\Merchant\\TaxType' => $vendorDir . '/safe2pay/sdk/lib/Models/Merchant/TaxType.php',
    'Safe2Pay\\Models\\Payment\\BankSlip' => $vendorDir . '/safe2pay/sdk/lib/Models/Payment/BankSlip.php',
    'Safe2Pay\\Models\\Payment\\BankTransfer' => $vendorDir . '/safe2pay/sdk/lib/Models/Payment/BankTransfer.php',
    'Safe2Pay\\Models\\Payment\\Carnet' => $vendorDir . '/safe2pay/sdk/lib/Models/Payment/Carnet.php',
    'Safe2Pay\\Models\\Payment\\CarnetBankslip' => $vendorDir . '/safe2pay/sdk/lib/Models/Payment/Carnet.php',
    'Safe2Pay\\Models\\Payment\\CarnetItems' => $vendorDir . '/safe2pay/sdk/lib/Models/Payment/CarnetItems.php',
    'Safe2Pay\\Models\\Payment\\CarnetLot' => $vendorDir . '/safe2pay/sdk/lib/Models/Payment/CarnetLot.php',
    'Safe2Pay\\Models\\Payment\\CarnetLotBankSlip' => $vendorDir . '/safe2pay/sdk/lib/Models/Payment/CarnetItems.php',
    'Safe2Pay\\Models\\Payment\\CreditCard' => $vendorDir . '/safe2pay/sdk/lib/Models/Payment/CreditCard.php',
    'Safe2Pay\\Models\\Payment\\Cryptocoin' => $vendorDir . '/safe2pay/sdk/lib/Models/Payment/Cryptocoin.php',
    'Safe2Pay\\Models\\Payment\\DebitCard' => $vendorDir . '/safe2pay/sdk/lib/Models/Payment/DebitCard.php',
    'Safe2Pay\\Models\\Payment\\PaymentMethod' => $vendorDir . '/safe2pay/sdk/lib/Models/Payment/PaymentMethod.php',
    'Safe2Pay\\Models\\Payment\\PaymentObjectCarnet' => $vendorDir . '/safe2pay/sdk/lib/Models/Payment/CarnetItems.php',
    'Safe2Pay\\Models\\Payment\\Pix' => $vendorDir . '/safe2pay/sdk/lib/Models/Payment/Pix.php',
    'Safe2Pay\\Models\\Plan\\Plans' => $vendorDir . '/safe2pay/sdk/lib/Models/Plan/Plans.php',
    'Safe2Pay\\Models\\Response\\Response' => $vendorDir . '/safe2pay/sdk/lib/Models/Response/Response.php',
    'Safe2Pay\\Models\\SingleSale\\SingleSale' => $vendorDir . '/safe2pay/sdk/lib/Models/SingleSale/SingleSale.php',
    'Safe2Pay\\Models\\SingleSale\\SingleSalePayment' => $vendorDir . '/safe2pay/sdk/lib/Models/SingleSale/SingleSalePayment.php',
    'Safe2Pay\\Models\\SingleSale\\SingleSalePaymentMethod' => $vendorDir . '/safe2pay/sdk/lib/Models/SingleSale/SingleSalePaymentMethod.php',
    'Safe2Pay\\Models\\SingleSale\\SingleSaleProduct' => $vendorDir . '/safe2pay/sdk/lib/Models/SingleSale/SingleSaleProduct.php',
    'Safe2Pay\\Models\\Subscription\\Subscription' => $vendorDir . '/safe2pay/sdk/lib/Models/Subscription/Subscription.php',
    'Safe2Pay\\Models\\Subscription\\SubscriptionRequest' => $vendorDir . '/safe2pay/sdk/lib/Models/Subscription/SubscriptionRequest.php',
    'Safe2Pay\\Models\\Transactions\\Base' => $vendorDir . '/safe2pay/sdk/lib/Models/Transactions/Base.php',
    'Safe2Pay\\Models\\Transactions\\Splits' => $vendorDir . '/safe2pay/sdk/lib/Models/Transactions/Splits.php',
    'Safe2Pay\\Models\\Transactions\\Transaction' => $vendorDir . '/safe2pay/sdk/lib/Models/Transactions/Transaction.php',
    'Safe2Pay\\Models\\Transactions\\TransactionStatus' => $vendorDir . '/safe2pay/sdk/lib/Models/Transactions/TransactionStatus.php',
    'Safe2Pay\\Models\\Transference\\Transfer' => $vendorDir . '/safe2pay/sdk/lib/Models/Transference/Transfer.php',
    'Safe2Pay\\Models\\Transference\\TransferRegister' => $vendorDir . '/safe2pay/sdk/lib/Models/Transference/TransferRegister.php',
);
