<?php

namespace Safe2Pay\Test;

require_once __DIR__ . './vendor/autoload.php';

use Safe2Pay\API\MarketplaceRequest;
use Safe2Pay\API\AccountDepositRequest;
use Safe2Pay\Models\Bank\AccountType;
use Safe2Pay\Models\General\Address;
use Safe2Pay\Models\Bank\Bank;
use Safe2Pay\Models\Bank\BankData;
use Safe2Pay\Models\Merchant\Merchant;
use Safe2Pay\Models\Merchant\MerchantSplit;
use Safe2Pay\Models\Merchant\MerchantSplitTax;
use Safe2Pay\Models\Core\Config as Enviroment;

$enviroment = new Enviroment();
$enviroment->setAPIKEY('BD2B54D7DE2C4035BE68C6EFD7EFC16A');
//D29B70C3FD224CF6B7C723E1C701EE2E

/**
 * Class MarketplaceTest
 *
 * @package Safe2Pay\Test
 */
class MarketplaceTest {

    public static function Save($conn, $loja) {
        $Merchant = new Merchant();
        $Merchant->setIdentity("75097991000171");
        $Merchant->setName("FR Oliveira ME");
        $Merchant->setCommercialName("FR Softwares");
        if (is_numeric($id)) {
            $Merchant->setId($id);
        } else {
            $Merchant->setIsPanelRestricted(false);
        }
        //Dados do responsável 
        $Merchant->setResponsibleIdentity("12180423705");
        $Merchant->setResponsibleName("Fabricio Oliveira");
        $Merchant->setEmail("fabricio2237@hotmail.com");
        //Dados do responsável técnico
        $Merchant->setTechIdentity("14111449026");
        $Merchant->setTechName("Douglas Mendonça");
        $Merchant->setTechEmail("douglas486@gmail.com");
        //Endereço
        $Merchant->Address = new Address();
        $Merchant->Address->setZipCode("27260210");
        $Merchant->Address->setStreet("Rua 42");
        $Merchant->Address->setDistrict("Vila Santa Cecília");        
        $Merchant->Address->setComplement("");        
        $Merchant->Address->setNumber("54");
        $Merchant->Address->setStateInitials("RJ");
        $Merchant->Address->setCityName("Volta Redonda");
        $Merchant->Address->setCountryName("Brasil");
        //Conta Bancária
        $bankData = new BankData();
        $bankData->setBank(new Bank("001"));
        $bankData->setBankAccount("0123");
        $bankData->setBankAccountDigit("0");
        $bankData->setBankAgency("50235");
        $bankData->setBankAgencyDigit("7");
        $bankData->setAccountType(new AccountType("CC"));        
        $merchantSplit = array(
            new MerchantSplit(false, "1", array(new MerchantSplitTax("1", "0.00"))), // 1 - Boleto bancário
            new MerchantSplit(false, "2", array(new MerchantSplitTax("1", "0.00"))), // 2 - Cartão de crédito
            new MerchantSplit(false, "3", array(new MerchantSplitTax("1", "0.00"))), // 3 - Criptomoeda
            new MerchantSplit(false, "4", array(new MerchantSplitTax("1", "0.00"))), // 4 - Cartão de débito 
        );
        $Merchant->setBankData($bankData);
        $Merchant->setMerchantSplit($merchantSplit);
        if (is_numeric($id)) {
            $response = MarketplaceRequest::Update($Merchant);
        } else {
            $response = MarketplaceRequest::Add($Merchant);
        }
        echo (json_encode($response));
    }

    public static function Delete($id) {
        $response = MarketplaceRequest::Delete($id);
        echo (json_encode($response));
    }

    public static function Get($id) {
        $response = MarketplaceRequest::Get($id);
        echo (json_encode($response));
    }

    public static function ListAll($PageNumber, $RowsPage) {
        $response = MarketplaceRequest::ListAll($PageNumber, $RowsPage);
        echo (json_encode($response));
    }
    
    public static function GetBalance($initialDate, $endDate) {
        $response = AccountDepositRequest::GetBalance($initialDate, $endDate);
        echo (json_encode($response));
    }
}

//MarketplaceTest::Save();
//MarketplaceTest::Get(187129);
//MarketplaceTest::ListAll(1, 20);
//MarketplaceTest::Delete();
MarketplaceTest::GetBalance("2022-06-01", "2022-06-28");