<?php

namespace Safe2Pay\Test;

require_once './vendor/autoload.php';

use Safe2Pay\API\PaymentRequest;
use Safe2Pay\API\RefundType;
use Safe2Pay\API\TokenizationRequest as TokenizationRequest;
use Safe2Pay\API\TransferenceRequest;
use Safe2Pay\Models\Payment\BankSlip;
use Safe2Pay\Models\Payment\Cryptocoin;
use Safe2Pay\Models\Payment\CreditCard;
use Safe2Pay\Models\Payment\DebitCard;
use Safe2Pay\Models\Payment\Pix;
use Safe2Pay\Models\Transactions\Splits;
use Safe2Pay\Models\Transactions\Transaction;
use Safe2Pay\Models\General\Customer;
use Safe2Pay\Models\General\Product;
use Safe2Pay\Models\General\Address;
use Safe2Pay\Models\Transference\Transfer;
use Safe2Pay\Models\Transference\TransferRegister;
use Safe2Pay\Models\Bank\AccountType;
use Safe2Pay\Models\Bank\BankData;
use Safe2Pay\Models\Bank\Bank;
use Safe2Pay\Models\Core\Config as Enviroment;

$enviroment = new Enviroment();
$enviroment->setAPIKEY('D29B70C3FD224CF6B7C723E1C701EE2E');

/**
 * Class PaymentTest
 *
 * @package Safe2Pay\Test
 */
class PaymentTest {

    public static function Save($tipo) {
        $payload = new Transaction();
        $payload->setIsSandbox(true);
        $payload->setApplication("GymBrother LTDA");
        $payload->setVendor("Vip Fitness");
        $payload->setCallbackUrl("https://callbacks.exemplo.com.br/api/Notify");

        $Customer = new Customer();
        $Customer->setName("Teste Cliente");
        $Customer->setIdentity("01579286000174");
        $Customer->setEmail("Teste@Teste.com.br");
        $Customer->setPhone("51999999999");
        $Customer->Address = new Address();
        $Customer->Address->setZipCode("90620000");
        $Customer->Address->setStreet("Avenida Princesa Isabel");
        $Customer->Address->setNumber("828");
        $Customer->Address->setComplement("Lado B");
        $Customer->Address->setDistrict("Santana");
        $Customer->Address->setStateInitials("RS");
        $Customer->Address->setCityName("Porto Alegre");
        $Customer->Address->setCountryName("Brasil");
        $payload->setCustomer($Customer);

        $payload->setPaymentMethod($tipo);
        if ($tipo == "1") {
            $BankSlip = new BankSlip();
            $BankSlip->setDueDate("16/06/2022");
            $BankSlip->setInstruction("Pagar até o vencimento");
            $BankSlip->setCancelAfterDue(true);
            $BankSlip->setIsEnablePartialPayment(false);
            $payload->setPaymentObject($BankSlip);
        } else if ($tipo == "2") {
            //$CreditCard = new CreditCard("João da Silva", "4111111111111111", "12/2022", "651");
            $CreditCard = new CreditCard("", "", "", "", "dfa68c9cc04d4eaaae6b5cbb985b711f");
            $payload->setPaymentObject($CreditCard);
        } else if ($tipo == "3") {
            $CryptoCoin = new Cryptocoin("BTC");
            $payload->setPaymentObject($CryptoCoin);
        } else if ($tipo == "4") {
            $DebitCard = new DebitCard("João da Silva", "4024007153763191", "12/2022", "241");
            $payload->setPaymentObject($DebitCard);
        } else if ($tipo == "6") {
            $Pix = new Pix(3600);
            $payload->setPaymentObject($Pix);
        }

        $Products = array();
        for ($i = 0; $i < 10; $i++) {
            $payloadProduct = new Product();
            $payloadProduct->setCode($i + 1);
            $payloadProduct->setDescription("Produto " . ($i + 1));
            $payloadProduct->setUnitPrice(10);
            $payloadProduct->setQuantity(1);
            array_push($Products, $payloadProduct);
        }
        $payload->setProducts($Products);

        /* $split1 = new Splits();
          $split1->setIdReceiver(182816);
          $split1->setName('FR Oliveira ME');
          $split1->setCodeReceiverType('2');  //1 para 'Empresa' ou 2 'Subconta
          $split1->setCodeTaxType('2'); //1 para Percentual ou 2 para Valor
          $split1->setAmount(90.00);
          $split1->setIsPayTax(false); */

        $split2 = new Splits();
        $split2->setIdentity('43959897000198');
        $split2->setName('Gym Brother');
        $split2->setCodeReceiverType('1');
        $split2->setCodeTaxType('2');
        $split2->setAmount(10.00);
        $split2->setIsPayTax(true);

        $splits = array($split2);
        $payload->setSplits($splits);
        //echo(json_encode($payload));        
        $response = PaymentRequest::CreatePayment($payload);
        echo(json_encode($response));
    }

    public static function Refund($id) {
        $type = RefundType::BANKSLIP; // $type = RefundType::BANKSLIP; // $type = RefundType::DEBIT;
        $response = PaymentRequest::Refund($id, $type);
        echo(json_encode($response));
    }

    public static function GetPaymentMethods() {
        $response = PaymentRequest::GetPaymentMethods();
        echo(json_encode($response));
    }

    public static function CreateCardToken() {
        $CreditCard = new CreditCard("Fabricio Oliveira", "4111 1111 1111 1111", "12/2022", "651", null, false);
        $response = TokenizationRequest::Create($CreditCard);
        echo (json_encode($response));
    }

    public static function CreateTransference() {
        $bankData = new BankData();
        $bankData->setBank(new Bank("041"));
        $bankData->setAccountType(new AccountType("CC"));
        $bankData->setBankAccount("1676");
        $bankData->setBankAccountDigit("0");
        $bankData->setBankAgency("41776");
        $bankData->setBankAgencyDigit("7");
        $transferRegisters = array(
            new TransferRegister("Saulo Rodrigues", 50.00, "12180423705", "Saque", "2022-06-02", "urldeexemplo.com.br", $bankData),
        );
        $payload = new Transfer(false, $transferRegisters);
        $response = TransferenceRequest::CreateTransference($payload);
        echo(json_encode($response));
    }
}

PaymentTest::Save($_GET["id"]);
//PaymentTest::Refund();
//PaymentTest::GetPaymentMethods();
//PaymentTest::CreateCardToken();
//PaymentTest::CreateTransference();