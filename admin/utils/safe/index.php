<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Safe</title>
        <style type="text/css">
            body{
                font-family: "Segoe UI", arial, sans-serif;
                font-size: 12px;
            }
            div{
                float: left;
                width: 45%;
                margin-right: 4%;
            }
            input,textarea{
                width: 100%;
            }
            p{
                margin-top: -25px;
                background-color: white;
                width: 100px;
                margin-bottom: 5px;
            }
            .borda{
                float: left;                
                border: 1px solid gray;
                width: 95%;
                padding: 15px;
                margin-bottom: 20px;
            }            
        </style>
    </head>
    <body>
        <form method="post" action="index.php">
            <div class="borda">
                <p style="width: 320px;">Pagamento:</p>
                <div>
                    <label for="txtTipoDocumento">Forma Pagamento:</label>
                    <select id="txtTipoDocumento" name="txtTipoDocumento" style="width: 100%;">
                        <option value="B">Boleto Bancário</option>                        
                        <option value="C">Cartão de Crédito</option>
                        <option value="P" selected>Pix</option>
                        <option value="T">CashBack</option>                        
                        <option value="S">Saque Loja</option>
                        <option value="X">Saque CashBack</option>
                    </select>
                </div>
                <div style="width: 8%">
                    <label for="txtIdPessoa">Id Pessoa:</label>
                    <input id="txtIdPessoa" name="txtIdPessoa" value="1" />
                </div>                
                <div style="width: 10%">
                    <label for="txtIdEndereco">Id Endereco:</label>
                    <input id="txtIdEndereco" name="txtIdEndereco" value="1" />
                </div>                
                <div style="width: 8%">
                    <label for="txtIdCartao">Id Cartão:</label>
                    <input id="txtIdCartao" name="txtIdCartao" value="" />
                </div>
                <div style="width: 7%">
                    <label for="txtParcelas">Parcelas:</label>
                    <select id="txtParcelas" name="txtParcelas" style="width: 100%;">
                        <?php for($i= 1; $i <= 12; $i++) { ?>
                        <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                        <?php } ?>
                    </select>
                </div>
                <div style="width: 12%">
                    <label for="txtAnuncios">Anuncios:</label>
                    <input id="txtAnuncios" name="txtAnuncios" value="5" />
                </div>
                <div style="width: 12%">
                    <label for="txtQuantidades">Quantidades:</label>
                    <input id="txtQuantidades" name="txtQuantidades" value="1" />
                </div>                
                <div style="width: 12%">
                    <label for="txtIdFrete">Id Frete:</label>
                    <input id="txtIdFrete" name="txtIdFrete" value="" />
                </div>                
                <div style="width: 20%; margin-left: 1%;">
                    <label for="txtCardNome">Nome Cartão:</label>
                    <input id="txtCardNome" name="txtCardNome" value="Fabricio R. Oliveira" />
                </div>
                <div style="width: 20%">
                    <label for="txtCardNumero">Número do Cartão:</label>
                    <input id="txtCardNumero" name="txtCardNumero" value="5401842747353851" />
                </div>
                <div style="margin-top: 16px;">
                    <button id="btnEnviar" name="btnEnviar" style="width: 100px;" type="button">Enviar</button>
                </div>                
                <div style="width: 12%;">
                    <label for="txtCardData">Validade</label>
                    <input id="txtCardData" name="txtCardData" value="02/2023" />
                </div>
                <div style="width: 12%">
                    <label for="txtCardCvv">Dv:</label>
                    <input id="txtCardCvv" name="txtCardCvv" value="651" />
                </div>                
                <div style="width: 13%">
                    <label for="txtSalvarCrt">Salvar Crt.:</label>
                    <select id="txtSalvarCrt" name="txtSalvarCrt" style="width: 100%;">
                        <option value="0">Não</option>
                        <option value="1">Sim</option>
                    </select>
                </div>                
            </div>     
            <div class="borda">
                <p>Dados Bancários:</p>
                <div style="width: 45%">
                    <label for="txtBanco">Banco:</label>
                    <input id="txtBanco" name="txtBanco" value="001"/>
                </div>
                <div style="width: 12%">
                    <label for="txtAgencia">Agencia:</label>
                    <input id="txtAgencia" name="txtAgencia" value="04560" />
                </div>
                <div style="width: 5%">
                    <label for="txtAgenciaDv">DV:</label>
                    <input id="txtAgenciaDv" name="txtAgenciaDv" value="1" />
                </div>
                <div style="width: 12%">
                    <label for="txtConta">Conta:</label>
                    <input id="txtConta" name="txtConta" value="506050" />
                </div>
                <div style="width: 5%">
                    <label for="txtContaDv">DV:</label>
                    <input id="txtContaDv" name="txtContaDv" value="1" />
                </div>
                <div style="width: 25%">
                    <label for="txtTitularNome">Titular:</label>
                    <input id="txtTitularNome" name="txtTitularNome" value="Fabricio R Oliveira" />
                </div>
                <div style="width: 16%">
                    <label for="txtTitularCpf">CPF Titular da Conta:</label>
                    <input id="txtTitularCpf" name="txtTitularCpf" value="121.804.237-05" />
                </div>                
                <div style="width: 12%">
                    <label for="txtTipoConta">Tipo:</label>
                    <input id="txtTipoConta" name="txtTipoConta" value="CHECKING" />
                </div>
                <div style="width: 12%">
                    <label for="txtCompConta">Complemento:</label>
                    <input id="txtCompConta" name="txtCompConta" value="" />
                </div>
                <div style="width: 14%">
                    <label for="txtValorSaque">Valor Saque:</label>
                    <input id="txtValorSaque" name="txtValorSaque" value="1,90" />
                </div>
            </div>
            <div class="borda">
                <p>Estorno:</p>
                <div>
                    <label for="txtIdVenda">Id:</label>
                    <input id="txtIdVenda" name="txtIdVenda" value=""/>
                </div>
                <div style="margin-top: 16px;">
                    <button id="btnEstorno" name="btnEstorno" style="width: 100px;" type="button">Enviar</button>
                </div>                
            </div>
            <div class="borda">
                <p>Processar Safe:</p>
                <div>
                    <label for="txtIdVendaSafe">Id:</label>
                    <input id="txtIdVendaSafe" name="txtIdVendaSafe" value="4"/>
                </div>
                <div style="margin-top: 16px;">
                    <button id="btnSafe" name="btnSafe" style="width: 100px;" type="button">Enviar</button>
                </div>                
            </div>            
        </form>
        <script src="../../assets/js/lib/jquery-3.5.1.js" type="text/javascript"></script>
        <script type="text/javascript">            
            
            $("#btnEnviar").click(function () {
                var formData = new FormData();
                formData.append('btnSalvar', "S");
                formData.append('txtTipoDocumento', $("#txtTipoDocumento").val());                   
                formData.append('txtIdFrete', null);
                formData.append('txtIdPessoa', $("#txtIdPessoa").val());                
                formData.append('txtIdEndereco', $("#txtIdEndereco").val());
                formData.append('txtParcelas', $("#txtParcelas").val());                
                //--------------------------------------------------------------                
                formData.append('txtBanco', $("#txtBanco").val());
                formData.append('txtAgencia', $("#txtAgencia").val());
                formData.append('txtAgenciaDv', $("#txtAgenciaDv").val());
                formData.append('txtConta', $("#txtConta").val());
                formData.append('txtContaDv', $("#txtContaDv").val());
                formData.append('txtTitularNome', $("#txtTitularNome").val());
                formData.append('txtTitularCpf', $("#txtTitularCpf").val());
                formData.append('txtTipoConta', $("#txtTipoConta").val());
                formData.append('txtCompConta', $("#txtCompConta").val());
                formData.append('txtValorSaque', $("#txtValorSaque").val());
                //--------------------------------------------------------------
                formData.append('txtIdCartao', $("#txtIdCartao").val());
                //id_cartao ou dados do cartão abaixo
                formData.append('txtCardNome', $("#txtCardNome").val());
                formData.append('txtCardNumero', $("#txtCardNumero").val());
                formData.append('txtCardData', "01/" + $("#txtCardData").val());
                formData.append('txtCardBandeira', "Visa");
                formData.append('txtCardCvv', $("#txtCardCvv").val());
                formData.append('txtCardHash', "");
                formData.append('txtCardSalvar', $("#txtSalvarCrt").val());
                //--------------------------------------------------------------
                $.each($("#txtAnuncios").val().split(","), function(index, value) {
                    formData.append('txtIdAnuncio[]', value);
                });                            
                $.each($("#txtQuantidades").val().split(","), function(index, value) {
                    formData.append('txtQuantidade[]', value);
                });
                $.ajax({type: "POST", dataType: "json", url: "../../ajax/FormVenda.php", data: formData, processData: false, contentType: false, 
                    error: function (request, status, error) {
                        console.log(request.responseText);
                    }
                }).done(function (data) {
                    $("#txtIdVenda").val(data.id_venda);
                    console.log(data);
                });                                
            });                                    

            $("#btnEstorno").click(function () {
                var formData = new FormData();
                formData.append('btnEstornar', "S");
                formData.append('txtId', $("#txtIdVenda").val());                            
                $.ajax({type: "POST", dataType: "json", url: "../../ajax/FormVenda.php", data: formData, processData: false, contentType: false, 
                    error: function (request, status, error) {
                        console.log(request.responseText);
                    }
                }).done(function (data) {            
                    console.log(data);
                });
            });
            
            $("#btnSafe").click(function () {
                var formData = new FormData();
                formData.append('btnSalvarSafe', "S");
                formData.append('txtId', $("#txtIdVendaSafe").val());                            
                $.ajax({type: "POST", dataType: "json", url: "../../ajax/FormVenda.php", data: formData, processData: false, contentType: false, 
                    error: function (request, status, error) {
                        console.log(request.responseText);
                    }
                }).done(function (data) {            
                    console.log(data);
                });
            });
        </script>    
    </body>    
</html>