<?php include './Connections/configini.php'; ?>
<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title><?php echo $name_page; ?></title>
        <link rel="icon" href="./assets/img/favicon.ico">
        <link href="./assets/css/bootstrap.css" rel="stylesheet">
        <link href="./assets/css/datatables.css" rel="stylesheet">        
        <link href="./assets/css/main.css" rel="stylesheet">        
    </head>
    <body>
        <div class="wraper">
            <aside><?php include './partials/menu-lateral.php'; ?></aside>
            <main>
                <header><?php include './partials/topo.php'; ?></header>
                <section>
                    <div class="container-fluid">
                        <div class="p-3 pt-4">
                            <h1 class="h5"><span class="align-baseline" data-feather="<?php echo $icon_page; ?>"></span> <?php echo $name_page; ?></h1>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="row justify-content-md-end">                                            
                                            <div class="col-auto pl-0">
                                                <label for="txtStatus">Status</label>
                                                <select id="txtStatus" class="form-control form-control-sm">
                                                    <option value="null">Todas</option>
                                                    <option value="A">Aguardando</option>
                                                    <option value="P">Pago</option>
                                                    <option value="N">Negado</option>                                                    
                                                    <option value="C">Cancelado</option>                                                    
                                                    <option value="E">Estornado</option>
                                                </select>
                                            </div>                                            
                                            <div class="col-auto pl-0">
                                                <label for="txtTipo">Tipo</label>
                                                <select id="txtTipo" class="form-control form-control-sm">
                                                    <option value="null">Todas</option>                                                    
                                                    <option value="C">Cartão de Crédito</option>
                                                    <option value="B">Boleto Bancário</option>
                                                    <option value="P">Pagamento em Pix</option>
                                                    <option value="T">Cashback</option>
                                                </select>
                                            </div>                                            
                                            <div class="col-auto pl-0">
                                                <label for="txtEntrega">Entrega</label>
                                                <select id="txtEntrega" class="form-control form-control-sm">
                                                    <option value="null">Todas</option>
                                                    <option value="E">Entregue</option>
                                                    <option value="P">Pendente</option>
                                                </select>
                                            </div>                                            
                                            <div class="col-auto pl-0">
                                                <label for="txtPesquisar">Pesquisar</label>
                                                <input id="txtPesquisar" type="text" class="form-control form-control-sm" value="">
                                            </div>                                            
                                            <div class="col-auto align-self-end pl-0">
                                                <button id="btnBuscar" type="button" class="btn btn-success btn-sm" title="Buscar"><span data-feather="search"></span></button>
                                            </div>
                                        </div>
                                        <hr>
                                        <table id="tbVendas" class="table table-striped table-bordered dt-responsive w-100 mt-3">
                                            <thead class="thead-light">
                                                <tr>
                                                    <th>Pedido</th>
                                                    <th>Data</th>
                                                    <th>Cliente</th>
                                                    <th>Status</th>                                                    
                                                    <th>Valor</th>
                                                    <th>Tipo</th>
                                                    <th>Entrega/Uso</th>
                                                    <th>Ação</th>
                                                </tr>
                                            </thead>
                                            <tbody></tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php include './includes/modal/mdTransferencia.php'; ?>
                    </div>
                </section>
            </main>
        </div>
        <?php include './partials/lib-js.php'; ?>
        <script src="assets/js/vendas.js" type="text/javascript"></script>
    </body>
</html>