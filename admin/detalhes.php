<?php include './Connections/configini.php'; ?>
<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title><?php echo $name_page; ?></title>
        <link rel="icon" href="./assets/img/favicon.ico">
        <link href="./assets/css/bootstrap.css" rel="stylesheet">
        <link href="./assets/css/datatables.css" rel="stylesheet">
        <link href="./assets/css/main.css" rel="stylesheet">
    </head>
    <body>
        <div class="wraper">
            <aside><?php include './partials/menu-lateral.php'; ?></aside>
            <main>
                <header><?php include './partials/topo.php'; ?></header>
                <section>
                    <div class="container-fluid">
                        <div class="p-3 pt-4">
                            <h1 class="h5"><span class="align-baseline" data-feather="<?php echo $icon_page; ?>"></span> <?php echo $name_page; ?></h1>
                        </div>                    
                        <div class="row">
                            <div class="col-md-12">
                                <div class="stepwizard">
                                    <div class="stepwizard-row setup-panel">
                                        <div class="stepwizard-step">
                                            <span id="opPedido" class="btn rounded-circle">1</span>
                                            <p>Pedido</p>
                                        </div>
                                        <div class="stepwizard-step">
                                            <span id="opPagamento" class="btn rounded-circle">2</span>
                                            <p>Pagamento</p>
                                        </div>
                                        <div class="stepwizard-step">
                                            <span id="opEntregue" class="btn rounded-circle">3</span>
                                            <p>Entregue</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-3">
                            <div class="col-md-8">
                                <div class="card h-100">
                                    <div class="card-body">
                                        <h6 class="card-title mb-2">ITENS DO PEDIDO: <span id="lblId"></span><span id="lblData" class="float-right"></span></h6>
                                        <table id="tbItens" class="table table-bordered dt-responsive w-100 mt-3">
                                            <thead class="thead-light">
                                                <tr>                                                    
                                                    <th style="width: 50%;">Descrição</th>
                                                    <th style="width: 10%;">Quantidade</th>
                                                    <th style="width: 20%;">Valor</th>
                                                    <th style="width: 20%;">Total</th>
                                                </tr>
                                            </thead>
                                            <tbody></tbody>
                                            <tfoot>
                                                <tr>
                                                    <th colspan="3">Total</th>
                                                    <th>R$ 0,00</th>
                                                </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="card h-100">
                                    <div class="card-body">
                                        <h6 class="card-title mb-2">RESUMO DO PEDIDO:</h6>
                                        <table id="tbParcelas" class="table table-bordered dt-responsive w-100 mt-3">
                                            <thead class="thead-light">
                                                <tr>                                                    
                                                    <th style="width: 70%;">Descrição</th>
                                                    <th style="width: 30%;">Valor</th>
                                                </tr>
                                            </thead>
                                            <tbody></tbody>
                                            <tfoot>
                                                <tr>
                                                    <th>Total Líquido</th>
                                                    <th>R$ 0,00</th>
                                                </tr>
                                            </tfoot>                                            
                                        </table>                                        
                                    </div>
                                </div>                                
                            </div>
                        </div>                     
                        <div class="row mt-3">
                            <div class="col-md-4">
                                <div class="card h-100">
                                    <div class="card-body">
                                        <h6 class="card-title mb-2">INFORMAÇÃO DE ENVIO:</h6>
                                        <div id="dvEnvio"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="card h-100">
                                    <div class="card-body">
                                        <h6 class="card-title mb-2">INFORMAÇÕES DE PAGAMENTO:</h6>
                                        <div id="dvPagamento"></div>                                        
                                    </div>
                                </div>                                
                            </div>
                            <div class="col-md-4">
                                <div class="card h-100">
                                    <div class="card-body">
                                        <h6 class="card-title mb-2">INFORMAÇÕES DE ENTREGA:</h6>
                                        <div id="dvEntrega" class="text-center"></div>                                        
                                    </div>
                                </div>                                
                            </div>
                        </div>                     
                    </div>
                </section>
            </main>
        </div>
        <?php include './partials/lib-js.php'; ?>
        <script src="assets/js/detalhe.js" type="text/javascript"></script>        
    </body>
</html>