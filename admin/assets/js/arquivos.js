
$('#FormArquivo').on('show.bs.modal', function (event) {
    let button = $(event.relatedTarget);
    $("#formCadastroArquivo #txtType").val(button.data('type'));
    $("#formCadastroArquivo #txtIdItem").val(button.data('item'));
});

$("#btnArquivo").click(function () {
    if ($("#formCadastroArquivo").valid()) {
        $(".boxLoader").show();
        var formData = new FormData();
        formData.append('btnArquivo', "S");
        formData.append('txtId', $("#txtId").val()); 
        formData.append('txtType', $("#txtType").val());
        formData.append('txtIdItem', $("#txtIdItem").val());
        for (let i = 0; i < $('#txtArquivo')[0].files.length; i++) {
            formData.append('txtArquivo[]', $('#txtArquivo')[0].files[i]);
        }
        $.ajax({type: "POST", dataType: "json", url: "./ajax/FormArquivos.php", data: formData, processData: false, contentType: false, 
            error: function (request, status, error) {
                if ($("#txtType").val() === "pFotos" || $("#txtType").val() === "lFotos" || $("#txtType").val() === "videos") {
                    refreshImagem();                    
                }
                $(".boxLoader").hide();            
                bootbox.alert(request.responseText + "!");
            }
        }).done(function (data) {            
            refreshPage(data, $("#txtType").val());
            $(".boxLoader").hide();            
            $('#FormArquivo').modal('hide');
        });
    }
});

function refreshPage(data, type) {
    if (type === "perfil" && data.length > 0) {
        $(".img-thumbnail, .avatar").attr("src", "assets/imagens/pessoas/" + $("#sessionId").val() + "/" + data[0]);
    } else if (type === "produto" && data.length > 0) {        
        $(".img-thumbnail").attr("src", "assets/imagens/produtos/" + $("#txtId").val() + "/" + data[0]);
    } else if (type === "empresa" && data.length > 0) {        
        $(".img-thumbnail").attr("src", "assets/imagens/empresas/" + $("#txtId").val() + "/" + data[0]);
    } else if ((type === "pFotos" || type === "lFotos" || type === "videos") && data.length > 0) {
        refreshImagem();
    } else if (type === "documentos" && data.length > 0) {
        buscarDocumentos();
    }
}

let ruleformArquivo = {
    rules: {
        txtType: "required",
        txtArquivo: "required"
    },
    messages: {
        txtType: "Tipo Inválido",
        txtArquivo: "Arquivo Inválido"
    }};

createValidate("#formCadastroArquivo", ruleformArquivo);