
$("#tab2-tab").addClass("disabled");
$("#formEmpresa input, select, textarea").attr("disabled", true);
$("#btnAlterar, #btnAlterarImg, #btnAdicionarImgs").attr("disabled", false);
$("#btnProximo, #btnCancelar, #imageDirAdd, #imageDirDel").hide();
$("#txtCnpj").mask("99.999.999/9999-99");
$("#txtBanco").mask("999");
$("#txtAgencia").mask("9999999");
$("#txtConta").mask("9999999999");
$("#txtAgenciaDv, #txtContaDv").mask("9");

request('./ajax/FormEmpresa.php', 'post', {btnLoja: 'S'}, function (data) {
    if (data) {
        $("h4").html(getVal(data, "razao_social"));
        $("#formEmpresa #txtId").val(data.id);
        $("#formEmpresa #txtRazaoSocial").val(data.razao_social);
        $("#formEmpresa #txtNomeFantasia").val(data.nome_fantasia);
        $("#formEmpresa #txtCnpj").val(data.cnpj);
        $("h4").next().html(getVal(data, "cnpj"));
        $("#formEmpresa #txtIdEndereco").val(data.endereco.id);
        $("#formEmpresa #txtCep").val(data.endereco.cep);
        $("#formEmpresa #txtEndereco").val(data.endereco.endereco);
        $("#formEmpresa #txtBairro").val(data.endereco.bairro);
        $("#formEmpresa #txtComplemento").val(data.endereco.complemento);
        $("#formEmpresa #txtNumero").val(data.endereco.numero);
        $("#formEmpresa #txtEstado").attr("value", data.endereco.id_cidade.cidade_codigoEstado.estado_codigo);
        $("#formEmpresa #txtCidade").attr("value", data.endereco.id_cidade.cidade_codigo);
        $("h4").next().next().html(getStatus(getVal(data, "safe_id")));
        $("#formEmpresa #txtIdBanco").val(data.banco.id);
        $("#formEmpresa #txtBanco").attr("value", data.banco.banco);
        $("#formEmpresa #txtAgencia").val(data.banco.agencia);
        $("#formEmpresa #txtAgenciaDv").val(data.banco.agencia_dv);
        $("#formEmpresa #txtConta").val(data.banco.conta);
        $("#formEmpresa #txtContaDv").val(data.banco.conta_dv);
        $("#formEmpresa #txtTipoConta").val(data.banco.tipo);
        $("#imageNow").val((data.id_foto === null ? "" : data.id_foto));
        $(".img-thumbnail").attr("src", (data.id_foto === null ? "assets/img/no_image.png" : "assets/imagens/empresas/" + data.id + "/" + data.id_foto));        
        refreshImagem();
    } else {
        $("#btnAlterar").click();
    }
    buscarEstados();
});

$("#btnAlterar").click(function () {
    $("#formEmpresa input, select, textarea").attr("disabled", false);
    $("#" + ($(".img-thumbnail").attr("src") === "assets/img/no_image.png" ? "imageDirAdd" : "imageDirDel")).show();
    $("#btnProximo, #btnCancelar").show();
    $("#btnAlterar").hide();
});

$('.nav-link').click(function (e) {
    $("#btnProximo").html((e.currentTarget.id === "tab2-tab" ? "Finalizar" : "Próximo"));
});

$("#btnProximo").click(function () {
    let tab = $("#v-pills-tab .nav-link.active").attr("href");
    if (tab === "#tab1" && $("#formEmpresa").valid()) {
        request('./ajax/FormEmpresa.php', 'post', Object_assign(objForm("#formEmpresa"), {btnSalvar: 'S'}), function (data) {
            $("#txtId").val(data.id_empresa);
            $("#txtIdEndereco").val(data.id_endereco);
            $("#txtIdBanco").val(data.id_banco);
            $("#tab2-tab").removeClass("disabled");
            $("#tab2-tab").click();
        });
    } else if (tab === "#tab2") {
        $("#tab1-tab").removeClass("disabled");
        $("#tab1-tab").click();
    }
});

$("#btnCancelar").click(function () {
    document.location.reload(true);
});

function getStatus(value) {
    if ($.isNumeric(value)) {
        return `<span class="badge badge-pill badge-primary">Loja Ativa</span>`;
    } else {
        return `<span class="badge badge-pill badge-warning">Aguardando Cadastro</span>`;
    }
}

// <editor-fold defaultstate="collapsed" desc="Imagens">

function refreshImagem() {
    if ($.isNumeric($("#txtId").val())) {
        request('./ajax/FormArquivos.php', 'post', {lerArquivo: 'S', txtId: $("#txtId").val(), txtType: 'lFotos'}, function (data) {
            $("#dvImagens").html(data.map(function (value, key) {
                return `<div class="col-md-3">
                            <div class="card mb-3 border">                                                                                        
                                <div class="dropdown position-absolute">
                                    <button class="btn btn-sm btn-secondary" type="button" id="dropdown_${key}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <span data-feather="align-justify"></span>
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="dropdown_${key}">
                                        <a class="dropdown-item" href="javascript:void(0)" onclick="excluirImagem('${value.id}');"><span data-feather="trash"></span> Excluir</a>                                                        
                                    </div>
                                </div>                                
                                <` + (value.id.endsWith('.mp4') ? `video` : `img`) + ` src="assets/imagens/empresas/${$("#txtId").val()}/list/${value.id}" alt="" data-holder-rendered="true" class="card-img-top">
                            </div>
                        </div>`;
            }).join(''));
            feather.replace();
        }, function (erro) {
            bootbox.alert(erro.responseText + "!");
        });
    }
}

function excluirImagem(img) {
    bootbox.confirm({message: "Confirma excluir este registro?",
        callback: function (result) {
            if (result) {
                request('./ajax/FormArquivos.php', 'post', {delArquivo: 'S', txtId: $("#txtId").val(), txtType: 'lFotos', txtFile: img,
                    txtCapa: (img === $(".img-thumbnail").attr("src").split("/").pop() ? "S" : "N")}, function (data) {
                    if (img === $(".img-thumbnail").attr("src").split("/").pop()) {
                        $(".img-thumbnail").attr("src", "assets/img/no_image.png");
                    }
                    refreshImagem();
                }, function (erro) {
                    bootbox.alert(erro.responseText + "!");
                });
            }
        }
    });
}

// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="Validate">

let ruleformEmpresa = {
    rules: {
        txtCnpj: "cpf",        
        txtRazaoSocial: {
            required: true,
            minlength: 3
        },
        txtNomeFantasia: {
            required: true,
            minlength: 3
        },
        txtBanco: "required",
        txtAgencia: {
            required: true,
            minlength: 4
        },
        txtAgenciaDv: "required",
        txtConta: {
            required: true,
            minlength: 4
        },
        txtContaDv: "required",
        txtTipoConta: "required"
    },
    messages: {
        txtCnpj: "Documento inválido",        
        txtRazaoSocial: "Preencha corretamente",
        txtNomeFantasia: "Preencha corretamente",
        txtBanco: "Selecione um Banco",
        txtAgencia: "Agência inválida",
        txtAgenciaDv: "Inválido",
        txtConta: "Conta inválida",
        txtContaDv: "Inválido",
        txtTipoConta: "Tipo inválido"        
    }};

createValidate("#formEmpresa", ruleformEmpresa, ruleformPessoaEndereco);

// </editor-fold>