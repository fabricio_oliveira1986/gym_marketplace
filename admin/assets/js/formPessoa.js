
$("#txtDtNasc").mask("99/99/9999");
$("#txtCPF").mask("999.999.999-99");
$("#txtCelular").mask("(99) 99999-9999");
$("#btnEye").hide();

$("#btnEye").on("click", function () {
    $("#btnEye").hide();
    $("#btnEyeOff").show();
    $("#txtSenha").prop("type", "password");
});

$("#btnEyeOff").on("click", function () {
    $("#btnEye").show();
    $("#btnEyeOff").hide();
    $("#txtSenha").prop("type", "text");
});

let ruleformPessoa = {
    rules: {
        txtNome: {
            required: true,
            minlength: 3
        },
        txtDtNasc: "data",
        txtCPF: "cpf",
        txtCelular: {
            required: true,
            minlength: 15
        },
        txtEmail: {
            required: true,
            email: true
        },
        txtSenha: {
            required: true,
            minlength: 5
        }
    },
    messages: {
        txtNome: "Preencha o nome corretamente",
        txtCelular: "Celular inválido",
        txtEmail: "E-mail inválido",
        txtSenha: "Senha inválida, mínimo 5 caracteres"
    }};