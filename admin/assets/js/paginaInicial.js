
$("#txtValor").priceFormat({prefix: "", centsSeparator: ",", thousandsSeparator: "."});

am4core.ready(function () {
    request('./ajax/FormVenda.php', 'post', {btnVendasTipo: 'S'}, function (data) {
        am4core.useTheme(am4themes_animated);
        let chart = am4core.create("chartdiv", am4charts.PieChart);
        chart.data = data;
        chart.legend = new am4charts.Legend();        
        chart.language.locale["_decimalSeparator"] = ",";
        chart.language.locale["_thousandSeparator"] = ".";
        let pieSeries = chart.series.push(new am4charts.PieSeries());
        pieSeries.dataFields.value = "total";
        pieSeries.dataFields.category = "descricao";
        pieSeries.labels.template.text = "{value.percent.formatNumber('#.0')}%";        
        pieSeries.slices.template.propertyFields.fill = "color";        
        pieSeries.slices.template.stroke = am4core.color("#fff");
        pieSeries.slices.template.strokeWidth = 2;
        pieSeries.slices.template.strokeOpacity = 1;
        pieSeries.hiddenState.properties.opacity = 1;
        pieSeries.hiddenState.properties.endAngle = -90;
        pieSeries.hiddenState.properties.startAngle = -90;
        $("#id-66-title").parent().hide();
    });
});

function saldoTotal() {
    request('./ajax/FormVenda.php', 'post', {btnTotalVendas: 'S'}, function (data) {
        if (data) {
            $("#txtTotalVendas").html(numberFormat(data.total, 1));
            $("#txtTotalVendasNum").html(data.itens);
        }
    });
}

function saldoProdutos() {
    request('./ajax/FormProduto.php', 'post', {btnProdutosAtivos: 'S'}, function (data) {
        data.forEach(function (item, i) {
            $("#txtAnuncioAtivo" + item.tipo).html(item.total);
        });
    });
}

function saldoReceber() {
    request('./ajax/FormVenda.php', 'post', {btnVendasReceber: 'S'}, function (data) {
        if (data) {
            $("#txtReceber").html(numberFormat(data.total, 1));
            $("#txtReceberTotal").html(data.itens);
        }
    });
}

function saldoAtual() {
    $("#btnSaque").attr("disabled", true);
    request('./ajax/FormVenda.php', 'post', {btnVendasRecebidas: 'S'}, function (data) {
        if (data) {
            $("#txtRecebidas").html(numberFormat(data.total, 1));
            $("#txtRecebidasSaque").html(numberFormat(data.liberado, 1));
        }
    });
}

// <editor-fold defaultstate="collapsed" desc="Extrato">

$('#tbExtrato').dataTable({
    "iDisplayLength": 20,
    "aLengthMenu": [20, 40, 100],
    "ordering": false,
    "bFilter": false,
    "bProcessing": true,
    "bServerSide": true,
    "sAjaxSource": finalFind(),
    "aoColumns": [
        {data: function (row, type, set) {
                return `<b>#${("0000000" + row.id).slice(-7)}</b>`;
            }},
        {data: function (row, type, set) {
                return moment(row.data, "YYYY-MM-DD hh:mm:ss").format('DD/MM/YYYY HH:mm');
            }},
        {data: function (row, type, set) {
                return `<img src="${(row.id_foto === null ? "assets/img/no_person.png" : "assets/imagens/pessoas/" + row.id_pessoa + "/" + row.id_foto)}" alt="img" title="contact-img" class="rounded-circle me-3" height="30"> ${compactName(row.nome)}`;
            }},
        {data: function (row, type, set) {
                return returnStatus(row.status);
            }},
        {data: function (row, type, set) {
                return returnTipo(row.tipo_documento);
            }},
        {data: function (row, type, set) {
                return numberFormat((row.tipo_documento === "S" ? 0 : (row.tipo_documento === "T" ? (row.valor_total * -1) : row.valor_total)), 1);
            }},
        {data: function (row, type, set) {
                return numberFormat((row.tipo_documento === "S" ? (row.valor * -1) : (row.tipo_documento === "T" ? ((row.valor_total * -1) - row.valor) : (row.valor_total - row.valor))), 1);
            }},
        {data: function (row, type, set) {
                return numberFormat(row.valor, 1);
            }}
    ],
    'oLanguage': oLanguage(),
    "sPaginationType": "full_numbers",
    "fnDrawCallback": function (data) {
        saldoTotal();
        saldoProdutos();
        saldoReceber();
        saldoAtual();
        feather.replace();        
    }
});

function finalFind() {
    return "ajax/extrato_server_processing.php";
}

function refreshTable() {
    let table = $('#tbExtrato').dataTable();
    table.fnReloadAjax(finalFind());
}

$("#btnAnuncio").click(function () {
    window.location.href = 'produto.php';
});

// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="Transferência">

let ruleformTransf = {
    rules: {
        txtValor: "required"
    },
    messages: {
        txtValor: "Valor Inválido"
    }};

createValidate("#FormCadastroTransf", ruleformTransf);

$('#FormTransf').on('show.bs.modal', function (event) {
    let valor = (textToNumber($("#txtRecebidasSaque").html()) - 4.9);
    $("#FormCadastroTransf #txtNome").val("Valor disponível " + numberFormat((valor > 0 ? valor : 0),1));
    $("#FormCadastroTransf #txtNomeDetalhe").html("Custo de transferência R$ 4,90.");
    $("#FormCadastroTransf #txtValor").val(numberFormat(0));
});

$("#btnTransf").click(function () {
    if ($("#FormCadastroTransf").valid() && textToNumber($("#txtValor").val()) > 0 
        && textToNumber($("#txtRecebidas").html()) >= textToNumber($("#txtValor").val())
        && textToNumber($("#txtRecebidasSaque").html()) >= textToNumber($("#txtValor").val())) {
        request('./ajax/FormVenda.php', 'post', Object_assign(objForm("#FormCadastroTransf"), {btnSalvarTransf: 'S'}), function (data) {
            refreshTable();
            $('#FormTransf').modal('hide');            
            bootbox.alert("Uma requisição de transferência foi gerada, aguarde a transferência de crédito na conta informada!");            
        });
    } else {
        bootbox.alert("Valores inválidos!");
    }
});

// </editor-fold>