
$("#btnBuscar").click(function () {
    refreshTable();
});

$("#btnProduto").click(function () {
    window.location.href = 'produto.php';
});

request('./ajax/FormProduto.php', 'post', {btnProdutoPendentes: 'S'}, function (data) {
    $(".alert").hide();    
    if (data > 0) {
        $(".alert").html("Anúncios a publicar " + data);
        $(".alert").show();
    }
});

function excluir(id) {
    bootbox.confirm({message: "Confirma excluir este registro?",
        callback: function (result) {
            if (result) {
                request('./ajax/FormProduto.php', 'post', {btnDeleteProduto: 'S', txtId: id}, function (data) {
                    if (data === "YES") {
                        refreshTable();
                    }
                }, function (erro) {
                    bootbox.alert(erro.responseText + "!");
                });
            }
        }
    });
}

$('#tbProdutos').dataTable({
    "iDisplayLength": 20,
    "aLengthMenu": [20, 40, 100],
    "ordering": false,
    "bFilter": false,
    "bProcessing": true,
    "bServerSide": true,
    "sAjaxSource": finalFind(),
    "aoColumns": [
        {data: function (row, type, set) {
                return `<img src="${row.id_foto === null ? "assets/img/no_image.png" : "assets/imagens/produtos/" + row.id + "/" + row.id_foto}" alt="img" title="contact-img" class="rounded me-3" height="30"> ${row.nome}`;
            }},
        {data: "subtitulo"},
        {data: function (row, type, set) {
                return numberFormat(row.valor, 1);
            }},
        {data: function (row, type, set) {
                return row.vendas + `/` + row.quantidade;
            }},
        {data: function (row, type, set) {
                return (row.inativo === "0" ? `<span class="badge badge-primary">Ativo</span>` : `<span class="badge badge-warning">Publicar</span>`);
            }},
        {data: function (row, type, set) {
                return `<a href="produto.php?id=${row.id}"><span data-feather="edit-2"></span></a>
            <a href="javascript:void(0)" onclick="excluir(${row.id});"><span data-feather="trash"></span></a>`;
            }}
    ],
    'oLanguage': oLanguage(),
    "sPaginationType": "full_numbers",
    "fnDrawCallback": function (data) {
        feather.replace();
    }
});

function finalFind() {
    return "ajax/produtos_server_processing.php?sSearch=" + $("#text_Filter").val() + "&tipo=" + $("#txtTipo").val();
}

function refreshTable() {
    let table = $('#tbProdutos').dataTable();
    table.fnReloadAjax(finalFind());
}