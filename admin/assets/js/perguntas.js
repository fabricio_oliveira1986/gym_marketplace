
$("#btnBuscar").click(function () {
    refreshTable();
});

$('#tbPerguntas').dataTable({
    "iDisplayLength": 20,
    "aLengthMenu": [20, 40, 100],
    "ordering": false,
    "bFilter": false,
    "bProcessing": true,
    "bServerSide": true,
    "sAjaxSource": finalFind(),
    "aoColumns": [
        {data: function (row, type, set) {
            return moment(row.data, "YYYY-MM-DD hh:mm:ss").format('DD/MM/YYYY HH:mm');
        }},               
        {data: function (row, type, set) {
            return `<img src="${row.foto_produto === null ? "assets/img/no_image.png" : "assets/imagens/produtos/" + row.id_produto + "/" + row.foto_produto}" alt="img" title="contact-img" class="rounded me-3" height="30"> ${row.nome_produto} - ${row.descricao}`;
        }},      
        {data: function (row, type, set) {
            return `<img src="${(row.id_foto === null ? "assets/img/no_person.png" : "assets/imagens/pessoas/" + row.id_pessoa + "/" + row.id_foto)}" alt="img" title="contact-img" class="rounded-circle me-3" height="30"> ${compactName(row.nome)}`;
        }},                        
        {data: "pergunta"},
        {data: function (row, type, set) {
            return returnStatus(row.data_resposta, row.inativo);
        }},
        {data: function (row, type, set) {
            return `<a href="javascript:void(0)" data-toggle="modal" data-target="#FormPergunta" data-id="${row.id}"><span data-feather="edit-2"></span></a>
            ${row.inativo === "0" ? `<a href="javascript:void(0)" onclick="excluir(${row.id});"><span data-feather="trash"></span></a>` : ``}`;            
        }}
    ],
    'oLanguage': oLanguage(),
    "sPaginationType": "full_numbers",
    "fnDrawCallback": function (data) {
        feather.replace();
    }
});

function finalFind() {
    return "ajax/perguntas_server_processing.php?sSearch=" + $("#txtPesquisar").val() + "&tipo=" + $("#txtTipo").val();
}

function refreshTable() {
    let table = $('#tbPerguntas').dataTable();
    table.fnReloadAjax(finalFind());
    request('./ajax/FormProduto.php', 'post', {btnTotRespostas: 'S', txtTipo : 0}, function (data) {
        $('a[href$="perguntas.php"]').find(".badge").html(data.total);
        if (data.total === "0") {
            $('a[href$="perguntas.php"]').find(".badge").hide();
        } else {
            $('a[href$="perguntas.php"]').find(".badge").show();
        }
    });
}

function returnStatus(tp, inativo) {
    if (inativo === "1") {
        return `<span class="badge badge-pill badge-danger">Cancelado</span>`;                
    } else if (tp === null) {
        return `<span class="badge badge-pill badge-warning">Aguardando</span>`;        
    } else {
        return `<span class="badge badge-pill badge-success">Respondido</span>`;        
    }
}

let ruleformResposta = {
    rules: {
        txtDescricao: {
            required: true,
            minlength: 3
        }
    },
    messages: {
        txtNome: "Preencha a Resposta"
    }};

createValidate("#FormCadastroPergunta", ruleformResposta);

$('#FormPergunta').on('show.bs.modal', function (event) {
    let obj = getObjTable("tbPerguntas", event);
    $("#FormCadastroPergunta #txtId").val(getVal(obj, "id"));
    $("#FormCadastroPergunta #txtAnuncios").html(`<img src="${obj.foto_produto === null ? "assets/img/no_image.png" : "assets/imagens/produtos/" + obj.id_produto + "/" + obj.foto_produto}" alt="img" title="contact-img" class="rounded me-3" height="30"> ${obj.nome_produto} - ${obj.descricao}`);
    $("#FormCadastroPergunta #txtPergunta").html(getVal(obj, "pergunta"));
    $("#FormCadastroPergunta #txtResposta").val(getVal(obj, "resposta"));
});

$("#btnPergunta").click(function () {
    if ($("#FormCadastroPergunta").valid()) {
        request('./ajax/FormProduto.php', 'post', Object_assign(objForm("#FormCadastroPergunta"), {btnSalvarPergunta: 'S'}), function (data) {
            $('#FormPergunta').modal('hide');
            refreshTable();
        });
    }    
});

function excluir(id) {
    bootbox.confirm({message: "Confirma cancelar este registro?",
        callback: function (result) {
            if (result) {
                request('./ajax/FormProduto.php', 'post', {btnDeletePergunta: 'S', txtIdPergunta: id}, function (data) {
                    if (data === "YES") {
                        refreshTable();
                    }
                }, function (erro) {
                    bootbox.alert(erro.responseText + "!");
                });
            }
        }
    });
}