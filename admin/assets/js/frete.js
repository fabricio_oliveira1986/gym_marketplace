
$("#txtValor").priceFormat({prefix: "", centsSeparator: ",", thousandsSeparator: "."});

$("#btnBuscar").click(function () {
    refreshTable();
});

$("#btnLink").click(function () {
    let url = "https://descubra.melhorenvio.com.br/correios/?utm_source=google&utm_medium=cpm&utm_campaign=google&gclid=CjwKCAjwnPOEBhA0EiwA609ReTA1CECa3gT4taRa15W6CY4PfvNeJ_P8S1hm2hiH9hQ0WkuqyGYkLhoCTD4QAvD_BwE";
    window.open(url,'_blank');
});

$('#tbFretes').dataTable({
    "iDisplayLength": 20,
    "aLengthMenu": [20, 40, 100],
    "ordering": false,
    "bFilter": false,    
    "bProcessing": true,
    "bServerSide": true,
    "sAjaxSource": finalFind(),
    "aoColumns": [
        {data: "nome"},
        {data: "descricao"},
        {data: function (row, type, set) {
            return numberFormat(row.valor, 1);
        }},
        {data: function (row, type, set) {
            return (row.inativo === "0" ? "Não" : "Sim");
        }},
        {data: function (row, type, set) {
            return `<a href="javascript:void(0)" data-toggle="modal" data-target="#FormFrete" data-id="${row.id}"><span data-feather="edit-2"></span></a>
            <a href="javascript:void(0)" onclick="excluir(${row.id});"><span data-feather="trash"></span></a>`;
        }}
    ],
    'oLanguage': oLanguage(),
    "sPaginationType": "full_numbers",
    "fnDrawCallback": function (data) {
        feather.replace();
    }
});

function finalFind() {
    return "ajax/frete_server_processing.php?sSearch=" + $("#text_Filter").val();
}

function refreshTable() {
    let table = $('#tbFretes').dataTable();
    table.fnReloadAjax(finalFind());
}

let ruleformFrete = {
    rules: {
        txtNome: {
            required: true,
            minlength: 3
        }, 
        txtValor: "required"
    },
    messages: {
        txtNome: "Preencha o nome corretamente",
        txtValor: "Valor Inválido"
    }};

createValidate("#FormCadastroFrete", ruleformFrete);

$('#FormFrete').on('show.bs.modal', function (event) {
    let obj = getObjTable("tbFretes", event);
    $("#FormCadastroFrete #txtId").val(getVal(obj, "id"));
    $("#FormCadastroFrete #txtNome").val(getVal(obj, "nome"));
    $("#FormCadastroFrete #txtValor").val(numberFormat(getVal(obj, "valor")));
    $("#FormCadastroFrete #txtDescricao").val(getVal(obj, "descricao"));
    $("#FormCadastroFrete #txtInativo").prop("checked", (getVal(obj, "inativo") === "1" ? true : false));
});

$("#btnFrete").click(function () {
    if ($("#FormCadastroFrete").valid()) {
        request('./ajax/FormProduto.php', 'post', Object_assign(objForm("#FormCadastroFrete"), {btnSalvarFrete: 'S'}), function (data) {
            $('#FormFrete').modal('hide');
            refreshTable();
        });
    }    
});

function excluir(id) {
    bootbox.confirm({message: "Confirma excluir este registro?",
        callback: function (result) {
            if (result) {
                request('./ajax/FormProduto.php', 'post', {btnDeleteFrete: 'S', txtIdFrete: id}, function (data) {
                    if (data === "YES") {
                        refreshTable();
                    }
                }, function (erro) {
                    bootbox.alert(erro.responseText + "!");
                });
            }
        }
    });
}