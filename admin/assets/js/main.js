
$(document).on("keydown", "form", function(event) { 
    return event.key !== "Enter";
});

$(".custom-file-input").on("change", function () {
    let fileName = $(this).val().split("\\").pop();
    $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
});

$('.button-menu-mobile').on('click', function (e) {
    e.stopPropagation();
    $('body').toggleClass('menu-active');
});

$('main').on('click', function (e) {
    $('body').removeClass('menu-active');
});

feather.replace();
$('[data-toggle="tooltip"]').tooltip();

function compactName(name) {
    return name.split(/(\s).+\s/).join("");
}

function objForm(name) {
    let returnArray = {};
    let formArray = $(name).serializeArray();
    for (let i = 0; i < formArray.length; i++) {
        returnArray[formArray[i]['name']] = formArray[i]['value'];
    }
    return returnArray;
}

function Object_assign(target, ...sources) {
    sources.forEach(source => {
        Object.keys(source).forEach(key => {
            let s_val = source[key], t_val = target[key];
            target[key] = t_val && s_val && typeof t_val === 'object' && typeof s_val === 'object' ? Object_assign(t_val, s_val) : s_val;
        });
    });
    return target;
}

function request(url, method, data, success, error = null, type = 'json', options = {}) {
    $(".boxLoader").show();
    $.ajax({url: url, method: method, dataType: type, data: data, success: success, error: function (request, status, error) {
            $(".boxLoader").hide();
            bootbox.alert(request.responseText + "!");
        }, ...options}).done(function () {
        $(".boxLoader").hide();
    });
}

// <editor-fold defaultstate="collapsed" desc="Validate">

function createValidate(form, ...options) {
    let base = {
        errorElement: "em",
        errorPlacement: function (error, element) {
            error.addClass("invalid-feedback");
            if (element.prop("type") === "checkbox") {
                error.insertAfter(element.next("label"));
            } else {
                error.insertAfter(element);
            }
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass("is-invalid").removeClass("is-valid");
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).addClass("is-valid").removeClass("is-invalid");
        }
    };
    $(form).validate(Object_assign(base, ...options));
}

jQuery.validator.addMethod('cpf', function validate_cpf(value, element) {
    if (value) {
        return val_cpf(value) || val_cnpj(value);
    }
}, 'Documento inválido!');

function val_cnpj(val) {
    if (val.match(/^\d{2}\.\d{3}\.\d{3}\/\d{4}\-\d{2}$/) != null) {
        let val1 = val.substring(0, 2);
        let val2 = val.substring(3, 6);
        let val3 = val.substring(7, 10);
        let val4 = val.substring(11, 15);
        let val5 = val.substring(16, 18);
        let i;
        let number;
        let result = true;
        number = (val1 + val2 + val3 + val4 + val5);
        s = number;
        c = s.substr(0, 12);
        let dv = s.substr(12, 2);
        let d1 = 0;
        for (i = 0; i < 12; i++)
            d1 += c.charAt(11 - i) * (2 + (i % 8));
        if (d1 == 0)
            result = false;
        d1 = 11 - (d1 % 11);
        if (d1 > 9)
            d1 = 0;
        if (dv.charAt(0) != d1)
            result = false;
        d1 *= 2;
        for (i = 0; i < 12; i++) {
            d1 += c.charAt(11 - i) * (2 + ((i + 1) % 8));
        }
        d1 = 11 - (d1 % 11);
        if (d1 > 9)
            d1 = 0;
        if (dv.charAt(1) != d1)
            result = false;
        return result;
    }
    return false;
}

function val_cpf(strCPF) {
    if (strCPF) {
        strCPF = strCPF.replace('.', '');
        strCPF = strCPF.replace('.', '');
        strCPF = strCPF.replace('-', '');
        let cpf = strCPF;
        let numeros, digitos, soma, i, resultado, digitos_iguais;
        digitos_iguais = 1;
        if (cpf.length < 11)
            return false;
        for (i = 0; i < cpf.length - 1; i++)
            if (cpf.charAt(i) != cpf.charAt(i + 1)) {
                digitos_iguais = 0;
                break;
            }
        if (!digitos_iguais) {
            numeros = cpf.substring(0, 9);
            digitos = cpf.substring(9);
            soma = 0;
            for (i = 10; i > 1; i--)
                soma += numeros.charAt(10 - i) * i;
            resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
            if (resultado != digitos.charAt(0))
                return false;
            numeros = cpf.substring(0, 10);
            soma = 0;
            for (i = 11; i > 1; i--)
                soma += numeros.charAt(11 - i) * i;
            resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
            if (resultado != digitos.charAt(1))
                return false;
            return true;
        } else
            return false;
    }
}

jQuery.validator.addMethod('data', function validate_data(value, element) {
    if (value.length < 10)
        return false;
    return moment(value, 'DD/MM/YYYY').isValid();
}, 'Data inválida!');

jQuery.validator.addMethod("cep", function (value, element) {
    return this.optional(element) || /^[0-9]{2}.[0-9]{3}-[0-9]{3}$/.test(value);
}, "CEP inválido");

// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="Numeros">

function numberFormat(num, ident = 0) {
    x = 0;
    if (num < 0) {
        num = Math.abs(num);
        x = 1;
    }
    if (isNaN(num))
        num = '0';
    cents = Math.floor((num * 100 + 0.5) % 100);
    num = Math.floor((num * 100 + 0.5) / 100).toString();
    if (cents < 10)
        cents = '0' + cents;
    for (let i = 0; i < Math.floor((num.length - (1 + i)) / 3); i++)
        num = num.substring(0, num.length - (4 * i + 3)) + '.'
                + num.substring(num.length - (4 * i + 3));
    ret = num + ',' + cents;
    if (x === 1)
        ret = ' - ' + ret;
    return ident === 0 ? ret : "R$ " + ret;
}

function dateFormat(value) {
    if (moment(value).isValid()) {
        return moment(value).format("DD/MM/YYYY");
    }
    return "";
}

function textToNumber(valor) {
    valor = valor !== undefined ? valor.replace(/\ /g, "").replace("R$", "").replace("–", "-").replace(/\./g, "").replace(/\,/g, ".") : 0;
    return isNaN(valor) === false ? parseFloat(valor) : parseFloat(0);
}

function oLanguage() {
    return {'oPaginate': {
            'sFirst': "Primeiro",
            'sLast': "Último",
            'sNext': "Próximo",
            'sPrevious': "Anterior"
        }, 'sEmptyTable': "Não foi encontrado nenhum resultado",
        'sInfo': "Registro _START_ ao _END_ de _TOTAL_",
        'sInfoEmpty': "Registro 0 ao 0 de 0",
        'sInfoFiltered': "(filtrado de _MAX_ registros totais)",
        'sLengthMenu': "Visualização de _MENU_ registros",
        'sLoadingRecords': "Carregando...",
        'sProcessing': "Processando...",
        'sSearch': "Pesquisar:",
        'sZeroRecords': "Não foi encontrado nenhum resultado"};
}

//</editor-fold>

function sysLogout() {
    request('./ajax/FormPessoa.php', 'post', {btnLogout: 'S'}, function (data) {
        window.location.href = './../login.php';
    });
}

function getUrl() {
    return (window.location.href.includes("/admin") ? "./" : "./admin/");
}

function getObjTable(table, event) {
    let item_select = event.relatedTarget.closest("tr");
    if (item_select && $("#" + table).DataTable().row(item_select).data()) {
        return $("#" + table).DataTable().row(item_select).data();
    } else if (item_select && $("#" + table).DataTable().row($(item_select).prev()).data()) {
        return $("#" + table).DataTable().row($(item_select).prev()).data();
    } else {
        return {};
    }
}

function getEditObjTable(table, event) {
    let item_select = $(event).closest("tr");
    if (item_select && $("#" + table).DataTable().row(item_select).data()) {
        return $("#" + table).DataTable().row(item_select).data();
    } else if (item_select && $("#" + table).DataTable().row($(item_select).prev()).data()) {
        return $("#" + table).DataTable().row($(item_select).prev()).data();
    } else {
        return {};
    }
}

function getVal(obj, id) {
    return (obj.hasOwnProperty(id) ? obj[id] : "");
}

function getParams(k) {
    let p = {};    
    location.search.replace(/[?&]+([^=&]+)=([^&]*)/gi, function (s, k, v) {
        p[k] = v;
    });
    let toReturn = k ? p[k] : p;
    return (toReturn ? toReturn : "");
}

// <editor-fold defaultstate="collapsed" desc="Vendas">

function returnStatus(tp) {
    if (tp === "T") {
        return `<span class="badge badge-pill badge-primary">Saque</span>`;
    } else if (tp === "P") {
        return `<span class="badge badge-pill badge-success">Pago</span>`;
    } else if (tp === "E") {
        return `<span class="badge badge-pill badge-danger">Estornado</span>`;        
    } else if (tp === "A") {
        return `<span class="badge badge-pill badge-warning">Aguardando</span>`;        
    } else if (tp === "C") {
        return `<span class="badge badge-pill badge-warning">Cancelado</span>`;        
    } else if (tp === "N") {
        return `<span class="badge badge-pill badge-danger">Negado</span>`;        
    }
}

function returnTipo(tp) {
    if (tp === "C") {
        return `<span title="Cartão de Crédito"><span data-feather="credit-card"></span></span>`;
    } else if (tp === "B") {
        return `<span title="Boleto Bancário"><span style="transform: rotate(90deg);" data-feather="align-justify"></span><span style="transform: rotate(90deg); margin-left: -8px;" data-feather="align-justify"></span></span>`;
    } else if (tp === "P") {
        return `<span title="Pagamento em Pix"><span data-feather="grid"></span></span>`;        
    } else if (tp === "T") {        
        return `<span title="CashBack"><span data-feather="dollar-sign"></span></span>`;
    } else if (tp === "S") {        
        return `<span title="Saque em Loja"><span data-feather="briefcase"></span></span>`;
    }
}

function returnEntrega(tp) {
    if (tp === "E") {
        return `<span class="badge badge-pill badge-primary">Entregue</span>`;
    } else if (tp === "P") {
        return `<span class="badge badge-pill badge-warning">Pendente</span>`;        
    }
}

// </editor-fold>