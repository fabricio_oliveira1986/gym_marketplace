
$("#btnBuscar").click(function () {
    refreshTable();
});

$('#tbCategorias').dataTable({
    "iDisplayLength": 20,
    "aLengthMenu": [20, 40, 100],
    "ordering": false,
    "bFilter": false,    
    "bProcessing": true,
    "bServerSide": true,
    "sAjaxSource": finalFind(),
    "aoColumns": [
        {data: function (row, type, set) {
            return `<span class="badge" style="background-color: ${row.cor_fundo};color: ${row.cor_fonte};">${row.nome}</span>`;
        }},
        {data: function (row, type, set) {
            return (row.inativo === "0" ? "Não" : "Sim");
        }},
        {data: function (row, type, set) {
            return `<a href="javascript:void(0)" data-toggle="modal" data-target="#FormCategoria" data-id="${row.id}"><span data-feather="edit-2"></span></a>
            <a href="javascript:void(0)" onclick="excluir(${row.id});"><span data-feather="trash"></span></a>`;
        }}
    ],
    'oLanguage': oLanguage(),
    "sPaginationType": "full_numbers",
    "fnDrawCallback": function (data) {
        feather.replace();
    }
});

function finalFind() {
    return "ajax/categorias_server_processing.php?sSearch=" + $("#text_Filter").val();
}

function refreshTable() {
    let table = $('#tbCategorias').dataTable();
    table.fnReloadAjax(finalFind());
}

let ruleformCategoria = {
    rules: {
        txtNome: {
            required: true,
            minlength: 3
        }
    },
    messages: {
        txtNome: "Preencha o nome corretamente"
    }};

createValidate("#FormCadastroCategoria", ruleformCategoria);

$('#FormCategoria').on('show.bs.modal', function (event) {
    let obj = getObjTable("tbCategorias", event);
    $("#FormCadastroCategoria #txtId").val(getVal(obj, "id"));
    $("#FormCadastroCategoria #txtNome").val(getVal(obj, "nome"));
    $("#FormCadastroCategoria #txtCorFonte").val((getVal(obj, "cor_fonte") !== "" ? getVal(obj, "cor_fonte") : "#000000"));
    $("#FormCadastroCategoria #txtCorFundo").val((getVal(obj, "cor_fundo") !== "" ? getVal(obj, "cor_fundo") : "#ffffff"));
    $("#FormCadastroCategoria #txtDescricao").val(getVal(obj, "descricao"));
    $("#FormCadastroCategoria #txtInativo").prop("checked", (getVal(obj, "inativo") === "1" ? true : false));
});

$("#btnCategoria").click(function () {
    if ($("#FormCadastroCategoria").valid()) {
        request('./ajax/FormProduto.php', 'post', Object_assign(objForm("#FormCadastroCategoria"), {btnSalvarCategoria: 'S'}), function (data) {
            $('#FormCategoria').modal('hide');
            refreshTable();
        });
    }    
});

function excluir(id) {
    bootbox.confirm({message: "Confirma excluir este registro?",
        callback: function (result) {
            if (result) {
                request('./ajax/FormProduto.php', 'post', {btnDeleteCategoria: 'S', txtIdCategoria: id}, function (data) {
                    if (data === "YES") {
                        refreshTable();
                    }
                }, function (erro) {
                    bootbox.alert(erro.responseText + "!");
                });
            }
        }
    });
}