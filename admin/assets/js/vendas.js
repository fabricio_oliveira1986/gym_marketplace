$("#btnBuscar").click(function () {
    refreshTable();
});

$('#tbVendas').dataTable({
    "iDisplayLength": 20,
    "aLengthMenu": [20, 40, 100],
    "ordering": false,
    "bFilter": false,
    "bProcessing": true,
    "bServerSide": true,
    "sAjaxSource": finalFind(),
    "aoColumns": [
        {data: function (row, type, set) {
            return `<b>#${("0000000" + row.id).slice(-7)}</b>`;
        }},    
        {data: function (row, type, set) {
            return moment(row.data, "YYYY-MM-DD hh:mm:ss").format('DD/MM/YYYY HH:mm');
        }},       
        {data: function (row, type, set) {
            return `<img src="${(row.id_foto === null ? "assets/img/no_person.png" : "assets/imagens/pessoas/" + row.id_pessoa + "/" + row.id_foto)}" alt="img" title="contact-img" class="rounded-circle me-3" height="30"> ${compactName(row.nome)}`;
        }},        
        {data: function (row, type, set) {
            return returnStatus(row.status);
        }},    
        {data: function (row, type, set) {
            return numberFormat(row.valor, 1);
        }},    
        {data: function (row, type, set) {
            return returnTipo(row.tipo_documento);
        }},
        {data: function (row, type, set) {
            return returnEntrega(row.entrega);
        }},            
        {data: function (row, type, set) {
            return `<div class="btn-group">
                <button class="btn btn-success btn-sm dropdown-toggle dtoggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
                <div class="dropdown-menu dropdown-menu-right">
                    <a class="dropdown-item" href="detalhes.php?id=${row.id}" target="_blank">Ver Pedido</a>
                    ${(row.tipo_documento === "B" || row.tipo_documento === "P") && row.status === "A" ? 
                    `<a class="dropdown-item" target="_blank" href="${row.charge_link}">2ª Via Boleto</a>` : ``}
                    ${row.tipo_documento === "C" && row.status === "P" ? `<a class="dropdown-item" href="javascript:void(0)" onclick="estornar(${row.id});">Estorno Total</a>` : ``}             
                    ${row.tipo_documento === "C" && row.status === "P" ? `<a class="dropdown-item" href="javascript:void(0)" data-toggle="modal" data-target="#FormTransf" data-id="${row.id}">Estorno Parcial</a>` : ``}
                    ${row.entrega === "P" && row.status === "P" ? `<a class="dropdown-item" href="javascript:void(0)" onclick="baixar_entrega(${row.id});">Baixar Entrega/Uso</a>` : ``}                                    
                </div>
            </div>`;
        }}
    ],
    'oLanguage': oLanguage(),
    "sPaginationType": "full_numbers",
    "fnDrawCallback": function (data) {
        feather.replace();
    }
});

function finalFind() {
    return "ajax/vendas_server_processing.php?sSearch=" + $("#txtPesquisar").val() + "&status=" + $("#txtStatus").val() + "&tipo=" + $("#txtTipo").val() + "&entrega=" + $("#txtEntrega").val();
}

function refreshTable() {
    let table = $('#tbVendas').dataTable();
    table.fnReloadAjax(finalFind());
}

function estornar(id) {
    bootbox.confirm({message: "Confirma o estorno deste registro?",
        callback: function (result) {
            if (result) {
                request('./ajax/FormVenda.php', 'post', {btnEstornar: 'S', txtId: id}, function (data) {
                    if (data === "YES") {
                        refreshTable();
                    }
                }, function (erro) {
                    bootbox.alert(erro.responseText + "!");
                });
            }
        }
    });
}

function baixar_entrega(id) {
    bootbox.confirm({message: "Confirma baixa da entrega deste registro?",
        callback: function (result) {
            if (result) {
                request('./ajax/FormVenda.php', 'post', {btnBaixarEntrega: 'S', txtId: id}, function (data) {
                    if (data === "YES") {
                        refreshTable();
                    }
                }, function (erro) {
                    bootbox.alert(erro.responseText + "!");
                });
            }
        }
    });
}

// <editor-fold defaultstate="collapsed" desc="Transferência">

let ruleformTransf = {
    rules: {
        txtValor: "required"
    },
    messages: {
        txtValor: "Valor Inválido"
    }};

createValidate("#FormCadastroTransf", ruleformTransf);

$('#FormTransf').on('show.bs.modal', function (event) {
    let obj = getObjTable("tbVendas", event);
    let valor = (textToNumber(numberFormat(obj.valor)) + textToNumber(numberFormat(obj.valor_custo)));
    $("#FormCadastroTransf #txtId").val(obj.id);
    $("#FormCadastroTransf #txtNome").val("Valor disponível " + numberFormat((valor > 0 ? valor : 0),1));
    $("#FormCadastroTransf #txtValor").attr("value", numberFormat((valor > 0 ? valor : 0),1));    
    $("#FormCadastroTransf #txtValor").val(numberFormat(0));
});

$("#btnTransf").click(function () {
    if ($("#FormCadastroTransf").valid() && textToNumber($("#txtValor").val()) > 0 
        && textToNumber($("#txtValor").attr("value")) >= textToNumber($("#txtValor").val())) {
        request('./ajax/FormVenda.php', 'post', Object_assign(objForm("#FormCadastroTransf"), {btnEstornar: 'S'}), function (data) {
            refreshTable();
            $('#FormTransf').modal('hide');            
            bootbox.alert("Uma requisição de estorno parcial foi gerada!");            
        });
    } else {
        bootbox.alert("Valores inválidos!");
    }
});

// </editor-fold>