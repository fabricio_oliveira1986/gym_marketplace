let id_conta = getParams('conta');
let id_novo = getParams('novo');
createValidate("#formLogin", ruleformLogin);
createValidate("#formCadastro", ruleformPessoa, ruleformPessoaEndereco);

if (id_conta && id_conta.length > 0) {
    paginaAlterarSenha();
}

if (id_novo && id_novo.length > 0) {
    paginaCadastrar();
}

function paginaCadastrar() {
    $("#cdLogin, #cdLoginFt").hide();
    $("#cdCadastrar, #cdCadastrarFt").show();
    if ($("#txtEstado option").length === 0) {
        buscarEstados();
    }
}

function paginaLogin() {
    $("#cdLogin, #cdLoginFt, #cdRecuperar").show();
    $("#cdCadastrar, #cdCadastrarFt").hide();    
    $("#txtEmailLogin").parent().show();
    $("#txtSenhaLogin").parent().parent().show();
    $("#txtSenhaConfirmar").parent().parent().hide();    
    $("#btnLogin").show(); 
    $("#btnRecuperar, #btnAlterarSenha").hide();
}

function paginaRecuperar() {
    $("#cdLogin, #cdCadastrarFt").show();
    $("#cdCadastrar, #cdCadastrarFt, #cdRecuperar").hide();
    $("#txtEmailLogin").parent().show();    
    $("#txtSenhaLogin").parent().parent().hide();
    $("#txtSenhaConfirmar").parent().parent().hide();
    $("#btnLogin, #btnAlterarSenha").hide();    
    $("#btnRecuperar").show();
}

function paginaAlterarSenha() {
    $("#cdLogin, #cdLoginFt").show();
    $("#cdCadastrar, #cdCadastrarFt, #cdRecuperar").hide();
    $("#txtEmailLogin").parent().hide();    
    $("#txtSenhaLogin").parent().parent().show();
    $("#txtSenhaConfirmar").parent().parent().show();
    $("#btnLogin, #btnRecuperar").hide();
    $("#btnAlterarSenha").show();
}

$('#txtSenhaLogin').keyup(function (e) {
    if (e.keyCode === 13) {
        $("#btnLogin").click();
    }
});

$("#btnLogin").click(function () {
    if ($("#formLogin").valid()) {
        request('./admin/ajax/FormPessoa.php', 'post', Object_assign(objForm("#formLogin"), {btnLogin: 'S'}), function (data) {
            window.location.href = 'admin/index.php';
        });
    }
});

$("#btnRecuperar").click(function () {
    if ($("#formLogin").valid()) {
        request('./admin/ajax/FormPessoa.php', 'post', {btnRecuperar: 'S', txtEmail: $("#txtEmailLogin").val()}, function (data) {
            bootbox.alert("E-mail enviado com dados para recuperação de senha!");
        });
    }
});

$("#btnAlterarSenha").click(function () {
    if ($("#formLogin").valid()) {
        request('./admin/ajax/FormPessoa.php', 'post', {btnAlterarSenha: 'S', txtConta: id_conta, txtSenha: $("#txtSenhaLogin").val()}, function (data) {
            bootbox.alert("Senha alterada com sucesso. Entre com seu e-mail e senha!", function () {
                window.location.href = 'login.php';
            });
        });
    }
});

$("#btnConfirmar").click(function () {
    if ($('#txtConfirmar').is(':checked')) {
        if ($("#formCadastro").valid()) {        
            request('./admin/ajax/FormPessoa.php', 'post', Object_assign(objForm("#formCadastro"), {btnSalvar: 'S'}), function (data) {
                bootbox.alert('Cadastro realizado com sucesso!', function () {
                    window.location.href = 'admin/index.php';
                });
            });
        }
    } else {
        bootbox.alert("Leia os termos e clique em aceitar antes de prosseguir!");
    }
});