
$('input[type=radio][name=txtCadastro]').change(function () {
    if (this.value === '0') {
        $("#cdLogin").show();
        $("#cdCadastrar").hide();
    } else if (this.value === '1') {
        $("#cdLogin").hide();
        $("#cdCadastrar").show();                
    }
});

$('input[type=radio][name=rbEndereco]').change(function () {
    if (this.value === '0') {
        $("#cdEndereco").show();
    } else {
        $("#cdEndereco").hide();
    }    
});