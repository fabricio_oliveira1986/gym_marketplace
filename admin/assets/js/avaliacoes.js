
$("#btnBuscar").click(function () {
    refreshTable();
});

$('#tbAvaliacoes').dataTable({
    "iDisplayLength": 20,
    "aLengthMenu": [20, 40, 100],
    "ordering": false,
    "bFilter": false,
    "bProcessing": true,
    "bServerSide": true,
    "sAjaxSource": finalFind(),
    "aoColumns": [
        {data: function (row, type, set) {
            return moment(row.data, "YYYY-MM-DD hh:mm:ss").format('DD/MM/YYYY HH:mm');
        }},               
        {data: function (row, type, set) {
            return `<img src="${row.foto_produto === null ? "assets/img/no_image.png" : "assets/imagens/produtos/" + row.id_produto + "/" + row.foto_produto}" alt="img" title="contact-img" class="rounded me-3" height="30"> ${row.nome_produto} - ${row.descricao}`;
        }},      
        {data: function (row, type, set) {
            return `<img src="${(row.id_foto === null ? "assets/img/no_person.png" : "assets/imagens/pessoas/" + row.id_pessoa + "/" + row.id_foto)}" alt="img" title="contact-img" class="rounded-circle me-3" height="30"> ${compactName(row.nome)}`;
        }},                        
        {data: function (row, type, set) {
            return `<div class="rating text-warning" data-rate-value=${row.avaliacao}></div>`;
        }},
        {data: "comentario"},
        {data: function (row, type, set) {
            return returnStatus(row.data_resposta, row.inativo);
        }},        
        {data: function (row, type, set) {
            return `<a href="javascript:void(0)" data-toggle="modal" data-target="#FormPergunta" data-id="${row.id}"><span data-feather="edit-2"></span></a>`;            
        }}
    ],
    'oLanguage': oLanguage(),
    "sPaginationType": "full_numbers",
    "fnDrawCallback": function (data) {
        feather.replace();
        $(".rating").rate({ max_value: 5, step_size: 0.5, readonly: true });        
    }
});

function finalFind() {
    return "ajax/avaliacoes_server_processing.php?sSearch=" + $("#txtPesquisar").val() + "&avaliacoes=" + $("#txtAvaliacoes").val() + "&tipo=" + $("#txtTipo").val();
}

function refreshTable() {
    let table = $('#tbAvaliacoes').dataTable();
    table.fnReloadAjax(finalFind());
    request('./ajax/FormProduto.php', 'post', {btnTotRespostas: 'S', txtTipo : 1}, function (data) {        
        $('a[href$="avaliacoes.php"]').find(".badge").html(data.total);
        if (data.total === "0") {
            $('a[href$="avaliacoes.php"]').find(".badge").hide();
        } else {
            $('a[href$="avaliacoes.php"]').find(".badge").show();
        }
    });    
}

function returnStatus(tp, inativo) {
    if (inativo === "1") {
        return `<span class="badge badge-pill badge-danger">Cancelado</span>`;                
    } else if (tp === null) {
        return `<span class="badge badge-pill badge-warning">Aguardando</span>`;        
    } else {
        return `<span class="badge badge-pill badge-success">Respondido</span>`;        
    }
}

let ruleformResposta = {
    rules: {
        txtDescricao: {
            required: true,
            minlength: 3
        }
    },
    messages: {
        txtNome: "Preencha a Resposta"
    }};

createValidate("#FormCadastroPergunta", ruleformResposta);

$('#FormPergunta').on('show.bs.modal', function (event) {
    let obj = getObjTable("tbAvaliacoes", event);
    $("#FormCadastroPergunta #txtId").val(getVal(obj, "id"));
    $("#FormCadastroPergunta #txtAnuncios").html(`<img src="${obj.foto_produto === null ? "assets/img/no_image.png" : "assets/imagens/produtos/" + obj.id_produto + "/" + obj.foto_produto}" alt="img" title="contact-img" class="rounded me-3" height="30"> ${obj.nome_produto} - ${obj.descricao}`);
    $("#FormCadastroPergunta #txtPergunta").html(getVal(obj, "comentario"));
    $("#FormCadastroPergunta #txtResposta").val(getVal(obj, "resposta"));
});

$("#btnPergunta").click(function () {
    if ($("#FormCadastroPergunta").valid()) {
        request('./ajax/FormProduto.php', 'post', Object_assign(objForm("#FormCadastroPergunta"), {btnSalvarAvaliacao: 'S'}), function (data) {
            $('#FormPergunta').modal('hide');
            refreshTable();
        });
    }    
});