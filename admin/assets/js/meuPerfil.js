
createValidate("#formCadastro", ruleformPessoa);
$("#formCadastro input").attr("disabled", true);
$("#btnPerfil, #btnCancelar, #imageDirAdd, #imageDirDel").hide();

request('./ajax/FormPessoa.php', 'post', {btnPerfil: 'S'}, function (data) {
    $("h4").html(data.nome);
    $("#formCadastro #txtNome").val(data.nome);
    $("#formCadastro #txtDtNasc").val(dateFormat(data.dt_nasc));
    $("#formCadastro #txtCPF").val(data.cpf);
    $("h4").next().next().html(data.celular);    
    $("#formCadastro #txtCelular").val(data.celular);
    $("#formCadastro input[name=txtSexo][value=" + data.sexo + "]").prop('checked', true);
    $("h4").next().html(data.email);    
    $("#formCadastro #txtEmail").val(data.email);
    $("#imageNow").val((data.id_foto === null ? "" : data.id_foto));
    $(".img-thumbnail").attr("src", (data.id_foto === null ? "assets/img/no_person.png" : "assets/imagens/pessoas/" + $("#sessionId").val() + "/" + data.id_foto));    
});

$("#btnAlterar").click(function () {
    $("#formCadastro input").attr("disabled", false);
    $("#" + ($(".img-thumbnail").attr("src") === "assets/img/no_person.png" ? "imageDirAdd" : "imageDirDel")).show();
    $("#btnPerfil, #btnCancelar").show();
    $("#btnAlterar").hide();
});

$("#btnPerfil").click(function () {
    if ($("#formCadastro").valid()) {
        request('./ajax/FormPessoa.php', 'post', Object_assign(objForm("#formCadastro"), {btnSalvar: 'S'}), function (data) {
            document.location.reload(true);
        });
    }
});

$("#btnCancelar").click(function () {
    document.location.reload(true);
});

// <editor-fold defaultstate="collapsed" desc="Endereço">

createValidate("#formCadastroEndereco", ruleformPessoaEndereco);

$('#tbEndereco').dataTable({
    "iDisplayLength": 20,
    "ordering": false,
    "bFilter": false,
    "bProcessing": true,
    "bServerSide": true,
    "bLengthChange": false,
    "sAjaxSource": finalFind(),
    "aoColumns": [
        {data: function (row, type, set) {
                return row.endereco + " N°" + row.numero + ", " + row.bairro + " " + row.cidade_nome + " - " + row.estado_sigla + ", cep:" + row.cep + ", " + row.complemento;
            }},
        {data: function (row, type, set) {
                return `<a href="javascript:void(0)" data-toggle="modal" data-target="#FormEndereco" data-id="${row.id}"><span data-feather="edit-2"></span></a>
            <a href="javascript:void(0)" onclick="excluir(${row.id});"><span data-feather="trash"></span></a>`;
            }}
    ],
    'oLanguage': oLanguage(),
    "sPaginationType": "full_numbers",
    "fnDrawCallback": function (data) {
        feather.replace();
    }
});

function finalFind() {
    return "ajax/enderecos_server_processing.php";
}

function refreshTable() {
    let table = $('#tbEndereco').dataTable();
    table.fnReloadAjax(finalFind());
}

$('#FormEndereco').on('show.bs.modal', function (event) {
    let obj = getObjTable("tbEndereco", event);
    $("#FormEndereco #txtIdEndereco").val(getVal(obj, "id"));
    $("#FormEndereco #txtCep").val(getVal(obj, "cep"));
    $("#FormEndereco #txtEndereco").val(getVal(obj, "endereco"));
    $("#FormEndereco #txtBairro").val(getVal(obj, "bairro"));
    $("#FormEndereco #txtComplemento").val(getVal(obj, "complemento"));
    $("#FormEndereco #txtNumero").val(getVal(obj, "numero"));
    $("#FormEndereco #txtEstado").attr("value", (getVal(obj, "estado_codigo")));
    $("#FormEndereco #txtCidade").attr("value", (getVal(obj, "id_cidade")));
    buscarEstados();
});

$("#btnEndereco").click(function () {
    if ($("#formCadastroEndereco").valid()) {
        request('./ajax/FormPessoa.php', 'post', Object_assign(objForm("#formCadastroEndereco"), {btnSalvarEndereco: 'S'}), function (data) {
            $('#FormEndereco').modal('hide');
            refreshTable();
        });
    }
});

function excluir(id) {
    bootbox.confirm({message: "Confirma excluir este registro?",
        callback: function (result) {
            if (result) {
                request('./ajax/FormPessoa.php', 'post', {btnDeleteEndereco: 'S', txtIdEndereco: id}, function (data) {
                    if (data === "YES") {
                        refreshTable();
                    }
                }, function (erro) {
                    bootbox.alert(erro.responseText + "!");
                });
            }
        }
    });
}

// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="Cartão">

$('#tbCartao').dataTable({
    "iDisplayLength": 20,
    "ordering": false,
    "bFilter": false,
    "bProcessing": true,
    "bServerSide": true,
    "bLengthChange": false,
    "sAjaxSource": finalFindCartao(),
    "aoColumns": [
        {data: function (row, type, set) {
            return `<img src="assets/img/payments/${row.bandeira}.png" alt="img" title="${row.bandeira}" class="rounded me-3" height="30">, Número: ${row.cartao} Validade: ${moment(row.data, "YYYY-MM-DD").format('MM/YYYY')}`;
        }},
        {data: function (row, type, set) {
            return `<a href="javascript:void(0)" onclick="excluirCartao(${row.id});"><span data-feather="trash"></span></a>`;
        }}
    ],
    'oLanguage': oLanguage(),
    "sPaginationType": "full_numbers",
    "fnDrawCallback": function (data) {
        feather.replace();
    }
});

function finalFindCartao() {
    return "ajax/cartao_server_processing.php";
}

function refreshTableCartao() {
    let tableCartao = $('#tbCartao').dataTable();
    tableCartao.fnReloadAjax(finalFindCartao());
}

function excluirCartao(id) {
    bootbox.confirm({message: "Confirma excluir este registro?",
        callback: function (result) {
            if (result) {
                request('./ajax/FormPessoa.php', 'post', {btnDeleteCartao: 'S', txtIdCartao: id}, function (data) {
                    if (data === "YES") {
                        refreshTableCartao();
                    }
                }, function (erro) {
                    bootbox.alert(erro.responseText + "!");
                });
            }
        }
    });
}
 
// </editor-fold>