let id_produto = getParams('id');
let ckeditor = CKEDITOR.replace('txtDescricao', {height: 254});
ckeditor.on('instanceReady', function() {
    inicio(id_produto);
});
$("#txtValor, #txtCashback").priceFormat({prefix: "", centsSeparator: ",", thousandsSeparator: "."});
$("#descricao-tab, #divulgacao-tab, #financeiro-tab, #imagens-tab").addClass("disabled");

request('./ajax/FormProduto.php', 'post', {btnProduto: 'S', txtId: id_produto}, function (data) {
    $("h4").html(getVal(data, "nome"));
    $(".img-thumbnail").attr("src", (getVal(data, "id_foto") === "" || data.id_foto === null ? "assets/img/no_image.png" : "assets/imagens/produtos/" + id_produto + "/" + data.id_foto));
    $("#imageNow").val((getVal(data, "id_foto") === "" || data.id_foto === null ? "" : getVal(data, "id_foto")));    
    $("#formCadastro #txtId").val(getVal(data, "id"));
    $("#formCadastro #txtNome").val(getVal(data, "nome"));
    $("#formCadastro #txtTipo").val(getVal(data, "tipo"));
    $("h4").next().html($("#txtTipo option:selected").text());    
    $("#formCadastro #txtGrupo").attr("value", getVal(data, "categoria"));
    $("#formCadastro #txtCategoria").attr("value", getVal(data, "categoria_empresa"));
    $("#formCadastro #txtInativo").prop("checked", (getVal(data, "inativo") === "1" ? true : false));
    ckeditor.setData(getVal(data, "descricao"));
    $("#formDivulgacao input[name=txtDivulgacao][value=" + (getVal(data, "divulgacao") === "" ? "1" : getVal(data, "divulgacao")) + "]").prop('checked', true);
    $("#lblAnuncio").html(setAnuncio((getVal(data, "divulgacao") === "" ? "1" : getVal(data, "divulgacao"))));    
    $("#btnAdicionarVideos").attr("disabled", (getVal(data, "divulgacao") === "3" ? false : true));    
    $(".star").rate({max_value: 3, readonly: true});
    $("#formFinanceiro #txtCartao").val((getVal(data, "cartao_credito") === "" ? "1" : getVal(data, "cartao_credito")));
    $("#formFinanceiro #txtBoleto").val((getVal(data, "boleto") === "" ? "1" : getVal(data, "boleto")));
    $("#formFinanceiro #txtPix").val((getVal(data, "pix") === "" ? "1" : getVal(data, "pix")));
    if ($.isNumeric(id_produto)) {
        $(".rating").rate({max_value: 5, step_size: 0.5, readonly: true});
        refreshImagem();        
    }
    buscarGrupos();
    buscarCategorias();
});

$('.nav-link').click(function (e) {
    $("#btnProduto").html((e.currentTarget.id === "imagens-tab" ? "Finalizar" : "Próximo"));
});

$("#btnProduto").click(function () {
    let tab = $("#v-pills-tab .nav-link.active").attr("href");
    if (tab === "#home" && $("#formCadastro").valid() && ($.isNumeric(id_produto) || (!$.isNumeric(id_produto) && $("#formAnuncio").valid()))) {
        if ($.isNumeric($("#txtIdAnuncio").val())) {
            $("#btnAnuncio").click();
        } else {
            $("#descricao-tab").removeClass("disabled");
            $("#descricao-tab").click();            
        }
    } else if (tab === "#descricao" && $("#formCadastro").valid()) {
        $("#divulgacao-tab").removeClass("disabled");
        $("#divulgacao-tab").click();
    } else if (tab === "#divulgacao" && $("#formCadastro").valid()) {
        $("#financeiro-tab").removeClass("disabled");
        $("#financeiro-tab").click();
    } else if (tab === "#financeiro" && $("#formCadastro").valid()) {
        request('./ajax/FormProduto.php', 'post', Object_assign(objForm("#formCadastro"), objForm("#formAnuncio"),
        objForm("#formDivulgacao"), objForm("#formFinanceiro"), {btnSalvar: 'S', txtDescricao: ckeditor.getData()}), function (data) {
            if ($.isNumeric(id_produto)) {
                $("#imagens-tab").removeClass("disabled");
                $("#imagens-tab").click();
            } else {
                window.location.href = "produto.php?id=" + data.id + "&tab=imagens-tab";
            }
        });
    } else if (tab === "#imagens" && $("#imageDir").val().length > 0) {
        request('./ajax/FormProduto.php', 'post', Object_assign(objForm("#formCadastro"), objForm("#formAnuncio"),
        objForm("#formDivulgacao"), objForm("#formFinanceiro"), {btnSalvar: 'S', txtDescricao: ckeditor.getData()}), function (data) {
            window.location.href = "produto.php?id=" + id_produto;
        });        
    } else if (tab === "#imagens" && $("#imageDir").val().length === 0) {
        window.location.href = "produto.php?id=" + id_produto;
    }
});

$("#btnAlterar").click(function () {
    $("#formCadastro input, select").attr("disabled", false);
    $("#" + ($(".img-thumbnail").attr("src") === "assets/img/no_image.png" ? "imageDirAdd" : "imageDirDel")).show();
    $("#btnProduto, #btnCancelar").show();
    $("#btnAlterar").hide();
    ckeditor.setReadOnly(false);    
});

$("input[type=radio][name=txtDivulgacao]").change(function () {
    $("#lblAnuncio").html(setAnuncio(this.value));
    $(".star").rate({max_value: 3, readonly: true});
    $("#btnAdicionarVideos").attr("disabled", (this.value === "3" ? false : true));
    feather.replace();
});

function inicio(id_produto) {
    if ($.isNumeric(id_produto)) {
        $("#formCadastro input, select").attr("disabled", true);
        $("#descricao-tab, #divulgacao-tab, #financeiro-tab, #imagens-tab").removeClass("disabled");
        $("#btnAdicionarImgs").attr("disabled", false);
        $("#btnAlterar, #tableAnuncio, #btnAnuncio").show();
        $("#btnProduto, #btnCancelar, #imageDirAdd, #imageDirDel").hide();
        ckeditor.setReadOnly(true);
        let tab_select = getParams('tab');
        if (tab_select.length > 0) {
            $("#" + tab_select + ", #btnAlterar").click();
        }        
    }
}

function buscarGrupos() {
    request('./ajax/FormProduto.php', 'post', {btnBuscarGrupos: 'S'}, function (data) {
        $("#txtGrupo").html(data.map(function (value, key) {
            return `<option value="${value.id}">${value.nome}</option>`;
        }).join(''));
        $("#txtGrupo").val($("#txtGrupo").attr("value"));
        $("h4").next().next().html($("#txtGrupo option:selected").text());
    }, function (erro) {
        bootbox.alert(erro.responseText + "!");
    });
}

function buscarCategorias() {
    request('./ajax/FormProduto.php', 'post', {btnBuscarCategorias: 'S'}, function (data) {
        $("#txtCategoria").html(data.map(function (value, key) {
            return `<option value="${value.id}">${value.nome}</option>`;
        }).join(''));
        $("#txtCategoria").val($("#txtCategoria").attr("value"));
    }, function (erro) {
        bootbox.alert(erro.responseText + "!");
    });
}

// <editor-fold defaultstate="collapsed" desc="Imagens">

function refreshImagem() {
    if ($.isNumeric($("#txtId").val())) {
        request('./ajax/FormArquivos.php', 'post', {lerArquivo: 'S', txtId: $("#txtId").val(), txtType: 'pFotos'}, function (data) {
            $("#dvImagens").html(data.map(function (value, key) {
                return `<div class="col-md-3">
                            <div class="card mb-3 border">                                                                                        
                                <div class="dropdown position-absolute">
                                    <button class="btn btn-sm btn-secondary" type="button" id="dropdown_${key}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <span data-feather="align-justify"></span>
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="dropdown_${key}">
                                        <a class="dropdown-item" href="javascript:void(0)" onclick="excluirImagem('${value.id}');"><span data-feather="trash"></span> Excluir</a>                                                        
                                    </div>
                                </div>                                
                                <` + (value.id.endsWith('.mp4') ? `video` : `img`) + ` src="assets/imagens/produtos/${$("#txtId").val()}/list/${value.id}" alt="" data-holder-rendered="true" class="card-img-top">
                            </div>
                        </div>`;
            }).join(''));
            if (data.length > 0) {
                $(".alert").hide();
            } else {
                $(".alert").show();
            }
            feather.replace();            
        }, function (erro) {
            bootbox.alert(erro.responseText + "!");
        });
    }
}

function excluirImagem(img) {
    bootbox.confirm({message: "Confirma excluir este registro?",
        callback: function (result) {
            if (result) {    
                request('./ajax/FormArquivos.php', 'post', {delArquivo: 'S', txtId: $("#txtId").val(), txtType: 'pFotos', txtFile: img, 
                txtCapa: (img === $(".img-thumbnail").attr("src").split("/").pop() ? "S" : "N")}, function (data) {
                    if (img === $(".img-thumbnail").attr("src").split("/").pop()) {
                        $(".img-thumbnail").attr("src", "assets/img/no_image.png");
                    }
                    refreshImagem();                    
                }, function (erro) {
                    bootbox.alert(erro.responseText + "!");
                });        
            }
        }
    });
}

// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="Anuncios">

var tbAnuncio = $('#tbAnuncio').dataTable({
    "iDisplayLength": 20,
    "ordering": false,
    "bFilter": false,
    "bProcessing": true,
    "bServerSide": true,
    "bLengthChange": false,
    "sAjaxSource": finalFind(),
    "aoColumns": [
        {data: function (row, type, set) {
            return row.id + " - " + row.descricao;
        }, "sWidth": "40%"},
        {data: function (row, type, set) {
            return numberFormat(row.valor);
        }, "sWidth": "20%"},
        {data: function (row, type, set) {
            return numberFormat(row.cashback);
        }, "sWidth": "20%"},
        {data: function (row, type, set) {
            return `<input type="checkbox" class="toggle" ${row.inativo === "0" ? "checked" : ""} data-toggle="toggle" data-on="Ativo" data-off="Publicar" data-onstyle="primary" data-offstyle="warning" data-size="xs">
            <a href="javascript:void(0)" onclick="editarAnuncio(this);"><span data-feather="edit-2"></span></a>
            <a href="javascript:void(0)" onclick="excluir(${row.id});"><span data-feather="trash"></span></a>`;
        }, "sWidth": "20%"}
    ],
    'oLanguage': oLanguage(),
    "sPaginationType": "full_numbers",
    "fnDrawCallback": function (data) {
        $(".toggle").bootstrapToggle();
        $('.toggle').change(function () {
            if (typeof $(this).prop('checked') !== 'undefined') {
                ativarAnuncio(this, $(this).prop('checked'));
            }
        });
        feather.replace();        
    }
});

$('#tbAnuncio tbody').on('click', 'td.dtr-control', function () {
    $(".toggle").bootstrapToggle();
    $('.toggle').change(function () {
        if (typeof $(this).prop('checked') !== 'undefined') {
            ativarAnuncio(this, $(this).prop('checked'));
        }
    });
    feather.replace();        
});

function editarAnuncio(event) {
    let obj = getEditObjTable("tbAnuncio", event);
    populateAnuncio(obj);
}

function ativarAnuncio(event, tipo) {
    let obj = getEditObjTable("tbAnuncio", event);
    request('./ajax/FormProduto.php', 'post', {btnAtivarAnuncio: 'S', txtIdAnuncio: obj.id, txtTipo: (tipo ? 0 : 1)}, function (data) {
        if (data === "YES") {
            refreshTable();
        }
    }, function (erro) {
        bootbox.alert(erro.responseText + "!");
    });
}

function finalFind() {
    return "ajax/anuncios_server_processing.php?id=" + id_produto;
}

function refreshTable() {
    let table = $('#tbAnuncio').dataTable();
    table.fnReloadAjax(finalFind());
}

$("#btnAnuncio").click(function () {
    if ($("#formAnuncio").valid()) {
        request('./ajax/FormProduto.php', 'post', Object_assign(objForm("#formAnuncio"),
            {btnSalvarAnuncio: 'S', txtId: id_produto}), function (data) {
            populateAnuncio({});
            refreshTable();
        });
    }
});

function populateAnuncio(obj) {
    $("#formAnuncio #txtIdAnuncio").val(getVal(obj, "id"));
    $("#formAnuncio #txtNomeSub").val(getVal(obj, "descricao"));
    $("#formAnuncio #txtQuantidade").val(getVal(obj, "quantidade"));
    $("#formAnuncio #txtValor").val(numberFormat(getVal(obj, "valor")));
    $("#formAnuncio #txtCashback").val(numberFormat(getVal(obj, "cashback")));
}

function excluir(id) {
    bootbox.confirm({message: "Confirma excluir este registro?",
        callback: function (result) {
            if (result) {
                request('./ajax/FormProduto.php', 'post', {btnDeleteAnuncio: 'S', txtIdAnuncio: id}, function (data) {
                    if (data === "YES") {
                        refreshTable();
                    }
                }, function (erro) {
                    bootbox.alert(erro.responseText + "!");
                });
            }
        }
    });
}

function setAnuncio(id) {
    let check = `<span class="text-success" data-feather="check"></span>`;
    let dest = `<b class="h6 text-success">`;
    if (id === "1") {
        return `<h5 class="card-title"><img src="assets/img/type/1.png" alt="" height="50"/> Bronze
        <div class="star text-warning" data-rate-value=1></div></h5>
        <p class="mb-0">${check} Anúncio no perfil da empresa</p>
        <p class="mb-0">${check} Anúncio fica listado em ofertas do dia</p>
        <p class="mb-0">${check} Duração do anúncio 45 dias ativo</p>
        <p class="mb-0">${check} Taxa transação cartão ${dest}6,5%</b> por venda</p>
        <p class="mb-0">${check} R$ ${dest}6,90</b> por boleto pago.</p>
        <p class="mb-0">${check} R$ ${dest}5,90</b> Pix.</p>`;
    } else if (id === "2") {
        return `<h5 class="card-title"><img src="assets/img/type/2.png" alt="" height="50"/> Prata
        <div class="star text-warning" data-rate-value=2></div></h5>
        <p class="mb-0">${check} Anúncio no perfil da empresa</p>
        <p class="mb-0">${check} Anúncio Fica listado em melhores ofertas</p>
        <p class="mb-0">${check} Anúncio volta ao topo 4 vezes ao dias por 7 dias</p>
        <p class="mb-0">${check} Duração do anúncio 60 dias ativo</p>
        <p class="mb-0">${check} Taxa transação cartão ${dest}7,5%</b> por venda</p>
        <p class="mb-0">${check} R$ ${dest}6,90</b> por boleto pago.</p>
        <p class="mb-0">${check} R$ ${dest}5,90</b> Pix.</p>`;
    } else if (id === "3") {
        return `<h5 class="card-title"><img src="assets/img/type/3.png" alt="" height="50"/> Ouro
        <div class="star text-warning" data-rate-value=3></div></h5>
        <p class="mb-0">${check} Anúncio Fica em oferta do dia no site gymbrother.com.br</p>
        <p class="mb-0">${check} Anúncio Fica listado em Melhores ofertas</p>
        <p class="mb-0">${check} Tops da semana</p>
        <p class="mb-0">${check} Anúncio volta ao topo 6 vezes ao dia por 7 dias.</p>
        <p class="mb-0">${check} Adicionar vídeo</p>
        <p class="mb-0">${check} Duração ilimitada.</p>
        <p class="mb-0">${check} Taxa transação cartão ${dest}8,5%</b> por venda</p>
        <p class="mb-0">${check} R$ ${dest}6,90</b> por boleto pago.</p>
        <p class="mb-0">${check} R$ ${dest}5,90</b> Pix.</p>`;
    }
}

// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="Validate">

let ruleformCadastro = {
    rules: {
        txtNome: {
            required: true,
            minlength: 3
        },
        txtTipo: "required",
        txtGrupo: "required"
    },
    messages: {
        txtNome: "Preencha o nome corretamente",
        txtTipo: "Tipo inválido",
        txtGrupo: "Grupo inválido"
    }};

let ruleformAnuncio = {
    rules: {
        txtNomeSub: {
            required: true,
            minlength: 3
        },
        txtQuantidade: "required",
        txtValor: "required",
        txtCashback: "required"
    },
    messages: {
        txtNomeSub: "Preencha o Subtítulo corretamente",
        txtQuantidade: "Quantidade inválida",
        txtValor: "Valor inválido",
        txtCashback: "Cashback inválido"
    }};

let ruleformFinanceiro = {
    rules: {
        txtCartao: "required",
        txtPix: "required",
        txtBoleto: "required"
    },
    messages: {
        txtCartao: "Cartão inválido",
        txtPix: "Pix inválido",
        txtBoleto: "Boleto inválido"
    }};

createValidate("#formCadastro", ruleformCadastro);
createValidate("#formAnuncio", ruleformAnuncio);
createValidate("#formFinanceiro", ruleformFinanceiro);

// </editor-fold>