
$("#txtCep").mask("99.999-999");

$("#txtCep").change(function () {
    if ($(this).val().length === 10) {
        $(".boxLoader").show();
        $.get("https://viacep.com.br/ws/" + $(this).val().replace(".", "").replace("-", "") + "/json/",
                function (data) {
                    $("#txtEndereco").val(decodeURIComponent(data.logradouro));
                    $("#txtBairro").val(decodeURIComponent(data.bairro));
                    $("#txtEstado option").filter(function () {
                        return $(this).text() === decodeURIComponent(data.uf);
                    }).prop("selected", true);
                    buscarCidades($("#txtEstado").val(), decodeURIComponent(data.localidade));
                    $("#txtNumero").focus();
                }).always(function () {
            $(".boxLoader").hide();
        });
    }
});

$("#txtEstado").change(function () {
    buscarCidades($(this).val());
});

function buscarEstados() {
    request(getUrl() + 'ajax/FormPessoa.php', 'post', {btnBuscarEstados: ''}, function (data) {
        $("#txtEstado").html(data.map(function (value, key) {
            return `<option value="${value.estado_codigo}">${value.estado_sigla}</option>`;
        }).join(''));
        buscarCidades($("#txtEstado").attr("value"), '');
        $("#txtEstado").val($("#txtEstado").attr("value"));
    }, function (erro) {
        bootbox.alert(erro.responseText + "!");
    });
}

function buscarCidades(idEstado, nomeCidade) {
    request(getUrl() + 'ajax/FormPessoa.php', 'post', {btnBuscarCidades: '', txtEstado: idEstado}, function (data) {
        $("#txtCidade").html(data.map(function (value, key) {
            return `<option value="${value.cidade_codigo}">${value.cidade_nome}</option>`;
        }).join(''));
        if (nomeCidade !== "") {
            $("#txtCidade option").filter(function () {
                return $(this).text() === nomeCidade;
            }).prop("selected", true);
        } else {
            $("#txtCidade").val($("#txtCidade").attr("value"));
        }
    }, function (erro) {
        bootbox.alert(erro.responseText + "!");
    });
}

let ruleformPessoaEndereco = {
    rules: {
        txtCep: "cep",
        txtEndereco: "required",
        txtBairro: "required",
        txtNumero: "required",
        txtEstado: "required",
        txtCidade: "required"
    },
    messages: {
        txtEndereco: "Endereço inválido",
        txtBairro: "Bairro inválido",
        txtNumero: "Número inválido",
        txtEstado: "Selecione um estado",
        txtCidade: "Selecione uma cidade"
    }};