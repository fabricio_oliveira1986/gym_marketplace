$("#btnEyeLogin, #btnEyeConfirmar").hide();

$("#btnEyeLogin").on("click", function () {
    $("#btnEyeLogin").hide();
    $("#btnEyeLoginOff").show();
    $("#txtSenhaLogin").prop("type", "password");
});

$("#btnEyeLoginOff").on("click", function () {
    $("#btnEyeLogin").show();
    $("#btnEyeLoginOff").hide();
    $("#txtSenhaLogin").prop("type", "text");
});

$("#btnEyeConfirmar").on("click", function () {
    $("#btnEyeConfirmar").hide();
    $("#btnEyeConfirmarOff").show();
    $("#txtSenhaConfirmar").prop("type", "password");
});

$("#btnEyeConfirmarOff").on("click", function () {
    $("#btnEyeConfirmar").show();
    $("#btnEyeConfirmarOff").hide();
    $("#txtSenhaConfirmar").prop("type", "text");
});

let ruleformLogin = {
    rules: {
        txtEmailLogin: {
            required: true,
            email: true
        },
        txtSenhaLogin: {
            required: true,
            minlength: 5
        },
        txtSenhaConfirmar: {
            required: true,
            minlength: 5,
            equalTo : "#txtSenhaLogin"
        }
    },
    messages: {
        txtEmailLogin: "E-mail inválido",
        txtSenhaLogin: "Senha inválida, mínimo 5 caracteres",
        txtSenhaConfirmar: "Senha inválida, mínimo 5 caracteres"
    }};