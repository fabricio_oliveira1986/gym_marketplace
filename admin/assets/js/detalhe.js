let id_venda = getParams('id');

request('./ajax/FormVenda.php', 'post', {getVenda: 'S', txtId: id_venda}, function (data) {
    $("#lblId").html("#" + ("0000000" + data.id).slice(-7));
    $("#lblData").html(moment(data.data, "YYYY-MM-DD hh:mm:ss").format('DD/MM/YYYY HH:mm'));
    $("#opPedido").addClass("btn-success");
    $("#opPagamento").addClass((data.status === "P" ? "btn-success" : "btn-secondary"));
    $("#opEntregue").addClass((data.entrega === "E" ? "btn-success" : "btn-secondary"));    
    dvEnvio(data);
    dvPagamento(data);
    dvEntrega(data);
    makeItens(data.itens);
    makeParcelas(data.parcelas);
    feather.replace();
});

function dvEnvio(data) {
    let dados = `<p class="mb-1 mt-3"><b>${data.id_pessoa.nome}</b></p>
    <p class="mb-0"><span data-feather="phone"></span>  ${data.id_pessoa.celular}</p>
    <p class="mb-1"><span data-feather="mail"></span>  ${data.id_pessoa.email}</p>`;
    if (data.id_endereco) {
        dados += `<p class="mb-0">${data.id_endereco.endereco} N° ${data.id_endereco.numero}, ${data.id_endereco.bairro}</p>
        <p class="mb-0">${data.id_endereco.id_cidade.cidade_nome} - ${data.id_endereco.id_cidade.cidade_codigoEstado.estado_sigla}, CEP: ${data.id_endereco.cep}</p>`;
    }
    $("#dvEnvio").html(dados);
}

function dvPagamento(data) {
    let dados = `<p class="mb-1 mt-3"><b>Tipo:</b> ${data.descricao}</p>`;
    if (data.id_cartao) {
        dados += `<p class="mb-1"><b>Número:</b> ${data.id_cartao.cartao}</p>
        <p class="mb-1"><b>Validade:</b> ${moment(data.id_cartao.data, "YYYY-MM-DD").format('MM/YYYY')}</p>
        <p class="mb-1"><b>Bandeira:</b> ${data.id_cartao.bandeira}</p>`;
    }
    $("#dvPagamento").html(dados);    
}

function dvEntrega(data) {
    let dados = `<p class="mb-1 mt-3"><b><span data-feather="truck"></span></b></p>`;
    if (data.id_frete) {
        dados += `<p class="mb-1"> ${data.id_frete.nome}</p>
        <p class="mb-1"> ${numberFormat(data.id_frete.valor, 1)}</p>`;
    }
    $("#dvEntrega").html(dados);    
}

function makeItens(obj) {
    $("#tbItens").dataTable({
        "iDisplayLength": -1,
        "ordering": false, 
        "bFilter": false, 
        "bInfo": false, 
        "bLengthChange": false,
        "bPaginate": false,
        "aaData": obj, 
        "aoColumns": [
            {data: function (row, type, set) {
                return row.nome + " " + row.descricao;
            }},
            {data: function (row, type, set) {
                return row.quantidade;
            }},
            {data: function (row, type, set) {
                return numberFormat(row.valor, 1);
            }},
            {data: function (row, type, set) {
                return numberFormat((row.quantidade * row.valor), 1);
            }}
        ],
        'oLanguage': oLanguage(),
        "fnDrawCallback": function (data) {
            let total = 0;
            for (var i = 0; i < data["aoData"].length; i++) {
                total += (parseFloat(data["aoData"][i]["_aData"].quantidade) * parseFloat(data["aoData"][i]["_aData"].valor));
            }            
            $("#tbItens tfoot tr th")[1].innerText = numberFormat(total, 1);
        }
    });
}

function makeParcelas(obj) {
    $("#tbParcelas").dataTable({
        "iDisplayLength": -1,
        "ordering": false, 
        "bFilter": false, 
        "bInfo": false, 
        "bLengthChange": false,
        "bPaginate": false,
        "aaData": obj, 
        "aoColumns": [
            {data: function (row, type, set) {
                return row.razao_social;
            }},
            {data: function (row, type, set) {
                return numberFormat(row.valor, 1);
            }}
        ],
        'oLanguage': oLanguage(),
        "fnDrawCallback": function (data) {
            let total = 0;
            for (var i = 0; i < data["aoData"].length; i++) {
                total += parseFloat(data["aoData"][i]["_aData"].valor);
            }
            $("#tbParcelas tfoot tr th")[1].innerText = $("#tbParcelas tbody tr td")[1].innerText; 
            $("#tbParcelas tbody tr td")[1].innerText = numberFormat(total, 1); 
        }        
    });
}