<form id="formLogin" novalidate>
    <div class="form-group">
        <label for="txtEmailLogin"><b>E-mail:</b></label>
        <input type="email" class="form-control" id="txtEmailLogin" name="txtEmailLogin" placeholder="E-mail" value="" required="">
            <small id="txtEmail" class="form-text text-muted">Entre com o e-mail vinculado ao seu cadastro.</small>
    </div>
    <div class="form-group">
        <label for="txtSenhaLogin"><b>Senha:</b></label>
        <div class="input-group">
            <input type="password" class="form-control" id="txtSenhaLogin" name="txtSenhaLogin" value="" placeholder="Senha" required="">
                <div class="input-group-append">
                    <span class="input-group-text">
                        <a id="btnEyeLogin" href="javascript:void(0)" style="display: none;"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-eye"><path d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path><circle cx="12" cy="12" r="3"></circle></svg></a>                                                        
                        <a id="btnEyeLoginOff" href="javascript:void(0)"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-eye-off"><path d="M17.94 17.94A10.07 10.07 0 0 1 12 20c-7 0-11-8-11-8a18.45 18.45 0 0 1 5.06-5.94M9.9 4.24A9.12 9.12 0 0 1 12 4c7 0 11 8 11 8a18.5 18.5 0 0 1-2.16 3.19m-6.72-1.07a3 3 0 1 1-4.24-4.24"></path><line x1="1" y1="1" x2="23" y2="23"></line></svg></a>                                                        
                    </span>
                </div>
        </div>
    </div>
    <div class="form-group hide">
        <label for="txtSenhaConfirmar"><b>Confirmar Senha:</b></label>
        <div class="input-group">
            <input type="password" class="form-control" id="txtSenhaConfirmar" name="txtSenhaConfirmar" value="" placeholder="Senha" required="">
                <div class="input-group-append">
                    <span class="input-group-text">
                        <a id="btnEyeConfirmar" href="javascript:void(0)" style="display: none;"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-eye"><path d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path><circle cx="12" cy="12" r="3"></circle></svg></a>                                                        
                        <a id="btnEyeConfirmarOff" href="javascript:void(0)"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-eye-off"><path d="M17.94 17.94A10.07 10.07 0 0 1 12 20c-7 0-11-8-11-8a18.45 18.45 0 0 1 5.06-5.94M9.9 4.24A9.12 9.12 0 0 1 12 4c7 0 11 8 11 8a18.5 18.5 0 0 1-2.16 3.19m-6.72-1.07a3 3 0 1 1-4.24-4.24"></path><line x1="1" y1="1" x2="23" y2="23"></line></svg></a>                                                        
                    </span>
                </div>
        </div>
    </div>
</form>