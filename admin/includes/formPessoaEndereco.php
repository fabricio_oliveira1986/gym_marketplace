<div class="form-row">
    <input id="txtIdEndereco" name="txtIdEndereco" type="hidden" value="">
    <div class="col-md-3">
        <label for="txtCep">CEP:</label>
        <input id="txtCep" name="txtCep" type="text" class="form-control" placeholder="00.000-000" value="">
    </div>
    <div class="col-md-9">
        <label for="txtEndereco">Endereço:</label>
        <input id="txtEndereco" name="txtEndereco" type="text" class="form-control" placeholder="Endereço" value="">
    </div>
    <div class="col-md-5">
        <label for="txtBairro">Bairro:</label>
        <input id="txtBairro" name="txtBairro" type="text" class="form-control" placeholder="Bairro" value="">
    </div>
    <div class="col-md-7">
        <label for="txtComplemento">Complemento:</label>
        <input id="txtComplemento" name="txtComplemento" type="text" class="form-control" placeholder="Complemento" value="">
    </div>
    <div class="col-md-2">
        <label for="txtNumero">Número:</label>
        <input id="txtNumero" name="txtNumero" type="text" class="form-control" placeholder="Num." value="">
    </div>
    <div class="col-md-3">
        <label for="txtEstado">Estado:</label>
        <select id="txtEstado" name="txtEstado" class="form-control" value=""></select>
    </div>
    <div class="col-md-7">
        <label for="txtCidade">Cidade:</label>
        <select id="txtCidade" name="txtCidade" class="form-control" value=""></select>
    </div>
</div>