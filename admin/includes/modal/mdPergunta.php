<div id="FormPergunta" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title">Responder:</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="FormCadastroPergunta" novalidate>                                   
                    <div class="form-row">
                        <input type="hidden" id="txtId" name="txtId" value="">
                        <div class="form-group col-md-12">
                            <label for="txtAnuncios"><b>Anúncios</b></label>
                            <p id="txtAnuncios"></p>
                        </div>
                        <div class="form-group col-md-12">
                            <label for="txtPergunta"><b>Pergunta</b></label>
                            <p id="txtPergunta"></p>
                        </div>
                        <div class="form-group col-md-12">
                            <label for="txtResposta"><b>Responder</b></label>
                            <textarea id="txtResposta" name="txtResposta" class="form-control" placeholder="" required></textarea>
                        </div>
                    </div>
                </form>                    
            </div>
            <div class="modal-footer">
                <button type="button" id="btnPergunta" class="btn btn-success">Salvar</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>