<div id="FormCropper" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title">Arquivo:</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-7 mb-2">
                        <div class="img-container">
                            <img id="image" class="img-container-preview" src="">
                        </div>
                    </div>
                    <div class="col-md-5">        
                        <div class="docs-buttons text-center">                         
                            <div class="btn-group mb-2">
                                <button type="button" class="btn btn-primary btn-icon btn-info" data-method="reset" title="Configurações iniciais">
                                    <i class="mdi mdi-undo"></i>
                                    <span data-feather="refresh-cw"></span>
                                </button>                                
                            </div>                        
                            <div class="btn-group mb-2">
                                <button type="button" class="btn btn-primary btn-icon btn-info" data-method="move" data-option="-20" data-second-option="0" title="Mover para esquerda">
                                    <i class="mdi mdi-arrow-left-bold"></i>
                                    <span data-feather="arrow-left"></span>
                                </button>                
                                <button type="button" class="btn btn-primary btn-icon btn-info" data-method="move" data-option="20" data-second-option="0" title="Mover para direita">
                                    <i class="mdi mdi-arrow-right-bold"></i>
                                    <span data-feather="arrow-right"></span>
                                </button>                                
                            </div>            
                            <div class="btn-group mb-2">                
                                <button type="button" class="btn btn-primary btn-icon btn-info" data-method="move" data-option="0" data-second-option="-20" title="Mover para cima">
                                    <span data-feather="arrow-up"></span>
                                </button>                
                                <button type="button" class="btn btn-primary btn-icon btn-info" data-method="move" data-option="0" data-second-option="20" title="Mover para baixo">
                                    <span data-feather="arrow-down"></span>
                                </button>                
                            </div>
                            <div class="btn-group mb-2">
                                <button type="button" class="btn btn-primary btn-icon btn-info" data-method="rotate" data-option="-90" title="Girar para esquerda">
                                    <span data-feather="rotate-ccw"></span>
                                </button>
                                <button type="button" class="btn btn-primary btn-icon btn-info" data-method="rotate" data-option="90" title="Girar para direita">
                                    <span data-feather="rotate-cw"></span>
                                </button>
                            </div>
                            <div class="btn-group mb-2">
                                <button type="button" class="btn btn-primary btn-icon btn-info" data-method="scaleX" data-option="-1" title="Inverter horizontalmente">
                                    <span data-feather="more-horizontal"></span>
                                </button>
                                <button type="button" class="btn btn-primary btn-icon btn-info" data-method="scaleY" data-option="-1" title="Inverter Verticalmente">
                                    <span data-feather="more-vertical"></span>
                                </button>
                            </div>            
                            <div class="btn-group mb-2">
                                <button type="button" class="btn btn-primary btn-icon btn-info" data-method="zoom" data-option="0.1" title="+ Zoom">
                                    <span data-feather="zoom-in"></span>
                                </button>
                                <button type="button" class="btn btn-primary btn-icon btn-info" data-method="zoom" data-option="-0.1" title="- Zoom">
                                    <span data-feather="zoom-out"></span>
                                </button>
                            </div>  
                            <div class="btn-group mb-2">
                                <label class="btn btn-success btn-icon btn-cropper" for="inputImage" title="Abrir">
                                    <input type="file" class="sr-only" id="inputImage" name="file" accept="image/*">
                                    <span data-feather="upload"></span> Anexar Foto
                                </label>
                            </div>                              
                            <div class="btn-group mb-2">
                                <button type="button" title="Capturar" class="btn btn-success btn-cropper" data-method="getCroppedCanvas" data-option="{ &quot;width&quot;: 320, &quot;height&quot;: 180 }">
                                    Capturar
                                </button>                
                            </div>
                        </div>
                        <input type="hidden" class="form-control" id="dataX" placeholder="x">
                        <input type="hidden" class="form-control" id="dataY" placeholder="x">
                        <input type="hidden" class="form-control" id="dataWidth" placeholder="width">
                        <input type="hidden" class="form-control" id="dataHeight" placeholder="height">
                        <input type="hidden" class="form-control" id="dataRotate" placeholder="rotate">
                        <input type="hidden" class="form-control" id="dataScaleX" placeholder="scaleX">
                        <input type="hidden" class="form-control" id="dataScaleY" placeholder="scaleY">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>