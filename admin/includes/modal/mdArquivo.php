<div id="FormArquivo" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title">Arquivo:</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="formCadastroArquivo" enctype="multipart/form-data" novalidate>                    
                    <div class="form-group">
                        <input type="hidden" id="txtType" name="txtType" value="">
                        <input type="hidden" id="txtIdItem" name="txtIdItem" value="">
                        <label for="txtArquivo" class="col-form-label">Arquivos:</label>
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="txtArquivo" name="txtArquivo[]" multiple="multiple" required>
                            <label class="custom-file-label" for="txtArquivo">Selecione</label>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">                
                <button type="button" id="btnArquivo" class="btn btn-success">Salvar</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>