<div id="FormTransf" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title">Solicitar:</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="FormCadastroTransf" novalidate>
                    <div class="form-row">
                        <input type="hidden" id="txtId" name="txtId" value="">
                        <div class="form-group col-md-8">
                            <label for="txtNome">Descrição:</label>
                            <input id="txtNome" type="text" class="form-control" value="" placeholder="" disabled>
                            <small id="txtNomeDetalhe" class="form-text text-muted"></small>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="txtValor">Valor a Solicitar</label>
                            <input id="txtValor" name="txtValor" type="text" class="form-control" value="" placeholder="0,00" required>
                        </div>
                    </div>
                </form>
            </div>            
            <div class="modal-footer">
                <button type="button" id="btnTransf" class="btn btn-success">Salvar</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>