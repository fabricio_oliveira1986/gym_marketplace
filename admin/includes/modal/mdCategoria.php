<div id="FormCategoria" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title">Categoria:</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="FormCadastroCategoria" novalidate>
                    <div class="form-row">
                        <input type="hidden" id="txtId" name="txtId" value="">
                        <div class="form-group col-md-8">
                            <label for="txtNome">Nome da Categoria</label>
                            <input id="txtNome" name="txtNome" type="text" class="form-control" value="" placeholder="Nome" required>
                        </div>
                        <div class="form-group col-md-2">
                            <label for="txtCorFonte">Fonte</label>
                            <input id="txtCorFonte" name="txtCorFonte" type="color" class="form-control" value="#000000" placeholder="Cor Fonte" required>
                        </div>
                        <div class="form-group col-md-2">
                            <label for="txtCorFundo">Fundo</label>
                            <input id="txtCorFundo" name="txtCorFundo" type="color" class="form-control" value="#ffffff" placeholder="Cor Fundo" required>
                        </div>
                        <div class="form-group col-md-12">
                            <label for="txtDescricao">Descrição da Categoria</label>
                            <textarea id="txtDescricao" name="txtDescricao" class="form-control" placeholder="Descrição"></textarea>
                        </div>
                        <div class="form-group col-md-12">
                            <div class="form-check">
                                <input id="txtInativo" name="txtInativo" type="checkbox" class="form-check-input" value="1">
                                <label class="form-check-label" for="txtInativo">Inativo</label>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnCategoria" class="btn btn-success">Salvar</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>