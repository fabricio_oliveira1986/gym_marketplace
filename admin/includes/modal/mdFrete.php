<div id="FormFrete" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title">Frete:</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="FormCadastroFrete" novalidate>
                    <div class="form-row">
                        <input type="hidden" id="txtId" name="txtId" value="">
                        <div class="form-group col-md-8">
                            <label for="txtNome">Titulo</label>
                            <input id="txtNome" name="txtNome" type="text" class="form-control" value="" placeholder="Ex: Frete dentro do município." required>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="txtValor">Valor</label>
                            <input id="txtValor" name="txtValor" type="text" class="form-control" value="" placeholder="0,00" required>
                        </div>                    
                        <div class="form-group col-md-12">
                            <label for="txtDescricao">Descrição</label>
                            <textarea id="txtDescricao" name="txtDescricao" class="form-control" placeholder="Explique melhor a condições para esse tipo de frete."></textarea>
                        </div>
                        <div class="form-group col-md-12">
                            <div class="form-check">
                                <input id="txtInativo" name="txtInativo" type="checkbox" class="form-check-input" value="1">
                                <label class="form-check-label" for="txtInativo">Inativo</label>
                            </div>
                        </div>
                    </div>
                </form>
            </div>            
            <div class="modal-footer">
                <button type="button" id="btnLink" class="btn btn-info mr-auto">Consultar</button>
                <button type="button" id="btnFrete" class="btn btn-success">Salvar</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>