<div id="FormSocio" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title">Sócio:</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="FormCadastroSocio" novalidate>
                    <input id="txtIdSocio" name="txtIdSocio" type="hidden" value="">
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label for="txtSocioNome">Nome do Sócio</label>
                            <input id="txtSocioNome" name="txtSocioNome" type="text" class="form-control" value="" placeholder="Nome do Sócio">
                        </div>                                                                        
                    </div>                
                    <div class="form-row">                                                
                        <div class="form-group col-md-8 col-6">
                            <label for="txtSocioCpf">CPF do Sócio</label>
                            <input id="txtSocioCpf" name="txtSocioCpf" type="text" class="form-control" value="" placeholder="CPF">
                        </div>                                        
                        <div class="form-group col-md-4 col-6">
                            <label for="txtSocioDt">Data de Nascimento</label>
                            <input id="txtSocioDt" name="txtSocioDt" type="text" class="form-control" value="" placeholder="00/00/0000">
                        </div>                    
                    </div>              
                </form>
            </div>
            <div class="modal-footer">                
                <button type="button" id="btnSocio" class="btn btn-success">Salvar</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>