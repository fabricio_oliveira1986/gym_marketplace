<div class="form-row">
    <div class="col-md-12">
        <label for="txtNome">Nome Completo:</label>
        <input id="txtNome" name="txtNome" type="text" class="form-control" placeholder="Nome Completo" value="">
    </div>
    <div class="col-md-4">
        <label for="txtDtNasc">Data de Nascimento:</label>
        <input id="txtDtNasc" name="txtDtNasc" type="text" class="form-control" placeholder="00/00/0000" value="">
    </div>
    <div class="col-md-4">
        <label for="txtCPF">CPF:</label>
        <input id="txtCPF" name="txtCPF" type="text" class="form-control" placeholder="000.000.000-00" value="">
    </div>
    <div class="col-md-4">
        <label for="txtCelular">Celular:</label>
        <input id="txtCelular" name="txtCelular" type="text" class="form-control" placeholder="(00) 00000-0000" value="">
    </div>        
    <div class="col-md-12">
        <label for="txtSexo">Gênero:</label>
        <div class="form-check form-check-inline">
            <input id="txtSexoM" name="txtSexo" type="radio" class="form-check-input" value="M" checked>
            <label class="form-check-label" for="txtSexoM">Masculino</label>
        </div>
        <div class="form-check form-check-inline">
            <input id="txtSexoF" name="txtSexo" type="radio" class="form-check-input" value="F">
            <label class="form-check-label" for="txtSexoF">Feminino</label>
        </div>
    </div>
    <div class="col-md-12">
        <label for="txtEmail">E-mail:</label>
        <input id="txtEmail" name="txtEmail" type="email" class="form-control" placeholder="E-mail" value="">
    </div>
    <div class="col-md-12">
        <label for="txtSenha">Senha:</label>
        <div class="input-group">
            <input id="txtSenha" name="txtSenha" type="password" class="form-control" placeholder="Senha" value="">
            <div class="input-group-append">
                <span class="input-group-text">
                    <a id="btnEye" href="javascript:void(0)" style="display: none;"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-eye"><path d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path><circle cx="12" cy="12" r="3"></circle></svg></a>                                                        
                    <a id="btnEyeOff" href="javascript:void(0)"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-eye-off"><path d="M17.94 17.94A10.07 10.07 0 0 1 12 20c-7 0-11-8-11-8a18.45 18.45 0 0 1 5.06-5.94M9.9 4.24A9.12 9.12 0 0 1 12 4c7 0 11 8 11 8a18.5 18.5 0 0 1-2.16 3.19m-6.72-1.07a3 3 0 1 1-4.24-4.24"></path><line x1="1" y1="1" x2="23" y2="23"></line></svg></a>                                                        
                </span>
            </div>
        </div>
    </div>        
</div>
