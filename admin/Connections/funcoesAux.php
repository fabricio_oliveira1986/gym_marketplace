<?php

function valoresTexto($campo) {
    return str_replace(array(".", "/", "-", "(", ")", " "), "", $campo);
}

function valoresNumericos($campo) {
    return str_replace(",", ".", str_replace(".", "", $campo));
}

function valoresData($campo) {
    return implode('-', array_reverse(explode('/', $campo)));
}

function escreverData($data) {
    return ($data == "" ? "" : date_format(date_create($data), 'd/m/Y'));
}

function escreverDataHora($data) {
    return ($data == "" ? "" : date_format(date_create($data), 'd/m/Y H:i'));
}

function escreverNumero($nome, $ident = 0, $decimal = 2, $dec_point = ",", $thousands_sep = ".") {
    return ($ident == 1 ? "R$ " : "") . number_format($nome, $decimal, $dec_point, $thousands_sep);
}

function deleteDirectory($dir) {
    if (!file_exists($dir)) {
        return true;
    }
    if (!is_dir($dir)) {
        return unlink($dir);
    }
    foreach (scandir($dir) as $item) {
        if ($item == '.' || $item == '..') {
            continue;
        }
        if (!deleteDirectory($dir . DIRECTORY_SEPARATOR . $item)) {
            return false;
        }
    }
    return rmdir($dir);
}

function saveImgDirectory($dados) {
    $imageViewer = getPathFile($dados);
    if (strlen($dados["imageNow"]) > 0 && file_exists($imageViewer . "/" . $dados["imageNow"])) {
        unlink($imageViewer . "/" . $dados["imageNow"]);
    }
    if ($dados["imageDir"] != "null") {
        if (!file_exists($imageViewer)) {
            mkdir($imageViewer, 0777, true);
        }      
        $img = md5(microtime()) . ".png";
        $ifp = fopen($imageViewer . "/" . $img, 'wb');
        fwrite($ifp, base64_decode(explode(',', $dados["imageDir"])[1]));
        fclose($ifp);
        return $img; 
    }
    return null;
}

function saveFile($conn, $dados, $newFile) {
    if ($dados["txtType"] == "perfil") {
        $prepareSQL = $conn->prepare("UPDATE sf_pessoa SET id_foto = :id_foto WHERE id = :id");
        $prepareSQL->bindValue(':id', $_SESSION['id']);
        $prepareSQL->bindValue(':id_foto', $newFile);
        $prepareSQL->execute();        
    } else if ($dados["txtType"] == "produto") {
        $prepareSQL = $conn->prepare("UPDATE sf_empresa_produto SET id_foto = :id_foto WHERE id = :id");
        $prepareSQL->bindValue(':id', $dados["txtId"]);
        $prepareSQL->bindValue(':id_foto', $newFile);
        $prepareSQL->execute();        
    } else if ($dados["txtType"] == "empresa") {
        $prepareSQL = $conn->prepare("UPDATE sf_empresa SET id_foto = :id_foto WHERE id = :id");
        $prepareSQL->bindValue(':id', $dados["txtId"]);
        $prepareSQL->bindValue(':id_foto', $newFile);
        $prepareSQL->execute();        
    }
}

function encrypt($frase, $chave, $crypt) {
    $retorno = "";
    if ($crypt) {
        $string = $frase;
        $i = strlen($string) - 1;
        $j = strlen($chave);
        do {
            $retorno .= ($string{$i} ^ $chave{$i % $j});
        } while ($i--);
        return base64_encode(strrev($retorno));
    } else {
        $string = base64_decode($frase);
        $i = strlen($string) - 1;
        $j = strlen($chave);
        do {
            $retorno .= ($string{$i} ^ $chave{$i % $j});
        } while ($i--);
        return strrev($retorno);
    }
}

function formatNameCompact($name) {
    $CompleteName = explode(" ", $name);
    $nomeUserLogin = $CompleteName[0];
    if (count($CompleteName) > 1) {
        $nomeUserLogin .= " " . $CompleteName[count($CompleteName) - 1];
    }
    return $nomeUserLogin;
}

function getLojaSession($conn) {
    $prepareSQL = $conn->prepare("SELECT * FROM sf_empresa WHERE id in (select id_empresa from sf_pessoa_empresa where id_pessoa = :id) limit 1");
    $prepareSQL->bindValue(':id', $_SESSION['id']);
    $prepareSQL->execute();
    $return = $prepareSQL->fetch(PDO::FETCH_OBJ);
    return $return;
}

function getPathFile($dados) {
    if ($dados["txtType"] == "perfil") {
        return "./../assets/imagens/pessoas/" . $_SESSION['id'] . "/";
    } else if ($dados["txtType"] == "produto") {
        return "./../assets/imagens/produtos/" . $dados["txtId"] . "/";
    } else if ($dados["txtType"] == "empresa") {
        return "./../assets/imagens/empresas/" . $dados["txtId"] . "/";         
    } else if ($dados["txtType"] == "pFotos" || $dados["txtType"] == "videos") {
        return "./../assets/imagens/produtos/" . $dados["txtId"] . "/list/";
    } else if ($dados["txtType"] == "lFotos") {
        return "./../assets/imagens/empresas/" . $dados["txtId"] . "/list/"; 
    }
}

function getUrlFile($tipo, $id, $arq) {
    $url = 'https://gymbrother.com.br/admin';
    if ($tipo == "perfil") {
        return $url . "/assets/imagens/pessoas/" . $id . "/" . $arq;
    } else if ($tipo == "produto") {
        return $url . "/assets/imagens/produtos/" . $id . "/" . $arq;
    } else if ($tipo == "empresa") {
        return $url . "/assets/imagens/empresas/" . $id . "/" . $arq;        
    } else if ($tipo == "pFotos" || $tipo == "videos") {
        return $url . "/assets/imagens/produtos/" . $id . "/list/" . $arq;
    } else if ($tipo == "lFotos") {
        return $url . "/assets/imagens/empresas/" . $id . "/list/" . $arq;
    } else if ($tipo == "categoria") {
        return $url . "/assets/img/categories/" . $arq;
    }
}

function getUrlImagens($tipo, $id) {
    $url = 'https://gymbrother.com.br/admin/utils/galeria/timthumb.php?src=../admin/';
    $data = [];
    $caminho = "assets/imagens/" . $tipo . "/" . $id . "/list/";
    $caminhoAbsoluto = __DIR__ . "/../" . $caminho;
    if (is_dir($caminhoAbsoluto)) {
        foreach (scandir($caminhoAbsoluto) as $key => $value) {
            if (!in_array($value, array(".", "..")) && !is_dir($caminhoAbsoluto . $value) && 
                in_array(substr($value, -3), array("gif", "jpg", "png", "peg"))) {
                $data[] = $url . $caminho . $value . '&w=600';
            }
        }
    }
    return $data;
}

function getUrlVideos($tipo, $id) {
    $url = 'https://gymbrother.com.br/admin/';
    $data = [];
    $caminho = "assets/imagens/" . $tipo . "/" . $id . "/list/";
    $caminhoAbsoluto = __DIR__ . "/../" . $caminho;
    if (is_dir($caminhoAbsoluto)) {
        foreach (scandir($caminhoAbsoluto) as $key => $value) {
            if (!in_array($value, array(".", "..")) && !is_dir($caminhoAbsoluto . $value) && 
                in_array(substr($value, -3), array("mp4"))) {
                $row = [];
                $row["image"] = $url . "assets/img/video.jpg";                
                $row["link"] = $url . $caminho . $value;
                $data[] = $row;
            }
        }
    }
    return $data;
}

function getBandeira($number) {
    $brand = "null";
    $brands = array(
	'visa'       => '/^4\d{12}(\d{3})?$/',
	'mastercard' => '/^(5[1-5]\d{4}|677189)\d{10}$/',
	'diners'     => '/^3(0[0-5]|[68]\d)\d{11}$/',
	'discover'   => '/^6(?:011|5[0-9]{2})[0-9]{12}$/',
	'elo'        => '/^((((636368)|(438935)|(504175)|(451416)|(636297))\d{0,10})|((5067)|(4576)|(4011))\d{0,12})$/',
	'amex'       => '/^3[47]\d{13}$/',
	'jcb'        => '/^(?:2131|1800|35\d{3})\d{11}$/',
	'aura'       => '/^(5078\d{2})(\d{2})(\d{11})$/',
	'hipercard'  => '/^(606282\d{10}(\d{3})?)|(3841\d{15})$/',
	'maestro'    => '/^(?:5[0678]\d\d|6304|6390|67\d\d)\d{8,15}$/');                
        foreach ($brands as $_brand => $regex) {
            if (preg_match($regex, $number)) {
                $brand = $_brand;
                break;
            }
        }
        return $brand;
}