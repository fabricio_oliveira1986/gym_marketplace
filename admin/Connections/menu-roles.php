<?php

$pageSel = basename($_SERVER['PHP_SELF']);

if (!isset($_SESSION['id']) && !in_array($pageSel, array("login.php", "FormPessoa.php", "FormApp.php", "FormVenda.php", "webhooks.php"))) {
    header('location:./../login.php');
} elseif (isset($_SESSION['id']) && is_numeric($_SESSION['id'])) {
    $prepareSQL = $conn->prepare("SELECT * FROM sf_pessoa WHERE id = :id limit 1");
    $prepareSQL->bindValue(':id', $_SESSION['id']);
    $prepareSQL->execute();
    $res = $prepareSQL->fetch();
    if ($res) {
        $pessoa_session = ["id" => $res['id'],
            "img" => ($res['id_foto'] == null ? "assets/img/no_person.png" : "assets/imagens/pessoas/" . $res['id'] . "/" . $res['id_foto']),
            "nome" => formatNameCompact($res['nome']),
            "empresa" => "Administrador"];
    }
}

function getTotalTypeItem($conn, $id, $table) {
    $prepareSQL = $conn->prepare("select count(pr.id) total from $table pr
    inner join sf_empresa_produto_anuncio epa on pr.id_anuncio = epa.id
    inner join sf_empresa_produto ep on ep.id = epa.id_empresa_produto
    inner join sf_pessoa_empresa pe on pe.id_empresa = ep.id_empresa
    where pe.id_pessoa = :id and pr.data_resposta is null and pr.inativo = 0 limit 1");
    $prepareSQL->bindValue(':id', $id);
    $prepareSQL->execute();
    $res = $prepareSQL->fetch();
    if ($res) {
        return $res['total'];
    }
}

if (isset($_SESSION['id'])) {
    $mnu_cliente[] = ["text" => "Página Inicial", "link" => "index.php", "icon" => "monitor", "info" => "0"];
    $mnu_cliente[] = ["text" => "Meu Perfil", "link" => "meu-perfil.php", "icon" => "user", "info" => "0"];
    $pessoa_menu_lateral[] = ["text" => "Principal", "menu" => $mnu_cliente];

    $mnu_projeto[] = ["text" => "Minha Loja", "link" => "minha-loja.php", "icon" => "users", "info" => "0"];
    $mnu_projeto[] = ["text" => "Minhas Categorias", "link" => "categorias.php", "icon" => "shopping-bag", "info" => "0"];
    $mnu_projeto[] = ["text" => "Meus Anúncios", "link" => "produtos.php", "icon" => "shopping-cart", "info" => "0"];
    $mnu_projeto[] = ["text" => "Anúncio", "link" => "produto.php", "icon" => "shopping-cart", "menu" => "produtos.php", "info" => "0"];
    $mnu_projeto[] = ["text" => "Fretes", "link" => "frete.php", "icon" => "truck", "info" => "0"];
    $pessoa_menu_lateral[] = ["text" => "Cadastros", "menu" => $mnu_projeto];

    $mnu_admin[] = ["text" => "Minhas Vendas", "link" => "vendas.php", "icon" => "dollar-sign", "info" => "0"];
    $mnu_admin[] = ["text" => "Detalhes Venda", "link" => "detalhes.php", "icon" => "dollar-sign", "menu" => "vendas.php", "info" => "0"];
    $mnu_admin[] = ["text" => "Perguntas e Respostas", "link" => "perguntas.php", "icon" => "help-circle", "info" => getTotalTypeItem($conn, $_SESSION['id'], "sf_pergunta_resposta")];
    $mnu_admin[] = ["text" => "Avaliações de Vendas", "link" => "avaliacoes.php", "icon" => "star", "info" => getTotalTypeItem($conn, $_SESSION['id'], "sf_vendas_avaliacao")];
    $pessoa_menu_lateral[] = ["text" => "Administrativo", "menu" => $mnu_admin];
    
    $mnu_suporte[] = ["text" => "(24) 98121-5823", "link" => "https://wa.me/+5524999231414", "icon" => "phone", "info" => "0"];
    $mnu_suporte[] = ["text" => "gymbrotheroficial@gmail.com", "link" => "mailto:gymbrotheroficial@gmail.com", "icon" => "mail", "info" => "0"];
    $pessoa_menu_lateral[] = ["text" => "Atendimento", "menu" => $mnu_suporte];

    $icon_page = "";
    $name_page = "";

    foreach ($pessoa_menu_lateral as $value) {
        foreach ($value["menu"] as $menu) {
            if ($pageSel == $menu["link"]) {
                $icon_page = $menu["icon"];
                $name_page = $menu["text"];
                if (isset($menu["menu"])) {
                    $pageSel = $menu["menu"];
                }
            }
        }
    }
}