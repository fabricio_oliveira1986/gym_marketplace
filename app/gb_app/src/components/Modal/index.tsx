import React from 'react';

import Card from '~/components/Card';

import ContainerModal from 'react-native-modal';

import {Box} from './style';
import {ViewStyle} from 'react-native';

interface ModalProps {
  children: any;
  width?: number;
  height?: number;
  background?: string;
  direction?: boolean;
  align?: 'center' | 'flex-start' | 'flex-end' | 'stretch';
  modalVisible: boolean;
  style?: ViewStyle;
  onRequestClose: () => void;
}

const Modal: React.FC<ModalProps> = ({
  children,
  modalVisible,
  onRequestClose,
  ...props
}) => {
  return (
    <ContainerModal
      isVisible={modalVisible}
      onBackdropPress={onRequestClose}
      style={{
        width: '100%',
        height: '100%',
        alignSelf: 'center',
      }}>
      <Box>
        <Card
          {...props}
          background={props.background}
          align={props.align}
          // width={props.width}
          direction={props.direction}>
          {children}
        </Card>
      </Box>
    </ContainerModal>
  );
};

export default Modal;
