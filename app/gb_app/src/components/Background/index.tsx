import React from 'react';
import {BackgroundImage, StatusBarHeader} from './style';
import Images from '~/assets/images/Background';

const Background: React.FC = props => {
  return (
    <>
      <StatusBarHeader
        barStyle="light-content"
        backgroundColor="transparent"
        translucent={true}
      />
      <BackgroundImage source={Images.Background}>
        {props.children}
      </BackgroundImage>
    </>
  );
};

export {Background};
