import styled from 'styled-components/native';
import {Platform, StatusBar} from 'react-native';
import {getStatusBarHeight} from 'react-native-iphone-x-helper';

const paddingTop =
  (Platform.OS === 'ios' ? getStatusBarHeight() : StatusBar.currentHeight) + 10;

export const BackgroundImage = styled.ImageBackground.attrs({
  resizeMode: 'cover',
})`
  flex: 1;
  width: 100%;
  justify-content: center;
  padding-top: ${paddingTop}px;
`;

export const StatusBarHeader = styled.StatusBar``;
