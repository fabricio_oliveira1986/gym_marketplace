import React, {useEffect} from 'react';
import {StyleSheet} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {HeaderBackButton} from '@react-navigation/stack';
import {useTheme} from 'styled-components/native';
import ImagesLogo from '~/assets/images/Logos';
import ImagesIcon from '~/assets/images/Icons';
import {Icon} from 'react-native-elements'
import {numberReal} from '~/global/utils/functions';
import {useCart} from '~/hooks/cart';
import * as cashbackService from '~/services/cashback';

import {
  Container,
  BoxLogo,
  Logo,
  BoxBalance,
  BoxBalanceItemFlow,
  BoxTextItemFlow,
  TextItemFlow,
  TextBalance,
  BtnSignIn
} from './style';
import {useAuth} from '~/hooks/auth';

const Header: React.FC = () => {
  const navigation = useNavigation();
  const theme = useTheme();
  const {setBalanceCashBack, balanceCashBack} = useCart();
  const {user} = useAuth();

  useEffect(() => {
    if (user?.id) {
      cashbackService.getValueCashback().then(values => {
        setBalanceCashBack(Number(values.total_liquido));
      });
    }
  }, [user]);

  return (
    <Container>
      {navigation.canGoBack() && (
        <HeaderBackButton
          tintColor={theme.colors.shape}
          onPress={() => navigation.goBack()}
          style={{marginRight: 10}}
        />
      )}
      <BoxLogo>
        <Logo source={ImagesLogo.LogoHeader} />
      </BoxLogo>

      
      {user?.id ? (<BoxBalance
        onPress={() =>
          navigation.navigate('Extracts', {
            screen: 'Extracts',
          })
        }>
        <BoxBalanceItemFlow>
          <ImagesIcon.MoneyFlow
            width={25}
            height={25}
            fill={theme.colors.success}
          />
          <BoxTextItemFlow>
            <TextItemFlow>Cashback</TextItemFlow>
            <TextBalance>{numberReal(balanceCashBack)}</TextBalance>
          </BoxTextItemFlow>
        </BoxBalanceItemFlow>
      </BoxBalance>) : (
        <BtnSignIn> 
        <Icon
        name='log-in-outline'
        type='ionicon'
        size={34}
        color={theme.colors.success}
        style={styles.btnSigin}
        onPress={() => {
          navigation.navigate('SignIn');
        }}
          />
        </BtnSignIn>
      )}
      
    </Container>
  );
};

const styles = StyleSheet.create({
  btnSigin: {

  }})


export default Header;
