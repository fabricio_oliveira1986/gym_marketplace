import styled from 'styled-components/native';
import {Platform, StatusBar} from 'react-native';
import {getStatusBarHeight} from 'react-native-iphone-x-helper';
import {RFValue} from 'react-native-responsive-fontsize';

const paddingTop =
  (Platform.OS === 'ios' ? getStatusBarHeight() : StatusBar.currentHeight) + 10;

export const Container = styled.View`
  background: ${({theme}) => theme.colors.primary};
  padding-top: ${RFValue(paddingTop)}px;
  padding-left: 15px;
  padding-right: 15px;
  padding-bottom: 10px;
  width: 100%;
  flex-direction: row;
  justify-content: flex-start;
  align-items: center;
`;

export const BoxLogo = styled.TouchableOpacity.attrs({
  activeOpacity: 0.7,
})``;

export const Logo = styled.Image.attrs({
  resizeMode: 'contain',
})`
  height: ${RFValue(30)}px;
  width: ${RFValue(90)}px;
`;

export const BoxBalance = styled.TouchableOpacity.attrs({
  activeOpacity: 0.7,
})`
  justify-content: center;
  flex: 1;
`;

export const BoxBalanceItemFlow = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: flex-end;
`;

export const BoxTextItemFlow = styled.View`
  margin-left: 8px;
`;

export const TextItemFlow = styled.Text`
  font-family: ${({theme}) => theme.fonts.medium};
  font-size: ${RFValue(10)}px;
  text-transform: uppercase;
  color: ${({theme}) => theme.colors.success};
`;

export const TextBalance = styled.Text`
  font-size: ${RFValue(15)}px;
  color: ${({theme}) => theme.colors.shape};
  font-family: ${({theme}) => theme.fonts.medium};
  text-align: right;
`;


export const BtnSignIn = styled.View`
  justify-content: flex-end;
  align-items: flex-end;
  flex: 1;
`;