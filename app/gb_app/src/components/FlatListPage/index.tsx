import React from 'react';
import {FlatListProps} from 'react-native';
import {FlatListComponent} from './style';

export interface Item {
  key: string;
  render: () => JSX.Element;
  isTitle?: boolean;
}

interface FlatListPageProps extends Omit<FlatListProps<Item>, 'renderItem'> {}

const FlatListPage: React.FC<FlatListPageProps> = ({...rest}) => {
  return (
    <FlatListComponent
      renderItem={({item}) => item.render()}
      keyExtractor={item => item.key}
      showsVerticalScrollIndicator={false}
      {...rest}
    />
  );
};

export default FlatListPage;
