import styled from 'styled-components/native';
import {FlatList} from 'react-native';
import {Item} from './';

export const FlatListComponent = styled(FlatList as new () => FlatList<Item>)``;
