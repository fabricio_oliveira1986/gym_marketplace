import styled from 'styled-components/native';

import {RFValue} from 'react-native-responsive-fontsize';

interface ContainerProps {
  hasPadding?: boolean;
}

export const Container = styled.View<ContainerProps>`
  margin-top: ${RFValue(10)}px;
  padding-left: ${props => (props.hasPadding ? '15px' : '0px')};
  padding-right: ${props => (props.hasPadding ? '15px' : '0px')};
`;

export const ListProductTitle = styled.Text`
  color: ${({theme}) => theme.colors.text};
  font-family: ${({theme}) => theme.fonts.medium};
  font-size: ${RFValue(16)}px;
  margin-bottom: 15px;
`;

export const ListProduct = styled.View``;
