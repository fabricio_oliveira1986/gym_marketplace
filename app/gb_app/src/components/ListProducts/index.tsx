import React from 'react';

import {Product} from '~/models/Product';

import ItemProduct from '~/components/ItemProduct';

import {Container, ListProductTitle, ListProduct} from './style';

interface ListProductProps {
  title?: string;
  showLogo?: boolean;
  hasPadding?: boolean;
  data: Product[];
}

const ListProducts: React.FC<ListProductProps> = ({
  title,
  showLogo,
  hasPadding,
  data,
}) => {
  return (
    <Container hasPadding={hasPadding}>
      {title && <ListProductTitle>{title}</ListProductTitle>}
      <ListProduct>
        {data.map(item => (
          <ItemProduct key={item.id} data={item} showLogo={showLogo} />
        ))}
      </ListProduct>
    </Container>
  );
};

export default ListProducts;
