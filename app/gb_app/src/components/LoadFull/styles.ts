import styled from 'styled-components/native';

export const Container = styled.View`
  width: 100%;
  height: 100%;
  position: absolute;
  z-index: 9999;
  justify-content: center;
  align-items: center;
  opacity: 0.7;
  background-color: white;
`;

export const ActivityIndicator = styled.ActivityIndicator``;
