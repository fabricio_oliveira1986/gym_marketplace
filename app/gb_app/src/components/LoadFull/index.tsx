import React from 'react';

import {Container, ActivityIndicator} from './styles';

const LoadFull: React.FC = () => {
  return (
    <Container>
      <ActivityIndicator size="large" color="#35B290" />
    </Container>
  );
};

export default LoadFull;
