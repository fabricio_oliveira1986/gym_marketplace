import styled from 'styled-components/native';

export const Container = styled.View`
  flex-direction: row;
  width: 100%;
`;

export const Item = styled.TouchableOpacity.attrs({
  activeOpacity: 0.7,
})``;
