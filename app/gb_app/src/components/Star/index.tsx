import React, {useState, useEffect, useCallback} from 'react';

import ImagesIcon from '~/assets/images/Icons';
import {Container, Item} from './style';

interface StarsProps {
  starNumber?: number;
  size: number;
  color: string;
  editable?: boolean;
  onChangeStar?: (stars: number[]) => void;
}

const Star: React.FC<StarsProps> = ({
  starNumber,
  size,
  color,
  editable,
  onChangeStar,
}) => {
  const [stars, setStars] = useState([0, 0, 0, 0, 0]);

  const selectStar = useCallback(
    (index: number, hasEditable: boolean | undefined) => {
      if (hasEditable) {
        setStars(itemStars =>
          itemStars.map((_, indexStar) => {
            return indexStar <= index ? 1 : 0;
          }),
        );
        onChangeStar && onChangeStar(stars);
        console.log(stars);
      }
    },
    [onChangeStar, stars],
  );

  useEffect(() => {
    setStars(itemStars =>
      itemStars.map((_, indexStar) => {
        return indexStar <= Number(starNumber) ? 1 : 0;
      }),
    );
  }, [starNumber]);

  return (
    <Container>
      {stars.map((item, index) => (
        <Item key={index} onPress={() => selectStar(index, editable)}>
          {item ? (
            <ImagesIcon.StarFull width={size} height={size - 2} fill={color} />
          ) : (
            <ImagesIcon.StarEmpty width={size} height={size - 2} fill={color} />
          )}
        </Item>
      ))}
    </Container>
  );
};

export default Star;
