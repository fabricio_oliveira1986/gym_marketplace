import React from 'react';

import {useTheme} from 'styled-components/native';

import {numberDecimal, dateTimeBr} from '~/global/utils/functions';

import Star from '~/components/Star';

import {Avaliation} from '~/models/Avaliation';

import {
  Container,
  NameUser,
  ContentStars,
  StarsNote,
  BoxAvaliation,
  DateAvaliation,
  TextComments,
  BoxComments,
  BoxReply,
} from './style';

interface ItemAvaliationProps {
  item: Avaliation;
}

const ItemAvaliation: React.FC<ItemAvaliationProps> = ({item: avaliation}) => {
  const theme = useTheme();

  return (
    <Container>
      <NameUser>{avaliation.user.name}</NameUser>
      <BoxAvaliation>
        <ContentStars>
          <StarsNote>{numberDecimal(avaliation.stars)}</StarsNote>
          <Star
            size={18}
            color={theme.colors.warning}
            starNumber={avaliation.stars}
          />
        </ContentStars>
        <DateAvaliation>{dateTimeBr(avaliation.date)}</DateAvaliation>
      </BoxAvaliation>
      <TextComments>{avaliation.commentary}</TextComments>
      {avaliation.reply && (
        <BoxComments>
          <BoxReply>
            <NameUser>Resposta:</NameUser>
            <DateAvaliation>{dateTimeBr(avaliation.dateReply)}</DateAvaliation>
          </BoxReply>
          <TextComments>{avaliation.reply}</TextComments>
        </BoxComments>
      )}
    </Container>
  );
};

export default ItemAvaliation;
