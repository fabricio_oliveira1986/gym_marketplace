import styled from 'styled-components/native';
import {RFValue} from 'react-native-responsive-fontsize';

export const Container = styled.View`
  width: 100%;
  padding: ${RFValue(10)}px ${RFValue(15)}px;
`;

export const NameUser = styled.Text`
  font-family: ${({theme}) => theme.fonts.medium};
  margin-bottom: ${RFValue(5)}px;
  font-size: ${RFValue(14)}px;
`;

export const ContentStars = styled.View`
  flex-direction: row;
`;

export const StarsNote = styled.Text`
  margin-right: ${RFValue(8)}px;
  font-family: ${({theme}) => theme.fonts.medium};
`;

export const BoxAvaliation = styled.View`
  flex-direction: row;
  margin-bottom: ${RFValue(5)}px;
`;

export const DateAvaliation = styled.Text`
  margin-left: ${RFValue(8)}px;
`;

export const TextComments = styled.Text`
  font-family: ${({theme}) => theme.fonts.regular};
  font-size: ${RFValue(14)}px;
  margin-bottom: ${RFValue(5)}px;
`;

export const BoxComments = styled.View`
  background: ${({theme}) => theme.colors.muted};
  padding: ${RFValue(15)}px;
  margin-top: ${RFValue(10)}px;
  border-radius: ${RFValue(8)}px;
`;

export const BoxReply = styled.View`
  flex-direction: row;
  justify-content: space-between;
`;
