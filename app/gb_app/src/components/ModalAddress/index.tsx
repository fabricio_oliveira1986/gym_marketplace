import React, {useState, useEffect} from 'react';
import {Alert} from 'react-native';
import * as AddressService from '~/services/address';
import {isEmpty} from 'lodash';
import {Address} from '~/models/Address';
import {useTheme} from 'styled-components/native';
import {Modal, InputEdit, Button, LoadFull, BoxPercent} from '~/components';
import {UserDataAddress, schemaUserDataAddress} from '~/models/Profile';

interface ModalProps {
  isVisibleModal: boolean;
  onRequestClose: () => void;
  setIsVisibleModal: (data: boolean) => void;
  dataAddressEdit: Address | {};
}

const initialStateAddress = {
  txtCep: '',
  txtEndereco: '',
  txtBairro: '',
  txtCidade: '',
  txtEstado: '',
  txtNumero: '',
  txtComplemento: '',
};

const ModalAddress: React.FC<ModalProps> = ({
  isVisibleModal,
  onRequestClose,
  setIsVisibleModal,
  dataAddressEdit,
}) => {
  const theme = useTheme();
  const [isLoading, setIsLoading] = useState(false);
  const [dataAddress, setDataAddress] = useState<UserDataAddress>(
    initialStateAddress as UserDataAddress,
  );

  const addNewAdress = async () => {
    try {
      const body = {
        ...dataAddress,
        txtCep: dataAddress.txtCep,
        ...(dataAddress.txtComplemento
          ? {txtComplemento: dataAddress.txtComplemento}
          : {txtComplemento: ''}),
      };

      const isValid = await schemaUserDataAddress.isValid(dataAddress);
      if (isValid) {
        await AddressService.postNewAddress(body);
        setDataAddress(initialStateAddress);
        setIsVisibleModal(false);
      } else {
        Alert.alert('Atenção', 'Verifique se todos os campos foram digitados');
      }
    } catch (error) {
      Alert.alert(
        'Ops! Algo deu Errado',
        'Não foi possível registrar um novo e-mail',
      );
    }
  };

  const searchZipCode = async () => {
    try {
      setIsLoading(true);
      const data = await AddressService.searchCep(
        dataAddress.txtCep.replace(/[.\s]/g, ''),
      );
      if (data.erro) {
        throw '';
      } else {
        setDataAddress({
          ...dataAddress,
          txtEndereco: data.logradouro,
          txtBairro: data.bairro,
          txtCidade: data.localidade,
          txtEstado: data.uf,
        });
      }

      setIsLoading(false);
    } catch (error) {
      setIsLoading(false);
      Alert.alert('', 'Cep Invalido!');
    }
  };

  useEffect(() => {
    if (!isEmpty(dataAddress)) {
      if (dataAddress.txtCep.length === 10) {
        searchZipCode();
      }
    }
  }, [dataAddress.txtCep]);

  const deleteAddress = async () => {
    try {
      setIsLoading(true);
      const body = {id_endereco: dataAddress.id_endereco};
      await AddressService.deleteAddress(body);
      setIsLoading(false);
      setIsVisibleModal(false);
    } catch (error) {
      Alert.alert('', 'Não foi possível excluir seu endereço');
      setIsLoading(false);
    }
  };

  const updateAddress = async () => {
    try {
      setIsLoading(true);
      const body = {...dataAddress, txtIdEndereco: dataAddress.id_endereco};
      const isValid = await schemaUserDataAddress.isValid(body);
      if (isValid) {
        await AddressService.updateAddress(body);
      } else {
        Alert.alert('', 'Verifique se todos os dados estão corretos');
      }
      setIsLoading(false);
      setIsVisibleModal(false);
    } catch (error) {
      setIsLoading(false);
      Alert.alert('', 'Não foi possível atualizar seu endereço');
    }
  };

  useEffect(() => {
    if (isVisibleModal && !isEmpty(dataAddressEdit)) {
      setDataAddress({
        ...dataAddress,
        txtCep: dataAddressEdit.postCode,
        txtEndereco: dataAddressEdit.street,
        txtBairro: dataAddressEdit.neighborhood,
        txtCidade: dataAddressEdit.city,
        txtEstado: dataAddressEdit.state,
        txtNumero: dataAddressEdit.number,
        txtComplemento: dataAddressEdit.complement,
        id_endereco: dataAddressEdit.id,
      });
    } else {
      setDataAddress({});
    }
  }, [isVisibleModal]);

  return (
    <Modal
      modalVisible={isVisibleModal}
      onRequestClose={onRequestClose}
      width={90}
      align="center"
      noPadding>
      {isLoading && <LoadFull />}

      <BoxPercent pl={20} pr={20} pt={20} pb={20} width="100%">
        <InputEdit
          label="CEP"
          masked
          type="custom"
          style={{fontSize: 13, width: '100%'}}
          options={{
            mask: '99.999-999',
          }}
          theme={theme}
          editable
          value={dataAddress.txtCep}
          onChangeText={(txt: String) => {
            setDataAddress({...dataAddress, txtCep: txt});
          }}
        />


        <BoxPercent direction justify="space-between" width="100%">
          <BoxPercent width="65%">
            <InputEdit
              label="Cidade"
              fontSize={13}
              theme={theme}
              editable={false}
              value={dataAddress.txtCidade}
              onChangeText={(txt: String) => {
                setDataAddress({...dataAddress, txtCidade: txt});
              }}
            />
          </BoxPercent>

          <BoxPercent width="32%">
            <InputEdit
              label="Estado"
              editable
              fontSize={13}
              editable={false}
              value={dataAddress.txtEstado}
              theme={theme}
              onChangeText={(txt: String) => {
                setDataAddress({...dataAddress, txtEstado: txt});
              }}
            />
          </BoxPercent>
        </BoxPercent>

        <InputEdit
          label="Bairro"
          value={dataAddress.txtBairro}
          theme={theme}
          editable
          fontSize={13}
          editable={false}
          onChangeText={(txt: String) => {
            setDataAddress({...dataAddress, txtBairro: txt});
          }}
        />

        <BoxPercent direction justify="space-between" width="100%">
          <BoxPercent width="65%">
            <InputEdit
              label="Endereço"
              theme={theme}
              editable={false}
              fontSize={13}
              value={dataAddress.txtEndereco}
              onChangeText={(txt: String) => {
                setDataAddress({...dataAddress, txtEndereco: txt});
              }}
            />
          </BoxPercent>
          <BoxPercent width="32%">
            <InputEdit
              label="Nº"
              theme={theme}
              editable
              fontSize={13}
              value={dataAddress.txtNumero}
              onChangeText={(txt: String) => {
                setDataAddress({...dataAddress, txtNumero: txt});
              }}
            />
          </BoxPercent>
        </BoxPercent>

        <InputEdit
          label="Complemento"
          theme={theme}
          editable
          fontSize={13}
          value={dataAddress.txtComplemento}
          onChangeText={(txt: String) => {
            setDataAddress({...dataAddress, txtComplemento: txt});
          }}
        />

        <BoxPercent width="100%" direction>
          <BoxPercent width={dataAddress.id_endereco ? '50%' : '100%'}>
            <Button
              onPress={() => {
                dataAddress.id_endereco ? updateAddress() : addNewAdress();
              }}
              block
              fontBold
              uppercase
              background={theme.colors.success}
              color={theme.colors.shape}
              fontSize={14}
              borderRadius={5}>
              {dataAddress.id_endereco ? 'Atualizar' : ' Salvar'}
            </Button>
          </BoxPercent>

          {dataAddress.id_endereco && (
            <BoxPercent width="50%">
              <Button
                onPress={() => {
                  deleteAddress();
                }}
                block
                fontBold
                uppercase
                background={theme.colors.badge}
                color={theme.colors.shape}
                fontSize={14}
                borderRadius={5}>
                Excluir
              </Button>
            </BoxPercent>
          )}
        </BoxPercent>
      </BoxPercent>
    </Modal>
  );
};

export default ModalAddress;
