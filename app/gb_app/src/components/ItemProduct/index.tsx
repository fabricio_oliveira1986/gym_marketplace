import React from 'react';

import {useNavigation} from '@react-navigation/native';
import {useTheme} from 'styled-components/native';

import {Product} from '~/models/Product';
import {numberReal, numberDecimal} from '~/global/utils/functions';

import ImagesIcon from '~/assets/images/Icons';

import {
  Item,
  ItemImage,
  ItemContainerTitle,
  ItemTitle,
  ItemSubTitle,
  ContainerItemValor,
  ItemValor,
  ItemPlots,
  ItemBoxCashBack,
  ItemInfoCashBack,
  TextInfoCashBack,
  LogoEmpresa,
} from './style';

interface ItemProductProps {
  data: Product;
  showLogo?: boolean;
}

const ItemProduct: React.FC<ItemProductProps> = props => {
  const navigation = useNavigation();
  const theme = useTheme();
  const product = props.data;
  return (
    <Item
      onPress={() =>
        navigation.navigate('ProductSingle', {
          data: product,
        })
      }>
      <ItemImage source={{uri: product.image}} />
      <ItemContainerTitle>
        <ItemTitle numberOfLines={1}>{product.title}</ItemTitle>
        <ItemSubTitle numberOfLines={2}>{product.subTitle}</ItemSubTitle>
        <ContainerItemValor>
          <ItemValor>{numberReal(product.value)}</ItemValor>
          {product.plots && (
            <ItemPlots>
              {product.plots.qtd}x {numberReal(product.plots.value)} sem juros
            </ItemPlots>
          )}
        </ContainerItemValor>
      </ItemContainerTitle>
      <ItemBoxCashBack>
        {product.cashBack && (
          <ItemInfoCashBack>
            <ImagesIcon.MoneyFlow
              width={18}
              height={18}
              fill={theme.colors.shape}
            />
            <TextInfoCashBack>
              {numberDecimal(Number(product.cashBack))}%
            </TextInfoCashBack>
          </ItemInfoCashBack>
        )}
        {props.showLogo && product.bussiness && (
          <LogoEmpresa source={{uri: product.bussiness.image}} />
        )}
      </ItemBoxCashBack>
    </Item>
  );
};

export default ItemProduct;
