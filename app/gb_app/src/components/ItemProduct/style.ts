import styled from 'styled-components/native';

import {RFValue} from 'react-native-responsive-fontsize';

export const Item = styled.TouchableOpacity.attrs({
  activeOpacity: 0.7,
})`
  flex-direction: row;
  margin-bottom: ${RFValue(10)}px;
  background-color: ${({theme}) => theme.colors.muted};
  border-radius: ${RFValue(10)}px;
  padding: ${RFValue(10)}px ${RFValue(15)}px;
`;

export const ItemImage = styled.Image.attrs({
  resizeMode: 'cover',
})`
  width: ${RFValue(85)}px;
  height: ${RFValue(85)}px;
`;

export const ItemContainerTitle = styled.View`
  flex: 1;
  padding: 0 ${RFValue(15)}px;
`;

export const ItemTitle = styled.Text`
  font-size: ${RFValue(14)}px;
  font-family: ${({theme}) => theme.fonts.medium};
  margin-bottom: ${RFValue(5)}px;
  text-transform: uppercase;
  color: ${({theme}) => theme.colors.title};
`;

export const ItemSubTitle = styled.Text`
  font-size: ${RFValue(14)}px;
  line-height: ${RFValue(14)}px;
  font-family: ${({theme}) => theme.fonts.regular};
  margin-bottom: ${RFValue(2)}px;
  color: ${({theme}) => theme.colors.title};
`;

export const ContainerItemValor = styled.View`
  flex-direction: column;
  align-items: flex-start;
  margin-top: ${RFValue(5)}px;
`;

export const ItemTimeCourse = styled.Text``;

export const ItemPlots = styled.Text`
  font-size: ${RFValue(14)}px;
  font-family: ${({theme}) => theme.fonts.medium};
  color: ${({theme}) => theme.colors.success};
  text-align: right;
`;

export const ItemValor = styled.Text`
  font-size: ${RFValue(16)}px;
  font-family: ${({theme}) => theme.fonts.medium};
`;

export const ItemBoxCashBack = styled.View`
  justify-content: space-between;
  align-items: center;
`;

export const ItemInfoCashBack = styled.View`
  width: ${RFValue(35)}px;
  align-items: center;
  background-color: ${({theme}) => theme.colors.success};
  border-bottom-left-radius: ${RFValue(17)}px;
  border-bottom-right-radius: ${RFValue(17)}px;
  padding: ${RFValue(5)}px 0 ${RFValue(10)}px;
`;

export const TextInfoCashBack = styled.Text`
  color: ${({theme}) => theme.colors.shape};
  margin-top: ${RFValue(2)}px;
  font-size: ${RFValue(12)}px;
  font-family: ${({theme}) => theme.fonts.medium};
`;

export const LogoEmpresa = styled.Image.attrs({
  resizeMode: 'cover',
})`
  margin-top: ${RFValue(2)}px;
  padding-right: ${RFValue(5)}px;
  width: ${RFValue(26)}px;
  height: ${RFValue(26)}px;
  border-radius: ${RFValue(13)}px;
`;
