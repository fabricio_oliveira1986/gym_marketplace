import React from 'react';
import {
  SwiperFlatList,
  SwiperFlatListProps,
} from 'react-native-swiper-flatlist';
import {NewProduct} from '~/models/Product';
import {Bussiness} from '~/models/Bussiness';

import CustomPagination from './CustomPagination';

const Swiper: React.FC<SwiperFlatListProps<NewProduct | Bussiness>> = props => {
  return (
    <SwiperFlatList
      showPagination={true}
      PaginationComponent={CustomPagination}
      {...props}
    />
  );
};

export default Swiper;
