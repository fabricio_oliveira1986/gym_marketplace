import React from 'react';

import { StyleSheet, Dimensions } from 'react-native';
import { Pagination, PaginationProps } from 'react-native-swiper-flatlist';

const { width } = Dimensions.get('window');

const styles = StyleSheet.create({
  paginationContainer: {
    width,
    top: 180,
  },
  pagination: {
    width: 10,
    height: 10,
    borderRadius: 5,
  },
});

const CustomPagination: React.FC<PaginationProps> = (props) => {
  return (
    <Pagination
      {...props}
      paginationStyle={styles.paginationContainer}
      paginationStyleItem={styles.pagination}
      paginationDefaultColor="#E4EEEB"
      paginationActiveColor="#35B290"
    />
  );
};

export default CustomPagination;
