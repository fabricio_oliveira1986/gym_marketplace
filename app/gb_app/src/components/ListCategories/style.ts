import styled, {css} from 'styled-components/native';
import {RFValue} from 'react-native-responsive-fontsize';

interface ItemProps {
  isActive: boolean;
}

export const Container = styled.View`
  width: 100%;
  height: ${RFValue(100)}px;
  padding: 0px ${RFValue(15)}px;
  margin: ${RFValue(10)}px 0;
`;

export const ListCategory = styled.ScrollView.attrs({
  horizontal: true,
  showsHorizontalScrollIndicator: false,
})``;

export const Item = styled.TouchableOpacity.attrs({
  activeOpacity: 0.7,
})`
  flex: 0 0 33%;
  min-width: ${RFValue(70)}px;
  margin-right: ${RFValue(10)}px;
`;

export const ContainerCategory = styled.View`
  flex: 1;
  align-items: center;
`;

export const BoxCategory = styled.View<ItemProps>`
  height: ${RFValue(65)}px;
  width: ${RFValue(65)}px;
  border-radius: ${RFValue(11)}px;
  margin-bottom: ${RFValue(5)}px;
  align-items: center;
  justify-content: center;
  padding: ${RFValue(5)}px;
  background-color: ${({theme}) => theme.colors.muted};
  ${props =>
    props.isActive &&
    css`
      border: 1px solid ${props.theme.colors.dark};
    `}
`;

export const ImageCategory = styled.Image.attrs({
  resizeMode: 'contain',
})`
  width: ${RFValue(40)}px;
  height: ${RFValue(40)}px;
`;

export const TextCategory = styled.Text`
  font-size: ${RFValue(8)}px;
  font-family: ${({theme}) => theme.fonts.medium};
  color: ${({theme}) => theme.colors.title};
  text-transform: uppercase;
`;
