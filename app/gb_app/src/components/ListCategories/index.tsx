import React, {useState, useCallback, useEffect} from 'react';
import {Alert} from 'react-native';
import {useTheme} from 'styled-components/native';

import {Category} from '~/models/Category';

import * as categoryService from '~/services/category';
import {useFilter} from '~/hooks/filter';

import {
  Container,
  ListCategory,
  Item,
  ContainerCategory,
  BoxCategory,
  ImageCategory,
  TextCategory,
} from './style';

interface PropsItemCategory {
  item: Category;
  isActive: boolean;
}

const ItemCategory: React.FC<PropsItemCategory> = ({
  item: category,
  isActive,
}) => {
  return (
    <ContainerCategory>
      <BoxCategory isActive={isActive}>
        <ImageCategory source={{uri: category.image}} />
      </BoxCategory>
      <TextCategory numberOfLines={1}>{category.name}</TextCategory>
    </ContainerCategory>
  );
};

interface ListCategoriesProps {
  category: string;
  setCategory(item: string): void;
}

const ListCategories: React.FC<ListCategoriesProps> = ({
  category,
  setCategory,
}) => {
  const [categories, setCategories] = useState<Category[]>([]);
  const {productFilter, setItemsFilter} = useFilter();

  const handleFilterCategory = useCallback(
    (item: Category) => {
      setCategory(item.id);
      if (category === item.id) {
        setCategory('');
        return;
      }

      const items = productFilter?.filters || {};
      setItemsFilter('Product', {
        ...items,
        category: {id: item.id, title: item.name},
      });
    },
    [productFilter?.filters, setCategory, setItemsFilter],
  );

  useEffect(() => {
    categoryService
      .getCategories()
      .then(item => setCategories(item))
      .catch(error => {
        console.log(error);
        Alert.alert('Erro', 'Ocorreu um erro ao buscar as categorias');
      });
  }, [setCategories]);

  return (
    <Container>
      <ListCategory>
        {categories.map(item => (
          <Item
            key={item.id}
            onPress={() => {
              handleFilterCategory(item);
            }}>
            <ItemCategory
              isActive={item.id === category ? true : false}
              item={item}
            />
          </Item>
        ))}
      </ListCategory>
    </Container>
  );
};

export default ListCategories;
