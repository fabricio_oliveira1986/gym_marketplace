import Modal from './Modal';
import ModalVideo from './ModalVideo';
import ModalImage from './ModalImage';
import Input from './Form/Input';
import InputEdit from './Form/InputEdit';
import SelectButton from './Form/SelectButton';
import Button from './Form/Button';
import Card from './Card/index';
import {
  Box,
  BoxPercent,
  Text,
  HeaderTitle,
  ContentScroll,
  BoxTouchablePercent,
  HR,
} from './BoxAndTitles/index';
import Load from '~/components/Load';
import LoadFull from '~/components/LoadFull';
import Bagde from './Bagde/index';
import {Background} from './Background/index';
import Header from '~/components/Header';
import ItemSliderVideo from '~/components/ItemSliderVideo';
import ItemSliderImg from '~/components/ItemSliderImg';
import BoxWarning from './BoxWarning';
import InputDate from './Form/InputDate';

export {
  Modal,
  ModalVideo,
  ModalImage,
  Input,
  InputEdit,
  InputDate,
  Button,
  Card,
  Box,
  BoxPercent,
  BoxWarning,
  Load,
  LoadFull,
  Text,
  Header,
  HeaderTitle,
  ContentScroll,
  BoxTouchablePercent,
  Bagde,
  Background,
  HR,
  ItemSliderVideo,
  ItemSliderImg,
  SelectButton,
};
