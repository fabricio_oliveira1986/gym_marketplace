import React from 'react';
import {Icon} from 'react-native-elements';
import {Box, Text, BoxPercent} from '~/components/index';
import {useTheme} from 'styled-components';

interface IBoxWarning {
  text: string;
}

const BoxWarning: React.FC<IBoxWarning> = ({text, child}) => {
  const theme = useTheme();
  return (
    <BoxPercent
      direction
      width="100%"
      align="center"
      borderColor={theme.colors.error}
      borderLeftWidth={1}
      borderBottomWidth={1}
      borderRightWidth={1}
      borderTopWidth={1}
      pt={10}
      pb={10}
      pl={10}
      pr={10}
      radius={10}>
      <Box pr={15}>
        <Icon
          name="alert-circle-outline"
          type="ionicon"
          color={theme.colors.error}
          size={40}
        />
      </Box>

      <Text
        fontSize={15}
        style={{flex: 1, flexWrap: 'wrap'}}
        color={theme.colors.error}>
        {text} {child}
      </Text>
    </BoxPercent>
  );
};

export default BoxWarning;
