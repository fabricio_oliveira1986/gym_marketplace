import React from 'react';
import {BoxPercent, BoxTouchablePercent} from '~/components/index';
import styled from 'styled-components/native';
import {Dimensions} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';
import {Icon} from 'react-native-elements';

const {width} = Dimensions.get('window');

export const Item = styled.Image.attrs({
  resizeMode: 'cover',
})`
  width: ${width}px;
  height: ${RFValue(250)}px;
`;

interface IItemSliderVideo {
  openVideo: () => void;
  theme: any;
  img: string;
}

const ItemSliderVideo: React.FC<IItemSliderVideo> = ({
  openVideo,
  theme,
  img,
}) => {
  return (
    <BoxTouchablePercent onPress={openVideo}>
      <BoxPercent
        bgColor={theme.colors.black_opacity}
        position="absolute"
        width="100%"
        height="100%"
        align="center"
        justify="center"
        zIndex={999}>
        <Icon
          name="play-circle-outline"
          type="ionicon"
          color={theme.colors.shape}
          size={60}
        />
      </BoxPercent>
      <Item source={{uri: img}} />
    </BoxTouchablePercent>
  );
};

export default ItemSliderVideo;
