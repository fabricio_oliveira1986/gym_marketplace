import styled, {css} from 'styled-components/native';
import {RFValue} from 'react-native-responsive-fontsize';

import {CardProps} from './';

export const Container = styled.View<Omit<CardProps, 'children'>>`
  width: ${props => (props.hasOwnProperty('width') ? props.width : 100)}%;
  flex-direction: ${props => (props.direction ? 'row' : 'column')};
  align-items: ${props => props.align || 'stretch'};
  margin-bottom: ${RFValue(8)}px;
  ${props =>
    props.hasOwnProperty('noPadding') && props.noPadding
      ? css`
          padding: 0;
        `
      : css`
          padding: ${RFValue(15)}px ${RFValue(10)}px;
        `};
  background-color: ${props => props.background || props.theme.colors.shape};
  border-radius: ${RFValue(6)}px;
  box-shadow: 0px ${RFValue(4)}px ${RFValue(8)}px
    ${({theme}) => theme.colors.dark_alfa};
`;
