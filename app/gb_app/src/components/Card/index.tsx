import React from 'react';

import {Container} from './style';

export interface CardProps {
  children: any;
  width?: number | string;
  background?: string;
  direction?: boolean;
  align?: 'center' | 'flex-start' | 'flex-end' | 'stretch';
  noPadding?: boolean;
}

const Card: React.FC<CardProps> = ({children, ...rest}) => {
  return <Container {...rest}>{children}</Container>;
};

export default Card;
