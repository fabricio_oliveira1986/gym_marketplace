import React, {useCallback, useState, useEffect} from 'react';

import {Ofert} from '~/models/Ofert';
import * as ofertService from '~/services/ofert';

import {Container, ListOfert, ItemOfert, ImageOfert} from './style';

interface ListOfertsProps {
  onPress(item: string): void;
  value: string;
}

const ListOferts: React.FC<ListOfertsProps> = ({onPress, value}) => {
  const [oferts, setOferts] = useState<Ofert[]>([]);

  const getOferts = useCallback(async () => {
    const item = await ofertService.getOferts();
    setOferts(item);
  }, [setOferts]);

  useEffect(() => {
    getOferts();
  }, [getOferts]);

  return (
    <Container>
      <ListOfert>
        {oferts.map(ofert => (
          <ItemOfert
            key={ofert.id}
            onPress={() => onPress(ofert.id)}
            isActive={value === ofert.id}>
            <ImageOfert source={ofert.image} />
          </ItemOfert>
        ))}
      </ListOfert>
    </Container>
  );
};

export default ListOferts;
