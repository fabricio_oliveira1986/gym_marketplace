import styled, {css} from 'styled-components/native';
import {RFValue} from 'react-native-responsive-fontsize';
import {Dimensions} from 'react-native';

const {width} = Dimensions.get('window');

interface ItemOfertProps {
  isActive: boolean;
}

export const Container = styled.View`
  width: 100%;
  height: ${RFValue(100)}px;
  margin-bottom: ${RFValue(10)}px;
  padding: 0px ${RFValue(15)}px;
`;

export const ListOfert = styled.ScrollView.attrs({
  horizontal: true,
  showsHorizontalScrollIndicator: false,
})`
  flex: 1;
  width: 100%;
`;

export const ItemOfert = styled.TouchableOpacity.attrs({
  activeOpacity: 0.7,
})<ItemOfertProps>`
  width: ${width / 2 - 40}px;
  height: 89px;
  margin-right: ${RFValue(15)}px;
  ${props =>
    props.isActive &&
    css`
      border: 1px solid ${({theme}) => theme.colors.dark};
      border-radius: 5px;
    `}
`;

export const ImageOfert = styled.Image.attrs({
  resizeMode: 'contain',
})`
  width: 100%;
  height: 89px;
`;

export const ItemOfertTitle = styled.Text`
  font-size: ${RFValue(8)}px;
  font-family: ${({theme}) => theme.fonts.medium};
  color: ${({theme}) => theme.colors.shape};
  line-height: ${RFValue(8)}px;
  margin-bottom: ${2}px;
  text-transform: uppercase;
`;
