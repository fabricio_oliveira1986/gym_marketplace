import React, {ReactNode} from 'react';
import {SvgProps} from 'react-native-svg';
import Card from '~/components/Card';

import {
  Title,
  ContainerBody,
  ContainerIcon,
  ContainerDescription,
} from './style';

interface EmptyProps {
  title: string;
  icon?: React.FC<SvgProps>;
  children?: ReactNode;
}

const Empty: React.FC<EmptyProps> = ({title, icon: Icon, children}) => {
  return (
    <Card>
      <Title>{title}</Title>
      <ContainerBody>
        <ContainerIcon>{Icon && <Icon height={35} width={35} />}</ContainerIcon>
        <ContainerDescription>{children}</ContainerDescription>
      </ContainerBody>
    </Card>
  );
};

export default Empty;
