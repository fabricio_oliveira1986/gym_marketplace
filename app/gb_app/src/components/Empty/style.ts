import styled from 'styled-components/native';
import {RFValue} from 'react-native-responsive-fontsize';

export const Title = styled.Text`
  font-family: ${({theme}) => theme.fonts.medium};
  color: ${({theme}) => theme.colors.success};
  font-size: ${RFValue(24)}px;
  text-transform: uppercase;
  margin-bottom: ${RFValue(15)}px;
`;

export const ContainerBody = styled.View``;

export const ContainerIcon = styled.View`
  justify-content: center;
  align-items: center;
  height: ${RFValue(60)}px;
  width: 100%;
  margin-bottom: ${RFValue(20)}px;
`;

export const ContainerDescription = styled.View``;
