import React, {useCallback, useEffect, useState} from 'react';
import {Alert} from 'react-native';
import Feather from 'react-native-vector-icons/Feather';
import {useTheme} from 'styled-components';

import Input from '~/components/Form/Input';
import Button from '~/components/Form/Button';

import {Ask} from '~/models/Ask';
import {Product} from '~/models/Product';

import * as AskService from '~/services/ask';

import {dateTimeBr} from '~/global/utils/functions';

import {
  ContainerAsk,
  TitleAsk,
  TextAsk,
  FormAsk,
  LabelAsk,
  ListAsk,
  ItemCard,
  BoxAsk,
  UserName,
  DateAsk,
  TitleCardAsk,
  MessageAsk,
  ContainerReply,
} from './style';

interface ListAsksProps {
  product: Product;
}

const ListAsks: React.FC<ListAsksProps> = ({product}) => {
  const theme = useTheme();
  const [asks, setAsks] = useState<Ask[]>([]);
  const [ask, setAsk] = useState('');
  const [disabled, setDisabled] = useState(false);

  useEffect(() => {
    AskService.getAsksProduct(product.id)
      .then(item => setAsks(item))
      .catch(error => {
        console.log(error);
        Alert.alert('Erro', 'Ocorreu um erro ao listar as perguntas');
      });
  }, [product.id]);

  const handleSendMessage = useCallback(async () => {
    try {
      console.log('chamei', ask);
      if (ask) {
        setDisabled(true);
        await AskService.saveAsk({
          productId: product.id,
          ask,
        });
        setAsk('');
        const listAsks = await AskService.getAsksProduct(product.id);
        setAsks(listAsks);
        Alert.alert('Sucesso', 'Pergunta enviada com sucesso!');
      }
    } catch (error) {
      console.log(error);
      Alert.alert('Erro', 'Ocorreu um erro ao enviar a pergunta');
    } finally {
      setDisabled(false);
    }
  }, [ask, product.id]);

  return (
    <ContainerAsk>
      <TitleAsk>
        <Feather name="help-circle" size={19} />
        <TextAsk>Perguntas Frequentes</TextAsk>
      </TitleAsk>
      <FormAsk>
        <LabelAsk>Faça sua pergunta ao vendedor</LabelAsk>
        <Input
          color={theme.colors.border}
          borderColor={theme.colors.border}
          height={80}
          fontSize={18}
          borderRadius={4}
          numberOfLines={3}
          onChangeText={setAsk}
          value={ask}
          multiline
          style={{textAlignVertical: 'top'}}
        />
        <Button
          background={theme.colors.success}
          color={theme.colors.shape}
          borderRadius={4}
          disabled={disabled}
          fontSize={14}
          style={{marginTop: 10}}
          onPress={handleSendMessage}>
          Enviar
        </Button>
      </FormAsk>
      <ListAsk>
        {asks.map(item => (
          <ItemCard key={item.id}>
            <BoxAsk>
              <UserName>{item.user.name}</UserName>
              <DateAsk>{dateTimeBr(item.date)}</DateAsk>
            </BoxAsk>
            <TitleCardAsk>{item.ask}</TitleCardAsk>
            {item.reply && (
              <ContainerReply>
                <BoxAsk>
                  <UserName>Resposta:</UserName>
                  <DateAsk>{dateTimeBr(item.dateReply)}</DateAsk>
                </BoxAsk>
                <MessageAsk>{item.reply}</MessageAsk>
              </ContainerReply>
            )}
          </ItemCard>
        ))}
      </ListAsk>
    </ContainerAsk>
  );
};

export default ListAsks;
