import styled from 'styled-components/native';
import {RFValue} from 'react-native-responsive-fontsize';

export const ContainerAsk = styled.View`
  margin: ${RFValue(20)}px 0 0;
  padding: 0 ${RFValue(15)}px;
`;

export const TitleAsk = styled.View`
  flex-direction: row;
  margin-bottom: ${RFValue(20)}px;
`;

export const TextAsk = styled.Text`
  font-size: ${RFValue(14)}px;
  text-transform: uppercase;
  color: ${({theme}) => theme.colors.title};
  font-family: ${({theme}) => theme.fonts.medium};
  margin-left: 8px;
`;

export const FormAsk = styled.View`
  padding: ${RFValue(20)}px ${RFValue(15)}px;
  background: ${({theme}) => theme.colors.shape};
  border-radius: ${RFValue(4)}px;
  box-shadow: 0px ${RFValue(4)}px ${RFValue(8)}px
    ${({theme}) => theme.colors.dark_alfa};
`;

export const LabelAsk = styled.Text`
  font-family: ${({theme}) => theme.fonts.medium};
  color: ${({theme}) => theme.colors.title};
  font-size: ${RFValue(14)}px;
  line-height: ${RFValue(19)}px;
  margin-bottom: ${RFValue(5)}px;
`;

export const ListAsk = styled.View`
  margin-top: ${RFValue(20)}px;
`;

export const ItemCard = styled.View`
  background: ${({theme}) => theme.colors.shape};
  padding: ${RFValue(20)}px ${RFValue(15)}px;
  border-radius: ${RFValue(6)}px;
  box-shadow: 0px ${RFValue(4)}px ${RFValue(8)}px
    ${({theme}) => theme.colors.dark_alfa};
  margin-bottom: ${RFValue(10)}px;
`;

export const BoxAsk = styled.View`
  flex-direction: row;
  justify-content: space-between;
  margin-bottom: ${RFValue(8)}px;
`;

export const UserName = styled.Text`
  font-family: ${({theme}) => theme.fonts.medium};
  font-size: ${RFValue(12)}px;
  color: ${({theme}) => theme.colors.title};
  margin-right: ${RFValue(10)}px;
`;

export const DateAsk = styled.Text`
  font-family: ${({theme}) => theme.fonts.medium};
  font-size: ${RFValue(12)}px;
  color: ${({theme}) => theme.colors.title};
`;

export const TitleCardAsk = styled.Text`
  font-family: ${({theme}) => theme.fonts.medium};
  color: ${({theme}) => theme.colors.title};
  font-size: ${RFValue(14)}px;
  line-height: ${RFValue(19)}px;
  margin-bottom: ${RFValue(8)}px;
`;

export const MessageAsk = styled.Text`
  font-family: ${({theme}) => theme.fonts.regular};
  color: ${({theme}) => theme.colors.text};
  font-size: ${RFValue(16)}px;
  line-height: ${RFValue(19)}px;
`;

export const ContainerReply = styled.View`
  margin-top: ${RFValue(10)}px;
`;
