import styled from 'styled-components/native';
import {RFValue} from 'react-native-responsive-fontsize';

export const Container = styled.View`
  flex-direction: row;
  width: 100%;
  height: ${RFValue(60)}px;
  border-bottom-left-radius: ${RFValue(15)}px;
  border-bottom-right-radius: ${RFValue(15)}px;
  background: ${({theme}) => theme.colors.shape};
  align-items: center;
  padding: 0 ${RFValue(20)}px;
  border-width: 0.3px;
  border-color: ${({theme}) => theme.colors.border};
  box-shadow: 0px ${RFValue(2)}px ${RFValue(4)}px
    ${({theme}) => theme.colors.shadow};
`;

export const ContainerSearch = styled.View`
  flex: 1;
  background: ${({theme}) => theme.colors.muted};
  flex-direction: row;
  height: ${RFValue(40)}px;
  padding: 0px ${RFValue(15)}px;
  align-items: center;
  border-radius: ${RFValue(15)}px;
  border-color: ${({theme}) => theme.colors.border};
`;

export const TextSearch = styled.TextInput`
  font-size: ${RFValue(18)}px;
  line-height: ${RFValue(18)}px;
  flex: 1;
  font-family: ${({theme}) => theme.fonts.regular};
  margin-left: ${RFValue(10)}px;
  color: ${({theme}) => theme.colors.text};
`;
export const ButtonFilter = styled.TouchableOpacity.attrs({
  activeOpacity: 0.7,
})`
  margin-left: ${RFValue(15)}px;
`;
