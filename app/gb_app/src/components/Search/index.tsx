import React from 'react';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import {useNavigation} from '@react-navigation/native';
import {useTheme} from 'styled-components';

import useDebounce from '~/global/utils/useDebounce';
import ImagesIcon from '~/assets/images/Icons';
import {Container, ContainerSearch, TextSearch, ButtonFilter} from './style';
import {useCallback} from 'react';
import {useState} from 'react';

interface SearchProps {
  value: string;
  setValue(item: string): void;
  type: 'Product' | 'Bussiness';
}

const Search: React.FC<SearchProps> = ({value, setValue, type}) => {
  const navigation = useNavigation();
  const theme = useTheme();
  const [displaySearch, setDisplaySearch] = useState(value);
  const debounceChange = useDebounce(setValue, 800);

  const setSearch = useCallback(
    (search: string) => {
      setDisplaySearch(search);
      debounceChange(search);
    },
    [debounceChange],
  );

  return (
    <Container>
      <ContainerSearch>
        <ImagesIcon.Search width={15} height={15} fill={theme.colors.text} />
        <TextSearch
          placeholder="Buscar"
          placeholderTextColor={theme.colors.text}
          value={displaySearch}
          onChangeText={setSearch}
        />
      </ContainerSearch>
      <ButtonFilter
        onPress={() =>
          navigation.navigate('Filter', {
            data: {type},
          })
        }>
        <FontAwesome5 name="sliders-h" size={20} color={theme.colors.title} />
      </ButtonFilter>
    </Container>
  );
};

export default Search;
