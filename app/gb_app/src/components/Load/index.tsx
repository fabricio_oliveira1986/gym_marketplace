import React from 'react';

import { Container, ActivityIndicator } from './styles';

const Load: React.FC = () => {
  return (
    <Container>
      <ActivityIndicator size="large" color="#35B290" />
    </Container>
  );
};

export default Load;
