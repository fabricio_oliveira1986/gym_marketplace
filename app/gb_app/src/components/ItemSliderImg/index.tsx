import React from 'react';
import {BoxPercent, BoxTouchablePercent} from '~/components/index';
import styled from 'styled-components/native';
import {Dimensions} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';
import {Icon} from 'react-native-elements';

const {width} = Dimensions.get('window');

export const Item = styled.Image.attrs({
  resizeMode: 'cover',
})`
  width: ${width}px;
  height: ${RFValue(250)}px;
`;

interface IItemSliderVideo {
  openImg: () => void;
  img: string;
}

const ItemSliderImg: React.FC<IItemSliderVideo> = ({openImg, img}) => {
  return (
    <BoxTouchablePercent onPress={openImg}>
      <Item source={{uri: img}} />
    </BoxTouchablePercent>
  );
};

export default ItemSliderImg;
