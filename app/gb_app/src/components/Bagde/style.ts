import styled from 'styled-components/native';
import {RFValue} from 'react-native-responsive-fontsize';
import {BagdeProps} from './';

export const Container = styled.View<Omit<BagdeProps, 'fontSize' | 'color'>>`
  position: ${props => props.position};
  background-color: ${props => props.background};
  width: ${props => RFValue(props.size)}px;
  height: ${props => RFValue(props.size)}px;
  border-radius: ${props => RFValue(props.size) / 2}px;
  right: ${props =>
    Number.isInteger(props.right) ? props.right + 'px' : 'auto'};
  top: ${props => (Number.isInteger(props.top) ? props.top + 'px' : 'auto')};
  left: ${props => (Number.isInteger(props.left) ? props.left + 'px' : 'auto')};
  bottom: ${props =>
    Number.isInteger(props.bottom) ? props.bottom + 'px' : 'auto'};
  align-items: center;
  justify-content: center;
  z-index: 3;
`;

interface BagdePropsText {
  fontSize: number;
  color: string;
}

export const Text = styled.Text<BagdePropsText>`
  font-family: ${({theme}) => theme.fonts.medium};
  font-size: ${props => RFValue(props.fontSize)}px;
  color: ${props => props.color};
`;
