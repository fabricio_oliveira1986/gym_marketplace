import React from 'react';
import {Container, Text} from './style';

export interface BagdeProps {
  children: string;
  background: string;
  size: number;
  fontSize: number;
  color: string;
  position: 'absolute' | 'relative';
  top?: number;
  right?: number;
  left?: number;
  bottom?: number;
}

const Bagde: React.FC<BagdeProps> = ({children, ...props}) => {
  return (
    <Container
      background={props.background}
      position={props.position}
      size={props.size}
      top={props.top}
      right={props.right}
      left={props.left}
      bottom={props.bottom}>
      <Text fontSize={props.fontSize} color={props.color}>
        {children}
      </Text>
    </Container>
  );
};

export default Bagde;
