import React, {useMemo} from 'react';
import {BottomTabBarProps} from '@react-navigation/bottom-tabs';
import {useTheme} from 'styled-components';

import {useCart} from '~/hooks/cart';

import Images from '~/assets/images/Icons';
import Bagde from '~/components/Bagde';

import {Container, Item, ItemText} from './style';
import {useAuth} from '~/hooks/auth';

const iconOptions = {
  Home: Images.Home,
  Extracts: Images.MoneyFlow,
  Order: Images.Cart,
  Profile: Images.UserLine,
  Bussiness: Images.Search,
};

const CustomTabBar: React.FC<BottomTabBarProps> = ({
  state,
  descriptors,
  navigation,
}) => {
  const theme = useTheme();
  const {items} = useCart();
  const {user} = useAuth();
  const itemsCart = useMemo(() => {
    return items.reduce((acc, item) => {
      acc += item.quantity;
      return acc;
    }, 0);
  }, [items]);

  const handlePressTab = (name: string) => {
    navigation.navigate(name, {
      screen: name,
    });
  };
  const getIcon = (
    name: 'Home' | 'Extracts' | 'Order' | 'Profile' | 'Bussiness',
  ) => {
    const nameIcon = iconOptions.hasOwnProperty(name)
      ? iconOptions[name]
      : Images.Home;
    return nameIcon;
  };
  return (
    <Container>
      {state.routes.map((route, index) => {
        const {options} = descriptors[route.key];
        const label =
          options.title !== undefined
            ? options.title
            : options.tabBarLabel !== undefined
            ? options.tabBarLabel
            : route.name;
        const color =
          state.index === index ? theme.colors.success : theme.colors.shape;
        const Icon = getIcon(route.name);
        return (
          <Item key={index} onPress={() => handlePressTab(route.name)}>
            <>
              {route.name === 'Order' && (
                <Bagde
                  fontSize={8}
                  position="absolute"
                  background={theme.colors.badge}
                  size={16}
                  color={theme.colors.shape}
                  top={-5}
                  left={user?.id ? 50 : 80}>
                  {itemsCart.toString()}
                </Bagde>
              )}
              <Icon height="20" width="20" fill={color} />
              <ItemText color={color}>{label}</ItemText>
            </>
          </Item>
        );
      })}
    </Container>
  );
};

export default CustomTabBar;
