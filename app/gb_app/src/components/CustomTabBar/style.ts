import styled from 'styled-components/native';
import {RFValue} from 'react-native-responsive-fontsize';

export const Container = styled.View`
  flex-direction: row;
  align-items: center;
  height: ${RFValue(50)}px;
  background: ${({theme}) => theme.colors.title};
  border-top-left-radius: ${RFValue(20)}px;
  border-top-right-radius: ${RFValue(20)}px;
  overflow: hidden;
`;

export const Item = styled.TouchableOpacity.attrs({
  activeOpacity: 0.7,
})`
  flex: 1;
  align-items: center;
  justify-content: center;
  position: relative;
`;

interface ItemTextProps {
  color: string;
}

export const ItemText = styled.Text<ItemTextProps>`
  color: ${props => props.color};
  margin-top: 5px;
  font-family: ${({theme}) => theme.fonts.regular};
  font-size: ${RFValue(12)}px;
  line-height: ${RFValue(12)}px;
  margin-bottom: -2px;
`;
