import React, {useEffect, useState} from 'react';
import {StyleSheet, View, Platform} from 'react-native';
import RNPickerSelect from 'react-native-picker-select';
import {Modal, Button} from '~/components/index';
import {theme} from '~/global/styles/theme';
import {getCidades, getEstados} from '~/services/address';
import {City} from '~/models/Address';
import {InputGroupForm, Label} from '~/screens/Filter/style';

interface IModalLocation {
  visible: boolean;
  setVisivelModalLocation: (data: boolean) => void;
  getCitySelected: (value: number) => void;
}

interface IDataSelect {
  label: string;
  value: string;
}

const ModalLocation: React.FC<IModalLocation> = ({
  visible,
  setVisivelModalLocation,
  getCitySelected,
}) => {
  const [allUF, setAllUF] = useState<IDataSelect[]>([]);
  const [allCity, setAllCity] = useState<IDataSelect[]>([]);
  const [UFSelected, setUFSelected] = useState<IDataSelect>({
    label: '',
    value: '',
  });
  const [visibleInternal, setVisibleInternal] = useState<boolean>(false);
  const [citySelected, setCitySelected] = useState<IDataSelect>({
    label: '',
    value: '',
  });
  const [isValid, setIsValid] = useState<boolean>(false);

  useEffect(() => {
    if (visible) {
      setTimeout(() => {
        setVisibleInternal(true);
      }, 2000);
    } else {
      setVisibleInternal(false);
    }
  }, [visible]);

  useEffect(() => {
    console.log('citySelected', citySelected);
    if (UFSelected.label !== '' && citySelected.label !== '') {
      setIsValid(true);
    } else {
      setIsValid(false);
    }
  }, [UFSelected, citySelected]);

  useEffect(() => {
    const getState = async () => {
      const listState = await getEstados();
      setAllUF(
        listState.map((item: any) => ({
          label: item.estado_sigla,
          value: item.estado_codigo,
        })),
      );
    };
    getState();
  }, []);

  useEffect(() => {
    const getCity = async () => {
      const dataCities: City[] = await getCidades(UFSelected);
      setAllCity([]);
      setAllCity(
        dataCities.map(
          (item): IDataSelect => ({
            label: item.cidade_nome,
            value: item.cidade_codigo,
          }),
        ),
      );
    };
    getCity();
  }, [UFSelected]);

  return (
    <Modal
      modalVisible={visibleInternal}
      width={90}
      height={350}
      background="white"
      onRequestClose={() => {
        setVisivelModalLocation(false);
      }}>
      <View style={styles.container}>
        <Label style={styles.title}>
          Selecione o estado e cidade que seja ver as ofertas mais próximas de
          você
        </Label>
        {allUF.length > 0 && (
          <>
            <InputGroupForm style={{width: '100%'}}>
              <Label>Estado</Label>
              <View style={styles.borderSelect}>
                <RNPickerSelect
                  pickerProps={{
                    style: styles.select,
                  }}
                  doneText="OK"
                  placeholder={{label: 'Selecione o estado'}}
                  key={'RNPickerSelectState'}
                  onValueChange={value => {
                    if (value) {
                      setCitySelected({
                        label: '',
                        value: '',
                      });
                      setUFSelected(value);
                    }
                  }}
                  items={allUF}
                  value={UFSelected.label}
                />
              </View>
            </InputGroupForm>
          </>
        )}

        <InputGroupForm style={{width: '100%'}}>
          <Label>Cidade</Label>
          <View style={styles.borderSelect}>
            <RNPickerSelect
              pickerProps={{style: styles.select}}
              key={'RNPickerSelectCities'}
              doneText="OK"
              onValueChange={value => {
                if (value) {
                  setCitySelected(value);
                }
              }}
              placeholder={{label: 'Selecione a cidade'}}
              items={allCity}
              value={citySelected.label}
            />
          </View>
        </InputGroupForm>
        <View style={styles.btnSend}>
          <Button
            color={isValid ? theme.colors.shape : theme.colors.shape}
            fontSize={16}
            disabled={!isValid}
            children="Ver Promoções"
            background={isValid ? theme.colors.success : theme.colors.edit}
            onPress={() => {
              setVisivelModalLocation(false);
              getCitySelected(citySelected);
            }}
          />
        </View>
      </View>
    </Modal>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    alignItems: 'center',
  },
  title: {
    marginBottom: 20,
    textAlign: 'center',
    fontSize: 16,
  },
  select: {
    ...Platform.select({
      ios: {
        height: 240,
      },
      android: {
        height: 40,
      },
    }),
    overflow: 'hidden',
    width: '100%',
  },
  btnSend: {
    width: '100%',
    position: 'absolute',
    bottom: 0,
  },
  borderSelect: {
    ...Platform.select({
      ios: {
        paddingHorizontal: 20,
        paddingVertical: 10,
      },
    }),
    borderWidth: 2,
    borderColor: '#7F8D95',
    borderRadius: 4,
    alignItems: 'center',
    display: 'flex',
    justifyContent: 'center',
  },
});

export default ModalLocation;
