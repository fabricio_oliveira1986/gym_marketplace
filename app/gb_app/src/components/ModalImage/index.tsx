import React from 'react';
import styled from 'styled-components/native';
import {Icon} from 'react-native-elements';
import {ImageBackground, StyleSheet} from 'react-native';
import {Modal, Box} from '~/components/index';

interface IModalImage {
  visible: boolean;
  uri: string;
  setVisivelModalImg: (data: boolean) => void;
}

export const Item = styled.Image.attrs({
  resizeMode: 'cover',
})`
  width: 100%;
  height: 100%;
`;

const ModalImage: React.FC<IModalImage> = ({
  visible,
  uri,
  setVisivelModalImg,
}) => {
  return (
    <Modal
      modalVisible={visible}
      onRequestClose={() => setVisivelModalImg(false)}
      align="center"
      style={styles.container}
      background="rgba(0, 0, 0, 0.7)">
      <>
        <Box
          position="absolute"
          zIndex={500}
          top={20}
          right={20}
          bgColor="rgba(0, 0, 0, 0.2)"
          radius={200}>
          <Icon
            type="ionicon"
            name={'close-outline'}
            color={'white'}
            size={36}
            onPress={() => {
              setVisivelModalImg(false);
            }}
          />
        </Box>
        <ImageBackground
          source={{uri}}
          resizeMode="contain"
          style={styles.image}
        />
      </>
    </Modal>
  );
};

export default ModalImage;

const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: '100%',
  },
  image: {
    flex: 1,
    width: '100%',
    height: '100%',
  },
});
