import React from 'react';
import VideoPlayer from 'react-native-video-controls';
import {Icon} from 'react-native-elements';
import {Modal, Box, BoxPercent} from '~/components/index';

interface ModalVideoProps {
  visible: boolean;
  uri: string;
  setVisivelModalVod: (data: boolean) => void;
}

const BoxVideo: React.FC<ModalVideoProps> = ({
  visible,
  uri,
  setVisivelModalVod,
}) => {
  return (
    <Modal
      modalVisible={visible}
      onRequestClose={() => setVisivelModalVod(false)}
      style={{width: '100%'}}
      background="#000"
      align="center">
      <BoxPercent width={'100%'} position="relative" height={'100%'}>
        <Box position="absolute" zIndex={500} right={1}>
          <Icon
            type="ionicon"
            name={'close-outline'}
            color={'white'}
            size={40}
            onPress={() => {
              setVisivelModalVod(false);
            }}
          />
        </Box>
        <VideoPlayer
          toggleResizeModeOnFullscreen={false}
          disablePlayPause={false}
          source={{uri}}
          controlTimeout={999999}
          onBack={() => {
            setVisivelModalVod(false);
          }}
          navigator={() => {}}
        />
      </BoxPercent>
    </Modal>
  );
};

export default BoxVideo;
