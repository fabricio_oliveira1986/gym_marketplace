import styled from 'styled-components/native';
import {RFValue} from 'react-native-responsive-fontsize';

export const Box = styled.View`
  align-self: ${props => (props.self ? props.self : 'auto')};
  height: ${props => (props.height ? `${RFValue(props.height)}px` : 'auto')};
  width: ${props => (props.width ? `${RFValue(props.width)}px` : 'auto')};
  padding-left: ${props => (props.pl ? `${RFValue(props.pl)}px` : '0')};
  justify-content: ${props => (props.justify ? props.justify : 'flex-start')};
  padding-right: ${props => (props.pr ? `${RFValue(props.pr)}px` : '0')};
  padding-bottom: ${props => (props.pb ? `${RFValue(props.pb)}px` : '0')};
  padding-top: ${props => (props.pt ? `${RFValue(props.pt)}px` : '0')};
  margin-left: ${props => (props.ml ? `${RFValue(props.ml)}px` : '0px')};
  margin-right: ${props => (props.mr ? `${RFValue(props.mr)}px` : '0px')};
  margin-bottom: ${props => (props.mb ? `${RFValue(props.mb)}px` : '0px')};
  margin-top: ${props => (props.mt ? `${RFValue(props.mt)}px` : '0px')};
  position: ${props => (props.position ? props.position : 'relative')};
  background-color: ${props => (props.bgColor ? props.bgColor : 'transparent')};
  z-index: ${props => (props.zIndex ? props.zIndex : '0')};
  bottom: ${props => (props.bottom ? `${RFValue(props.bottom)}px` : 'auto')};
  right: ${props => (props.right ? `${RFValue(props.right)}px` : 'auto')};
  border-radius: ${props =>
    props.radius ? `${RFValue(props.radius)}px` : '0px'};
`;

export const BoxPercent = styled.View`
  align-self: ${props => (props.self ? props.self : 'auto')};
  width: ${props => (props.width ? props.width : 'auto')};
  height: ${props => (props.height ? props.height : 'auto')};
  padding-left: ${props => (props.pl ? `${RFValue(props.pl)}px` : '0')};
  padding-right: ${props => (props.pr ? `${RFValue(props.pr)}px` : '0')};
  padding-bottom: ${props => (props.pb ? `${RFValue(props.pb)}px` : '0')};
  padding-top: ${props => (props.pt ? `${RFValue(props.pt)}px` : '0')};
  justify-content: ${props => (props.justify ? props.justify : 'flex-start')};
  margin-top: ${props => (props.mt ? `${RFValue(props.mt)}px` : '0px')};
  margin-bottom: ${props => (props.mb ? `${RFValue(props.mb)}px` : '0px')};
  margin-right: ${props => (props.mr ? `${RFValue(props.mr)}px` : '0px')};
  margin-left: ${props => (props.ml ? `${RFValue(props.ml)}px` : '0px')};
  align-items: ${props => (props.align ? props.align : 'flex-start')};
  flex-direction: ${props => (props.direction ? 'row' : 'column')};
  position: ${props => (props.position ? props.position : 'relative')};
  bottom: ${props => (props.bottom ? props.bottom : 'auto')};
  right: ${props => (props.right ? props.right : 'auto')};
  background-color: ${props => (props.bgColor ? props.bgColor : 'transparent')};
  border-right-width: ${props =>
    props.borderRightWidth ? `${RFValue(props.borderRightWidth)}px` : '0px'};
  border-left-width: ${props =>
    props.borderLeftWidth ? `${RFValue(props.borderLeftWidth)}px` : '0px'};
  border-bottom-width: ${props =>
    props.borderBottomWidth ? `${RFValue(props.borderBottomWidth)}px` : '0px'};
  border-top-width: ${props =>
    props.borderTopWidth ? `${RFValue(props.borderTopWidth)}px` : '0px'};
  border-color: ${props =>
    props.borderColor ? props.borderColor : 'transparent'};
  z-index: ${props => (props.zIndex ? props.zIndex : '0')};
  align-self: ${props => (props.self ? props.self : 'auto')};
  border-radius: ${props =>
    props.radius ? `${RFValue(props.radius)}px` : '0px'};
`;

export const Text = styled.Text`
  font-size: ${props =>
    props.fontSize ? `${RFValue(props.fontSize)}px` : `${RFValue(14)}px`};
  color: ${props => (props.color ? props.color : 'black')};
  font-family: ${({theme}) => theme.fonts.regular};
  font-weight: ${props => (props.bold ? 'bold' : 'normal')};
  margin-top: ${props => (props.mt ? `${RFValue(props.mt)}px` : '0px')};
  margin-bottom: ${props => (props.mb ? `${RFValue(props.mb)}px` : '0px')};
  margin-right: ${props => (props.mr ? `${RFValue(props.mr)}px` : '0px')};
  text-transform: ${props => (props.transform ? props.transform : 'none')};
`;

export const HeaderTitle = styled.Text`
  color: ${({theme}) => theme.colors.text};
  font-family: ${({theme}) => theme.fonts.medium};
  font-size: ${RFValue(16)}px;
  line-height: ${RFValue(21)}px;
  margin: ${RFValue(25)}px ${RFValue(20)}px 0;
  margin-bottom: ${props => (props.mb ? `${RFValue(props.mb)}px` : '0px')};
`;

export const ContentScroll = styled.ScrollView.attrs({
  showsVerticalScrollIndicator: false,
})`
  margin-left: ${props => (props.ml ? `${RFValue(props.ml)}px` : '0px')};
  margin-right: ${props => (props.mr ? `${RFValue(props.mr)}px` : '0px')};
  margin-bottom: ${props => (props.mb ? `${RFValue(props.mb)}px` : '0px')};
  margin-top: ${props => (props.mt ? `${RFValue(props.mt)}px` : '0px')};
  padding-left: ${props => (props.pl ? `${RFValue(props.pl)}px` : '0')};
  padding-right: ${props => (props.pr ? `${RFValue(props.pr)}px` : '0')};
  padding-bottom: ${props => (props.pb ? `${RFValue(props.pb)}px` : '0')};
  padding-top: ${props => (props.pt ? `${RFValue(props.pt)}px` : '0')};
  background-color: ${props => (props.bgColor ? props.bgColor : 'transparent')};
`;

export const BoxTouchablePercent = styled.TouchableOpacity`
  align-self: ${props => (props.self ? props.self : 'auto')};
  width: ${props => (props.width ? props.width : 'auto')};
  height: ${props => (props.height ? props.height : 'auto')};
  padding-left: ${props => (props.pl ? `${RFValue(props.pl)}px` : '0px')};
  padding-right: ${props => (props.pr ? `${RFValue(props.pr)}px` : '0px')};
  padding-bottom: ${props => (props.pb ? `${RFValue(props.pb)}px` : '0px')};
  padding-top: ${props => (props.pt ? `${RFValue(props.pt)}px` : '0px')};
  margin-left: ${props => (props.ml ? `${RFValue(props.ml)}px` : '0px')};
  margin-right: ${props => (props.mr ? `${RFValue(props.mr)}px` : '0px')};
  margin-bottom: ${props => (props.mb ? `${RFValue(props.mb)}px` : '0px')};
  margin-top: ${props => (props.mt ? `${RFValue(props.mt)}px` : '0px')};
  justify-content: ${props => (props.justify ? props.justify : 'flex-start')};
  align-items: ${props => (props.align ? props.align : 'flex-start')};
  flex-direction: ${props => (props.direction ? 'row' : 'column')};
  position: ${props => (props.position ? props.position : 'relative')};
  bottom: ${props => (props.bottom ? props.bottom : 'auto')};
  right: ${props => (props.right ? `${RFValue(props.right)}px` : 'auto')};
  top: ${props => (props.bottom ? props.top : 'auto')};
  background-color: ${props => (props.bgColor ? props.bgColor : 'transparent')};
  border-radius: ${props =>
    props.radius ? `${RFValue(props.radius)}px` : '0px'};
  border-left-width: ${props =>
    props.borderLeftWidth ? `${RFValue(props.borderLeftWidth)}px` : '0px'};
  border-right-width: ${props =>
    props.borderRightWidth ? `${RFValue(props.borderRightWidth)}px` : '0px'};
  border-top-width: ${props =>
    props.borderTopWidth ? `${RFValue(props.borderTopWidth)}px` : '0px'};
  border-bottom-width: ${props =>
    props.borderBottomWidth ? `${RFValue(props.borderBottomWidth)}px` : '0px'};
  border-color: ${props =>
    props.borderColor ? props.borderColor : 'transparent'};
  z-index: ${props => (props.zIndex ? props.zIndex : '0')};
`;

export const HR = styled.View`
  width: 100%;
  background-color: ${({theme}) => theme.colors.separator};
  height: ${RFValue(1)}px;
  padding-left: ${props => (props.pl ? `${RFValue(props.pl)}px` : '0px')};
  padding-right: ${props => (props.pr ? `${RFValue(props.pr)}px` : '0px')};
  padding-bottom: ${props => (props.pb ? `${RFValue(props.pb)}px` : '0px')};
  padding-top: ${props => (props.pt ? `${RFValue(props.pt)}px` : '0px')};
  margin-left: ${props => (props.ml ? `${RFValue(props.ml)}px` : '0px')};
  margin-right: ${props => (props.mr ? `${RFValue(props.mr)}px` : '0px')};
  margin-bottom: ${props => (props.mb ? `${RFValue(props.mb)}px` : '0px')};
  margin-top: ${props => (props.mt ? `${RFValue(props.mt)}px` : '0px')};
`;
