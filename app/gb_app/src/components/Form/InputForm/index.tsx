import React from 'react';
import {Control, Controller} from 'react-hook-form';

import {Container, Error} from './style';

import Input, {InputProps} from '../Input';

interface InputFormProps extends InputProps {
  control: Control;
  name: string;
  error: string;
}

const InputForm: React.FC<InputFormProps> = ({
  control,
  name,
  error,
  ...props
}) => {
  return (
    <Container>
      <Controller
        name={name}
        control={control}
        render={({field: {onChange, value}}) => (
          <Input onChangeText={onChange} value={value} {...props} />
        )}
      />
      {error && <Error>{error}</Error>}
    </Container>
  );
};

export default InputForm;
