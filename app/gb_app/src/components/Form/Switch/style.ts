import styled from 'styled-components/native';
import {RFValue} from 'react-native-responsive-fontsize';

interface PropsSwitch {
  active: boolean;
}

export const Container = styled.TouchableOpacity.attrs({
  activeOpacity: 0.7,
})<PropsSwitch>`
  background-color: ${({theme, active}) =>
    active ? theme.colors.success : theme.colors.placeholder};
  border-radius: ${RFValue(16)}px;
  height: ${RFValue(30)}px;
  width: ${RFValue(56)}px;
  flex-direction: row;
  justify-content: ${({active}) => (active ? 'flex-end' : 'flex-start')};
  align-items: center;
  padding: ${RFValue(2)}px;
`;

export const IconSwitch = styled.View`
  width: ${RFValue(26)}px;
  height: ${RFValue(26)}px;
  border-radius: ${RFValue(13)}px;
  background-color: ${({theme}) => theme.colors.shape};
`;
