import React from 'react';

import {Container, IconSwitch} from './style';

interface SwitchProps {
  enabled: boolean;
  setEnabled(item: boolean): void;
}

const Switch: React.FC<SwitchProps> = ({enabled, setEnabled}) => {
  const handleToggleSwitch = () => {
    setEnabled(!enabled);
  };
  return (
    <Container onPress={handleToggleSwitch} active={enabled}>
      <IconSwitch />
    </Container>
  );
};

export default Switch;
