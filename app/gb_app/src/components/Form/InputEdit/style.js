import styled from 'styled-components/native';
import {RFValue} from 'react-native-responsive-fontsize';

export const ItemInput = styled.View`
  width: 100%;
  margin-bottom: ${RFValue(15)}px;
`;

export const HeaderInput = styled.View`
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`;

export const ButtonEditInput = styled.TouchableOpacity.attrs({
  activeOpacity: 0.7,
})``;

export const ContainerButton = styled.View`
  margin-top: ${RFValue(20)}px;
`;
export const LabelInput = styled.Text`
  color: ${props => (props.color ? props.color : props.theme.colors.edit)};
  text-transform: uppercase;
  font-family: ${({theme}) => theme.fonts.medium};
  font-size: ${RFValue(12)}px;
  line-height: ${RFValue(16)}px;
`;

export const Box = styled.View`
  width: ${props => (props.width ? props.width : '100%')};
  justify-content: ${props => (props.justify ? props.justify : 'flex-start')};
  align-items: ${props => (props.align ? props.align : 'flex-start')};
  flex-direction: ${props => (props.direction ? 'row' : 'column')};
  padding-right: ${props => (props.pr ? `${RFValue(props.pr)}px` : '0')};
`;
