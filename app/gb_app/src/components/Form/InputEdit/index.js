import React from 'react';
import {Icon} from 'react-native-elements';
import ImageIcon from '~/assets/images/Icons';
import {ItemInput, HeaderInput, LabelInput, Box} from './style';

import Input from '../Input/index';

const InputEdit = ({
  label,
  theme,
  value,
  onChangeText,
  masked,
  hasIcon,
  hasIconRight,
  onPressIcon,
  editable,
  fontSize,
  height,
  visiblePassword,
  setVisiblePassword,
  ...rest
}) => {
  return (
    <ItemInput>
      <HeaderInput>
        <LabelInput color={editable ? theme.colors.dark_36 : theme.colors.edit}>
          {label}
        </LabelInput>
      </HeaderInput>
      <Box direction align="center" pr={hasIconRight ? '30' : '0'}>
        <Input
          color={editable ? theme.colors.dark_36 : theme.colors.edit}
          typeInput="line"
          value={value}
          fontSize={fontSize}
          height={height}
          editable={editable}
          masked={masked}
          onChangeText={txt => onChangeText(txt)}
          borderColor={theme.colors.border}
          {...rest}
        />

        {hasIconRight && (
          <Icon
            type="ionicon"
            name={visiblePassword ? 'eye-outline' : 'eye-off-outline'}
            color={theme.colors.success}
            size={20}
            onPress={() => setVisiblePassword(!visiblePassword)}
          />
        )}
      </Box>
    </ItemInput>
  );
};

export default InputEdit;
