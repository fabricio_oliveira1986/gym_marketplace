import React from 'react';
import Feather from 'react-native-vector-icons/Feather';
import { useTheme } from 'styled-components';

import { Container, Button, Text } from './style';

interface CounterProps {
  value: number;
  setValueCounter(value: number): void;
}

const Counter: React.FC<CounterProps> = ({ value, setValueCounter }) => {
  const theme = useTheme();

  const handleDecrement = () => {
    if (value > 1) {
      setValueCounter(value - 1);
    }
  };

  const handleIncrement = () => {
    setValueCounter(value + 1);
  };

  return (
    <Container>
      <Button onPress={handleIncrement}>
        <Feather name="chevron-up" size={16} color={theme.colors.title} />
      </Button>
      <Text>{value}</Text>
      <Button onPress={handleDecrement}>
        <Feather name="chevron-down" size={16} color={theme.colors.title} />
      </Button>
    </Container>
  );
};

export default Counter;
