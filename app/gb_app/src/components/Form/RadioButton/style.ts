import styled, {css} from 'styled-components/native';
import {RFValue} from 'react-native-responsive-fontsize';

interface ContainerProps {
  isActive: boolean;
}

export const Container = styled.View<ContainerProps>`
  ${({isActive, theme}) =>
    isActive
      ? css`
          background-color: ${theme.colors.success};
        `
      : css`
          border: 1.5px solid ${({theme}) => theme.colors.text};
        `}
  flex: 1;
`;

export const Button = styled.TouchableOpacity.attrs({
  activeOpacity: 0.7,
})`
  align-items: center;
  justify-content: center;
  align-items: center;
  height: ${RFValue(40)}px;
`;

interface TitleProps {
  color: string;
  size: number;
  isActive: boolean;
}
export const Title = styled.Text<TitleProps>`
  color: ${({color, isActive, theme}) =>
    isActive ? theme.colors.shape : color};
  font-size: ${({size}) => RFValue(size)}px;
  font-family: ${({theme}) => theme.fonts.regular};
`;
