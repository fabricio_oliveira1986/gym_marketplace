import React from 'react';
import {TouchableOpacityProps} from 'react-native';
import {Container, Button, Title} from './style';

interface RadioButtonProps extends TouchableOpacityProps {
  title: string;
  color: string;
  size: number;
  isActive: boolean;
}

const RadioButton: React.FC<RadioButtonProps> = ({
  title,
  color,
  size,
  isActive,
  ...rest
}) => {
  return (
    <Container isActive={isActive}>
      <Button {...rest}>
        <Title isActive={isActive} color={color} size={size}>
          {title}
        </Title>
      </Button>
    </Container>
  );
};

export default RadioButton;
