import React, {useState, useEffect} from 'react';
import {Modalize} from 'react-native-modalize';
import {RFPercentage} from 'react-native-responsive-fontsize';
import Feather from 'react-native-vector-icons/Feather';
import {useTheme} from 'styled-components';

import {DataProps} from '~/services/filter';

import {
  ContainerHeader,
  HeaderTitle,
  ButtonClose,
  ButtonOption,
  TextOption,
  Separator,
} from './style';

export interface SelectProps {
  title: string;
  data(): Promise<DataProps[]>;
  onPress(item: DataProps): void;
}

interface SelectModalProps {
  data: SelectProps;
  onClose(): void;
  modalizeRef: React.RefObject<Modalize>;
}

const SelectModal2: React.FC<SelectModalProps> = ({
  data,
  onClose,
  modalizeRef,
}) => {
  const theme = useTheme();
  const [values, setValues] = useState<DataProps[]>();
  const [value, setValue] = useState<DataProps>({} as DataProps);

  const handleClose = () => {
    modalizeRef.current?.close();
    onClose();
  };

  useEffect(() => {
    data &&
      data.data &&
      data.data().then(items => {
        setValues(items);
      });
  }, [data]);

  const handleItemSelect = (item: DataProps) => {
    setValue(item);
    data.onPress(item);
    handleClose();
  };

  const renderItem = ({item}: {item: DataProps}) => {
    return (
      <ButtonOption onPress={() => handleItemSelect(item)}>
        <TextOption>{item.title}</TextOption>
        {value.id === item.id && <Feather name="check" size={28} />}
      </ButtonOption>
    );
  };

  return (
    <Modalize
      snapPoint={RFPercentage(90)}
      ref={modalizeRef}
      HeaderComponent={
        <ContainerHeader>
          <HeaderTitle>{data.title}</HeaderTitle>
          <ButtonClose onPress={() => handleClose()}>
            <Feather name="x" size={28} color={theme.colors.shape} />
          </ButtonClose>
        </ContainerHeader>
      }
      flatListProps={{
        data: values,
        keyExtractor: item => item.id,
        renderItem: renderItem,
        ItemSeparatorComponent: () => <Separator />,
      }}
    />
  );
};

export default SelectModal2;
