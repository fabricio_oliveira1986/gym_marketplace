import styled from 'styled-components/native';
import {RFValue} from 'react-native-responsive-fontsize';

export const ContainerHeader = styled.View`
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  padding: ${RFValue(10)}px ${RFValue(15)}px;
  background-color: ${({theme}) => theme.colors.primary};
`;

export const HeaderTitle = styled.Text`
  font-family: ${({theme}) => theme.fonts.medium};
  font-size: ${RFValue(18)}px;
  color: ${({theme}) => theme.colors.shape};
  padding: ${RFValue(10)}px 0px;
`;

export const ButtonClose = styled.TouchableOpacity.attrs({
  activeOpacity: 0.7,
})``;

export const ButtonOption = styled.TouchableOpacity.attrs({
  activeOpacity: 0.7,
})`
  flex-direction: row;
  justify-content: space-between;
  padding: ${RFValue(15)}px;
`;

export const TextOption = styled.Text`
  font-size: ${RFValue(18)}px;
  font-family: ${({theme}) => theme.fonts.regular};
  color: ${({theme}) => theme.colors.title};
`;

export const ContainerFooter = styled.View`
  width: 100%;
  height: ${RFValue(60)}px;
  justify-content: center;
  align-items: center;
  padding: ${RFValue(10)}px ${RFValue(15)}px;
`;

export const Separator = styled.View`
  height: ${RFValue(1)}px;
  width: 100%;
  background-color: ${({theme}) => theme.colors.separator};
`;
