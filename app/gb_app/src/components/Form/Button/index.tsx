import React from 'react';
import {TouchableOpacityProps} from 'react-native';
import {SvgProps} from 'react-native-svg';

import {Container, Text, ContainerButton} from './style';

export interface ButtonProps extends TouchableOpacityProps {
  children: string;
  background?: string;
  color: string;
  border?: boolean;
  borderColor?: string;
  borderRadius?: number;
  fontSize: number;
  fontBold?: boolean;
  uppercase?: boolean;
  Icon?: SvgProps;
  marginLeftText?: number;
  block?: boolean;
  width?: number;
  height?: number;
  direction?: 'row' | 'column';
  align?: 'center' | 'flex-start' | 'flex-end' | 'stretch';
  justify?:
    | 'center'
    | 'flex-start'
    | 'flex-end'
    | 'space-between'
    | 'space-around';
  paddingHorizontal?: number;
  paddingVertical?: number;
  onPress: () => void;
}

const Button: React.FC<ButtonProps> = ({
  children,
  color,
  fontSize,
  fontBold,
  uppercase,
  marginLeftText,
  border,
  borderColor,
  borderRadius,
  block,
  width,
  Icon,
  onPress,
  ...props
}) => {
  return (
    <Container
      border={border}
      borderColor={borderColor}
      borderRadius={borderRadius}
      width={width}
      block={block}>
      <ContainerButton borderRadius={borderRadius} onPress={onPress} {...props}>
        {Icon && <Icon fill={color} color={color} />}
        <Text
          color={color}
          fontSize={fontSize}
          fontBold={fontBold}
          uppercase={uppercase}
          marginLeftText={marginLeftText}>
          {children}
        </Text>
      </ContainerButton>
    </Container>
  );
};

export default Button;
