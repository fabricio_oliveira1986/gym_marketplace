import styled, {css} from 'styled-components/native';
import {RFValue} from 'react-native-responsive-fontsize';

import {ButtonProps} from '.';

export const Container = styled.View<
  Pick<
    ButtonProps,
    'border' | 'borderColor' | 'borderRadius' | 'width' | 'block'
  >
>`
  ${({border, borderRadius, borderColor, theme}) =>
    border &&
    css`
      border-radius: ${borderRadius ? RFValue(borderRadius) : 0}px;
      border: 1px solid ${borderColor || theme.colors.text};
    `}
  width: ${props =>
    props.hasOwnProperty('block')
      ? '100%'
      : props.width
      ? RFValue(props.width) + 'px'
      : 'auto'};
`;

export const ContainerButton = styled.TouchableOpacity.attrs({
  activeOpacity: 0.7,
})<
  Omit<
    ButtonProps,
    | 'children'
    | 'color'
    | 'fontSize'
    | 'fontBold'
    | 'Icon'
    | 'marginLeftText'
    | 'borderRadius'
    | 'width'
    | 'block'
  >
>`
  background-color: ${props => props.background || 'transparent'};
  height: ${props => RFValue(props.height || 40)}px;
  border-radius: ${props =>
    `${props.borderRadius ? RFValue(props.borderRadius) : 0}px;`};
  flex-direction: ${props => props.direction || 'row'};
  align-items: ${props => props.align || 'center'};
  justify-content: ${props => props.justify || 'center'};
`;

export const Text = styled.Text<
  Pick<
    ButtonProps,
    'color' | 'fontSize' | 'fontBold' | 'marginLeftText' | 'uppercase'
  >
>`
  color: ${props => props.color};
  font-size: ${props => RFValue(props.fontSize)}px;
  font-family: ${props =>
    props.fontBold ? props.theme.fonts.medium : props.theme.fonts.regular};
  margin-left: ${props => props.marginLeftText || 0}px;
  text-transform: ${props =>
    props.hasOwnProperty('uppercase') ? 'uppercase' : 'none'};
`;
