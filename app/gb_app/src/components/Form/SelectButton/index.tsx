import React from 'react';
import Feather from 'react-native-vector-icons/Feather';

import {Container, Select, Placeholder} from './style';

interface SelectProps {
  placeholder: string;
  value: string;
  onPress(): void;
  width: string;
  height: number;
}

const SelectButton: React.FC<SelectProps> = ({
  placeholder,
  value,
  onPress,
  width,
  height,
}) => {
  return (
    <Container width={width}>
      <Select height={height} onPress={onPress}>
        <Placeholder>{value || placeholder}</Placeholder>
        <Feather name="chevron-down" size={18} />
      </Select>
    </Container>
  );
};

export default SelectButton;
