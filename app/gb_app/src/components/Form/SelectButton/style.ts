import styled from 'styled-components/native';
import {RFValue} from 'react-native-responsive-fontsize';

export const Container = styled.View`
  margin-bottom: ${RFValue(20)}px;
  border: 1px solid ${({theme}) => theme.colors.border};
  background-color: ${({theme}) => theme.colors.shape};
  border-radius: ${RFValue(4)}px;
  width: ${props => (props.width ? props.width : 'auto')};
`;

export const Select = styled.TouchableOpacity.attrs({
  activeOpacity: 0.7,
})`
  flex-direction: row;
  justify-content: space-between;
  padding: ${props =>
    props.height ? `${RFValue(props.height)}px` : `${RFValue(20)}px`};
`;

export const Placeholder = styled.Text`
  color: ${({theme}) => theme.colors.border};
  font-family: ${({theme}) => theme.fonts.medium};
  font-size: ${RFValue(12)}px;
  line-height: ${RFValue(16)}px;
`;
