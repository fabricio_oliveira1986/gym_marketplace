import styled, {css} from 'styled-components/native';
import {RFValue} from 'react-native-responsive-fontsize';

import {InputProps} from '.';

interface ButtonVisiblePasswordProps {
  height: number;
}

export const Container = styled.View<
  Pick<
    InputProps,
    | 'background'
    | 'borderColor'
    | 'width'
    | 'marginBottom'
    | 'typeInput'
    | 'borderRadius'
  >
>`
  flex-direction: row;
  align-items: center;
  background-color: ${props => props.background || 'transparent'};
  width: ${props => (props.width ? RFValue(props.width) + 'px' : '100%')};
  margin-bottom: ${props =>
    props.marginBottom ? props.marginBottom + 'px' : '0px'};
  ${props =>
    props.typeInput === 'normal'
      ? css`
          border-color: ${props.borderColor || props.theme.colors.primary};
          border-width: ${RFValue(1)}px;
          padding: 0 ${RFValue(10)}px;
        `
      : css`
          border-bottom-color: ${props.borderColor ||
          props.theme.colors.primary};
          border-bottom-width: ${RFValue(1)}px;
        `}
`;

export const TextInput = styled.TextInput<
  Omit<
    InputProps,
    | 'background'
    | 'borderColor'
    | 'width'
    | 'marginBottom'
    | 'typeInput'
    | 'borderRadius'
  >
>`
  flex: 1;
  height: ${props => (props.height ? RFValue(props.height) + 'px' : 'auto')};
  color: ${({color}) => color};
  font-size: ${({fontSize}) => fontSize}px;
  text-transform: ${({uppercase}) => (uppercase ? 'uppercase' : 'none')};
  ${({fontBold, theme}) =>
    fontBold
      ? css`
          font-family: ${theme.fonts.medium};
        `
      : css`
          font-family: ${theme.fonts.regular};
        `};
`;

export const ButtonVisiblePassword = styled.TouchableOpacity<ButtonVisiblePasswordProps>`
  height: ${({height}) => (height ? RFValue(height) + 'px' : 'auto')};
  align-items: center;
  justify-content: center;
`;
