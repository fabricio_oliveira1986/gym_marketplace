import React, {useState, useCallback} from 'react';
import {TextInputProps} from 'react-native';
import {SvgProps} from 'react-native-svg';
import {
  TextInputMask,
  TextInputMaskOptionProp,
  TextInputMaskTypeProp,
} from 'react-native-masked-text';
import Feather from 'react-native-vector-icons/Feather';
import {useTheme} from 'styled-components/native';

import {Container, TextInput, ButtonVisiblePassword} from './style';

export interface InputProps extends TextInputProps {
  background?: string;
  borderColor?: string;
  borderRadius?: number;
  color: string;
  fontSize: number;
  fontBold?: boolean;
  uppercase?: boolean;
  typeInput?: 'normal' | 'line';
  password?: boolean;
  masked?: boolean;
  options?: TextInputMaskOptionProp;
  Icon?: SvgProps;
  width?: number;
  height: number;
  marginBottom?: number;
  type?: TextInputMaskTypeProp;
}

const Input: React.FC<InputProps> = ({
  background,
  borderColor,
  borderRadius,
  typeInput = 'normal',
  password,
  width,
  height,
  fontSize,
  marginBottom,
  Icon,
  masked,
  options,
  type,
  ...rest
}) => {
  const theme = useTheme();
  const [visiblePassword, setVisiblePassword] = useState(true);
  const handleVisiblePassword = useCallback(() => {
    setVisiblePassword(item => !item);
  }, []);
  return (
    <Container
      background={background}
      borderColor={borderColor}
      borderRadius={borderRadius}
      typeInput={typeInput}
      width={width}
      marginBottom={marginBottom}>
      {Icon && <Icon />}

      {masked ? (
        <TextInputMask
          {...rest}
          type={type || 'custom'}
          options={
            options
              ? options
              : {
                  maskType: 'BRL',
                  withDDD: true,
                  dddMask: '(99) ',
                }
          }
        />
      ) : (
        <TextInput
          height={height}
          secureTextEntry={!password ? false : visiblePassword}
          fontSize={fontSize}
          {...rest}
        />
      )}
      {password && (
        <ButtonVisiblePassword height={height} onPress={handleVisiblePassword}>
          <Feather
            name={visiblePassword ? 'eye' : 'eye-off'}
            color={theme.colors.success}
            size={20}
          />
        </ButtonVisiblePassword>
      )}
    </Container>
  );
};

export default Input;
