import React, {useState} from 'react';
import {TouchableOpacityProps} from 'react-native';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import {BoxTouchablePercent, Text} from '~/components/index';
import {useTheme} from 'styled-components/native';
import {isEmpty} from 'lodash';
import moment from 'moment';

interface IInputDate extends TouchableOpacityProps {
  placeholder: string;
  value: string;
  changeValue: (value: string) => void;
}

const InputDate: React.FC<IInputDate> = ({
  placeholder,
  value,
  changeValue,
  error,
}) => {
  const theme = useTheme();
  const [isDatePickerVisible, setIsDatePickerVisible] = useState(false);

  return (
    <>
      <BoxTouchablePercent
        borderBottomWidth={1}
        justify="center"
        height={40}
        borderColor={theme.colors.primary}
        onPress={() => {
          setIsDatePickerVisible(true);
        }}>
        {isEmpty(value) ? (
          <Text fontSize={14} color={theme.colors.placeholder}>
            {placeholder}
          </Text>
        ) : (
          <Text fontSize={14} color={theme.colors.dark}>
            {moment(value).format('DD/MM/YYYY')}
          </Text>
        )}
      </BoxTouchablePercent>
      {error && <Text color={theme.colors.error}>{error}</Text>}
      <DateTimePickerModal
        headerTextIOS="Selecione uma data"
        cancelTextIOS="Cancelar"
        confirmTextIOS="Confirmar"
        display="spinner"
        isVisible={isDatePickerVisible}
        mode="date"
        date={
          moment(value, 'DD-MM-YYYY').isValid()
            ? new Date(moment(value))
            : new Date()
        }
        onConfirm={data => {
          setIsDatePickerVisible(false);
          changeValue(moment(data).format('YYYY-MM-DD'));
        }}
        onCancel={() => setIsDatePickerVisible(false)}
      />
    </>
  );
};

export default InputDate;
