import {api} from './api';

import {Bussiness} from '~/models/Bussiness';

interface FilterData {
  [key: string]: string;
}

export const transformFilterServer = (data: FilterData) => {
  const keys = {
    city: 'cidade',
    region: 'regiao',
    search: 'texto',
  };
  return Object.keys(data).reduce((acc, key) => {
    if (keys.hasOwnProperty(key)) {
      const keyFieid = keys[key];
      acc[keyFieid] = data[key] ? data[key] : '';
    } else {
      acc[key] = data[key] ? data[key] : '';
    }
    return acc;
  }, {} as FilterData);
};

export const getBussinesses = async (options: {}) => {
  try {
    const data = {...options, getEmpresas: 'S'} as FilterData;
    const response = await api.post<Bussiness[]>('', data);
    return response.data;
  } catch (error) {
    throw error;
  }
};

export const getBussiness = async (id: string) => {
  try {
    const response = await api.get<Bussiness>(`?getEmpresa=S&id=${id}`);
    return response.data;
  } catch (error) {
    throw error;
  }
};
