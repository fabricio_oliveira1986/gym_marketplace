import { api } from './api';

import { Avaliation } from '~/models/Avaliation';

export const getAvaliationsProduct = async (idProduct: string) => {
  try {
    const response = await api.get<Avaliation[]>(
      `?getAvaliacoes=S&id_anuncio=${idProduct}`,
    );
    return response.data;
  } catch (e) {
    throw e;
  }
};



export const setAvaliationPurchase = async (data: string) => {
  try {
    const response = await api.post('', { setAvaliacao: 'S', ...data });
    return response.data;
  } catch (e) {
    throw e;
  }
};