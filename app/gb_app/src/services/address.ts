import {api} from './api';
import axios from 'axios';

import {Address, City, Estado} from '~/models/Address';
import {UserDataAddress} from '~/models/Profile';

export const getAddresses = async () => {
  try {
    const item = await api.get<Address[]>(`?getEnderecos`);
    return item.data;
  } catch (e) {
    throw e;
  }
};

export const searchCep = async cep => {
  try {
    const item = await axios.get(`https://viacep.com.br/ws/${cep}/json/`);
    return item.data;
  } catch (e) {
    throw e;
  }
};

export const postNewAddress = async (data: UserDataAddress) => {
  try {
    const body = await mountAddress(data);
    const item = await api.post<Address[]>('', {setEndereco: 'S', ...body});
    return item.data;
  } catch (e) {
    throw e;
  }
};

export const deleteAddress = async data => {
  try {
    const item = await api.post<Address[]>('', {delEndereco: 'S', ...data});
    return item.data;
  } catch (e) {
    throw e;
  }
};

export const updateAddress = async (data: UserDataAddress) => {
  try {
    const body = await mountAddress(data);
    const item = await api.post<Address[]>('', {setEndereco: 'S', ...body});
    return item.data;
  } catch (e) {
    throw e;
  }
};

export const mountAddress = async (data: UserDataAddress) => {
  const listUF = await api.get<Address>(`?getEstados`);
  const UF = listUF.data.filter(item => item.estado_sigla === data.txtEstado);
  const listCity = await api.get<Address>(
    `?getCidades=S&txtEstado=${UF[0].estado_codigo}`,
  );
  const city = listCity.data.filter(
    item => item.cidade_nome === data.txtCidade,
  );

  const body = {
    ...data,
    txtCidade: city[0].cidade_codigo,
    txtEstado: city[0].cidade_codigoEstado,
  };
  return body;
};

export const getEstados = async () => {
  const {data} = await api.get<Estado[]>(`?getEstados&onlyActived=true`);
  return data;
};

export const getCidades = async (idEstado: string) => {
  const {data} = await api.get<City[]>(
    `?getCidades=S&onlyActived=true&txtEstado=${idEstado}`,
  );
  return data;
};
