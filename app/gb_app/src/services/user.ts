import {api} from './api';

import {
  User,
  UserLogin,
  UserData,
  UserPasswordResetData,
  UserPasswordResetResponse,
} from './../models/User';

export interface UserResponse {
  token: string;
  user: User;
}

export const createUser = async (data: UserData) => {
  try {
    const item = await api.post<UserResponse>('', {cadastrar: 'S', ...data});
    return item.data;
  } catch (e) {
    throw e;
  }
};

export const login = async (data: UserLogin) => {
  try {
    const item = await api.post<UserResponse>('', {login: 'S', ...data});
    return item.data;
  } catch (e) {
    throw e;
  }
};

export interface UserResponse {
  txtEmail: string;
}

export const resetPassword = async (data: UserPasswordResetData) => {
  try {
    const item = await api.post<UserPasswordResetResponse>('', {
      setRecuperarSenha: 'S',
      ...data,
    });
    return item.data;
  } catch (e) {
    throw e;
  }
};

export const getTerms = async () => {
  try {
    const item = await api.get('?getTermos=S');
    return item.data;
  } catch (e) {
    throw e;
  }
};
