import {api} from './api';
import AsyncStorage from '@react-native-async-storage/async-storage';

export const getMyPurchases = async () => {
  try {
    const token = await AsyncStorage.getItem('@GB:token');
    const item = await api.get(`?getVendas=S`, {
      headers: {
        authorization: `Bearer ${token}`,
      },
    });
    return item.data;
  } catch (e) {
    throw e;
  }
};

export const cancelMyPurchases = async data => {
  try {
    const item = await api.post('', {setEstornarVendas: 'S', ...data});
    return item.data;
  } catch (e) {
    throw e;
  }
};

export const getOnlyPurchases = async (id: string) => {
  try {
    const token = await AsyncStorage.getItem('@GB:token');
    const item = await api.get(`?getVenda=S&id_venda=${id}`, {
      headers: {
        authorization: `Bearer ${token}`,
      },
    });
    return item.data;
  } catch (e) {
    throw e;
  }
};
