import {api} from './api';

export const getCreditCards = async () => {
  try {
    const item = await api.get(`?getCartoes`);
    return item.data;
  } catch (e) {
    throw e;
  }
};

export const inactivateCreditCards = async body => {
  try {
    const item = await api.post('', {delCartao: 'S', ...body});
    return item.data;
  } catch (e) {
    throw e;
  }
};
