import {api} from './api';

import {Product} from '~/models/Product';

interface FilterData {
  [key: string]: string;
}

export const transformFilterServer = (data: FilterData) => {
  const keys = {
    category: 'categoria',
    city: 'cidade',
    region: 'regiao',
    search: 'texto',
  };
  return Object.keys(data).reduce((acc, key) => {
    if (keys.hasOwnProperty(key)) {
      const keyFieid = keys[key];
      acc[keyFieid] = data[key] ? data[key] : '';
    } else if (key === 'price') {
      const price = data[key];
      const [valor_min, valor_max] = price ? price.split('-') : ['', ''];
      acc.valor_min = valor_min;
      acc.valor_max = valor_max;
    } else {
      acc[key] = data[key] ? data[key] : '';
    }
    return acc;
  }, {} as FilterData);
};

export const getProducts = async (options: {}) => {
  try {
    const data = {...options, getAnuncios: 'S'} as FilterData;
    const response = await api.post<Product[]>('', data);
    return response.data;
  } catch (erro) {
    throw erro;
  }
};

export const getProduct = async (id: string) => {
  try {
    const response = await api.get<Product>(`?getAnuncio=S&id=${id}`);
    return response.data;
  } catch (erro) {
    throw erro;
  }
};
