import {api} from './api';

import {Ask} from './../models/Ask';

interface ItemAsk {
  productId: string;
  ask: string;
}

export const getAsksProduct = async (idProduct: string) => {
  try {
    const response = await api.get<Ask[]>(
      `?getPerguntas=S&id_anuncio=${idProduct}`,
    );
    console.log(idProduct);
    return response.data;
  } catch (e) {
    throw e;
  }
};

export const saveAsk = async (data: ItemAsk) => {
  try {
    const response = await api.post('', {
      setPergunta: 'S',
      txtAnuncio: data.productId,
      txtPergunta: data.ask,
    });
    return response.data;
  } catch (erro) {
    throw erro;
  }
};
