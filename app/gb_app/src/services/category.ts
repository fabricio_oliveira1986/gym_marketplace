import { api } from './api';
import { Category } from './../models/Category';


export const getCategories = async() => {
  try {
    const response = await api.get<Category[]>('?getMenuCategorias=S');
    return response.data;
  } catch (e) {
    throw e;
  }
};
