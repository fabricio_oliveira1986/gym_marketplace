import axios from 'axios';
import {env} from '~/config';
import {create} from 'apisauce';

let apiInstance;
const api = axios.create({
  baseURL: env()?.url,
});

apiInstance = create({axiosInstance: api});

export {api, apiInstance};
