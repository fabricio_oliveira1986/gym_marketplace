import {Ofert} from '~/models/Ofert';
import Images from '~/assets/images/Oferts';

function getLista(): Ofert[] {
  return [
    {
      id: '0',
      title: 'Melhores',
      name: 'Melhores',
      image: Images.OfertsBests,
    },
    {
      id: '1',
      title: 'Destaques da Semana',
      name: 'Destaques da Semana',
      image: Images.Planos,
    },
    {
      id: '2',
      title: 'Maior Cashback',
      name: 'Maior Cashback',
      image: Images.Cashback,
    },
    {
      id: '3',
      title: 'Produtos e Serviços perto de voce',
      name: 'Produtos',
      image: Images.Products,
    },
  ];
}

export const getOferts = () => {
  return new Promise<Ofert[]>(resolve => {
    resolve(getLista());
  });
};
