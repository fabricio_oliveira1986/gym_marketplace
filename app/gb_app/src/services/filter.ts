import {getCategories} from './category';

import {api} from './api';

import {numberReal} from '~/global/utils/functions';

export interface DataProps {
  id: string;
  title: string;
}

interface ItemRegion {
  id: string;
  name: string;
}

interface ItemCity {
  id: string;
  city: string;
  state: string;
}

interface ItemValues {
  min_val: string;
  max_val: string;
}

interface ItemCashback {
  id: Number;
  descricao: string;
}

export const getCategoriesFilter = async () => {
  try {
    const response = await getCategories();
    const categoriesFilter = response.map(category => {
      return {
        id: category.id,
        title: category.name,
      };
    });
    return categoriesFilter as DataProps[];
  } catch (e) {
    throw e;
  }
};

export const getRegionsFilter = async () => {
  try {
    const response = await api.get<ItemRegion[]>('?getMenuRegioes');
    const regionsFilter = response.data.map(regions => {
      return {
        id: regions.id,
        title: regions.name,
      };
    });
    return regionsFilter as DataProps[];
  } catch (e) {
    throw e;
  }
};

export const getCitiesFilter = async () => {
  try {
    const response = await api.get<ItemCity[]>('?getMenuCidades');
    const citiesFilter = response.data.map(city => {
      return {
        id: city.id,
        title: city.city + ' - ' + city.state,
      };
    });
    return citiesFilter as DataProps[];
  } catch (e) {
    throw e;
  }
};

export const getValuesFilter = async () => {
  try {
    const response = await api.get<ItemValues[]>('?getMenuValores');
    const valuesFilter = response.data.map(item => ({
      id: item.min_val + '-' + item.max_val,
      title: numberReal(item.min_val) + ' - ' + numberReal(item.max_val),
    }));
    return valuesFilter as DataProps[];
  } catch (e) {
    throw e;
  }
};

export const getValuesCashbackFilter = async () => {
  try {
    const response = await api.get<ItemCashback[]>('?getMenuCashback');
    const valuesCashbackFilter = response.data.map(item => ({
      id: item.id.toString(),
      title: item.descricao,
    }));
    return valuesCashbackFilter as DataProps[];
  } catch (e) {
    throw e;
  }
};
