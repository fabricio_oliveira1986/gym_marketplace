import {api} from './api';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {UserProfileResponse} from '~/models/Profile';

export const getInfoProfile = async () => {
  try {
    const dataUser = JSON.parse(await AsyncStorage.getItem('@GB:user'));
    const response = await api.get<UserProfileResponse[]>(
      `?getPerfil=S&id_usuario=${dataUser.id}`,
    );
    return response.data;
  } catch (e) {
    throw e;
  }
};

export const updateInfoProfile = async data => {
  try {
    const token = await AsyncStorage.getItem('@GB:token');
    const response = await api.post<UserProfileResponse[]>(
      '',
      {setPerfil: 'S', ...data},
      {
        headers: {
          authorization: `Bearer ${token}`,
        },
      },
    );

    try {
      // regrava o async Storage por causa do nome que aparece no menu do perfil
      const dataUser = JSON.parse(await AsyncStorage.getItem('@GB:user'));
      await AsyncStorage.setItem(
        '@GB:user',
        JSON.stringify({...dataUser, name: data.txtNome}),
      );
    } catch (error) {
      console.log(error);
    }

    return response.data;
  } catch (e) {
    throw e;
  }
};

export const updateInfoPassword = async data => {
  try {
    const token = await AsyncStorage.getItem('@GB:token');
    const response = await api.post<UserDataPassword[]>(
      '',
      {setAlterarSenha: 'S', ...data},
      {
        headers: {
          authorization: `Bearer ${token}`,
        },
      },
    );
    return response.data;
  } catch (e) {
    throw e;
  }
};

export const searchCep = async data => {
  try {
    const token = await AsyncStorage.getItem('@GB:token');
    const response = await api.post<UserDataPassword[]>(
      '',
      {setAlterarSenha: 'S', ...data},
      {
        headers: {
          authorization: `Bearer ${token}`,
        },
      },
    );
    return response.data;
  } catch (e) {
    throw e;
  }
};

export const deleteAccountApi = async () => {
  try {
    const token = await AsyncStorage.getItem('@GB:token');
    const {data} = await api.post(
      '',
      {deleteAccount: 'S'},
      {
        headers: {
          authorization: `Bearer ${token}`,
        },
      },
    );

    return data;
  } catch (e) {
    throw e;
  }
};
