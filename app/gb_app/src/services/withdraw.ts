import {api} from './api';

import {User, UserLogin, UserData} from './../models/User';

export interface UserResponse {
  token: string;
  user: User;
}

export const orderWithdraw = async (data: UserData) => {
  try {
    const item = await api.post<UserResponse>('', {setVenda: 'S', ...data});
    return item.data;
  } catch (e) {
    throw e;
  }
};
