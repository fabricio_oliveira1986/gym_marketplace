import {api} from './api';

interface PaymentProps {
  idProducts: string[];
}

interface ResponsePayments {
  boleto: string;
  credit_card: string;
  pix: string;
}

export const getPayments = async (data: PaymentProps) => {
  try {
    const response = await api.post<ResponsePayments>('', {
      getFormasPagamento: 'S',
      IdAnuncio: data.idProducts,
    });
    return response.data;
  } catch (e) {
    throw e;
  }
};

interface DataPaymentProps {
  setVenda: string;
  txtTipoDocumento: string;
  txtIdFrete: number;
  txtIdEndereco: number;
  txtParcelas: number;
  txtIdAnuncio: Array<string | number>;
  txtQuantidade: Array<string | number>;
}

interface ResponsePayment {
  data: {id_venda: string};
}

export const makePayment = async (payload: DataPaymentProps) => {
  try {
    const response = await api.post<ResponsePayment>('', payload);
    return response.data;
  } catch (e) {
    console.log('valor de error', e);
    throw e;
  }
};
