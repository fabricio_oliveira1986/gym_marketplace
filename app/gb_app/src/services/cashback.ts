import {api} from './api';

interface ValueCashbackData {
  total: number;
  total_liquido: number;
}

export const getValueCashback = async () => {
  try {
    const response = await api.get<ValueCashbackData>('?getCashback=S');
    return response.data;
  } catch (e) {
    throw e;
  }
};
