import { api } from './api';
import { numberFormat } from '~/global/utils/functions';
import { Freight } from './../models/Freight';

interface FreightProps {
  idProducts: string[];
}

export interface DataProps {
  id: string;
  title: string;
}

interface ResponseFreight {
  id: string;
  title: string;
  value: string;
}

export const getFreights = async (data: FreightProps) => {
  try {
    const response = await api.post<Freight[]>('', {
      getFretes: 'S',
      IdAnuncio: data.idProducts,
    });
    if (Array.isArray(response.data)) {
      return response.data;
    }
    throw 'Erro ao listar a lista de fretes';
  } catch (e) {
    throw e;
  }
};

export const transformFreight = (freights: Freight[]) => {
  return freights.map(item => ({
    id: item.id,
    title: `${item.title} (${numberFormat(item.value, 1)})`,
  })) as DataProps[];
};
