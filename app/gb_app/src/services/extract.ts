import {api} from './api';
import {Extract} from '~/models/Extract';

interface ResponseExtract {
  cashback: number;
  cashbackDisponible: number;
  data: Extract[];
}

export const getExtractCashback = async () => {
  try {
    const response = await api.get<ResponseExtract>('?getExtrato=S');
    return response.data;
  } catch (e) {
    throw e;
  }
};
