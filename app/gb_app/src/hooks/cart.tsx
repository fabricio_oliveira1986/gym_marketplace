import React, {
  createContext,
  ReactNode,
  useState,
  useCallback,
  useContext,
  useEffect,
} from 'react';

import AsyncStorage from '@react-native-async-storage/async-storage';

import {Freight} from '~/models/Freight';
import {Product} from '~/models/Product';
import {Address} from '~/models/Address';
import {Payment} from '~/models/Payment';

interface CartProviderProps {
  children: ReactNode;
}
interface ICartContextData {
  items: ItemCart[];
  subTotal: number;
  total: number;
  cashBack: number;
  freight?: Freight;
  payment?: Payment;
  address: Address;
  updateFreight(freight: Freight): void;
  checkAddCart(product: Product): boolean;
  addToCart(item: ItemCart): Promise<void>;
  updateCart(id: string, quantity: number): Promise<void>;
  clearCart(): Promise<void>;
  removeItem(item: ItemCart): Promise<void>;
  updateAddress(data: Address): void;
  setItemPayment(data: Payment): void;
  setBalanceCashBack(value: number): void;
  balanceCashBack: number;
}

interface ItemCart {
  product: Product;
  quantity: number;
  value: number;
}

const CartContext = createContext({} as ICartContextData);

const CartProvider: React.FC<CartProviderProps> = ({children}) => {
  const [items, setItems] = useState<ItemCart[]>([]);
  const [subTotal, setSubTotal] = useState(0);
  const [total, setTotal] = useState(0);
  const [cashBack, setCashBack] = useState(0);
  const [balanceCashBack, setBalanceCashBack] = useState(0);
  const [freight, setFreight] = useState({} as Freight);
  const [address, setAddress] = useState({} as Address);
  const [payment, setPayment] = useState({} as Payment);

  const checkAddCart = useCallback(
    (product: Product) => {
      if (!items.length) {
        return true;
      } else {
        const isAdd = items.find(item => {
          return item.product?.bussiness?.id === product?.bussiness?.id;
        });
        return !!isAdd;
      }
    },
    [items],
  );

  const setItemPayment = (data: Payment) => {
    setPayment(data);
  };

  const addToCart = useCallback(
    async (item: ItemCart) => {
      console.log('item', item);
      const indexItem = items.findIndex(
        oldItem => oldItem.product.id === item.product.id,
      );
      if (indexItem !== -1) {
        const itemCart = items[indexItem];
        setItems(items =>
          items.map(oldItem =>
            oldItem.product.id === itemCart.product.id ? itemCart : oldItem,
          ),
        );
      } else {
        setItems([...items, item]);
      }
      await AsyncStorage.setItem('@GB:cart', JSON.stringify(items));
    },
    [items],
  );

  const updateFreight = useCallback((item: Freight) => {
    setFreight(item);
  }, []);

  const cleanFreight = useCallback(() => {
    setFreight({});
  }, []);

  const updateAddress = useCallback((data: Address) => {
    setAddress(data);
  }, []);

  const updateCart = useCallback(
    async (id: string, quantity: number) => {
      const indexItem = items.findIndex(oldItem => oldItem.product.id === id);
      if (indexItem !== -1) {
        const item = items[indexItem];
        item.quantity = quantity;
        setItems(items =>
          items.map(oldItem =>
            oldItem.product.id === item.product.id ? item : oldItem,
          ),
        );
        await AsyncStorage.setItem('@GB:cart', JSON.stringify(items));
      }
    },
    [items],
  );

  const removeItem = useCallback(
    async (item: ItemCart) => {
      const indexItem = items.findIndex(
        oldItem => oldItem.product.id === item.product.id,
      );
      if (indexItem !== -1) {
        setItems(oldItems =>
          oldItems.filter(oldItem => oldItem.product.id !== item.product.id),
        );
        await AsyncStorage.setItem('@GB:cart', JSON.stringify(items));
      }
    },
    [items],
  );

  const clearCart = useCallback(async () => {
    setItems([]);
    updateFreight({});
    await AsyncStorage.setItem('@GB:cart', JSON.stringify([]));
  }, [updateFreight]);

  useEffect(() => {
    const cartSubTotal = items.reduce((acc, item) => {
      acc += item.value * item.quantity;
      return acc;
    }, 0);
    const cartCashBack = items.reduce((acc, item) => {
      acc +=
        ((item.value * Number(item.product.cashBack)) / 100) * item.quantity;
      return acc;
    }, 0);
    const carTotal = cartSubTotal + (freight?.value || 0);

    setSubTotal(cartSubTotal);
    setCashBack(cartCashBack);
    setTotal(carTotal);
  }, [items, freight]);

  useEffect(() => {
    const loadStoragedData = async () => {
      const response = await AsyncStorage.getItem('@GB:cart');
      //await AsyncStorage.setItem('@GB:cart', '');
      if (response) {
        console.log(response);
        setItems(JSON.parse(response));
      }
    };
    loadStoragedData();
  }, []);

  return (
    <CartContext.Provider
      value={{
        checkAddCart,
        addToCart,
        updateCart,
        clearCart,
        removeItem,
        items,
        subTotal,
        freight,
        payment,
        updateFreight,
        cleanFreight,
        cashBack,
        total,
        address,
        updateAddress,
        setItemPayment,
        balanceCashBack,
        setBalanceCashBack,
      }}>
      {children}
    </CartContext.Provider>
  );
};

const useCart = () => {
  const context = useContext(CartContext);
  if (!context) {
    throw new Error('useCart must be used within a CartProvider');
  }
  return context;
};

export {CartProvider, useCart, ItemCart};
