import React, {
  createContext,
  ReactNode,
  useCallback,
  useContext,
  useEffect,
  useState,
} from 'react';

import AsyncStorage from '@react-native-async-storage/async-storage';

interface FilterProviderProps {
  children: ReactNode;
}

interface DataValue {
  id: string;
  title: string;
}

interface FilterData {
  [key: string]: DataValue;
}

interface Filter {
  filters: FilterData;
  search?: string;
}

interface IFilterContextData {
  productFilter: Filter;
  bussinessFilter: Filter;
  setItemsFilter(
    type: 'Product' | 'Bussiness',
    newItems: FilterData,
    search?: string,
  ): void;
  clearFilter(type: 'Product' | 'Bussiness'): void;
}

const FilterContext = createContext({} as IFilterContextData);

const FilterProvider: React.FC<FilterProviderProps> = ({children}) => {
  const [productFilter, setProductFilter] = useState<Filter>({} as Filter);
  const [bussinessFilter, setBussinessFilter] = useState<Filter>({} as Filter);

  const setItemsFilter = useCallback(
    async (type, newItems: FilterData, search) => {
      if (type === 'Product') {
        setProductFilter({filters: newItems, search});
      } else {
        setBussinessFilter({filters: newItems, search});
      }
      await AsyncStorage.setItem(
        '@GB:filter',
        JSON.stringify({productFilter, bussinessFilter}),
      );
    },
    [productFilter, bussinessFilter],
  );

  const clearFilter = useCallback(
    async type => {
      if (type === 'Product') {
        setProductFilter({} as Filter);
      } else {
        setBussinessFilter({} as Filter);
      }
      await AsyncStorage.setItem(
        '@GB:filter',
        JSON.stringify({productFilter, bussinessFilter}),
      );
    },
    [productFilter, bussinessFilter],
  );

  useEffect(() => {
    const loadStoragedData = async () => {
      const response = await AsyncStorage.getItem('@GB:filter');
      if (response) {
        const data = JSON.parse(response) as {
          productFilter: Filter;
          bussinessFilter: Filter;
        };
        setProductFilter(data.productFilter);
        setBussinessFilter(data.bussinessFilter);
      }
    };
    loadStoragedData();
  }, []);

  return (
    <FilterContext.Provider
      value={{
        setItemsFilter,
        clearFilter,
        productFilter,
        bussinessFilter,
      }}>
      {children}
    </FilterContext.Provider>
  );
};

const useFilter = () => {
  const context = useContext(FilterContext);
  if (!context) {
    throw new Error('useFilter must be used within a FilterProvider');
  }
  return context;
};

export {FilterProvider, useFilter, FilterData, DataValue};
