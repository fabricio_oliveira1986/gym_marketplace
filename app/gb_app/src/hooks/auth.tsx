import React, {
  createContext,
  ReactNode,
  useCallback,
  useEffect,
  useContext,
  useState,
} from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {useCart} from '~/hooks/cart';
import {login, createUser} from '~/services/user';
import {api} from '../services/api';
import {User, UserLogin, UserData} from '~/models/User';

interface AuthProviderProps {
  children: ReactNode;
}

interface IAuthContextData {
  user: User;
  loading: boolean;
  signIn(credentials: UserLogin): Promise<void>;
  signUp(data: UserData): Promise<void>;
  signOut(): Promise<void>;
}

const AuthContext = createContext({} as IAuthContextData);
const AuthProvider: React.FC<AuthProviderProps> = ({children}) => {
  const {clearCart, cleanFreight} = useCart();
  const [user, setUser] = useState<User>({} as User);
  const [loading, setLoading] = useState(true);

  const signIn = useCallback(async (credentials: UserLogin) => {
    try {
      const response = await login(credentials);
      await AsyncStorage.multiSet([
        ['@GB:token', response.token],
        ['@GB:user', JSON.stringify(response.user)],
      ]);
      api.defaults.headers.authorization = `Bearer ${response.token}`;
      setUser(response.user);
    } catch (e) {
      throw e;
    }
  }, []);

  const signOut = useCallback(async () => {
    cleanFreight;
    clearCart;
    await AsyncStorage.clear();
    setUser({} as User);
  }, []);

  const signUp = useCallback(async (data: UserData) => {
    try {
      const response = await createUser(data);
      await AsyncStorage.multiSet([
        ['@GB:token', response.token],
        ['@GB:user', JSON.stringify(response.user)],
      ]);
      api.defaults.headers.authorization = `Bearer ${response.token}`;
      setUser(response.user);
    } catch (e) {
      throw e;
    }
  }, []);

  useEffect(() => {
    const loadStoragedData = async () => {
      const [storageToken, storageUser] = await AsyncStorage.multiGet([
        '@GB:token',
        '@GB:user',
      ]);
      if (storageToken[1] && storageUser[1]) {
        api.defaults.headers.Authorization = `Bearer ${storageToken[1]}`;
        setUser(JSON.parse(storageUser[1]) as User);
      }
      setLoading(false);
    };
    loadStoragedData();
  }, [signOut]);

  return (
    <AuthContext.Provider
      value={{
        user,
        loading,
        signIn,
        signUp,
        signOut,
      }}>
      {children}
    </AuthContext.Provider>
  );
};

const useAuth = () => {
  const context = useContext(AuthContext);
  if (!context) {
    throw new Error('useAuth must be used within a AuthProvider');
  }
  return context;
};

export {AuthProvider, useAuth};
