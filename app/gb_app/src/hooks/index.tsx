import React from 'react';
import {StatusBar} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';

import {AuthProvider} from './auth';

const AppProvider: React.FC = ({children}) => {
  const linking = {
    prefixes: ['https://gymbrother.com.br/', 'gymbrother://'],
    /* config: {
      screens: {
        Chat: 'Home/Bussiness/BussinessSingle/:id',
      },
    }, */
  };

  return (
    <NavigationContainer linking={linking}>
      <StatusBar
        barStyle="light-content"
        backgroundColor="transparent"
        translucent
      />
      <AuthProvider>{children}</AuthProvider>
    </NavigationContainer>
  );
};

export default AppProvider;
