import React from 'react';

import {useAuth} from '~/hooks/auth';

import Splash from '~/screens/Splash';
import {FilterProvider} from '~/hooks/filter';
import {CartProvider} from '~/hooks/cart';
import AuthRoutes from './Auth';

const Routes: React.FC = () => {
  const {loading} = useAuth();

  if (loading) {
    return <Splash />;
  }

  return (
    <CartProvider>
      <FilterProvider>
        <AuthRoutes />
      </FilterProvider>
    </CartProvider>
  );
};

export default Routes;
