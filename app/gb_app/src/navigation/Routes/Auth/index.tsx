import React, {useEffect} from 'react';
import {
  createStackNavigator,
  StackNavigationOptions,
} from '@react-navigation/stack';

import SignIn from '~/screens/SignIn';
import SignUp from '~/screens/SignUp';
import ResetPassword from '~/screens/ResetPassword';
import Contract from '~/screens/Contract';
import AppRoutes from '../App';
import {useAuth} from '~/hooks/auth';
import {useFilter} from '~/hooks/filter';

const Stack = createStackNavigator();

const options: StackNavigationOptions = {
  headerShown: false,
};

const AuthRoutes = () => {
  const {user} = useAuth();
  const {clearFilter} = useFilter();
  useEffect(() => {
    clearFilter('');
    clearFilter('Product');
  }, []);
  return (
    <Stack.Navigator initialRouteName="Home">
      {!user.id && (
        <>
          <Stack.Screen component={SignIn} name="SignIn" options={options} />
          <Stack.Screen component={SignUp} name="SignUp" options={options} />
          <Stack.Screen
            component={ResetPassword}
            name="ResetPassword"
            options={options}
          />
        </>
      )}
      <Stack.Screen component={Contract} name="Contract" options={options} />
      <Stack.Screen component={AppRoutes} name="Home" options={options} />
    </Stack.Navigator>
  );
};

export default AuthRoutes;
