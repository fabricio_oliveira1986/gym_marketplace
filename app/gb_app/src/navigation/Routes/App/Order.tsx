import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import Order from '~/screens/Order';
import OrderSuccess from '~/screens/OrderSuccess/index';
import OrderConfirm from '~/screens/OrderConfirm/index';
import Payment from '~/screens/Payment';
import ListAddressPayment from '~/screens/ListAddressPayment';
import MyPurchaseDetail from '~/screens/MyPurchaseDetail';

const Stack = createStackNavigator();
const options = {
  headerShown: false,
};

const OrderRoutes: React.FC = () => {
  return (
    <Stack.Navigator initialRouteName="Order">
      <Stack.Screen component={Order} name="Order" options={options} />
      <Stack.Screen component={Payment} name="Payment" options={options} />
      <Stack.Screen
        component={OrderSuccess}
        name="OrderSuccess"
        options={options}
      />
      <Stack.Screen
        component={OrderConfirm}
        name="OrderConfirm"
        options={options}
      />
      <Stack.Screen
        component={ListAddressPayment}
        name="ListAddressPayment"
        options={options}
      />
      <Stack.Screen
        component={MyPurchaseDetail}
        name="MyPurchaseDetail"
        options={options}
      />
    </Stack.Navigator>
  );
};

export default OrderRoutes;
