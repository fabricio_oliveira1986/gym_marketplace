import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import Extracts from '~/screens/Extracts';
import ToWithdraw from '~/screens/ToWithdraw';
import MyPurchaseDetail from '~/screens/MyPurchaseDetail';
const Stack = createStackNavigator();
const options = {
  headerShown: false,
};

const ExtractRoutes: React.FC = () => {
  return (
    <Stack.Navigator initialRouteName="Extracts">
      <Stack.Screen name="Extracts" component={Extracts} options={options} />
      <Stack.Screen
        component={MyPurchaseDetail}
        name="MyPurchaseDetail"
        options={options}
      />
      <Stack.Screen
        component={ToWithdraw}
        name="ToWithdraw"
        options={options}
      />
    </Stack.Navigator>
  );
};

export default ExtractRoutes;
