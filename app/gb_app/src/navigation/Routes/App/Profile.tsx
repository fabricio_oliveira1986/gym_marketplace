import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';

import Profile from '~/screens/Profile';
import InfoProfile from '~/screens/InfoProfile';
import ListAddressProfile from '~/screens/ListAddressProfile';
import FormPayment from '~/screens/FormPayment';
import MyPurchases from '~/screens/MyPurchases';
import MyPurchaseDetail from '~/screens/MyPurchaseDetail';
import ToWithdraw from '~/screens/ToWithdraw';
const Stack = createStackNavigator();
const options = {
  headerShown: false,
};

const ProfileRoutes: React.FC = () => {
  return (
    <Stack.Navigator initialRouteName="Profile">
      <Stack.Screen component={Profile} name="Profile" options={options} />
      <Stack.Screen
        component={InfoProfile}
        name="InfoProfile"
        options={options}
      />
      <Stack.Screen
        component={ListAddressProfile}
        name="ListAddressProfile"
        options={options}
      />
      <Stack.Screen
        component={FormPayment}
        name="FormPayment"
        options={options}
      />
      <Stack.Screen
        component={MyPurchases}
        name="MyPurchases"
        options={options}
      />
      <Stack.Screen
        component={MyPurchaseDetail}
        name="MyPurchaseDetail"
        options={options}
      />
      <Stack.Screen
        component={ToWithdraw}
        name="ToWithdraw"
        options={options}
      />
    </Stack.Navigator>
  );
};

export default ProfileRoutes;
