import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';

import CustomTabBar from '~/components/CustomTabBar';

import HomeRoutes from './Home';
import BussinessRoutes from './Bussiness';
import ExtractsRoutes from './Extract';
import OrderRoutes from './Order';
import ProfileRoutes from './Profile';
import {useAuth} from '~/hooks/auth';

const Tab = createBottomTabNavigator();

const AppRoutes: React.FC = () => {
  const {user} = useAuth();
  return (
    <Tab.Navigator tabBar={props => <CustomTabBar {...props} />}>
      <Tab.Screen
        name="Home"
        component={HomeRoutes}
        options={{title: 'Home'}}
      />
      <Tab.Screen
        name="Bussiness"
        component={BussinessRoutes}
        options={{title: 'Empresa'}}
      />

      {user?.id && (
        <Tab.Screen
          name="Extracts"
          component={ExtractsRoutes}
          options={{title: 'Extrato'}}
        />
      )}

      <Tab.Screen
        name="Order"
        component={OrderRoutes}
        options={{title: 'Carrinho'}}
      />
      {user?.id && (
        <Tab.Screen
          name="Profile"
          component={ProfileRoutes}
          options={{title: 'Perfil'}}
        />
      )}
    </Tab.Navigator>
  );
};

export default AppRoutes;
