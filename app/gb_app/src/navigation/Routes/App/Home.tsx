import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';

import ProductSingle from '~/screens/ProductSingle';
import Home from '~/screens/Home';
import Filter from '~/screens/Filter';
import BussinessSingle from '~/screens/BussinessSingle';

const Stack = createStackNavigator();
const options = {
  headerShown: false,
};

const HomeRoutes: React.FC = () => {
  return (
    <Stack.Navigator initialRouteName="Home">
      <Stack.Screen component={Home} name="Home" options={options} />
      <Stack.Screen
        component={ProductSingle}
        name="ProductSingle"
        options={options}
      />
      <Stack.Screen
        component={BussinessSingle}
        name="BussinessSingle"
        options={options}
      />
      <Stack.Screen component={Filter} name="Filter" options={options} />
    </Stack.Navigator>
  );
};

export default HomeRoutes;
