import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';

import BussinessSingle from '~/screens/BussinessSingle';
import Bussiness from '~/screens/Bussiness';
import Filter from '~/screens/Filter';
import ProductSingle from '~/screens/ProductSingle';

const Stack = createStackNavigator();
const options = {
  headerShown: false,
};

const BussinessRoutes: React.FC = () => {
  return (
    <Stack.Navigator initialRouteName="Bussiness">
      <Stack.Screen component={Bussiness} name="Bussiness" options={options} />
      <Stack.Screen
        component={BussinessSingle}
        name="BussinessSingle"
        options={options}
      />
      <Stack.Screen
        component={ProductSingle}
        name="ProductSingle"
        options={options}
      />
      <Stack.Screen component={Filter} name="Filter" options={options} />
    </Stack.Navigator>
  );
};

export default BussinessRoutes;
