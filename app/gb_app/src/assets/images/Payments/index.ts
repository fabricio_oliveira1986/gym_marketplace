import BandeiraCartoes from './bandeira-dos-cartoes-de-credito.png';
import CreditCardVisa from './CreditCardVisa.png';
import CreditCardMaster from './CreditCardMasterCard.png';

export default {
  BandeiraCartoes,
  CreditCardVisa,
  CreditCardMaster,
};
