import Home from './Home.svg';
import Location from './Location.svg';
import LogoWhite from './LogoWhite.svg';
import Menu from './Menu.svg';
import MoneyFlow from './MoneyFlow.svg';
import Search from './Search.svg';
import ShoppingBag from './ShoppingBag.svg';
import UserLine from './UserLine.svg';
import User from './User.svg';
import Star from './Star.svg';
import Cart from './Cart.svg';
import CartInvert from './CartInvert.svg';
import BarsCode from './BarsCode.svg';
import CreditCard from './CreditCard.svg';
import Pix from './Pix.svg';
import CheckCircle from './CheckCircle.svg';
import Check from './Check.svg';
import Wallet from './Wallet.svg';
import Edit from './Edit.svg';
import StarFull from './StarFull.svg';
import StarEmpty from './StarEmpty.svg';
import Shipping from './Shipping.svg';
import ShoppingCart from './ShoppingCart.svg';
import Share from './Share.svg';

export default {
  Home,
  Location,
  LogoWhite,
  Menu,
  MoneyFlow,
  Search,
  ShoppingBag,
  UserLine,
  User,
  Star,
  Cart,
  CartInvert,
  BarsCode,
  CreditCard,
  Pix,
  CheckCircle,
  Check,
  Wallet,
  Edit,
  StarFull,
  StarEmpty,
  Shipping,
  ShoppingCart,
  Share,
};
