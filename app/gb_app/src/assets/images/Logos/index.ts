import LogoDefault from './LogoDefault.png';
import LogoGyn from './LogoGyn.png';
import LogoHeader from './LogoHeader.png';
import LogoWhite from './LogoWhite.png';
import LogoSplash from './LogoSplash.png';

export default {
  LogoDefault,
  LogoGyn,
  LogoHeader,
  LogoWhite,
  LogoSplash,
};
