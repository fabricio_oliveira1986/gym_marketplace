import OfertsBests from './OfertsBests.png';
import Planos from './Planos.png';
import Cashback from './Cashback.png';
import Products from './Products.png';
import Promotions from './Promotions.png';

export default {
  OfertsBests,
  Planos,
  Cashback,
  Products,
  Promotions,
};
