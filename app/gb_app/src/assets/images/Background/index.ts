import Background from './Background.png';
import BackgroundSplash from './BackgroundSplash.png';

export default {
  Background,
  BackgroundSplash,
};
