export interface Ofert {
  id: string;
  name: string;
  title: string;
  subTitle?: string;
  background?: string;
  color?: string; 
  image?: any;
}
