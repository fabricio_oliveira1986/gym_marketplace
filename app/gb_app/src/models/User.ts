export interface User {
  id: string;
  name: string;
  image: string;
}

export interface UserLogin {
  email: string;
  password: string;
}

export interface UserData extends UserLogin {
  name: string;
  genre: string;
  celphone: string;
  cpf: string;
  birthDate: string;
}

export interface UserPasswordResetData extends UserLogin {
  name: string;
  genre: string;
}

export interface UserPasswordResetResponse {
  send: Boolean;
}
