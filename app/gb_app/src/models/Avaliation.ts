import { User } from "./User";

export interface Avaliation {
  id: string;
  commentary: string;
  stars: number;
  date: string;
  dateReply: string;
  reply: string;
  user: User;
}