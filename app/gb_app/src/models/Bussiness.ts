import {Ofert} from './Ofert';
import {Product, INewObjVideo} from './Product';

export interface Bussiness {
  id: string;
  name: string;
  image: string;
  id_address?: string;
  city?: string;
  neighborhood?: string;
  distance?: number;
  rating?: number;
  images?: string[];
  categories?: Ofert[];
  products?: Product[];
  newObjVideo?: INewObjVideo[];
}
