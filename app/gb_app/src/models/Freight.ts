export interface Freight {
  id: string;
  title: string;
  description: string;
  value: number;
}
