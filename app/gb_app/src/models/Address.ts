export interface Address {
  id: string;
  postCode: string;
  street: string;
  number: string;
  neighborhood: string;
  complement: string;
  city: string;
  state: string;
}

export interface Estado {
  estado_codigo: string;
  estado_codigoPais: string;
  estado_gov: string;
  estado_nome: string;
  estado_sigla: string;
}

export interface City {
  cidade_codigo: string;
  cidade_codigoEstado: string;
  cidade_ddd: string;
  cidade_gov: string;
  cidade_nome: string;
}
