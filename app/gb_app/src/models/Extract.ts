export interface Extract {
  id: string;
  description: string;
  date: string;
  value: string;
  status: string;
}
