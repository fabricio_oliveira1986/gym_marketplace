import * as yup from 'yup';

import {isValidCPF} from '~/global/utils/functions';

export interface UserProfileResponse {
  id: string;
  nome: string;
  dt_nasc: string;
  sexo: string;
  cpf: string;
  email: string;
  celular: string;
  id_foto: string;
}

export interface UserDataAddress {
  id_endereco?: string;
  txtCep: string;
  txtEndereco: string;
  txtBairro: string;
  txtComplemento: string;
  txtNumero: string;
  txtCidade: string;
  txtEstado: string;
}

export interface UserDataPassword {
  txtSenhaAtual: string;
  txtNovaSenha: string;
  txtConfirmSenha: string;
}

export const schemaUserDataPassword = yup.object().shape({
  txtSenhaAtual: yup.string().min(5).required(),
  txtNovaSenha: yup.string().min(5).required(),
  txtConfirmSenha: yup
    .mixed()
    .test('match', 'Passwords do not match', function () {
      return this.parent.txtNovaSenha === this.parent.txtConfirmSenha;
    }),
});

export const schemaUserDataAddress = yup.object().shape({
  txtCep: yup.string().min(8).required(),
  txtEndereco: yup.string().min(2).required(),
  txtBairro: yup.string().min(2).required(),
  // txtComplemento: yup.string(),
  txtNumero: yup.string().min(1).required(),
});

export const schemUser = yup.object().shape({
  txtNome: yup.string().min(10).required(),
  txtSexo: yup.string().required(),
  txtEmail: yup.string().email().required(),
  txtCelular: yup.string().min(10).required(),
  txtCPF: yup
    .string()
    .test(value => isValidCPF(value))
    .required(),
  txtDtNasc: yup.string(),
});
