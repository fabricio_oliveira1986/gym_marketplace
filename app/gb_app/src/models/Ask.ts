import { User } from './User';

export interface Ask {
  id: string;
  ask: string;
  date: string;
  dateReply: string;
  reply: string;
  user: User;
}