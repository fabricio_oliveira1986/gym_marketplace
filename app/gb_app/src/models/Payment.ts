export interface Billet {
  link: string;
}

export interface Pix {
  link: string;
}

export interface CreditCard {
  name: string;
  numberCard: string;
  validate: string;
  codeSecurity: string;
  hash?: string;
  saveCard?: boolean;
  txtIdCartao?: string;
}

export interface Payment {
  type: 'credit_card' | 'pix' | 'billet' | 'credit_card_saved';
  method: CreditCard;
  splots: number;
}

export interface ICreditCardSaved {
  id: string;
  cardNumber: string;
  expiration: string;
  banner: string;
  image: string;
}

export interface IPaymentData {
  boleto: number;
  credit_card: number;
  pix: number;
}
