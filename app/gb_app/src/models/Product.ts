import {Bussiness} from '~/models/Bussiness';

export interface ProductVideo {
  image: string;
  link: string;
}

export interface INewObjVideo {
  isVoid: boolean;
  img: string;
  linkVideo: string;
}

export interface Product {
  id: string;
  title: string;
  type: string;
  subTitle: string;
  description: string;
  image: string;
  bussiness?: Bussiness;
  value: string;
  plots?: {
    qtd: number;
    value: number;
  };
  stars: number;
  starsTotal: number;
  cashBack?: string;
  images?: string[];
  videos?: ProductVideo[];
  newObjVideo: INewObjVideo[] | {};
}
