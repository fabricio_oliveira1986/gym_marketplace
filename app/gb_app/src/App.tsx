import 'react-native-gesture-handler';
import React from 'react';
import {ThemeProvider} from 'styled-components';
import AppProvider from '~/hooks';
import {theme} from '~/global/styles/theme';
import Routes from '~/navigation/Routes';
import moment from 'moment';
import 'moment/locale/pt-br';

moment.locale('pt-br');
const App: React.FC = () => {
  return (
    <ThemeProvider theme={theme}>
      <AppProvider>
        <Routes />
      </AppProvider>
    </ThemeProvider>
  );
};

export default App;
