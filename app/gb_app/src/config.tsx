interface IConfig {
  name: string;
  url: string;
  selected: boolean;
}

const Config: IConfig[] = [
  {
    name: 'produção',
    url: 'https://gymbrother.com.br/admin/ajax/FormApp.php',
    selected: true,
  },
  /*{
    name: 'homolog',
    url: 'https://gymbrother.online/admin/ajax/FormApp.php',
    selected: true,
    juno: {
      publicKey:
        'D7296AD822D250B3C6C45D31D3477D2A2991ABF0145D1C4D65A29068BC0A5C93',
      env: 'sandbox',
    },
  },*/
];

export default Config;

export function env() {
  return Config.find((item: IConfig) => item.selected);
}
