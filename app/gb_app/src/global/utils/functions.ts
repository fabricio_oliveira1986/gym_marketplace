import {Alert} from 'react-native';
import {theme} from '../styles/theme';
import {NewProduct} from '~/models/Bussiness';
import moment from 'moment';

export function numberReal(value: string | number) {
  return numberFormat(Number(value), 1);
}

export function dateBr(value: string) {
  return convertDataBr(value);
}

export function dateTimeBr(value: string) {
  return convertDataBr(value);
}

export function numberDecimal(value: string | number, precision = 1) {
  return Number(value).toFixed(precision).replace('.', ',');
}

export function numberFormat(num: number, ident = 0) {
  let x = 0;
  let numString = '';
  if (num < 0) {
    num = Math.abs(num);
    x = 1;
  }
  if (isNaN(num)) {
    numString = '0';
  }
  let cents = Math.floor((num * 100 + 0.5) % 100);
  let centsString = cents.toString();
  numString = Math.floor((num * 100 + 0.5) / 100).toString();
  if (cents < 10) {
    centsString = '0' + cents;
  }
  for (let i = 0; i < Math.floor((numString.length - (1 + i)) / 3); i++) {
    numString =
      numString.substring(0, numString.length - (4 * i + 3)) +
      '.' +
      numString.substring(numString.length - (4 * i + 3));
  }
  let ret = numString + ',' + centsString;
  if (x === 1) {
    ret = ' - ' + ret;
  }
  return ident === 0 ? ret : 'R$' + ' ' + ret;
}

export function textToNumber(value: string) {
  const valueTransform =
    value !== undefined
      ? value
          .replace(/\ /g, '')
          .replace('R$', '')
          .replace('–', '-')
          .replace(/\./g, '')
          .replace(/\,/g, '.')
      : '0';
  return isNaN(Number(valueTransform)) === false
    ? parseFloat(valueTransform)
    : parseFloat('0');
}

export function convertDataBr(sData: string) {
  if (sData !== '' && sData !== null) {
    const [data, tempo] = sData.split(/\s/);
    if (
      /^([0-2][0-9]|(3)[0-1])(\/)(((0)[0-9])|((1)[0-2]))(\/)\d{4}$/.test(data)
    ) {
      return data;
    } else if (/^\d{4}-\d{2}-\d{2}$/.test(data)) {
      const [ano, mes, dia] = data.split('-');
      return dia + '/' + mes + '/' + ano + (tempo ? ' ' + tempo : '');
    }
  }
}

export const clearSpecialSharacters = text => {
  if (!text) return '';
  return `${text.replace(/[+-./()\s]/g, '')}`;
};

export const isValidCPF = cpf => {
  if (!cpf) return false;
  cpf = cpf.replace(/[\s.-]*/gim, '');
  if (cpf.length !== 11 || !Array.from(cpf).filter(e => e !== cpf[0]).length) {
    return false;
  }

  let soma = 0;
  let resto;
  for (var i = 1; i <= 9; i++) {
    soma += parseInt(cpf.substring(i - 1, i)) * (11 - i);
  }
  resto = (soma * 10) % 11;
  if (resto == 10 || resto == 11) resto = 0;
  if (resto != parseInt(cpf.substring(9, 10))) return false;
  soma = 0;
  for (var i = 1; i <= 10; i++) {
    soma += parseInt(cpf.substring(i - 1, i)) * (12 - i);
  }
  resto = (soma * 10) % 11;
  if (resto == 10 || resto == 11) resto = 0;
  if (resto != parseInt(cpf.substring(10, 11))) return false;
  return true;
};

export const middlewareError = error => {
  if (error.hasOwnProperty('response')) {
    Alert.alert('Ops! Algo deu Errado', error.response.data.erro, [
      {
        text: 'Ok',
        onPress: () => {},
      },
    ]);
  } else {
    Alert.alert('Ops! Algo deu Errado', '', [
      {
        text: 'Ok',
        onPress: () => {},
      },
    ]);
  }
};

export const getTypeStatusPayment = data => {
  const obj = {
    P: {
      color: theme.colors.success,
      description: 'Pago',
    },
    E: {
      color: theme.colors.badge,
      description: 'Estornado',
    },
    A: {
      color: theme.colors.warning,
      description: 'Aguardando',
    },
    N: {
      color: theme.colors.badge,
      description: 'Negado',
    },
    C: {
      color: theme.colors.badge,
      description: 'Cancelado',
    },
  };
  return obj[data];
};

export const getTypePayment = (data: string) => {
  const obj = {
    C: {
      color: theme.colors.success,
      description: 'Cartão de Crédito',
    },
    B: {
      color: theme.colors.badge,
      description: 'Boleto Bancário',
    },
    P: {
      color: theme.colors.warning,
      description: 'Pagamento com PIX',
    },
    T: {
      color: theme.colors.badge,
      description: 'Cash Back',
    },
    S: {
      color: theme.colors.badge,
      description: 'Saque em Loja',
    },
  };
  return obj[data];
};

export const getTypeStatusDelivery = data => {
  const obj = {
    E: {
      color: theme.colors.success,
      description: 'Entregue',
    },
    P: {
      color: theme.colors.warning,
      description: 'Pendente',
    },
  };
  return obj[data];
};

export const parseAvaliationStars = data => {
  const result = data.filter(item => item === 1).length;
  return result;
};

export const diffTwoDates = (dateOne, dateTwo) => {
  let a = moment([
    moment(dateOne).year(),
    moment(dateOne).month(),
    moment(dateOne).date(),
  ]);

  let b = moment([
    moment(dateTwo).year(),
    moment(dateTwo).month(),
    moment(dateTwo).date(),
  ]);

  return a.diff(b, 'days');
};

export const adaptObjVideo = (data: NewProduct) => {
  let result: any = {};
  const dataImg: NewProduct[] | undefined = data.images?.map(item => ({
    isVoid: false,
    img: item,
    linkVideo: '',
  }));
  const dataVideo: NewProduct[] | undefined = data.videos?.map(item => ({
    isVoid: true,
    img: item.image,
    linkVideo: item.link,
  }));
  result = dataImg?.concat(dataVideo);
  data.newObjVideo = result;
  return data;
};
