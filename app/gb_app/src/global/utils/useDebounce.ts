import {useRef} from 'react';

export default function useDebounce(
  fn: (...args: any[]) => void,
  delay: number,
) {
  const timeoutRef = useRef(null);
  function debounceFn(...args: any[]) {
    clearInterval(timeoutRef.current);
    timeoutRef.current = setTimeout(() => {
      fn(...args);
    }, delay);
  }

  return debounceFn;
}
