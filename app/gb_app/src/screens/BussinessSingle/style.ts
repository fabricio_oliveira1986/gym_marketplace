import styled from 'styled-components/native';
import { RFValue } from 'react-native-responsive-fontsize';
import { Dimensions } from 'react-native';

const { width } = Dimensions.get('window');

export const Container = styled.View`
  flex: 1;
`;

export const ContainerSingle = styled.ScrollView`
`;

export const Item = styled.Image.attrs({
  resizeMode: 'cover',
})`
  width: ${width}px;
  height: ${RFValue(250)}px;
`;

export const BoxSingle = styled.View`
  background-color: ${({ theme }) => theme.colors.shape};
  border-top-left-radius: ${RFValue(30)}px;
  border-top-right-radius: ${RFValue(30)}px;
  margin-top: ${props => (props.mt ? `${RFValue(props.mt)}px` : '0')};
  width: 100%;
  flex: 1;
  padding: ${RFValue(30)}px ${RFValue(15)}px;
`;

export const ImageSingle = styled.Image.attrs({
  resizeMode: 'contain',
})`
  align-self: center;
  margin-bottom: ${RFValue(20)}px;
  height: ${RFValue(60)}px;
  width: ${RFValue(60)}px;
  margin-right: ${RFValue(10)}px;
`;

export const ContainerInfoTop = styled.View`
  flex-direction: row;
  justify-content: space-between;
  margin-bottom: ${RFValue(30)}px;
`;

export const InfoTop = styled.View``;

export const TextInfoTop = styled.Text`
  font-size: ${RFValue(14)}px;
  color: ${({ theme }) => theme.colors.text};
  font-family: ${({ theme }) => theme.fonts.regular};
  margin-bottom: ${RFValue(3)}px;
`;

export const TextInfoTopBold = styled.Text`
  font-size: ${RFValue(14)}px;
  color: ${({ theme }) => theme.colors.text};
  font-family: ${({ theme }) => theme.fonts.medium};
  margin-bottom: ${RFValue(3)}px;
`;

export const BoxRating = styled.View`
  flex-direction: row;
`;

export const TextRating = styled.Text`
  color: ${({ theme }) => theme.colors.success};
  font-family: ${({ theme }) => theme.fonts.medium};
  margin-left: ${RFValue(5)}px;
`;

export const ListCategoriesBussiness = styled.ScrollView.attrs({
  horizontal: true,
  showsHorizontalScrollIndicator: false,
})`
  flex: 1;
  width: 100%;
  margin-bottom: ${RFValue(10)}px;
`;

interface ItemCategoriesProps {
  background: string;
  borderColor?: string;
}

export const ItemCategoriesBussiness = styled.TouchableOpacity.attrs({
  activeOpacity: 0.7,
}) <ItemCategoriesProps>`
  position: relative;
  width: ${width / 2 - 40}px;
  height: ${RFValue(100)}px;
  background: ${props => props.background};
  margin-right: ${RFValue(10)}px;
  border-radius: ${RFValue(5)}px;
  justify-content: center;
  align-items: center;
  border: 2px solid ${props => props.borderColor};
  padding: ${RFValue(10)}px;
`;

export const ItemCategoriesImage = styled.Image.attrs({
  resizeMode: 'contain',
})`
  position: absolute;
  height: 80%;
  top: 8%;
  right: 0;
  opacity: 0.3;
`;

export const ContainerItemCategories = styled.View`
  flex: 1;
  align-items: center;
  justify-content: center;
`;

interface ItemCategoriesText {
  color: string;
}

export const ItemCategoriesTitle = styled.Text<ItemCategoriesText>`
  font-size: ${RFValue(24)}px;
  line-height: ${RFValue(28)}px;
  font-family: ${({ theme }) => theme.fonts.text_regular};
  flex-wrap: wrap;
  color: ${props => props.color};
  text-transform: uppercase;
`;

export const ItemCategoriesSubTitle = styled.Text<ItemCategoriesText>`
  font-size: ${RFValue(30)}px;
  line-height: ${RFValue(30)}px;
  font-family: ${({ theme }) => theme.fonts.text_regular};
  margin-bottom: -${RFValue(10)}px;
  color: ${props => props.color};
  text-transform: uppercase;
`;

export const ContentLoad = styled.View`
  flex: 1;
  width: 100%;
  justify-content: center;
  align-items: center;
`;

export const TitleLoad = styled.Text`
  font-size: ${RFValue(16)}px;
  font-family: ${({ theme }) => theme.fonts.medium};
  margin-bottom: ${RFValue(20)}px;
`;
