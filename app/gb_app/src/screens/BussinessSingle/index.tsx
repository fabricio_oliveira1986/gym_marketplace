import React, {useState, useCallback, useEffect} from 'react';
import {Alert} from 'react-native';
import {useRoute} from '@react-navigation/native';
import {useTheme} from 'styled-components/native';
import ListProducts from '~/components/ListProducts';
import Swiper from '~/components/Swiper';
import Header from '~/components/Header';
import Load from '~/components/Load';
import ImagesOfert from '~/assets/images/Oferts';
import {Bussiness} from '~/models/Bussiness';
import * as bussinessService from '~/services/bussiness';
import * as productService from '~/services/product';
import {
  BoxPercent,
  ModalVideo,
  ItemSliderVideo,
  ItemSliderImg,
  ModalImage,
} from '~/components/index';
import {adaptObjVideo} from '~/global/utils/functions';
import {
  Container,
  ContainerSingle,
  Item,
  BoxSingle,
  ImageSingle,
  InfoTop,
  TextInfoTop,
  TextInfoTopBold,
  BoxRating,
  TextRating,
  ListCategoriesBussiness,
  ItemCategoriesBussiness,
  ItemCategoriesImage,
  ContainerItemCategories,
  ItemCategoriesTitle,
  ContentLoad,
  TitleLoad,
} from './style';
import {Product} from '~/models/Product';

const BussinessSingle: React.FC = () => {
  const {params} = useRoute<any>();
  const [bussiness, setBussiness] = useState<Bussiness>({} as Bussiness);
  const [loading, setLoading] = useState(true);
  const [isVisibleModalVideo, setIsVisibleModalVideo] = useState(false);
  const [linkVideo, setLinkVideo] = useState('');
  const [isVisibleModalImg, setIsVisibleModalImg] = useState<boolean>(false);
  const [linkImg, setLinkImg] = useState('');
  const theme = useTheme();

  useEffect(() => {
    if (params?.data?.id || params.id) {
      const id = params?.data?.id || params.id;
      bussinessService
        .getBussiness(id)
        .then(item => {
          const data = adaptObjVideo(item);
          setBussiness(data);
          setLoading(false);
        })
        .catch(error => {
          console.log(error);
          Alert.alert('Erro', 'Ocorreu um erro ao buscar a empresa');
        });
    }
  }, [setBussiness, params]);

  const openVideo = (link: string) => {
    setIsVisibleModalVideo(true);
    setLinkVideo(link);
  };

  const openImage = (link: string) => {
    setIsVisibleModalImg(true);
    setLinkImg(link);
  };

  return (
    <Container>
      <Header />
      <ContainerSingle>
        {loading ? (
          <Load />
        ) : (
          <>
            {bussiness?.newObjVideo && (
              <Swiper
                data={bussiness.newObjVideo}
                renderItem={({item}) =>
                  item.isVoid ? (
                    <ItemSliderVideo
                      openVideo={() => openVideo(item.linkVideo)}
                      theme={theme}
                      img={item.img}
                    />
                  ) : (
                    <ItemSliderImg
                      img={item.img}
                      openImg={() => {
                        openImage(item.img);
                      }}
                    />
                  )
                }
              />
            )}
            <Box bussiness={bussiness} theme={theme} />
          </>
        )}
      </ContainerSingle>
      <ModalVideo
        visible={isVisibleModalVideo}
        setVisivelModalVod={setIsVisibleModalVideo}
        uri={linkVideo}
      />
      <ModalImage
        visible={isVisibleModalImg}
        uri={linkImg}
        setVisivelModalImg={setIsVisibleModalImg}
      />
    </Container>
  );
};

interface BoxProps {
  bussiness: Bussiness;
  theme: any;
}

const Box: React.FC<BoxProps> = ({bussiness, theme}) => {
  const [products, setProducts] = useState<Product[]>(bussiness.products || []);
  const [category, setCategory] = useState('');
  const [loading, setLoading] = useState(false);

  const updateCategory = useCallback(
    (item: string) => {
      category === item ? setCategory('') : setCategory(item);
    },
    [category],
  );

  useEffect(() => {
    setLoading(true);
    productService
      .getProducts({id_empresa: bussiness.id, categoria_empresa: category})
      .then(item => {
        setProducts(item);
        setLoading(false);
      })
      .catch(error => {
        console.log(error);
        setLoading(false);
      });
  }, [bussiness.id, category]);

  return (
    <BoxSingle mt={bussiness.images.length > 0 ? '-40' : '0'}>
      <BoxPercent position="relative" width="100%" direction>
        <ImageSingle source={{uri: bussiness.image}} />
        <InfoTop>
          <TextInfoTop>{bussiness.name}</TextInfoTop>
          <TextInfoTopBold>
            {bussiness.neighborhood} | {bussiness.distance} Km
          </TextInfoTopBold>
        </InfoTop>
        <BoxRating>
          <TextRating>{bussiness.rating}</TextRating>
        </BoxRating>
      </BoxPercent>
      <ListCategoriesBussiness>
        {bussiness?.categories &&
          bussiness.categories.map(item => (
            <ItemCategoriesBussiness
              key={item.id}
              background={item.background || theme.colors.success_light}
              borderColor={item.color || theme.colors.title}
              onPress={() => updateCategory(item.id)}>
              <ItemCategoriesImage source={ImagesOfert.Promotions} />
              <ContainerItemCategories>
                <ItemCategoriesTitle color={item.color || theme.colors.title}>
                  {item.title}
                </ItemCategoriesTitle>
              </ContainerItemCategories>
            </ItemCategoriesBussiness>
          ))}
      </ListCategoriesBussiness>
      {loading ? (
        <ContentLoad>
          <TitleLoad>Aguarde, carregando anuncios...</TitleLoad>
          <Load />
        </ContentLoad>
      ) : (
        <ListProducts data={products} />
      )}
    </BoxSingle>
  );
};

export default BussinessSingle;
