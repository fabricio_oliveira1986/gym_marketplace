import React, {useCallback, useState} from 'react';
import {
  Alert,
  Keyboard,
  Platform,
  TouchableWithoutFeedback,
  TouchableHighlight,
} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {useTheme} from 'styled-components';
import {useForm} from 'react-hook-form';
import * as Yup from 'yup';
import {yupResolver} from '@hookform/resolvers/yup';
import {useAuth} from '~/hooks/auth';
import Load from '~/components/Load';
import {Modal, BoxTouchablePercent} from '~/components/index';
import {Background} from '~/components/Background';
import Images from '~/assets/images/Logos';
import Config, {env} from '~/config';
import {apiInstance} from '~/services/api';
import {
  Container,
  Card,
  Logo,
  BoxForm,
  InputForm,
  ContainerButtonForm,
  ButtonForm,
  BoxLink,
  ButtonLink,
  TextButtonLink,
} from './style';

interface FormData {
  email: string;
  password: string;
}

const schema = Yup.object().shape({
  email: Yup.string()
    .required('E-mail é obrigatório')
    .email('Digite um e-mail válido'),
  password: Yup.string().required('Senha obrigatória'),
});

const SignIn: React.FC = () => {
  const navigation = useNavigation();
  const theme = useTheme();
  const [loading, setLoading] = useState(false);
  const [modalEnvironment, setModalEnvironment] = useState(false);
  const {signIn} = useAuth();
  const {
    control,
    handleSubmit,
    formState: {errors},
  } = useForm({
    resolver: yupResolver(schema),
  });

  const signInForm = useCallback(
    async (form: FormData) => {
      try {
        setLoading(true);
        await signIn(form);
        setLoading(false);
      } catch (e) {
        console.log(e);
        setLoading(false);
        Alert.alert('Erro', 'Erro ao efetuar o Login', [
          {text: 'OK', onPress: () => console.log('OK Pressed')},
        ]);
      }
    },
    [signIn],
  );

  const handlerLongClick = async data => {
    await Config.map(item => {
      item.name === data.name
        ? (item.selected = true)
        : (item.selected = false);
    });

    apiInstance.setBaseURL(env().url);
    setModalEnvironment(false);
  };

  const handleSignIn = (form: FormData) => {
    signInForm(form);
  };

  const goToResetPassword = () => {
    navigation.navigate('ResetPassword');
  };

  const goToSignUp = () => {
    navigation.navigate('SignUp');
  };

  return (
    <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
      <Container
        behavior={Platform.OS === 'ios' ? 'padding' : undefined}
        enabled>
        <Background>
          <Card>
            <TouchableHighlight
              onPress={() => {
                setModalEnvironment(true);
              }}>
              <Logo source={Images.LogoDefault} />
            </TouchableHighlight>

            {loading ? (
              <Load />
            ) : (
              <>
                <BoxForm>
                  <InputForm
                    control={control}
                    name="email"
                    placeholder="EMAIL"
                    keyboardType="email-address"
                    placeholderTextColor={theme.colors.placeholder}
                    color={theme.colors.text}
                    error={errors.email && errors.email.message}
                  />
                  <InputForm
                    control={control}
                    name="password"
                    placeholder="SENHA"
                    password
                    placeholderTextColor={theme.colors.placeholder}
                    color={theme.colors.text}
                    error={errors.password && errors.password.message}
                  />
                  <ContainerButtonForm>
                    <ButtonForm
                      borderColor={theme.colors.dark}
                      color={theme.colors.dark}
                      onPress={handleSubmit(handleSignIn)}>
                      Login
                    </ButtonForm>
                  </ContainerButtonForm>
                </BoxForm>
                <BoxLink>
                  <ButtonLink onPress={goToResetPassword}>
                    <TextButtonLink>Esqueci Minha Senha</TextButtonLink>
                  </ButtonLink>
                  <ButtonLink onPress={goToSignUp}>
                    <TextButtonLink>Criar Conta</TextButtonLink>
                  </ButtonLink>
                </BoxLink>
              </>
            )}
          </Card>
        </Background>
        <Modal
          modalVisible={modalEnvironment}
          width={90}
          onRequestClose={() => setModalEnvironment(false)}>
          {Config.map(item => (
            <BoxTouchablePercent
              key={item.name}
              pt={20}
              pb={20}
              height={100}
              align="center"
              justify="center"
              borderTopWidth={2}
              bgColor={item.selected ? theme.colors.edit : 'transparent'}
              borderBottomWidth={2}
              onPress={() => handlerLongClick(item)}
              borderColor={theme.colors.edit}>
              <TextButtonLink>{item.name}</TextButtonLink>
              <TextButtonLink>{item.url}</TextButtonLink>
            </BoxTouchablePercent>
          ))}
        </Modal>
      </Container>
    </TouchableWithoutFeedback>
  );
};

export default SignIn;
