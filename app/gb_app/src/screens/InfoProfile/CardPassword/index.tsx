import React, {useState} from 'react';
import {Button, InputEdit, Card, Box} from '~/components/index';
import {ContainerButton} from '../style';
import ImageIcon from '~/assets/images/Icons';

const CardPassword: React.FC = ({
  editable,
  setEditable,
  theme,
  dataPassword,
  setDataPassword,
  onPressUpdate,
}) => {
  const [visiblePassword, setVisiblePassword] = useState(false);
  return (
    <>
      <Card>
        <Box self="flex-end" height={30}>
          <ImageIcon.Edit
            width={15}
            height={15}
            fill={editable ? theme.colors.edit : theme.colors.dark_36}
            onPress={() => setEditable(!editable)}
          />
        </Box>

        <InputEdit
          label="Senha Atual"
          hasIcon
          hasIconRight
          secureTextEntry={visiblePassword}
          onPressIcon={() => {
            setEditable(!editable);
          }}
          editable={editable}
          theme={theme}
          placeholder="*****"
          visiblePassword={visiblePassword}
          setVisiblePassword={setVisiblePassword}
          fontSize={16}
          value={dataPassword.txtSenhaAtual}
          onChangeText={(txt: String) => {
            setDataPassword({...dataPassword, txtSenhaAtual: txt});
          }}
        />

        <InputEdit
          label="Nova Senha"
          hasIconRight
          onPressIcon={() => {
            setEditable(!editable);
          }}
          editable={editable}
          placeholder="*****"
          secureTextEntry={visiblePassword}
          theme={theme}
          visiblePassword={visiblePassword}
          setVisiblePassword={setVisiblePassword}
          fontSize={16}
          value={dataPassword.txtNovaSenha}
          onChangeText={(txt: String) => {
            setDataPassword({...dataPassword, txtNovaSenha: txt});
          }}
        />

        <InputEdit
          label="Confirmar Senha"
          hasIconRight
          onPressIcon={() => {
            setEditable(!editable);
          }}
          editable={editable}
          placeholder="*****"
          secureTextEntry={visiblePassword}
          theme={theme}
          visiblePassword={visiblePassword}
          setVisiblePassword={setVisiblePassword}
          fontSize={16}
          value={dataPassword.txtConfirmSenha}
          onChangeText={(txt: String) => {
            setDataPassword({...dataPassword, txtConfirmSenha: txt});
          }}
        />
      </Card>

      <ContainerButton mt={1} mb={50}>
        <Button
          onPress={() => onPressUpdate()}
          block
          fontBold
          uppercase
          background={editable ? theme.colors.success : theme.colors.edit}
          color={theme.colors.shape}
          fontSize={14}
          disabled={!editable}
          borderRadius={5}>
          Atualizar Senha
        </Button>
      </ContainerButton>
    </>
  );
};

export default CardPassword;
