import styled from 'styled-components/native';
import { RFValue } from 'react-native-responsive-fontsize';

export const Container = styled.View`
  flex: 1;
`;

export const HeaderTitle = styled.Text`
  color: ${({ theme }) => theme.colors.text};
  font-family: ${({ theme }) => theme.fonts.medium};
  font-size: ${RFValue(16)}px;
  line-height: ${RFValue(21)}px;
  margin: ${RFValue(25)}px ${RFValue(20)}px 0;
`;

export const Content = styled.ScrollView.attrs({
  showsVerticalScrollIndicator: false,
})`
  margin: ${RFValue(25)}px ${RFValue(20)}px;
`;

export const ItemInput = styled.View`
  margin-bottom: ${RFValue(25)}px;
`;

export const HeaderInput = styled.View`
  flex-direction: row;
  justify-content: space-between;
  margin-bottom: ${RFValue(5)}px;
  align-items: center;
`;


export const ButtonEditInput = styled.TouchableOpacity.attrs({
  activeOpacity: 0.7,
})``;

export const ContainerButton = styled.View`
  margin-top: ${({ mt }) => mt ? `${RFValue(mt)}px` : `0px`};
  margin-bottom: ${({ mb }) => mb ? `${RFValue(mb)}px` : `0px`};
`;
