import React from 'react';
import {Button, InputEdit, Card} from '~/components/index';
import {ContainerButton} from '../style';
import {Box} from '~/components/index';
import ImageIcon from '~/assets/images/Icons';

const CardPersonalData: React.FC = ({
  dataUser,
  theme,
  editable,
  setEditable,
  setDataUser,
  updateInfoProfileUser,
}) => {
  return (
    <>
      <Card>
        <Box self="flex-end" height={30}>
          <ImageIcon.Edit
            width={15}
            height={15}
            fill={editable ? theme.colors.edit : theme.colors.dark_36}
            onPress={() => setEditable(!editable)}
          />
        </Box>

        <InputEdit
          label="NOME"
          hasIcon
          onPressIcon={() => {
            setEditable(!editable);
          }}
          editable={editable}
          theme={theme}
          fontSize={16}
          value={dataUser.nome}
          onChangeText={(txt: String) => {
            setDataUser({...dataUser, nome: txt});
          }}
        />
        <InputEdit
          label="E-MAIL"
          theme={theme}
          editable={editable}
          fontSize={16}
          value={dataUser.email}
          onChangeText={(txt: String) => {
            setDataUser({...dataUser, email: txt});
          }}
        />
        <InputEdit
          label="CPF"
          theme={theme}
          fontSize={16}
          editable={false}
          value={dataUser.cpf}
        />

        <InputEdit
          label="TELEFONE"
          theme={theme}
          editable={editable}
          value={dataUser.celular}
          masked
          fontSize={16}
          type={'cel-phone'}
          onChangeText={(txt: String) => {
            setDataUser({...dataUser, celular: txt});
          }}
        />
      </Card>
      <ContainerButton mb={50}>
        <Button
          onPress={() => updateInfoProfileUser()}
          block
          fontBold
          uppercase
          background={editable ? theme.colors.success : theme.colors.edit}
          color={theme.colors.shape}
          fontSize={14}
          disabled={!editable}
          borderRadius={5}>
          Atualizar
        </Button>
      </ContainerButton>
    </>
  );
};

export default CardPersonalData;
