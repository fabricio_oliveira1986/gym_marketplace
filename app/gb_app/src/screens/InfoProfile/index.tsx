import React, {useState, useEffect} from 'react';
import {Alert} from 'react-native';
import {isEmpty} from 'lodash';
import Header from '~/components/Header';
import Load from '~/components/Load/index';
import {useTheme} from 'styled-components/native';
import {Container, HeaderTitle, Content, ContainerButton} from './style';
import CardPassword from './CardPassword/index';
import CardPersonalData from './CardPersonalData/index';
import {middlewareError} from '~/global/utils/functions';
import {
  UserProfileResponse,
  UserDataPassword,
  schemUser,
  schemaUserDataPassword,
} from '~/models/Profile';
import {
  getInfoProfile,
  updateInfoProfile,
  updateInfoPassword,
  deleteAccountApi,
} from '~/services/profile';
import {Button} from '~/components';
import {useAuth} from '~/hooks/auth';

const InfoProfile: React.FC = () => {
  const [isLoading, setIsLoading] = useState<Boolean>(false);
  const theme = useTheme();
  const {signOut} = useAuth();

  const [dataUser, setDataUser] = useState<UserProfileResponse>(
    {} as UserProfileResponse,
  );

  const [dataPassword, setDataPassword] = useState<UserDataPassword>(
    {} as UserDataPassword,
  );

  const [editablePersonalData, setEditablePersonalData] =
    useState<Boolean>(false);
  const [editablePassword, setEditablePassword] = useState<Boolean>(false);

  const getInfoProfileUser = async () => {
    try {
      setIsLoading(true);
      const dados = await getInfoProfile();
      setDataUser(dados);
      setIsLoading(false);
    } catch (error) {
      console.log(error);
      setIsLoading(false);
    }
  };

  useEffect(() => {
    getInfoProfileUser();
  }, []);

  const updateInfoProfileUser = async () => {
    try {
      setIsLoading(true);

      const body = {
        txtNome: dataUser.nome,
        txtSexo: dataUser.sexo,
        txtEmail: dataUser.email,
        txtCelular: dataUser.celular,
        txtCPF: dataUser.cpf,
        txtDtNasc: isEmpty(dataUser.dt_nasc) ? '' : dataUser.dt_nasc,
      };
      const isValid = await schemUser.isValid(body);
      if (isValid) {
        await updateInfoProfile(body);
        setEditablePersonalData(false);
        Alert.alert('', 'Seus dados foram atualizados');
      } else {
        Alert.alert(
          'Atenção',
          'Todos os dados são obrigatórios e precisam ser validos',
        );
      }
      setIsLoading(false);
    } catch (error) {
      Alert.alert(
        'Atenção',
        'Todos os dados são obrigatórios e precisam ser validos',
      );
      setIsLoading(false);
    }
  };

  const updatePassword = async () => {
    setIsLoading(true);
    try {
      const validPass = await schemaUserDataPassword.isValid(dataPassword);
      if (validPass) {
        await updateInfoPassword(dataPassword);
        Alert.alert('', 'Seus dados foram atualizados');
      } else {
        Alert.alert(
          'Atenção',
          'Todos os dados são obrigatórios e precisam ser validos. \n A senha precisa ter no mínimo 5 digitos',
        );
      }
      setIsLoading(false);
    } catch (error) {
      setIsLoading(false);
      middlewareError(error);
    }
  };

  const deleteAccount = async () => {
    await deleteAccountApi();
    signOut();
  };

  return (
    <Container>
      <Header />
      <HeaderTitle>Dados Pessoais</HeaderTitle>
      {isLoading ? (
        <Load />
      ) : (
        <Content>
          <CardPassword
            setEditable={setEditablePassword}
            editable={editablePassword}
            theme={theme}
            dataPassword={dataPassword}
            setDataPassword={setDataPassword}
            onPressUpdate={updatePassword}
          />
          <CardPersonalData
            setIsLoading={setIsLoading}
            setDataUser={setDataUser}
            dataUser={dataUser}
            theme={theme}
            setEditable={setEditablePersonalData}
            editable={editablePersonalData}
            updateInfoProfileUser={updateInfoProfileUser}
          />

          <ContainerButton mt={1} mb={50}>
            <Button
              onPress={() => {
                Alert.alert('Atenção', 'Deseja realmente excluir sua conta?', [
                  {
                    text: 'Cancel',
                    onPress: () => {},
                    style: 'cancel',
                  },
                  {
                    text: 'Sim',
                    onPress: () => deleteAccount(),
                  },
                ]);
              }}
              block
              fontBold
              uppercase
              background={theme.colors.badge}
              color={theme.colors.shape}
              fontSize={14}
              borderRadius={5}>
              Excluir Conta
            </Button>
          </ContainerButton>
        </Content>
      )}
    </Container>
  );
};

export default InfoProfile;
