import React from 'react';
import {
  BoxPercent,
  Button,
  Text,
  BoxWarning,
  BoxTouchablePercent,
} from '~/components/index';
import {theme} from '~/global/styles/theme';
import {numberFormat} from '~/global/utils/functions';
import {TextItem} from '../../style';

interface IHeaderExtracts {
  balanceAvailable: number;
  balance: number;
  navigation: any;
}

const HeaderExtracts: React.FC<IHeaderExtracts> = ({
  balanceAvailable,
  balance,
  navigation,
}) => {
  return (
    <>
      <BoxPercent direction mt={20} mb={20}>
        <BoxPercent style={{flex: 1}} align="center">
          <TextItem bold mr={5}>
            Saldo Disponível
          </TextItem>

          <BoxPercent
            direction
            align="flex-end"
            justify="center"
            mb={10}
            mt={10}>
            <TextItem bold mr={5} mt={0}>
              R$
            </TextItem>
            <Text fontSize={22} color={theme.colors.title}>
              {numberFormat(Number(balanceAvailable || 0))}
            </Text>
          </BoxPercent>

          <Button
            fontSize={16}
            background={theme.colors.success}
            color={theme.colors.shape}
            borderRadius={20}
            onPress={() => {
              navigation.navigate('ToWithdraw');
            }}>
            Resgatar
          </Button>
        </BoxPercent>

        <BoxPercent style={{flex: 1}} align="center">
          <TextItem bold mr={5}>
            Saldo Total
          </TextItem>
          <BoxPercent
            direction
            align="flex-end"
            justify="center"
            mb={10}
            mt={10}>
            <TextItem bold mr={5} mt={0}>
              R$
            </TextItem>
            <Text fontSize={22} color={theme.colors.title}>
              {numberFormat(Number(balance || 0))}
            </Text>
          </BoxPercent>
        </BoxPercent>
      </BoxPercent>
      <BoxWarning
        text="Valor mínimo para saque é de R$ 40,00 e será cobrado R$ 4,90 de taxa de transferencia para mais informações "
        child={
          <BoxTouchablePercent>
            <Text fontSize={15} color={theme.colors.success}>
              Clique aqui
            </Text>
          </BoxTouchablePercent>
        }
      />
    </>
  );
};

export default HeaderExtracts;
