import styled, {css} from 'styled-components/native';
import {RFValue} from 'react-native-responsive-fontsize';

export const Container = styled.View``;

export const HeaderTitle = styled.Text`
  color: ${({theme}) => theme.colors.text};
  font-family: ${({theme}) => theme.fonts.medium};
  font-size: ${RFValue(16)}px;
  line-height: ${RFValue(21)}px;
  margin: ${RFValue(25)}px ${RFValue(0)}px ${RFValue(10)}px 0px;
`;

export const Content = styled.ScrollView.attrs({
  showsVerticalScrollIndicator: false,
  contentContainerStyle: {
    paddingBottom: 90,
  },
})`
  margin: ${RFValue(25)}px ${RFValue(20)}px ${RFValue(100)}px;
`;

export const ContainerValues = styled.View`
  margin: 0 ${RFValue(20)}px ${RFValue(10)}px;
`;

export const ItemValue = styled.View`
  flex-direction: row;
  justify-content: flex-end;
`;

export const ItemExtract = styled.TouchableOpacity`
  margin-bottom: ${RFValue(8)}px;
`;

interface ContainerItemProps {
  content?: 'space-between' | 'flex-start' | 'flex-end';
  mr?: number;
  ml?: number;
  mb?: number;
  mt?: number;
}
export const ContainerItem = styled.View<ContainerItemProps>`
  flex-direction: row;
  margin-left: ${props => (props.ml ? RFValue(props.ml) : 0)}px;
  margin-right: ${props => (props.mr ? RFValue(props.mr) : 0)}px;
  margin-bottom: ${props => (props.mb ? RFValue(props.mb) : 0)}px;
  margin-top: ${props => (props.mt ? RFValue(props.mt) : 0)}px;
  justify-content: ${props => props.content || 'flex-start'};
`;

interface TextItemProps extends ContainerItemProps {
  color?: string;
  bold?: boolean;
}

export const TextItem = styled.Text<TextItemProps>`
  align-items: flex-end;
  margin-left: ${props => (props.ml ? RFValue(props.ml) : 0)}px;
  margin-right: ${props => (props.mr ? RFValue(props.mr) : 0)}px;
  margin-bottom: ${props => (props.mb ? RFValue(props.mb) : 0)}px;
  margin-top: ${props => (props.mt ? RFValue(props.mt) : 0)}px;
  justify-content: ${props => props.content || 'flex-start'};
  color: ${props => props.color || props.theme.colors.title};
  ${props =>
    props.hasOwnProperty('bold')
      ? css`
          font-family: ${({theme}) => theme.fonts.medium};
          font-size: ${RFValue(16)}px;
          line-height: ${RFValue(20)}px;
        `
      : css`
          font-family: ${({theme}) => theme.fonts.regular};
          font-size: ${RFValue(14)}px;
          line-height: ${RFValue(16)}px;
        `};
`;
