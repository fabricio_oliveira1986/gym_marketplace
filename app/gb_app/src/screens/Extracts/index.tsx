import React, {useState, useCallback} from 'react';
import {useTheme} from 'styled-components/native';
import {useFocusEffect} from '@react-navigation/native';
import Card from '~/components/Card';
import moment from 'moment';
import Header from '~/components/Header';
import {LoadFull} from '~/components/index';
import {numberFormat, dateTimeBr} from '~/global/utils/functions';
import * as extractServices from '~/services/extract';
import * as purchasesService from '~/services/purchases';
import ImageIcon from '~/assets/images/Icons';
import {Extract} from '~/models/Extract';
import HeaderExtracts from './Components/HeaderExtracts';

import {
  Container,
  HeaderTitle,
  ContainerValues,
  ItemValue,
  Content,
  ItemExtract,
  ContainerItem,
  TextItem,
} from './style';

interface ValuesCashback {
  cashback: number;
  cashbackDisponible: number;
}

const Extracts: React.FC = props => {
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [extracts, setExtracts] = useState<Extract[]>([]);
  const [values, setValues] = useState<ValuesCashback>({} as ValuesCashback);
  const theme = useTheme();

  useFocusEffect(
    useCallback(() => {
      setIsLoading(true);
      extractServices.getExtractCashback().then(item => {
        setIsLoading(false);
        setExtracts(item.data);
        setValues({
          cashback: item.cashback,
          cashbackDisponible: item.cashbackDisponible,
        });
      });
    }, []),
  );

  return (
    <Container>
      <Header />
      {isLoading && <LoadFull />}

      <Content>
        <HeaderExtracts
          navigation={props.navigation}
          balance={values?.cashback}
          balanceAvailable={values?.cashbackDisponible}
        />

        <HeaderTitle>Extrato do Cashback</HeaderTitle>
        {extracts.map(extract => (
          <ItemExtract
            key={extract.id}
            onPress={async () => {
              setIsLoading(true);
              const data = await purchasesService.getOnlyPurchases(extract.id);
              setIsLoading(false);
              props.navigation.navigate('MyPurchaseDetail', {data});
            }}>
            <Card>
              <ContainerItem content="space-between">
                <TextItem bold>{extract.description}</TextItem>
                <TextItem bold>
                  {numberFormat(Number(extract.value), 1)}
                </TextItem>
              </ContainerItem>
              <TextItem>{dateTimeBr(extract.date)}</TextItem>
              {extract.status === 'P' && (
                <TextItem>
                  Disponivel em:{' '}
                  {moment(
                    moment(extract.date).format('MM-DD-YYYY'),
                    'MM-DD-YYYY',
                  )
                    .add('days', 60)
                    .format('DD/MM/YYYY')}
                </TextItem>
              )}

              <ContainerItem mt={15}>
                {extract.status === 'C' && (
                  <>
                    <TextItem bold color={theme.colors.success} mr={5}>
                      Dinheiro de volta confirmado
                    </TextItem>
                    <ImageIcon.Check
                      width={14}
                      height={10}
                      fill={theme.colors.success}
                      style={{marginTop: 4}}
                    />
                  </>
                )}
                {extract.status === 'P' && (
                  <TextItem bold color={theme.colors.warning} mr={5}>
                    Pendente
                  </TextItem>
                )}
                {extract.status === 'B' && (
                  <TextItem bold color={theme.colors.error} mr={5}>
                    Baixa de Cashback
                  </TextItem>
                )}
              </ContainerItem>
            </Card>
          </ItemExtract>
        ))}
      </Content>
    </Container>
  );
};

export default Extracts;
