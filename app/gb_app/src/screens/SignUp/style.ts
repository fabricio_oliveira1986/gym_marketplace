import styled from 'styled-components/native';
import {RFValue} from 'react-native-responsive-fontsize';

import InputFormComponent from '~/components/Form/InputForm';
import Button from '~/components/Form/Button';

export const Container = styled.KeyboardAvoidingView`
  flex: 1;
`;

export const Card = styled.View`
  margin: ${RFValue(30)}px;
  background: ${({theme}) => theme.colors.shape};
  padding: ${RFValue(40)}px ${RFValue(30)}px;
  border-radius: ${RFValue(5)}px;
  box-shadow: 0px 0px ${RFValue(2)}px ${({theme}) => theme.colors.shadow};
`;

export const CardTitle = styled.Text`
  font-size: ${RFValue(20)}px;
  text-align: center;
  margin-bottom: ${RFValue(30)}px;
  text-transform: uppercase;
`;

export const Logo = styled.Image.attrs({
  resizeMode: 'contain',
})`
  width: ${RFValue(90)}px;
  align-self: center;
  margin-bottom: ${RFValue(20)}px;
`;

export const BoxForm = styled.View``;

export const InputForm = styled(InputFormComponent).attrs({
  height: 40,
  fontSize: 14,
  typeInput: 'line',
  marginBottom: 10,
})``;

export const ContainerButtonForm = styled.View`
  margin-top: ${RFValue(20)}px;
`;

export const ButtonForm = styled(Button).attrs({
  borderRadius: 5,
  border: true,
  height: 40,
  fontSize: 14,
  fontBold: true,
  uppercase: true,
})``;

export const BoxGroupGenre = styled.View`
  flex-direction: row;
  margin: ${RFValue(15)}px 0;
`;

export const TextButtonForm = styled.Text`
  font-size: ${RFValue(14)}px;
  font-weight: bold;
  color: ${({theme}) => theme.colors.dark};
  text-transform: uppercase;
`;

export const BoxLink = styled.View`
  margin-top: ${RFValue(40)}px;
  flex-direction: row;
  justify-content: center;
`;

export const ButtonLink = styled.TouchableOpacity.attrs({
  activeOpacity: 0.7,
})`
  border-bottom-color: ${({theme}) => theme.colors.title};
  border-bottom-width: ${RFValue(1)}px;
`;

export const TextButtonLink = styled.Text`
  color: ${({theme}) => theme.colors.title};
  text-transform: uppercase;
  font-size: ${RFValue(10)}px;
`;
