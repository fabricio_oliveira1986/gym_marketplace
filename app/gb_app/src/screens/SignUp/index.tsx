import React, {useEffect, useState} from 'react';
import {useNavigation} from '@react-navigation/native';
import {
  Alert,
  Keyboard,
  TouchableWithoutFeedback,
  ScrollView,
} from 'react-native';
import {isEmpty} from 'lodash';
import {CheckBox} from 'react-native-elements';
import {useTheme} from 'styled-components';
import moment from 'moment';
import {useForm} from 'react-hook-form';
import * as Yup from 'yup';
import {yupResolver} from '@hookform/resolvers/yup';
import {useAuth} from '~/hooks/auth';
import {Background} from '~/components/Background';
import RadioButton from '~/components/Form/RadioButton';
import Load from '~/components/Load';
import {BoxPercent, BoxTouchablePercent, InputDate} from '~/components/index';
import {isValidCPF} from '~/global/utils/functions';

import {
  Container,
  Card,
  CardTitle,
  BoxForm,
  InputForm,
  BoxGroupGenre,
  ContainerButtonForm,
  ButtonForm,
  BoxLink,
  ButtonLink,
  TextButtonLink,
} from './style';

interface FormData {
  name: string;
  email: string;
  celphone: string;
  password: string;
  passwordConfirmation: string;
  iAgree: boolean;
  cpf: string;
  birthDate: string;
}

const SignUp: React.FC = () => {
  const theme = useTheme();
  const navigation = useNavigation();
  const [iAgree, setIAgree] = useState(false);
  const [birthDate, setBirthDate] = useState('1990-01-01');
  const [genre, setGenre] = useState('M');
  const [loading, setLoading] = useState(false);

  const {signUp} = useAuth();

  useEffect(() => {
    console.log('birthDate', birthDate);
  }, [birthDate]);

  const schema = Yup.object().shape({
    name: Yup.string()
      .min(3, 'Mínimo 3 caracteres')
      .required('Nome é obrigatório'),
    celphone: Yup.string().required('Celular é obrigatório'),
    email: Yup.string()
      .required('E-mail é obrigatório')
      .email('Digite um e-mail válido'),
    password: Yup.string().required('Senha obrigatória'),
    iAgree: Yup.boolean().test(() => iAgree === true),
    cpf: Yup.string()
      .min(14, 'Mínimo 11 caracteres e válido')
      .test('cpf', 'Cpf é um campo obrigatório e precisa ser válido', value =>
        isValidCPF(value),
      )
      .required('Cpf é um campo obrigatório e precisa ser válido'),
    passwordConfirmation: Yup.string().oneOf(
      [Yup.ref('password'), null],
      'Senha não correspondente',
    ),
  });

  const {
    control,
    handleSubmit,
    formState: {errors},
  } = useForm({
    resolver: yupResolver(schema),
  });

  const handleSelectGenre = (value: string) => {
    setGenre(value);
  };

  const handleSignUp = async (form: FormData) => {
    const {name, email, password, celphone, cpf} = form;
    try {
      setLoading(true);
      await signUp({
        name,
        email,
        celphone,
        password,
        genre,
        cpf,
        birthDate: moment(birthDate).format('DD/MM/YYYY'),
      });
      setLoading(false);
    } catch (e) {
      console.log(e);
      setLoading(false);
      Alert.alert('Erro', 'Erro ao cadastrar o usuário', [
        {text: 'OK', onPress: () => console.log('OK Pressed')},
      ]);
    }
  };

  const goToLogin = () => {
    navigation.navigate('SignIn');
  };

  return (
    <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
      <Container>
        <ScrollView>
          <Background>
            <Card>
              <CardTitle>Cadastro</CardTitle>
              {loading ? (
                <Load />
              ) : (
                <>
                  <BoxForm>
                    <InputForm
                      name="name"
                      control={control}
                      placeholder="NOME"
                      color={theme.colors.dark}
                      placeholderTextColor={theme.colors.placeholder}
                      error={errors.name && errors.name.message}
                    />

                    <InputForm
                      name="celphone"
                      masked
                      control={control}
                      placeholder="CELULAR"
                      color={theme.colors.dark}
                      keyboardType="phone-pad"
                      type={'cel-phone'}
                      placeholderTextColor={theme.colors.placeholder}
                      error={errors.celphone && errors.celphone.message}
                    />
                    <InputForm
                      name="email"
                      control={control}
                      placeholder="EMAIL"
                      color={theme.colors.dark}
                      keyboardType="email-address"
                      placeholderTextColor={theme.colors.placeholder}
                      error={errors.email && errors.email.message}
                    />
                    <InputForm
                      name="cpf"
                      masked
                      control={control}
                      placeholder="CPF"
                      color={theme.colors.dark}
                      keyboardType="phone-pad"
                      type={'cpf'}
                      placeholderTextColor={theme.colors.placeholder}
                      error={errors.cpf && errors.cpf.message}
                    />

                    <InputForm
                      name="password"
                      control={control}
                      placeholder="SENHA"
                      color={theme.colors.dark}
                      password
                      placeholderTextColor={theme.colors.placeholder}
                      error={errors.password && errors.password.message}
                    />
                    <InputForm
                      name="passwordConfirmation"
                      control={control}
                      placeholder="CONFIRMAR SENHA"
                      color={theme.colors.dark}
                      password
                      placeholderTextColor={theme.colors.placeholder}
                      error={
                        errors.passwordConfirmation &&
                        errors.passwordConfirmation.message
                      }
                    />

                    <BoxPercent align="center" direction width="100%">
                      <CheckBox
                        title=""
                        name="iAgree"
                        control={control}
                        onPress={() => setIAgree(!iAgree)}
                        checked={iAgree}
                        containerStyle={{
                          backgroundColor: 'transparent',
                          borderWidth: 0,
                          width: 20,
                          padding: 0,
                        }}
                        checkedColor={theme.colors.success}
                      />
                      <BoxPercent align="center" direction>
                        <TextButtonLink>Eu li e concordo com</TextButtonLink>
                        <BoxTouchablePercent
                          borderColor={theme.colors.dark}
                          borderBottomWidth={1}
                          pb={3}
                          pt={4}
                          onPress={() => {
                            navigation.navigate('Contract');
                          }}>
                          <TextButtonLink> os termos de uso</TextButtonLink>
                        </BoxTouchablePercent>
                      </BoxPercent>
                    </BoxPercent>

                    <ContainerButtonForm>
                      <ButtonForm
                        borderColor={theme.colors.dark}
                        color={theme.colors.dark}
                        onPress={handleSubmit(handleSignUp)}>
                        Criar a conta
                      </ButtonForm>
                    </ContainerButtonForm>
                  </BoxForm>
                  <BoxLink>
                    <ButtonLink onPress={goToLogin}>
                      <TextButtonLink>
                        Já tem uma conta? Faça Login
                      </TextButtonLink>
                    </ButtonLink>
                  </BoxLink>
                </>
              )}
            </Card>
          </Background>
        </ScrollView>
      </Container>
    </TouchableWithoutFeedback>
  );
};

export default SignUp;
