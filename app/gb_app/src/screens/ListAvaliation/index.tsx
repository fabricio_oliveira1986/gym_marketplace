import React, {useState, useEffect} from 'react';
import Feather from 'react-native-vector-icons/Feather';
import {RFPercentage} from 'react-native-responsive-fontsize';
import {useTheme} from 'styled-components';
import {Modalize} from 'react-native-modalize';
import {Alert} from 'react-native';

import ItemAvaliation from '~/components/ItemAvaliation';

import * as avaliationService from '~/services/avaliation';

import {Avaliation} from '~/models/Avaliation';
import {Product} from '~/models/Product';

import {ContainerTitle, ButtonClose, Title, Separator} from './style';

interface ListAvalitionProps {
  product: Product;
  modalizeRef: React.RefObject<Modalize>;
}

const ListAvaliation: React.FC<ListAvalitionProps> = ({
  product,
  modalizeRef,
}) => {
  const theme = useTheme();

  const [avaliations, setAvaliations] = useState<Avaliation[]>([]);

  const handleClose = () => {
    modalizeRef.current?.close();
  };

  useEffect(() => {
    avaliationService
      .getAvaliationsProduct(product.id)
      .then(item => setAvaliations(item))
      .catch(erro => {
        console.log(erro);
        Alert.alert('Erro', 'Ocorreu um erro ao buscar as avaliações');
      });
  }, [product.id]);

  return (
    <Modalize
      snapPoint={RFPercentage(90)}
      ref={modalizeRef}
      HeaderComponent={
        <ContainerTitle>
          <Title>
            Avaliações do Anúncio: {'\n'} {product.title}
          </Title>
          <ButtonClose onPress={handleClose}>
            <Feather name="x" size={28} color={theme.colors.shape} />
          </ButtonClose>
        </ContainerTitle>
      }
      flatListProps={{
        data: avaliations,
        keyExtractor: item => item.id,
        renderItem: ({item}) => <ItemAvaliation item={item} />,
        ItemSeparatorComponent: () => <Separator />,
      }}
    />
  );
};

export default ListAvaliation;
