import styled from 'styled-components/native';
import {FlatList} from 'react-native';

import {RFValue} from 'react-native-responsive-fontsize';

import {Avaliation} from '~/models/Avaliation';

export const Container = styled.View`
  flex: 1;
`;

export const ContainerTitle = styled.View`
  padding: ${RFValue(10)}px ${RFValue(15)}px;
  background: ${({theme}) => theme.colors.primary};
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`;

export const ButtonClose = styled.TouchableOpacity.attrs({
  activeOpacity: 0.7,
})``;

export const Title = styled.Text`
  font-family: ${({theme}) => theme.fonts.medium};
  font-size: ${RFValue(18)}px;
  line-height: ${RFValue(24)}px;
  color: ${({theme}) => theme.colors.shape};
`;

export const List = styled(FlatList as new () => FlatList<Avaliation>)``;

export const Separator = styled.View`
  width: 100%;
  height: 1px;
  margin: ${RFValue(10)}px;
`;
