import React, {useState, useMemo, useRef} from 'react';
import {Alert} from 'react-native';
import Header from '~/components/Header';
import {Modalize} from 'react-native-modalize';
import {Container, Body, Title, InputForm} from './style';
import {useTheme} from 'styled-components';
import SelectModal2, {SelectProps} from '~/components/Form/SelectModal2';
import {
  Card,
  Button,
  BoxPercent,
  LoadFull,
  SelectButton,
} from '~/components/index';
import {useNavigation} from '@react-navigation/native';
import {useForm} from 'react-hook-form';
import * as Yup from 'yup';
import {yupResolver} from '@hookform/resolvers/yup';
import {isValidCPF} from '~/global/utils/functions';
import {useCart} from '~/hooks/cart';
import {isEmpty} from 'lodash';
import * as cashbackService from '~/services/cashback';
import {orderWithdraw} from '~/services/withdraw';

interface DataProps {
  id: string;
  title: string;
}

interface IForm {
  banco: string;
  agencia: string;
  dvAgencia: string;
  conta: string;
  dvConta: string;
  titular: string;
  valor: string;
  cpf: string;
  txtCompConta: string;
}

const ToWithdraw: React.FC = () => {
  const theme = useTheme();
  const navigation = useNavigation();
  const [isLoading, setIsLoading] = useState(false);
  const [typeAccount, setTypeAccount] = useState('');
  const {setBalanceCashBack} = useCart();
  const [isCaixa, setIsCaixa] = useState(false);

  const modalizeRef = useRef<Modalize>(null);
  const schema = Yup.object().shape({
    banco: Yup.string()
      .min(1, 'Mínimo 1 caracteres')
      .required('Banco é obrigatório'),
    agencia: Yup.string()
      .min(3, 'Mínimo 3 caracteres')
      .required('Agencia é um campo obrigatório'),
    dvAgencia: Yup.string()
      .min(1, 'Mínimo 1 caracteres')
      .required('Digito verificador é obrigatório'),
    conta: Yup.string()
      .min(3, 'Mínimo 3 caracteres')
      .required('Conta é um campo obrigatório'),
    dvConta: Yup.string()
      .min(1, 'Digito verificador é obrigatório')
      .required('Digito verificador é obrigatório'),
    titular: Yup.string()
      .min(3, 'Digite o nome do titular da conta.')
      .required('Digite o nome do titular da conta.'),
    valor: Yup.string()
      .min(1, 'Digite um valor requerido')
      .required('Digite um valor requerido'),
    cpf: Yup.string()
      .min(14, 'Mínimo 11 caracteres e válido')
      .test('cpf', 'Cpf é um campo obrigatório e precisa ser válido', value =>
        isValidCPF(value),
      )
      .required('Cpf é um campo obrigatório e precisa ser válido'),
    txtTipoConta: Yup.string().test(
      'txtTipoConta',
      'Data de Nascimento é um campo obrigatório',
      () => !isEmpty(typeAccount),
    ),
    txtCompConta: Yup.string(),
  });

  const {
    control,
    handleSubmit,
    formState: {errors, isValid},
    watch,
    setValue,
  } = useForm({
    resolver: yupResolver(schema),
    mode: 'onChange',
  });

  React.useEffect(() => {
    watch((data, {name, type}) => {
      if (name === 'banco') {
        if (data.banco === '104') {
          setIsCaixa(true);
        } else {
          setIsCaixa(false);
          setValue('txtCompConta', '');
        }
      }
    });
  }, [watch]);

  const dataSelect = useMemo(() => {
    const item: SelectProps = {
      title: 'Selecione um tipo de conta',
      data: () => {
        return new Promise((resolve, _) =>
          resolve([
            {id: 'CHECKING', title: 'Conta Corrente'},
            {id: 'SAVINGS', title: 'Poupança'},
          ]),
        );
      },
      onPress: data => {
        setTypeAccount(data);
      },
    };
    return item;
  }, []);

  const handleToWithdraw = async (form: IForm) => {
    try {
      setIsLoading(true);
      console.log(form);
      const body = {
        txtBanco: form.banco,
        txtAgencia: form.agencia,
        txtAgenciaDv: form.dvAgencia,
        txtConta: form.conta,
        txtContaDv: form.dvConta,
        txtTitularNome: form.titular,
        txtTitularCpf: form.cpf,
        txtTipoConta: typeAccount.id,
        txtCompConta: form.txtCompConta,
        txtValorSaque: form.valor.replace('R$', ''),
        txtTipoDocumento: 'X',
        txtParcelas: 1,
      };
      await orderWithdraw(body);

      const data = await cashbackService.getValueCashback();
      setBalanceCashBack(Number(data.total_liquido));
      navigation.navigate('Home');
      Alert.alert('', 'Saque realizado com sucesso!!!');
      setIsLoading(false);
    } catch (error) {
      setIsLoading(false);
      Alert.alert(
        '',
        'Não foi possível realizar seu saque, confira seus dados ou tente mais tarde',
      );
      console.log('valor de error');
    }
  };

  return (
    <>
      <Container>
        <Header />
        <Body>
          {isLoading && <LoadFull />}
          <Title>Saque Cashback</Title>

          <Card style={{marginBottom: 70}}>
            <BoxPercent direction justify="space-between">
              <BoxPercent width="48%">
                <InputForm
                  name="banco"
                  control={control}
                  keyboardType="phone-pad"
                  placeholder="Número do Banco"
                  color={theme.colors.dark}
                  placeholderTextColor={theme.colors.placeholder}
                  error={errors.banco && errors.banco.message}
                />
              </BoxPercent>
              <BoxPercent width="48%" align="center" justify="center">
                <SelectButton
                  width="100%"
                  height={10}
                  placeholder="Tipo de Conta"
                  value={typeAccount.title}
                  onPress={() => {
                    modalizeRef.current?.open();
                  }}
                />
              </BoxPercent>
            </BoxPercent>

            <BoxPercent direction justify="space-between">
              <BoxPercent width={isCaixa ? '30%' : '65%'}>
                <InputForm
                  name="agencia"
                  control={control}
                  placeholder="Agencia"
                  color={theme.colors.dark}
                  keyboardType="phone-pad"
                  placeholderTextColor={theme.colors.placeholder}
                  error={errors.agencia && errors.agencia.message}
                />
              </BoxPercent>
              <BoxPercent width={isCaixa ? '20%' : '30%'}>
                <InputForm
                  name="dvAgencia"
                  control={control}
                  placeholder="DV"
                  keyboardType="phone-pad"
                  maxLength={1}
                  color={theme.colors.dark}
                  placeholderTextColor={theme.colors.placeholder}
                  error={errors.dvAgencia && errors.dvAgencia.message}
                />
              </BoxPercent>

              {isCaixa && (
                <BoxPercent width="30%">
                  <InputForm
                    name="txtCompConta"
                    control={control}
                    placeholder="Complemento"
                    color={theme.colors.dark}
                    keyboardType="phone-pad"
                    placeholderTextColor={theme.colors.placeholder}
                  />
                </BoxPercent>
              )}
            </BoxPercent>

            <BoxPercent direction justify="space-between">
              <BoxPercent width="65%">
                <InputForm
                  name="conta"
                  control={control}
                  placeholder="Conta"
                  keyboardType="phone-pad"
                  color={theme.colors.dark}
                  placeholderTextColor={theme.colors.placeholder}
                  error={errors.conta && errors.conta.message}
                />
              </BoxPercent>

              <BoxPercent width="30%">
                <InputForm
                  name="dvConta"
                  control={control}
                  placeholder="DV"
                  maxLength={1}
                  keyboardType="phone-pad"
                  color={theme.colors.dark}
                  placeholderTextColor={theme.colors.placeholder}
                  error={errors.dvConta && errors.dvConta.message}
                />
              </BoxPercent>
            </BoxPercent>

            <InputForm
              name="titular"
              control={control}
              placeholder="Titular"
              color={theme.colors.dark}
              placeholderTextColor={theme.colors.placeholder}
              error={errors.titular && errors.titular.message}
            />

            <InputForm
              name="cpf"
              masked
              control={control}
              placeholder="CPF"
              color={theme.colors.dark}
              keyboardType="phone-pad"
              type={'cpf'}
              placeholderTextColor={theme.colors.placeholder}
              error={errors.cpf && errors.cpf.message}
            />

            <InputForm
              name="valor"
              control={control}
              masked
              placeholder="Valor Saque"
              color={theme.colors.dark}
              keyboardType="phone-pad"
              type={'money'}
              placeholderTextColor={theme.colors.placeholder}
              error={errors.valor && errors.valor.message}
            />

            <BoxPercent mt={30}>
              <Button
                color={isValid ? theme.colors.shape : theme.colors.shape}
                fontSize={16}
                disabled={!isValid}
                children="Sacar"
                background={isValid ? theme.colors.success : theme.colors.edit}
                onPress={handleSubmit(handleToWithdraw)}
              />
            </BoxPercent>
          </Card>
        </Body>
        <SelectModal2
          modalizeRef={modalizeRef}
          data={dataSelect}
          onClose={() => {}}
        />
      </Container>
    </>
  );
};

export default ToWithdraw;
