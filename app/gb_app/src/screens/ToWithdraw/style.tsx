import styled from 'styled-components/native';
import {RFValue} from 'react-native-responsive-fontsize';
import InputFormComponent from '~/components/Form/InputForm';

export const Container = styled.View`
  flex: 1;
  background-color: ${({theme}) => theme.colors.background};
`;

export const Body = styled.ScrollView.attrs({
  showsVerticalScrollIndicator: false,
})`
  flex: 1;
  padding: ${RFValue(30)}px ${RFValue(20)}px 0px;
`;

export const Title = styled.Text`
  font-family: ${({theme}) => theme.fonts.medium};
  font-size: ${RFValue(16)}px;
  line-height: ${RFValue(21)}px;
  color: ${({theme}) => theme.colors.text};
  margin-bottom: ${RFValue(30)}px;
`;

export const InputForm = styled(InputFormComponent).attrs({
  height: RFValue(60),
  fontSize: 14,
  flex: 1,
  typeInput: 'line',
  marginBottom: 10,
})``;
