import styled from 'styled-components/native';
import {RFValue} from 'react-native-responsive-fontsize';
import {TouchableOpacityProps} from 'react-native';

export const Container = styled.View`
  flex: 1;
  background-color: ${({theme}) => theme.colors.background};
`;

export const Box = styled.ScrollView.attrs({
  showsVerticalScrollIndicator: false,
})`
  padding: ${RFValue(30)}px ${RFValue(15)}px;
`;

export const ItemLink = styled.TouchableOpacity``;

export const CardImage = styled.View`
  position: relative;
  margin-right: ${RFValue(15)}px;
  background-color: ${({theme}) => theme.colors.muted};
  width: ${RFValue(94)}px;
  height: ${RFValue(94)}px;
  border-radius: ${RFValue(47)}px;
`;

export const Bagde = styled.View`
  position: absolute;
  background-color: ${({theme}) => theme.colors.badge};
  width: ${RFValue(24)}px;
  height: ${RFValue(24)}px;
  border-radius: ${RFValue(12)}px;
  right: 0;
  top: 0;
  align-items: center;
  justify-content: center;
  z-index: 3;
`;

export const BagdeText = styled.Text`
  font-family: ${({theme}) => theme.fonts.medium};
  font-size: ${RFValue(12)}px;
  color: ${({theme}) => theme.colors.shape};
`;

export const Image = styled.Image.attrs({
  resizeMode: 'cover',
})`
  width: ${RFValue(94)}px;
  height: ${RFValue(94)}px;
  border-radius: ${RFValue(8)}px;
`;

export const CardBody = styled.View`
  flex: 1;
  justify-content: center;
`;

export const CardTitle = styled.Text`
  font-size: ${RFValue(14)}px;
  font-family: ${({theme}) => theme.fonts.medium};
  color: ${({theme}) => theme.colors.title};
  margin-bottom: ${RFValue(3)}px;
`;

export const CardDescription = styled.Text`
  font-size: ${RFValue(14)}px;
  font-family: ${({theme}) => theme.fonts.regular};
  color: ${({theme}) => theme.colors.title};
  margin-bottom: ${RFValue(2)}px;
`;

export const ContainerMoreItems = styled.View`
  padding: ${RFValue(30)}px 0;
`;

export const ButtonMoreItems = styled.TouchableOpacity.attrs({
  activeOpacity: 0.7,
})`
  flex-direction: row;
  align-items: center;
  justify-content: center;
`;

export const TextMoreItems = styled.Text`
  font-size: ${RFValue(14)}px;
  font-family: ${({theme}) => theme.fonts.medium};
  line-height: ${RFValue(19)}px;
  text-transform: uppercase;
  margin-right: ${RFValue(10)}px;
`;

export const ContainerItemValue = styled.View`
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  margin-bottom: ${RFValue(15)}px;
`;

export const ContainerSelectFreight = styled.View`
  margin: ${RFValue(20)}px 0;
`;

export const TitleFreight = styled.Text`
  font-family: ${({theme}) => theme.fonts.medium};
  color: ${({theme}) => theme.colors.success};
  font-size: ${RFValue(14)}px;
  line-height: ${RFValue(19)}px;
  margin-bottom: ${RFValue(10)}px;
`;

interface ItemValueProps {
  bold?: boolean;
  color?: string;
}

export const ItemValue = styled.Text<ItemValueProps>`
  font-family: ${({theme}) => theme.fonts.medium};
  font-size: ${props =>
    props.hasOwnProperty('bold') ? RFValue(17) : RFValue(14)}px;
  line-height: ${props =>
    props.hasOwnProperty('bold') ? RFValue(23) : RFValue(19)}px;
  text-transform: uppercase;
  color: ${props => props.color || props.theme.colors.title};
`;

export const Separator = styled.View`
  width: 100%;
  background-color: ${({theme}) => theme.colors.separator};
  height: ${RFValue(1)}px;
  margin-bottom: ${RFValue(30)}px;
`;

export const ButtonItemValue = styled.TouchableOpacity.attrs({
  activeOpacity: 0.7,
})`
  flex-direction: row;
  background-color: ${({theme}) => theme.colors.error};
  margin-top: ${RFValue(30)}px;
  width: 100%;
  height: ${RFValue(40)}px;
  align-items: center;
  justify-content: center;
`;

export const ButtonItemValueText = styled.Text`
  font-family: ${({theme}) => theme.fonts.medium};
  font-size: ${RFValue(16)}px;
  color: ${({theme}) => theme.colors.shape};
  margin-left: ${RFValue(5)}px;
  text-transform: uppercase;
`;

export const ContainerButton = styled.View`
  padding: ${RFValue(30)}px 0 ${RFValue(60)}px;
`;

export const ContainerModal = styled.View``;

export const TitleModal = styled.Text`
  font-family: ${({theme}) => theme.fonts.medium};
  font-size: ${RFValue(16)}px;
  line-height: ${RFValue(21)}px;
  margin-bottom: ${RFValue(5)}px;
  color: ${({theme}) => theme.colors.title};
`;

export const SubtitleModal = styled.Text`
  font-family: ${({theme}) => theme.fonts.regular};
  font-size: ${RFValue(12)}px;
  line-height: ${RFValue(16)}px;
  color: ${({theme}) => theme.colors.title};
  padding-bottom: ${RFValue(8)}px;
  text-decoration: underline;
`;

export const ContainerModalBody = styled.View`
  margin: ${RFValue(30)}px 0;
  flex-direction: row;
  width: 100%;
  justify-content: space-around;
  align-items: center;
`;

export const BoxValue = styled.View`
  width: ${RFValue(112)}px;
  height: ${RFValue(40)}px;
  border-color: ${({theme}) => theme.colors.muted};
  border-width: ${RFValue(1)}px;
  align-items: center;
  justify-content: center;
`;

export const TextValue = styled.Text`
  font-family: ${({theme}) => theme.fonts.medium};
  font-size: ${RFValue(14)}px;
  line-height: ${RFValue(19)}px;
  color: ${({theme}) => theme.colors.title};
`;
interface Update extends TouchableOpacityProps {
  background?: string;
}
export const ButtonUpdateValue = styled.TouchableOpacity.attrs({
  activeOpacity: 0.7,
})<Update>`
  width: ${RFValue(190)}px;
  height: ${RFValue(40)}px;
  background-color: ${props => props.background || props.theme.colors.success};
  align-items: center;
  justify-content: center;
  margin-bottom: ${RFValue(15)}px;
`;

export const ButtonUpdateText = styled.Text`
  font-family: ${({theme}) => theme.fonts.medium};
  font-size: ${RFValue(14)}px;
  line-height: ${RFValue(19)}px;
  color: ${({theme}) => theme.colors.shape};
  text-transform: uppercase;
`;
