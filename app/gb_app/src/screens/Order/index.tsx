import React, {useState, useCallback, useEffect, useMemo, useRef} from 'react';

import {useNavigation} from '@react-navigation/native';
import {useTheme} from 'styled-components';
import {Alert} from 'react-native';
import {Modalize} from 'react-native-modalize';

import {useCart, ItemCart} from '~/hooks/cart';
import {useAuth} from '~/hooks/auth';

import Card from '~/components/Card';
import Counter from '~/components/Form/Counter';
import Modal from '~/components/Modal';
import Button from '~/components/Form/Button';
import Header from '~/components/Header';
import SelectButton from '~/components/Form/SelectButton';
import SelectModal2, {SelectProps} from '~/components/Form/SelectModal2';

import {numberReal} from '~/global/utils/functions';
import ImageIcon from '~/assets/images/Icons';
import * as orderService from '~/services/order';
import {Freight} from '~/models/Freight';

import {
  Container,
  Box,
  CardImage,
  Image,
  CardBody,
  CardTitle,
  CardDescription,
  ContainerMoreItems,
  ContainerItemValue,
  ContainerSelectFreight,
  TitleFreight,
  ItemValue,
  Separator,
  ContainerButton,
  TitleModal,
  SubtitleModal,
  ContainerModalBody,
  BoxValue,
  TextValue,
  ButtonUpdateValue,
  ButtonUpdateText,
  ItemLink,
} from './style';

const Order: React.FC = () => {
  const {
    items,
    updateCart,
    clearCart,
    removeItem,
    subTotal,
    total,
    cashBack,
    freight: cartFreigth,
    updateFreight,
    address,
  } = useCart();
  const {user} = useAuth();
  const theme = useTheme();
  const navigation = useNavigation();
  const modalizeRef = useRef<Modalize>(null);

  const [visibleModal, setVisibleModal] = useState(false);
  const [itemSelected, setItemSelected] = useState<ItemCart>({} as ItemCart);
  const [quantityItem, setQuantityItem] = useState(1);
  const [freight, setFreight] = useState<orderService.DataProps>(
    {} as orderService.DataProps,
  );

  const [typesFreight, setTypesFreight] = useState<Freight[]>([]);

  const setCartFreight = useCallback(
    (item: orderService.DataProps) => {
      setFreight(item);
      const itemFreight = typesFreight.find(type => type.id === item.id);
      updateFreight(itemFreight || ({} as Freight));
    },
    [typesFreight, updateFreight],
  );

  const dataSelect = useMemo(() => {
    const item: SelectProps = {
      title: 'Escolha um tipo de frete',
      data: () => {
        return new Promise((resolve, _) =>
          resolve(orderService.transformFreight(typesFreight)),
        );
      },
      onPress: setCartFreight,
    };
    return item;
  }, [setCartFreight, typesFreight]);

  const handleOpenFreight = useCallback(() => {
    if (typesFreight.length) {
      modalizeRef.current?.open();
    }
  }, [typesFreight.length]);

  const handleUpdateQuantity = (value: number) => {
    setQuantityItem(value);
  };

  const handleUpdateCart = useCallback(
    async (item: ItemCart) => {
      await updateCart(item.product.id, quantityItem);
      setVisibleModal(false);
    },
    [quantityItem, updateCart],
  );
  const handleRemoveItemCart = useCallback(
    async (item: ItemCart) => {
      await removeItem(item);
      setVisibleModal(false);
      if (!items.length) {
        navigation.navigate('Home', {
          screen: 'Home',
        });
      }
    },
    [items.length, navigation, removeItem],
  );

  const handleFinalizarPedido = useCallback(() => {
    if (typesFreight.length > 0 && !freight?.id) {
      Alert.alert('Atenção', 'Antes de avançar selecione um tipo de frete');
    } else if (!user.id) {
      navigation.navigate('SignIn');
    } else if (!address?.id) {
      navigation.navigate('ListAddressPayment');
    } else {
      navigation.navigate('Payment');
    }
  }, [typesFreight.length, freight?.id, address, user, navigation]);

  const openModal = useCallback((item: ItemCart) => {
    setItemSelected(item);
    setQuantityItem(item.quantity);
    setVisibleModal(true);
  }, []);

  const handleAlertClearCart = () => {
    Alert.alert('Limpar o Carrinho', 'Você deseja limpar o carrinho?', [
      {
        text: 'Sim',
        onPress: () => {
          clearCart();
          setFreight({} as orderService.DataProps);
        },
      },
      {
        text: 'Não',
        style: 'cancel',
      },
    ]);
  };

  const openBussinessPage = useCallback(() => {
    if (items.length) {
      const item = items[0];
      navigation.navigate('Bussiness', {
        screen: 'BussinessSingle',
        params: {data: item.product?.bussiness},
      });
    } else {
      navigation.navigate('Home', {
        screen: 'Home',
      });
    }
  }, [navigation, items]);

  useEffect(() => {
    const idProducts = items.map(item => item.product.id);
    if (idProducts.length) {
      orderService
        .getFreights({idProducts})
        .then(freights => {
          setTypesFreight(freights);
        })
        .catch(erro => {
          console.log(erro);
          Alert.alert('Erro', 'Ocorreu um erro ao listar os fretes');
        });
    }
  }, [items]);

  return (
    <>
      <Container>
        <Header />
        <Box>
          {items.map(item => (
            <ItemLink key={item.product.id} onPress={() => openModal(item)}>
              <Card direction align="center">
                <CardImage>
                  <Image source={{uri: item.product.image}} />
                </CardImage>
                <CardBody>
                  <CardTitle numberOfLines={1}>{item.product.title}</CardTitle>
                  <CardDescription numberOfLines={2}>
                    {item.product.subTitle}
                  </CardDescription>
                  <CardDescription>
                    {item.quantity}x {numberReal(item.value)} ={' '}
                    <TextValue>
                      {numberReal(item.value * item.quantity)}
                    </TextValue>
                  </CardDescription>
                </CardBody>
              </Card>
            </ItemLink>
          ))}
          <ContainerMoreItems>
            <Button
              onPress={openBussinessPage}
              block
              fontBold
              Icon={props => (
                <ImageIcon.CartInvert
                  width={25}
                  height={21}
                  fill={props.color}
                />
              )}
              borderRadius={5}
              marginLeftText={5}
              background={theme.colors.error}
              color={theme.colors.shape}
              fontSize={16}>
              adicionar mais itens
            </Button>
            {items && items.length > 0 && (
              <Button
                onPress={handleAlertClearCart}
                block
                fontBold
                borderRadius={5}
                style={{marginTop: 10}}
                marginLeftText={5}
                background={theme.colors.primary}
                color={theme.colors.shape}
                fontSize={16}>
                limpar o carrinho
              </Button>
            )}
          </ContainerMoreItems>
          {typesFreight.length > 0 && (
            <ContainerSelectFreight>
              <TitleFreight>Frete:</TitleFreight>
              <SelectButton
                placeholder="Selecione um tipo de frete"
                value={freight.title}
                onPress={handleOpenFreight}
              />
            </ContainerSelectFreight>
          )}

          <Card>
            <ContainerItemValue>
              <ItemValue>Subtotal</ItemValue>
              <ItemValue>{numberReal(subTotal)}</ItemValue>
            </ContainerItemValue>
            {cartFreigth?.value && (
              <ContainerItemValue>
                <ItemValue>Frete</ItemValue>
                <ItemValue>{numberReal(cartFreigth?.value)}</ItemValue>
              </ContainerItemValue>
            )}
            <Separator />
            <ContainerItemValue>
              <ItemValue bold>Total</ItemValue>
              <ItemValue bold>{numberReal(total)}</ItemValue>
            </ContainerItemValue>
            <ContainerItemValue>
              <ItemValue color={theme.colors.success}>Cashback</ItemValue>
              <ItemValue color={theme.colors.success}>
                {numberReal(cashBack)}
              </ItemValue>
            </ContainerItemValue>
          </Card>
          <ContainerButton>
            <Button
              onPress={handleFinalizarPedido}
              block
              fontBold
              uppercase
              disabled={!items.length}
              background={
                !items.length ? theme.colors.edit : theme.colors.success
              }
              color={theme.colors.shape}
              fontSize={14}
              borderRadius={5}>
              Finalizar Pedido
            </Button>
          </ContainerButton>
        </Box>
      </Container>
      {typesFreight && (
        <SelectModal2
          modalizeRef={modalizeRef}
          data={dataSelect}
          onClose={() => {}}
        />
      )}
      <Modal
        modalVisible={visibleModal}
        onRequestClose={() => setVisibleModal(false)}
        width={85}
        align="center">
        {itemSelected.product && (
          <>
            <TitleModal>{itemSelected.product.title}</TitleModal>
            <SubtitleModal>Revisar Item</SubtitleModal>
            <ContainerModalBody>
              <Counter
                setValueCounter={handleUpdateQuantity}
                value={quantityItem}
              />
              <BoxValue>
                <TextValue>
                  {numberReal(quantityItem * itemSelected.value)}
                </TextValue>
              </BoxValue>
            </ContainerModalBody>
            <ButtonUpdateValue onPress={() => handleUpdateCart(itemSelected)}>
              <ButtonUpdateText>Atualizar</ButtonUpdateText>
            </ButtonUpdateValue>
            <ButtonUpdateValue
              onPress={() => handleRemoveItemCart(itemSelected)}
              background={theme.colors.error}>
              <ButtonUpdateText>Remover</ButtonUpdateText>
            </ButtonUpdateValue>
          </>
        )}
      </Modal>
    </>
  );
};

export default Order;
