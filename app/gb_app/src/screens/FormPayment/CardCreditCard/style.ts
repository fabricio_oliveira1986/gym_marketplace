import styled from 'styled-components/native';
import { RFValue } from 'react-native-responsive-fontsize';

export const ItemFormPayment = styled.View`
  margin-bottom: ${RFValue(8)}px;
`;

export const ContainerInfoPayment = styled.View`
  flex: 1;
`;

export const BoxInfoPayment = styled.View`
  flex-direction: row;
  align-items: center;
  margin-bottom: ${RFValue(5)}px;
`;

export const ImagePayment = styled.Image`
  width: ${RFValue(36)}px;
  height: ${RFValue(22)}px;
  margin-right: ${RFValue(8)}px;
`;

export const TextInfoPayment = styled.Text`
  font-family: ${({ theme }) => theme.fonts.medium};
  color: ${({ theme }) => theme.colors.title};
  font-size: ${RFValue(14)}px;
  line-height: ${RFValue(22)}px;
`;

export const Circle = styled.View`
  width: ${RFValue(7)}px;
  height: ${RFValue(7)}px;
  background-color: ${({ theme }) => theme.colors.primary};
  border-radius: ${RFValue(3.5)}px;
  margin-right: ${RFValue(4)}px;
`;

export const TextTypePayment = styled.Text`
  color: ${({ theme }) => theme.colors.text};
  font-size: ${RFValue(12)}px;
  font-family: ${({ theme }) => theme.fonts.medium};
  line-height: ${RFValue(16)}px;
  text-transform: uppercase;
  letter-spacing: ${RFValue(1)}px;
`;
