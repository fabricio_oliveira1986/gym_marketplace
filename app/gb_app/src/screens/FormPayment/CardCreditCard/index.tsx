import React from 'react';
import {Icon} from 'react-native-elements';
import {Alert} from 'react-native';
import {
  ItemFormPayment,
  ContainerInfoPayment,
  BoxInfoPayment,
  ImagePayment,
  TextInfoPayment,
  TextTypePayment,
} from './style';
import Card from '~/components/Card';

const CardCreditCard: React.FC = ({theme, data, inactivateCard}) => {
  return (
    <ItemFormPayment>
      <Card direction>
        <ContainerInfoPayment>
          <BoxInfoPayment>
            <ImagePayment source={{uri: data.image}} />
            <TextInfoPayment>{data.banner}</TextInfoPayment>
          </BoxInfoPayment>
          <BoxInfoPayment>
            <TextInfoPayment>{data.cardNumber}</TextInfoPayment>
          </BoxInfoPayment>
          <TextTypePayment>Crédito</TextTypePayment>
        </ContainerInfoPayment>
        <Icon
          type="ionicon"
          name={'trash-outline'}
          color={theme.colors.badge}
          size={20}
          onPress={() => {
            Alert.alert('', 'Deseja Inativar seu cartão de credito?', [
              {
                text: 'Não',
                onPress: () => console.log('Cancel Pressed'),
                style: 'cancel',
              },
              {
                text: 'Sim',
                onPress: () => {
                  inactivateCard(data.id);
                },
              },
            ]);
          }}
        />
      </Card>
    </ItemFormPayment>
  );
};

export default CardCreditCard;
