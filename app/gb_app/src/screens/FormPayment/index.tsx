import React, {useEffect, useState} from 'react';
import {Alert} from 'react-native';
import {useTheme} from 'styled-components/native';
import Header from '~/components/Header';
import * as typePaymentService from '~/services/formPayment';
import CardCreditCard from './CardCreditCard/index';
import {LoadFull} from '~/components/index';
import {Container, HeaderTitle, Content} from './style';

const FormPayment: React.FC = () => {
  const [listCard, setListCard] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const theme = useTheme();

  const getListCards = async () => {
    try {
      setIsLoading(true);
      const data = await typePaymentService.getCreditCards();
      setListCard(data);
      setIsLoading(false);
    } catch (error) {
      Alert.alert('', 'Não foi possível listar seus cartões');
      setIsLoading(false);
    }
  };

  const inactivateCard = async id => {
    try {
      setIsLoading(true);
      const body = {
        id_cartao: id,
      };
      const data = await typePaymentService.inactivateCreditCards(body);
      getListCards();
      setIsLoading(false);
    } catch (error) {
      setIsLoading(false);
      Alert.alert('', 'Não foi possível inativar seu cartão');
    }
  };

  useEffect(() => {
    getListCards();
  }, []);

  return (
    <>
      {isLoading && <LoadFull />}
      <Container>
        <Header />
        <HeaderTitle>Formas de Pagamento</HeaderTitle>
        <Content>
          {listCard.map(item => (
            <CardCreditCard
              theme={theme}
              data={item}
              key={item.id}
              inactivateCard={inactivateCard}
            />
          ))}
        </Content>
      </Container>
    </>
  );
};

export default FormPayment;
