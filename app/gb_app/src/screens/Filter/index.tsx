import React, {useState, useCallback, useEffect, useMemo, useRef} from 'react';
import {RFValue} from 'react-native-responsive-fontsize';
import {useTheme} from 'styled-components/native';
import {useNavigation, useRoute} from '@react-navigation/native';
import {Modalize} from 'react-native-modalize';

import {useFilter} from '~/hooks/filter';

import Button from '~/components/Form/Button';
import Header from '~/components/Header';
import SelectModal2, {SelectProps} from '~/components/Form/SelectModal2';
import SelectButton from '~/components/Form/SelectButton';

import {
  DataProps,
  getCategoriesFilter,
  getCitiesFilter,
  getRegionsFilter,
  getValuesFilter,
  getValuesCashbackFilter,
} from '~/services/filter';

import {
  Container,
  ScrollViewFilter,
  InputGroupForm,
  Label,
  ContentFilter,
  TitleFilter,
  ContainerButtonFilter,
} from './style';

interface GroupSelectProps {
  [key: string]: SelectProps;
}

const ModalFilter: React.FC = () => {
  const modalizeRef = useRef<Modalize>(null);
  const {params} = useRoute<any>();

  const [typeFilter, setTypeFilter] = useState<'Bussiness' | 'Product'>(
    'Product',
  );
  const [dataSelect, setDataSelect] = useState<SelectProps>({} as SelectProps);

  const [category, setCategory] = useState<DataProps>({} as DataProps);
  const [region, setRegion] = useState<DataProps>({} as DataProps);
  const [city, setCity] = useState<DataProps>({} as DataProps);
  const [price, setPrice] = useState<DataProps>({} as DataProps);
  const [cashback, setCashback] = useState<DataProps>({} as DataProps);

  const {productFilter, bussinessFilter, setItemsFilter, clearFilter} =
    useFilter();
  const {goBack} = useNavigation();

  const theme = useTheme();

  const groupSelect = useMemo(() => {
    const items: GroupSelectProps = {
      categories: {
        title: 'Escolha uma categoria',
        data: getCategoriesFilter,
        onPress: setCategory,
      },
      cities: {
        title: 'Escolha uma cidade',
        data: getCitiesFilter,
        onPress: setCity,
      },
      regions: {
        title: 'Escolha uma região',
        data: getRegionsFilter,
        onPress: setRegion,
      },
      prices: {
        title: 'Escolha uma faixa de Valores',
        data: getValuesFilter,
        onPress: setPrice,
      },
      cashback: {
        title: 'Escolha uma faixa de Cashback',
        data: getValuesCashbackFilter,
        onPress: setCashback,
      },
    };
    return items;
  }, []);

  const openFilter = (key: string) => {
    if (groupSelect.hasOwnProperty(key)) {
      setDataSelect(groupSelect[key]);
      modalizeRef.current?.open();
    }
  };

  const handleSelectFilter = useCallback(() => {
    setItemsFilter(typeFilter, {category, region, city, price, cashback});
    goBack();
  }, [
    cashback,
    category,
    city,
    goBack,
    price,
    region,
    setItemsFilter,
    typeFilter,
  ]);

  const handClearFilter = useCallback(() => {
    setCategory({} as DataProps);
    setRegion({} as DataProps);
    setCity({} as DataProps);
    setPrice({} as DataProps);
    setCashback({} as DataProps);
    clearFilter(typeFilter);
  }, [clearFilter, typeFilter]);

  useEffect(() => {
    setTypeFilter(params.data.type);
  }, [params]);

  useEffect(() => {
    const itemsFilterSet = {
      category: setCategory,
      region: setRegion,
      city: setCity,
      price: setPrice,
      cashback: setCashback,
    };
    if (typeFilter === 'Product' && productFilter.filters) {
      Object.keys(productFilter.filters).forEach(key => {
        if (itemsFilterSet.hasOwnProperty(key)) {
          const funcSet = itemsFilterSet[key];
          funcSet(productFilter.filters[key]);
        }
      });
    } else if (bussinessFilter.filters) {
      Object.keys(bussinessFilter.filters).forEach(key => {
        if (itemsFilterSet.hasOwnProperty(key)) {
          const funcSet = itemsFilterSet[key];
          funcSet(bussinessFilter.filters[key]);
        }
      });
    }
  }, [productFilter, bussinessFilter, typeFilter]);

  return (
    <>
      <Container>
        <Header />
        <ScrollViewFilter>
          {typeFilter === 'Product' && (
            <InputGroupForm>
              <Label>Categorias</Label>
              <SelectButton
                placeholder="Todas as categorias"
                value={category.title}
                onPress={() => openFilter('categories')}
              />
            </InputGroupForm>
          )}
          <ContentFilter>
            <TitleFilter>Ordenar Por</TitleFilter>
            <InputGroupForm>
              <Label>Localização</Label>
              <SelectButton
                placeholder="Todas as Regiões"
                value={region.title}
                onPress={() => openFilter('regions')}
              />
              <SelectButton
                placeholder="Todas as cidades"
                value={city.title}
                onPress={() => openFilter('cities')}
              />
            </InputGroupForm>
          </ContentFilter>
          {typeFilter === 'Product' && (
            <>
              <InputGroupForm>
                <Label>Faixa de preço</Label>
                <SelectButton
                  placeholder="Todas as faixas de preço"
                  value={price.title}
                  onPress={() => openFilter('prices')}
                />
              </InputGroupForm>
              <InputGroupForm>
                <Label>Cashback</Label>
                <SelectButton
                  placeholder="Todas as faixas de cashback"
                  value={cashback.title}
                  onPress={() => openFilter('cashback')}
                />
              </InputGroupForm>
            </>
          )}
        </ScrollViewFilter>
        <ContainerButtonFilter>
          <Button
            block
            fontBold
            uppercase
            background={theme.colors.success}
            color={theme.colors.shape}
            fontSize={RFValue(14)}
            borderRadius={RFValue(5)}
            height={RFValue(40)}
            onPress={handleSelectFilter}>
            Filtrar
          </Button>
          <Button
            block
            fontBold
            uppercase
            background={theme.colors.error}
            color={theme.colors.shape}
            fontSize={RFValue(14)}
            borderRadius={RFValue(5)}
            height={RFValue(40)}
            onPress={handClearFilter}
            style={{marginTop: 10}}>
            Limpar
          </Button>
        </ContainerButtonFilter>
      </Container>
      <SelectModal2
        modalizeRef={modalizeRef}
        data={dataSelect}
        onClose={() => {
          setDataSelect({} as SelectProps);
        }}
      />
    </>
  );
};

export default ModalFilter;
