import styled from 'styled-components/native';
import {RFPercentage, RFValue} from 'react-native-responsive-fontsize';

export const Container = styled.View`
  flex: 1;
  background: ${({theme}) => theme.colors.shape};
`;

export const ScrollViewFilter = styled.ScrollView.attrs({
  contentContainerStyle: {
    paddingBottom: 40,
  },
})`
  padding: ${RFValue(40)}px ${RFValue(30)}px;
  height: ${RFPercentage(80) - 40}px;
`;

export const Text = styled.Text``;

export const InputGroupForm = styled.View`
  margin-bottom: ${RFValue(15)}px;
`;

export const Label = styled.Text`
  font-family: ${({theme}) => theme.fonts.medium};
  color: ${({theme}) => theme.colors.success};
  font-size: ${RFValue(14)}px;
  line-height: ${RFValue(19)}px;
  margin-bottom: ${RFValue(10)}px;
`;

export const ContentFilter = styled.View`
  margin-bottom: ${RFValue(30)}px;
`;

export const TitleFilter = styled.Text`
  font-family: ${({theme}) => theme.fonts.medium};
  font-size: ${RFValue(16)}px;
  text-transform: uppercase;
  color: ${({theme}) => theme.colors.dark};
  margin-bottom: ${RFValue(20)}px;
`;

export const ContainerButtonFilter = styled.View`
  margin: ${RFValue(20)}px ${RFValue(30)}px ${RFValue(65)}px;
  height: ${RFValue(40)}px;
`;
