import React from 'react';
import {MaskService} from 'react-native-masked-text';
import {isEmpty} from 'lodash';
import {Address} from '~/models/Address';
import {CheckBox} from 'react-native-elements';
import {BoxTouchablePercent, Text, BoxPercent} from '~/components/index';

interface ItemAddressPropTypes {
  data: Address;
  onPress: (data: string | null) => void;
  addressSelected: string | null;
  theme: {};
}

const ItemAddress: React.FC<ItemAddressPropTypes> = ({
  data,
  onPress,
  addressSelected,
  theme,
}) => {
  const cep = MaskService.toMask('zip-code', data.postCode);
  return (
    <BoxTouchablePercent
      width="100%"
      mb={10}
      direction
      align="center"
      onPress={onPress}
      borderColor={theme.colors.primary}
      borderTopWidth={2}
      borderBottomWidth={2}
      borderRightWidth={2}
      pt={10}
      pb={10}
      radius={5}
      borderLeftWidth={2}>
      <BoxPercent>
        <CheckBox
          center
          checkedIcon="dot-circle-o"
          uncheckedIcon="circle-o"
          checkedColor={theme.colors.success}
          checked={data.id === addressSelected}
        />
      </BoxPercent>
      <BoxPercent>
        <Text>{cep}</Text>
        <Text>{`${data.street}, Nº ${data.number}${
          isEmpty(data.complement) ? '' : ', ' + data.complement
        }`}</Text>
        <Text>{`${data.neighborhood} ${data.state}  ${data.city}`}</Text>
      </BoxPercent>
    </BoxTouchablePercent>
  );
};

export default ItemAddress;
