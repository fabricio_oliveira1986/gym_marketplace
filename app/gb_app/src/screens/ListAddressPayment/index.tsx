import React, {useCallback, useState, useEffect} from 'react';
import {Alert} from 'react-native';
import {useTheme} from 'styled-components/native';
import {isEmpty} from 'lodash';
import {useNavigation} from '@react-navigation/native';
import * as AddressService from '~/services/address';
import {Address} from '~/models/Address';
import ModalAddress from '~/components/ModalAddress/index';
import Header from '~/components/Header';
import Button from '~/components/Form/Button';
import ItemAddress from './ItemAddress';
import {LoadFull, BoxWarning, BoxPercent} from '~/components/index';
import {useCart} from '~/hooks/cart';
import {Container, HeaderTitle, Content, Box} from './style';

const ListAddressPayment: React.FC = () => {
  const theme = useTheme();
  const navigation = useNavigation();
  const [addresses, setAddresses] = useState<Address[]>([]);
  const [dataAddressEdit, setDataAddressEdit] = useState({});
  const [addressSelected, setAddressSelected] = useState('');
  const [isVisibleModal, setIsVisibleModal] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const {address, updateAddress} = useCart();

  const getAddresses = useCallback(async () => {
    try {
      setIsLoading(true);
      const item = await AddressService.getAddresses();
      setAddresses(item);
      setIsLoading(false);
    } catch (e) {
      setIsLoading(false);
      Alert.alert(
        'Erro',
        'Não foi possível carregar seus endereços para entrega',
      );
    }
  }, [setAddresses]);

  useEffect(() => {
    isVisibleModal === false && getAddresses();
  }, [isVisibleModal]);

  return (
    <Container>
      <Header />
      <HeaderTitle>Meu endereço</HeaderTitle>
      {isLoading && <LoadFull />}
      <Content>
        {isEmpty(addresses) && !isLoading && (
          <BoxPercent>
            <BoxWarning
              text={'Não existe endereços cadastrados.\nCadastre um endereço.'}
            />
          </BoxPercent>
        )}
        {addresses.map((address, index) => {
          return (
            <ItemAddress
              data={address}
              key={index}
              addressSelected={addressSelected}
              theme={theme}
              onPress={() => {
                setAddressSelected(address.id);
                updateAddress(address);
                navigation.navigate('Payment');
              }}
            />
          );
        })}
      </Content>

      <Box>
        <Button
          onPress={() => {
            setDataAddressEdit({});
            setIsVisibleModal(true);
          }}
          block
          fontBold
          uppercase
          background={theme.colors.success}
          color={theme.colors.shape}
          fontSize={14}
          borderRadius={5}>
          Adicionar
        </Button>
      </Box>

      <ModalAddress
        onRequestClose={() => {
          setIsVisibleModal(!isVisibleModal);
        }}
        setIsVisibleModal={setIsVisibleModal}
        isVisibleModal={isVisibleModal}
        dataAddressEdit={dataAddressEdit}
      />
    </Container>
  );
};

export default ListAddressPayment;
