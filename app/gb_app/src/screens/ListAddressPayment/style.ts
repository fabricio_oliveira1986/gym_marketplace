import styled from 'styled-components/native';
import { RFValue } from 'react-native-responsive-fontsize';

export const Container = styled.View``;

export const HeaderTitle = styled.Text`
  color: ${({ theme }) => theme.colors.text};
  font-family: ${({ theme }) => theme.fonts.medium};
  font-size: ${RFValue(16)}px;
  line-height: ${RFValue(21)}px;
  margin: ${RFValue(25)}px ${RFValue(20)}px 0;
`;

export const Content = styled.ScrollView.attrs({
  showsVerticalScrollIndicator: false,
})`
  margin: ${RFValue(25)}px ${RFValue(20)}px ${RFValue(100)}px;
  height: 65%;
`;

export const Box = styled.View`
  width: 100%;
  position: absolute;
  height: 50px;
  bottom: 10%;
  padding: ${RFValue(25)}px ${RFValue(20)}px 0;
`;
