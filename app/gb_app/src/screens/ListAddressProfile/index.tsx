import React, {useCallback, useState, useEffect} from 'react';
import {useTheme} from 'styled-components/native';
import * as AddressService from '~/services/address';
import {Address} from '~/models/Address';
import ModalAddress from '~/components/ModalAddress/index';
import Card from '~/components/Card';
import Header from '~/components/Header';
import Button from '~/components/Form/Button';
import ImageIcon from '~/assets/images/Icons';

import {
  Container,
  HeaderTitle,
  Content,
  ItemAddress,
  BoxAddressInfo,
  AddressInfo,
  Box,
} from './style';

const ListAddress: React.FC = () => {
  const theme = useTheme();
  const [addresses, setAddresses] = useState<Address[]>([]);
  const [isVisibleModal, setIsVisibleModal] = useState(false);
  const [dataAddressEdit, setDataAddressEdit] = useState({});

  const getAddresses = useCallback(async () => {
    try {
      const item = await AddressService.getAddresses();
      setAddresses(item);
    } catch (e) {
      console.log(e);
    }
  }, [setAddresses]);

  useEffect(() => {
    isVisibleModal === false && getAddresses();
  }, [isVisibleModal]);

  return (
    <Container>
      <Header />
      <HeaderTitle>Endereços de Entrega</HeaderTitle>
      <Content>
        {addresses.map((address, index) => (
          <ItemAddress
            key={index}
            onPress={() => {
              setDataAddressEdit(address);
              setIsVisibleModal(true);
            }}>
            <Card direction>
              <BoxAddressInfo>
                <AddressInfo>
                  {address.street}, {address.number}
                  {address.complement ? ', ' + address.complement : ''}{' '}
                </AddressInfo>
                <AddressInfo>
                  {address.neighborhood}, {address.city}
                </AddressInfo>
                <AddressInfo>{address.postCode}</AddressInfo>
              </BoxAddressInfo>
              <ImageIcon.Edit width={15} height={15} fill={theme.colors.edit} />
            </Card>
          </ItemAddress>
        ))}

        <ModalAddress
          onRequestClose={() => {
            setIsVisibleModal(!isVisibleModal);
          }}
          setIsVisibleModal={setIsVisibleModal}
          isVisibleModal={isVisibleModal}
          dataAddressEdit={dataAddressEdit}
        />
      </Content>

      <Box>
        <Button
          onPress={() => {
            setDataAddressEdit({});
            setIsVisibleModal(true);
          }}
          block
          fontBold
          uppercase
          background={theme.colors.success}
          color={theme.colors.shape}
          fontSize={14}
          borderRadius={5}>
          Adicionar
        </Button>
      </Box>
    </Container>
  );
};

export default ListAddress;
