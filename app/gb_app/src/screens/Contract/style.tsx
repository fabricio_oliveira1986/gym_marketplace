import styled from 'styled-components/native';
import {RFValue} from 'react-native-responsive-fontsize';

export const Container = styled.KeyboardAvoidingView`
  flex: 1;
`;

export const Title = styled.Text`
  font-size: ${RFValue(25)}px;
  text-align: center;
  margin-bottom: ${RFValue(15)}px;
  color: ${({theme}) => theme.colors.success};
  font-weight: bold;
`;
