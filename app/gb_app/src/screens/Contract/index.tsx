import React, {useEffect, useState} from 'react';
import {useNavigation} from '@react-navigation/native';
import {useWindowDimensions, Alert} from 'react-native';
import HTML from 'react-native-render-html';
import {
  Background,
  LoadFull,
  Box,
  Card,
  BoxPercent,
  Button,
  ContentScroll,
} from '~/components/index';
import {Container, Title} from './style';
import {useTheme} from 'styled-components/native';
import * as userServices from '~/services/user';

const Contract: React.FC = () => {
  const {width: contentWidth} = useWindowDimensions();
  const [isLoading, setIsLoading] = useState(false);
  const [contract, setContract] = useState(false);
  const navigation = useNavigation();
  const theme = useTheme();

  const getContract = async () => {
    try {
      setIsLoading(true);
      const data = await userServices.getTerms();
      setContract(data);
      setIsLoading(false);
    } catch (error) {
      console.log('valor de error =>', error);
      setIsLoading(false);
      Alert.alert('', 'Não foi possível carregar os termos de uso', [
        {
          text: 'OK',
          onPress: () => {
            navigation.goBack();
          },
        },
      ]);
    }
  };

  useEffect(() => {
    getContract();
  }, []);

  return (
    <Container>
      <Background>
        <BoxPercent width="90%" self="center">
          <Card>
            <ContentScroll height="90%">
              <Box>
                {isLoading && <LoadFull />}
                <Title>Termos de Uso</Title>
                <HTML
                  source={{
                    html: contract || '<div></div>',
                  }}
                  contentWidth={contentWidth}
                />
                <Button
                  fontSize={14}
                  color={theme.colors.shape}
                  background={theme.colors.success}
                  onPress={() => {
                    navigation.goBack();
                  }}>
                  Voltar
                </Button>
              </Box>
            </ContentScroll>
          </Card>
        </BoxPercent>
      </Background>
    </Container>
  );
};

export default Contract;
