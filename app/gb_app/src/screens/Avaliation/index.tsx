import React from 'react';

import {useTheme} from 'styled-components/native';

import Button from '~/components/Form/Button';
import Card from '~/components/Card';
import Star from '~/components/Star';
import Header from '~/components/Header';

import {
  Container,
  Body,
  CardTitle,
  ContainerItems,
  ItemAvaliation,
  TextAvaliation,
  ContainerStarAvaliation,
  StarTitle,
  ContainerButton,
} from './style';

const Avaliation: React.FC = () => {
  const theme = useTheme();
  return (
    <Container>
      <Header />
      <Body>
        <Card>
          <CardTitle>Avalie seu pedido</CardTitle>
          <ContainerItems>
            <ItemAvaliation>
              <TextAvaliation>2x Whey Optimun Nutrition</TextAvaliation>
              <TextAvaliation>R$ 300,00</TextAvaliation>
            </ItemAvaliation>
            <ItemAvaliation>
              <TextAvaliation>1x Best Vegan Atlhetica</TextAvaliation>
              <TextAvaliation>R$ 150,00</TextAvaliation>
            </ItemAvaliation>
          </ContainerItems>
          <ContainerStarAvaliation>
            <StarTitle>Que nota você dá para essa compra?</StarTitle>
            <Star color={theme.colors.warning} starNumber={1} />
            <ContainerButton>
              <Button
                width={190}
                fontBold
                uppercase
                background={theme.colors.success}
                color={theme.colors.shape}
                fontSize={14}
                borderRadius={5}
                onPress={() => {}}>
                Avaliar
              </Button>
            </ContainerButton>
          </ContainerStarAvaliation>
        </Card>
      </Body>
    </Container>
  );
};

export default Avaliation;
