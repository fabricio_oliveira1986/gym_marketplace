import styled from 'styled-components/native';
import {RFValue} from 'react-native-responsive-fontsize';

export const Container = styled.View`
  flex: 1;
`;

export const Body = styled.View`
  flex: 1;
  padding: ${RFValue(30)}px ${RFValue(15)}px;
`;

export const CardTitle = styled.Text`
  font-family: ${({theme}) => theme.fonts.medium};
  font-size: ${RFValue(16)}px;
  line-height: ${RFValue(26)}px;
  letter-spacing: 1px;
  text-transform: uppercase;
  text-align: center;
  color: ${({theme}) => theme.colors.text};
  margin-bottom: ${RFValue(20)}px;
`;

export const ContainerItems = styled.View``;

export const ItemAvaliation = styled.View`
  flex-direction: row;
  width: 100%;
  justify-content: space-between;
  padding-bottom: ${RFValue(5)}px;
  border-bottom-width: 1px;
  border-bottom-color: ${({theme}) => theme.colors.muted};
  padding: ${RFValue(15)}px ${RFValue(5)}px;
`;

export const TextAvaliation = styled.Text`
  font-family: ${({theme}) => theme.fonts.medium};
  font-size: ${RFValue(14)}px;
  line-height: ${RFValue(19)}px;
  color: ${({theme}) => theme.colors.title};
`;

export const ContainerStarAvaliation = styled.View`
  margin: ${RFValue(20)}px 0;
`;

export const StarTitle = styled.Text`
  font-family: ${({theme}) => theme.fonts.medium};
  font-size: ${RFValue(16)}px;
  color: ${({theme}) => theme.colors.title};
  text-align: center;
  margin-bottom: ${RFValue(15)}px;
`;

export const ContainerButton = styled.View`
  align-items: center;
  margin-top: ${RFValue(20)}px;
`;
