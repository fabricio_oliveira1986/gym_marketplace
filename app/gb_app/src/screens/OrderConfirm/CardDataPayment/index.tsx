import React from 'react';
import {Text, Card, Box} from '~/components/index';
import {getTypePayment} from '~/global/utils/functions';
import {isEmpty} from 'lodash';
import {Payment} from '~/models/Payment';
import {Freight} from '~/models/Freight';
import {numberReal} from '~/global/utils/functions';

interface ICardDataPayment {
  total: number;
  theme: {};
  typePayment: string;
  dataCC: Payment | undefined;
  subTotal: number;
  cashBack: number;
  splots: number;
  freight: Freight | undefined;
}
const CardDataPayment: React.FC<ICardDataPayment> = ({
  theme,
  typePayment,
  total,
  subTotal,
  dataCC,
  cashBack,
  splots,
  freight,
}) => {
  return (
    <Card>
      <Text fontSize={18} color={theme.colors.success} bold>
        Dados de pagamento
      </Text>

      <Box mt={10}>
        <Text>
          Forma de Pagamento: {getTypePayment(typePayment).description}{' '}
        </Text>
        {!isEmpty(dataCC) && (
          <>
            {dataCC?.method.name && <Text>Nome: {dataCC?.method.name} </Text>}
            <Text>Numero cartão: {dataCC?.method.numberCard} </Text>
            <Text>Validade do cartão: {dataCC?.method.validate} </Text>
          </>
        )}
      </Box>
      <Box>
        {!isEmpty(freight) && (
          <Text fontSize={14} mt={3}>
            Valor do Frete: {numberReal(freight.value)}
          </Text>
        )}

        {typePayment !== 'T' && <Text>CashBack: {numberReal(cashBack)} </Text>}
        {splots > 1 ? (
          <>
            <Text>
              Parcelamento: {splots}x {numberReal(total / splots)}
            </Text>
            <Text>Valor Total: {numberReal(total)}</Text>
          </>
        ) : (
          <Text>Valor Total: {numberReal(total)}</Text>
        )}
      </Box>
    </Card>
  );
};

export default CardDataPayment;
