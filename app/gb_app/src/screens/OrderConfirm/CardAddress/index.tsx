import React from 'react';
import {isEmpty} from 'lodash';
import {Text, Card} from '~/components/index';

const CardAddress: React.FC = ({theme, data}) => {
  return (
    <Card>
      <Text fontSize={18} color={theme.colors.success} bold>
        Dados de entrega
      </Text>

      <Text fontSize={14} mt={10}>
        {`${data.street}, Nº ${data.number} ${
          isEmpty(data.complement) ? '' : ', ' + data.complement
        }`}
      </Text>
      <Text fontSize={14} mt={3}>
        {`${data.neighborhood}, ${data.city}, ${data.state}`}
      </Text>
      <Text fontSize={14} mt={3}>
        {data.postCode}
      </Text>
    </Card>
  );
};

export default CardAddress;
