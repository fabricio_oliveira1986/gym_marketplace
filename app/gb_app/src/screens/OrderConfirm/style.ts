import styled from 'styled-components/native';
import { RFValue } from 'react-native-responsive-fontsize';

export const Container = styled.View`
    flex: 1;
`;
export const ItemImage = styled.Image.attrs({
  resizeMode: 'cover',
})`
  width: ${RFValue(65)}px;
  height: ${RFValue(65)}px;
  border-radius: 9999px;
`;
