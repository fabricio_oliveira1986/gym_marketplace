import React from 'react';
import {Text, Card, Box, BoxPercent} from '~/components/index';
import {ItemImage} from '../style';

const CardDataSeller: React.FC = ({theme, data}) => {
  return (
    <Card>
      <Text fontSize={18} color={theme.colors.success} bold mb={10}>
        Dados do Vendedor
      </Text>

      <BoxPercent width="100%" direction align="center">
        <ItemImage
          source={{
            uri: data.image,
          }}
        />

        <Box pl={10}>
          <Text>{data.name}</Text>
        </Box>
      </BoxPercent>
    </Card>
  );
};

export default CardDataSeller;
