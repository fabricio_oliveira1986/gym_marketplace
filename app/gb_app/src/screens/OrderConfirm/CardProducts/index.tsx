import React from 'react';
import {Text, Card, BoxPercent, Box} from '~/components/index';
import {useWindowDimensions} from 'react-native';
import HTML from 'react-native-render-html';
import {ItemImage} from '../style';

const CardDataPayment: React.FC = ({theme, data}) => {
  const {width: contentWidth} = useWindowDimensions();

  return (
    <Card>
      <Text fontSize={18} color={theme.colors.success} bold>
        Produtos
      </Text>

      {data.map(item => (
        <BoxPercent
          borderBottomWidth={1}
          borderColor={theme.colors.success}
          pt={10}
          pb={10}
          width="100%"
          key={item.product.id}
          direction>
          <ItemImage
            source={{
              uri: item.product.image,
            }}
          />
          <Box pl={10} style={{width: '80%'}}>
            <Text>{item.product.title}</Text>
            <Text>{item.product.subTitle}</Text>
            <HTML
              source={{
                html: item.product.description || '<div></div>',
              }}
              contentWidth={contentWidth}
            />
            <Text>R$ {item.product.value}</Text>
          </Box>
        </BoxPercent>
      ))}
    </Card>
  );
};

export default CardDataPayment;
