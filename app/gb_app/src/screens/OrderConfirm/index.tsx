import React, {useState} from 'react';
import {Alert} from 'react-native';
import {
  Header,
  HeaderTitle,
  Button,
  ContentScroll,
  LoadFull,
} from '~/components/index';
import {useTheme} from 'styled-components';
import {Container} from './style';
import CardDataPayment from './CardDataPayment';
import * as paymentService from '~/services/payment';
import CardAddress from './CardAddress';
import CardProducts from './CardProducts';
import CardDataSeller from './CardDataSeller';
import {useCart} from '~/hooks/cart';

const OrderConfirm: React.FC = props => {
  const theme = useTheme();
  const [isLoading, setIsLoading] = useState(false);
  const {
    items,
    freight,
    address,
    payment: paymentItem,
    subTotal,
    total,
    cashBack,
  } = useCart();

  const {
    params: {typePayment},
  } = props.route;

  const registerPayment = async () => {
    try {
      setIsLoading(true);
      const body = {
        setVenda: 'S',
        txtTipoDocumento: typePayment,
        txtIdFrete: parseInt(freight.id),
        txtIdEndereco: parseInt(address.id),
        txtParcelas: paymentItem?.splots ? paymentItem?.splots : 1,
        txtIdAnuncio: items.map(data => data.product.id),
        txtQuantidade: items.map(data => data.quantity),
        ...(paymentItem &&
          paymentItem.type === 'credit_card_saved' && {
            txtIdCartao: paymentItem.method.txtIdCartao,
          }),
      };
      if (paymentItem && paymentItem.type === 'credit_card') {
        const {name, numberCard, codeSecurity, validate, saveCard, hash} =
          paymentItem.method;
        Object.assign(body, {
          txtCardNome: name,
          txtCardNumero: numberCard,
          txtCardData: `01/${validate}`,
          txtCardCvv: codeSecurity,
          txtCardHash: hash,
          txtCardSalvar: saveCard,
        });
      }
      const response = await paymentService.makePayment(body);
      setIsLoading(false);
      if (response?.id_venda) {
        props.navigation.reset({
          index: 0,
          routes: [
            {
              name: 'OrderSuccess',
              params: {
                id_venda: response.id_venda,
                typePayment,
              },
            },
          ],
        });
      } else {
        throw new Error(response);
      }
    } catch (error) {
      setIsLoading(false);
      Alert.alert(
        'Erro',
        error?.message
          ? error.message
          : 'Não foi possível realizar sua compra, tente novamente mais tarde.',
      );
    }
  };

  return (
    <Container bgColor={theme.colors.background}>
      <Header />
      <HeaderTitle mb={15}>Confirmar dados da compra</HeaderTitle>
      {isLoading && <LoadFull />}
      <ContentScroll pr={15} pl={15}>
        <CardDataPayment
          theme={theme}
          typePayment={typePayment}
          dataCC={paymentItem}
          subTotal={subTotal}
          total={total}
          freight={freight}
          cashBack={cashBack}
          splots={paymentItem?.splots ? paymentItem?.splots : 1}
        />
        <CardAddress theme={theme} data={address} />
        <CardProducts theme={theme} data={items} />
        {items.length > 0 && (
          <CardDataSeller theme={theme} data={items[0].product.bussiness} />
        )}
        <Button
          onPress={() => {
            registerPayment();
          }}
          block
          fontBold
          uppercase
          background={theme.colors.success}
          color={theme.colors.shape}
          fontSize={14}
          style={{marginBottom: 50, marginTop: 50}}
          borderRadius={5}>
          Confirmar
        </Button>
      </ContentScroll>
    </Container>
  );
};

export default OrderConfirm;
