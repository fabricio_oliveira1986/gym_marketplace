import React from 'react';
import {Card, BoxPercent, HR, Text} from '~/components/index';
import {isEmpty} from 'lodash';
import Switch from '~/components/Form/Switch';
import Feather from 'react-native-vector-icons/Feather';
import Image from '~/assets/images/Payments';
import {ICreditCardSaved, IPaymentData} from '~/models/Payment';
import {
  ItemAccordion,
  CardHeader,
  CardHeaderText,
  CardBody,
  ContainerImageCartao,
  ImageCartao,
  FormCartao,
  FormInput,
  Row,
  ContainerSwitch,
  Gutter,
  Column,
  ContainerPlots,
  Separator,
  LabelSwitch,
  LabelPlots,
  BoxSelectPlots,
  ContainerButton,
  ButtonForm,
  TextSelectPlots,
} from '../style';
import ImageIcon from '~/assets/images/Icons';
import ListCreditCard from '../Components/ListCreditCard/index';

interface ICardCreditCard {
  creditCards: ICreditCardSaved[];
  theme: {};
  saveCard: boolean;
  setSaveCard: (value: boolean) => void;
  typesPayment: IPaymentData;
}

const CardCreditCard: React.FC<ICardCreditCard> = ({
  control,
  errors,
  saveCard,
  setSaveCard,
  typesPayment,
  handleOpenSplot,
  handleSubmit,
  handlePaymentCard,
  splot,
  creditCards,
  theme,
}) => {
  return (
    <ItemAccordion>
      <Card noPadding={true}>
        <CardHeader>
          <ImageIcon.CreditCard width={24} height={16} />
          <CardHeaderText>Cartão de Crédito</CardHeaderText>
        </CardHeader>

        {!isEmpty(creditCards) && (
          <>
            <ListCreditCard
              data={creditCards}
              theme={theme}
              handleOpenSplot={handleOpenSplot}
              splot={splot}
            />
            <BoxPercent direction width="100%" align="center">
              <HR style={{flex: 1}} mr={20} />
              <Text>OU</Text>
              <HR ml={20} style={{flex: 1}} />
            </BoxPercent>
          </>
        )}

        <CardBody>
          <ContainerImageCartao>
            <ImageCartao
              source={Image.BandeiraCartoes}
              width={75}
              height={188}
            />
          </ContainerImageCartao>
          <FormCartao>
            <FormInput
              placeholder="Nome do Titular"
              control={control}
              name="name"
              error={errors.name && errors.name.message}
            />
            <FormInput
              masked
              style={{paddingVertical: 16, flex: 1}}
              type="credit-card"
              placeholder="Número do Cartão"
              control={control}
              name="numberCard"
              error={errors.numberCard && errors.numberCard.message}
            />
            <Row>
              <Column>
                <FormInput
                  masked
                  options={{
                    mask: '99/9999',
                  }}
                  style={{paddingVertical: 16, flex: 1}}
                  placeholder="Validade"
                  control={control}
                  name="validate"
                  error={errors.validate && errors.validate.message}
                />
              </Column>
              <Gutter />
              <Column>
                <FormInput
                  placeholder="Cod. de Segurança"
                  control={control}
                  name="codeSecurity"
                  style={{paddingVertical: 16, flex: 1}}
                  error={errors.codeSecurity && errors.codeSecurity.message}
                />
              </Column>
            </Row>
            <ContainerSwitch>
              <LabelSwitch>Salvar Cartão</LabelSwitch>
              <Switch enabled={saveCard} setEnabled={setSaveCard} />
            </ContainerSwitch>
            {typesPayment.credit_card > 1 && (
              <>
                <Separator />
                <ContainerPlots>
                  <LabelPlots>Número de Parcelas</LabelPlots>
                  <BoxSelectPlots onPress={handleOpenSplot}>
                    <>
                      <TextSelectPlots>{splot.title}</TextSelectPlots>
                      <Feather name="chevron-down" size={12} />
                    </>
                  </BoxSelectPlots>
                </ContainerPlots>
              </>
            )}
            <ContainerButton>
              <ButtonForm onPress={handleSubmit(handlePaymentCard)} block>
                Finalizar Pedido
              </ButtonForm>
            </ContainerButton>
          </FormCartao>
        </CardBody>
      </Card>
    </ItemAccordion>
  );
};

export default CardCreditCard;
