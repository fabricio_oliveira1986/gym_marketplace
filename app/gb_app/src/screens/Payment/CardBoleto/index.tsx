import React from 'react';
import {BoxTouchablePercent, Card} from '~/components/index';
import {ItemAccordion, CardHeader, CardHeaderText, CardBody} from '../style';
import ImageIcon from '~/assets/images/Icons';

interface ICardBoleto {
  navigation: any;
  setItemPayment: any;
}

const CardBoleto: React.FC<ICardBoleto> = ({navigation, setItemPayment}) => (
  <ItemAccordion>
    <BoxTouchablePercent
      width="100%"
      onPress={() => {
        setItemPayment({});
        navigation.navigate('OrderConfirm', {typePayment: 'B'});
      }}>
      <Card noPadding={true}>
        <CardHeader>
          <ImageIcon.BarsCode width={24} height={16} />
          <CardHeaderText>Boleto Bancário</CardHeaderText>
        </CardHeader>
        <CardBody />
      </Card>
    </BoxTouchablePercent>
  </ItemAccordion>
);

export default CardBoleto;
