import React from 'react';
import {BoxTouchablePercent, Card} from '~/components/index';
import {ItemAccordion, CardHeader, CardHeaderText, CardBody} from '../../style';
import ImageIcon from '~/assets/images/Icons';

interface ICardPix {
  navigation: any;
  setItemPayment: any;
}

const CardPix: React.FC<ICardPix> = ({navigation, setItemPayment}) => (
  <ItemAccordion>
    <BoxTouchablePercent
      width="100%"
      onPress={() => {
        setItemPayment({});
        navigation.navigate('OrderConfirm', {typePayment: 'P'});
      }}>
      <Card noPadding={true}>
        <CardHeader>
          <ImageIcon.BarsCode width={24} height={16} />
          <CardHeaderText>PIX</CardHeaderText>
        </CardHeader>
        <CardBody />
      </Card>
    </BoxTouchablePercent>
  </ItemAccordion>
);

export default CardPix;
