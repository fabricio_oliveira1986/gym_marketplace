import React from 'react';
import {Alert} from 'react-native';
import {BoxTouchablePercent, Card} from '~/components/index';
import {ItemAccordion, CardHeader, CardHeaderText, CardBody} from '../../style';
import ImageIcon from '~/assets/images/Icons';

interface ICardCashBack {
  navigation: any;
  setItemPayment: any;
  balanceCashBack: number;
  total: number;
}

const CardCashBack: React.FC<ICardCashBack> = ({
  navigation,
  setItemPayment,
  balanceCashBack,
  total,
}) => (
  <ItemAccordion>
    <BoxTouchablePercent
      width="100%"
      onPress={() => {
        setItemPayment({});
        balanceCashBack > total
          ? navigation.navigate('OrderConfirm', {typePayment: 'T'})
          : Alert.alert(
              '',
              'O seu saldo de cashback é inferior ao valor total da compra.',
            );
      }}>
      <Card noPadding={true}>
        <CardHeader>
          <ImageIcon.BarsCode width={24} height={16} />
          <CardHeaderText>CashBack</CardHeaderText>
        </CardHeader>
        <CardBody />
      </Card>
    </BoxTouchablePercent>
  </ItemAccordion>
);

export default CardCashBack;
