import React, {useState} from 'react';
import {BoxTouchablePercent, BoxPercent, Box, Text} from '~/components/index';
import {CheckBox} from 'react-native-elements';
import Feather from 'react-native-vector-icons/Feather';
import {useNavigation} from '@react-navigation/native';
import {
  ImagePayment,
  ButtonForm,
  Separator,
  ContainerPlots,
  LabelPlots,
  BoxSelectPlots,
  TextSelectPlots,
} from '../../style';
import {useCart} from '~/hooks/cart';
import {ICreditCardSaved} from '~/models/Payment';
import {convertDataBr} from '~/global/utils/functions';

interface IListCreditCard {
  theme: any;
  data: ICreditCardSaved[];
  handleOpenSplot: () => void;
  splot: string;
}

const ListCreditCard: React.FC<IListCreditCard> = ({
  theme,
  data,
  handleOpenSplot,
  splot,
}) => {
  const {setItemPayment} = useCart();
  const navigation = useNavigation();
  const [checked, setChecked] = useState('');
  return (
    <BoxPercent width="100%" pr={20} pl={20} mt={20} mb={20}>
      {data.map((item: ICreditCardSaved) => (
        <BoxPercent
          key={item.id}
          borderLeftWidth={1}
          borderBottomWidth={1}
          borderRightWidth={1}
          borderTopWidth={1}
          borderColor={theme.colors.title}
          mb={10}
          width="100%"
          radius={9}>
          <BoxTouchablePercent
            width="100%"
            key={item.id}
            pt={5}
            pb={5}
            direction
            align="center"
            onPress={() => setChecked(item.id)}>
            <CheckBox
              center
              onPress={() => setChecked(item.id)}
              checkedIcon="dot-circle-o"
              uncheckedIcon="circle-o"
              checkedColor={theme.colors.success}
              checked={item.id === checked}
            />
            <Box style={{flex: 1}}>
              <ImagePayment source={{uri: item.image}} />
              <Text mt={5}>{item.cardNumber}</Text>
            </Box>
            <Box style={{flex: 1}}>
              <Text>{item.banner}</Text>
              <Text mt={5}>{convertDataBr(item.expiration)}</Text>
            </Box>
          </BoxTouchablePercent>

          {item.id === checked && (
            <BoxPercent width="100%" justify="center" align="center">
              <Separator />
              <BoxPercent width="100%" pr={30}>
                <ContainerPlots>
                  <LabelPlots>Número de Parcelas</LabelPlots>
                  <BoxSelectPlots onPress={handleOpenSplot}>
                    <>
                      <TextSelectPlots>{splot.title}</TextSelectPlots>
                      <Feather name="chevron-down" size={12} />
                    </>
                  </BoxSelectPlots>
                </ContainerPlots>
              </BoxPercent>

              <ButtonForm
                block
                onPress={() => {
                  setItemPayment({
                    type: 'credit_card_saved',
                    method: {
                      txtIdCartao: item.id,
                      numberCard: item.cardNumber,
                      validate: item.expiration,
                      hash: item.banner,
                    },
                    splots: Number(splot.id),
                  });
                  navigation.navigate('OrderConfirm', {typePayment: 'C'});
                }}>
                Finalizar Pedido
              </ButtonForm>
            </BoxPercent>
          )}
        </BoxPercent>
      ))}
    </BoxPercent>
  );
};

export default ListCreditCard;
