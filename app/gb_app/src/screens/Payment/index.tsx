import React, {useState, useEffect, useCallback, useMemo, useRef} from 'react';
import {Alert} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {useForm} from 'react-hook-form';
import * as Yup from 'yup';
import {yupResolver} from '@hookform/resolvers/yup';
import {Modalize} from 'react-native-modalize';
import {decode} from 'html-entities';
import * as paymentService from '~/services/payment';
import {useTheme} from 'styled-components/native';
import * as formPaymentService from '~/services/formPayment';
import SelectModal2, {SelectProps} from '~/components/Form/SelectModal2';
import Header from '~/components/Header';
import {LoadFull} from '~/components/index';
import CardBoleto from './CardBoleto/index';
import CardPix from './Components/CardPix/index';
import CardCashBack from './Components/CardCashBack/index';
import CardCreditCard from './CardCredit/index';
import {Container, Body, Title, Accordion} from './style';
import {IPaymentData} from '~/models/Payment';
import {useCart} from '~/hooks/cart';
import {numberReal} from '~/global/utils/functions';
import _ from 'lodash';

interface DataProps {
  id: string;
  title: string;
}

const Payment: React.FC = () => {
  const {items, setItemPayment, total, balanceCashBack} = useCart();
  const navigation = useNavigation();
  const modalizeRef = useRef<Modalize>(null);
  const [isLoading, setIsLoading] = useState(false);
  const [creditCards, setCreditCards] = useState([]);
  const theme = useTheme();

  const [splot, setSplot] = useState<DataProps>({
    id: '1',
    title: `1x - ${numberReal(total)}`,
  });
  const [saveCard, setSaveCard] = useState(false);
  const [typesPayment, setTypesPayment] = useState<IPaymentData>({
    credit_card: 0,
    boleto: 0,
    pix: 0,
  });

  const schema = Yup.object().shape({
    numberCard: Yup.string().required('Número do cartão obrigatório'),
    name: Yup.string().required('Nome do titular obrigatório'),
    validate: Yup.string().required('Validade do cartão é obrigatório'),
    codeSecurity: Yup.string().required('Código de segurança obrigatório'),
  });

  const {
    control,
    handleSubmit,
    formState: {errors},
  } = useForm({
    resolver: yupResolver(schema),
  });

  const splots = useMemo(() => {
    const qtdSplots = typesPayment.credit_card;
    let values: DataProps[] = [];
    for (let i = 0; i < qtdSplots; i++) {
      const number = i + 1;
      const valueSplot = numberReal(total / number);
      values[i] = {id: number.toString(), title: `${number}x - ${valueSplot}`};
    }
    const data = new Promise<DataProps[]>(resolve => resolve(values));
    const item: SelectProps = {
      title: 'Escolha um parcelamento',
      data: () => data,
      onPress: setSplot,
    };
    return item;
  }, [total, typesPayment.credit_card]);

  const handleOpenSplot = useCallback(() => {
    if (typesPayment.credit_card > 1) {
      modalizeRef.current?.open();
    }
  }, [typesPayment.credit_card]);

  const getMyFormsPayment = async () => {
    try {
      const response = await formPaymentService.getCreditCards();
      setCreditCards(response);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    getMyFormsPayment();
    const allSplots = isDifferentSplots();
    if (allSplots.length > 1) {
      Alert.alert(
        '',
        'Você adicionou intens com  parcelar diferentes. Neste caso o parcelamento segue o do item com menor número parcela. \nCaso queira manter o parcelamento indicado no anúncio, limpe o carrinho e compre o produto separadamente.',
      );
    }
  }, []);

  const isDifferentSplots = useCallback(() => {
    return _.uniq(items.map(item => item.product.plots?.qtd));
  }, [items]);

  const handlePaymentCard = useCallback(
    async form => {
      try {
        setIsLoading(true);
        const {name, numberCard, validate, codeSecurity} = form;
        const hash = '';
        setItemPayment({
          type: 'credit_card',
          method: {
            name,
            numberCard: numberCard.replace(/\D/g, ''),
            codeSecurity,
            validate,
            saveCard,
            hash,
          },
          splots: Number(splot.id),
        });
        setIsLoading(false);
        navigation.navigate('OrderConfirm', {typePayment: 'C'});
      } catch (erro) {
        setIsLoading(false);
        Alert.alert(
          'Erro',
          erro?.details?.errorMessage
            ? decode(erro.details.errorMessage)
            : 'Ocorreu um erro ao efetuar o pagamento',
        );
      }
    },
    [navigation, saveCard, setItemPayment, splot],
  );

  useEffect(() => {
    const idProducts = items.map(item => item.product.id);
    if (idProducts.length) {
      paymentService
        .getPayments({idProducts})
        .then(itemPayments => {
          setTypesPayment({
            boleto: Number(itemPayments.boleto),
            credit_card: Number(itemPayments.credit_card),
            pix: Number(itemPayments.pix),
          });
        })
        .catch(erro => {
          console.log(erro);
          Alert.alert(
            'Erro',
            'Ocorreu um erro ao listar os tipos de pagamentos',
          );
        });
    }
  }, [items]);

  return (
    <>
      <Container>
        <Header />
        <Body>
          {isLoading && <LoadFull />}
          <Title>Pagamento</Title>
          <Accordion>
            <CardCashBack
              navigation={navigation}
              setItemPayment={setItemPayment}
              balanceCashBack={balanceCashBack}
              total={total}
            />
            {typesPayment.pix > 0 && (
              <CardPix
                navigation={navigation}
                setItemPayment={setItemPayment}
              />
            )}
            {typesPayment.boleto > 0 && (
              <CardBoleto
                navigation={navigation}
                setItemPayment={setItemPayment}
              />
            )}
            {typesPayment.credit_card > 0 && (
              <CardCreditCard
                navigation={navigation}
                control={control}
                errors={errors}
                saveCard={saveCard}
                setSaveCard={setSaveCard}
                typesPayment={typesPayment}
                handleOpenSplot={handleOpenSplot}
                handleSubmit={handleSubmit}
                handlePaymentCard={handlePaymentCard}
                creditCards={creditCards}
                splot={splot}
                theme={theme}
              />
            )}
          </Accordion>
        </Body>
      </Container>
      {typesPayment.credit_card > 1 && (
        <SelectModal2
          modalizeRef={modalizeRef}
          data={splots}
          onClose={() => {}}
        />
      )}
    </>
  );
};

export default Payment;
