import styled from 'styled-components/native';
import {RFValue} from 'react-native-responsive-fontsize';

import InputFormComponent from '~/components/Form/InputForm';
import Button from '~/components/Form/Button';

import {theme as themeGlobal} from '~/global/styles/theme';

export const Container = styled.View`
  flex: 1;
  background-color: ${({theme}) => theme.colors.background};
`;

export const Body = styled.ScrollView.attrs({
  showsVerticalScrollIndicator: false,
})`
  flex: 1;
  padding: ${RFValue(30)}px ${RFValue(20)}px 0px;
`;

export const Title = styled.Text`
  font-family: ${({theme}) => theme.fonts.medium};
  font-size: ${RFValue(16)}px;
  line-height: ${RFValue(21)}px;
  color: ${({theme}) => theme.colors.text};
  margin-bottom: ${RFValue(30)}px;
`;

export const Accordion = styled.View`
  flex: 1;
  margin-bottom: ${RFValue(50)}px;
`;

export const ItemAccordion = styled.View`
  margin-bottom: ${RFValue(8)}px;
`;

export const CardHeader = styled.View`
  width: 100%;
  height: ${RFValue(46)}px;
  align-items: center;
  flex-direction: row;
  padding: 0 ${RFValue(20)}px;
`;

export const CardHeaderText = styled.Text`
  font-family: ${({theme}) => theme.fonts.regular};
  font-size: ${RFValue(16)}px;
  line-height: ${RFValue(21)}px;
  margin-left: ${RFValue(15)}px;
`;

export const CardBody = styled.View`
  padding: 0 ${RFValue(20)}px;
`;

export const ContainerImageCartao = styled.View`
  width: 100%;
  align-items: center;
`;

export const ImageCartao = styled.Image``;

export const FormCartao = styled.View``;

export const FormInput = styled(InputFormComponent).attrs({
  height: 48,
  fontSize: 14,
  borderRadius: 4,
  flex: 1,
  marginBottom: 15,
  color: themeGlobal.colors.title,
  borderColor: themeGlobal.colors.text,
  background: themeGlobal.colors.shape,
  placeholderTextColor: themeGlobal.colors.text,
})``;

export const Row = styled.View`
  flex-direction: row;
`;

export const Column = styled.View`
  flex: 1;
`;

export const Gutter = styled.View`
  width: ${RFValue(15)}px;
  height: ${RFValue(30)}px;
`;

export const ContainerSwitch = styled.View`
  flex-direction: row;
  margin: ${RFValue(20)}px 0;
  align-items: center;
`;

export const LabelSwitch = styled.Text`
  font-family: ${({theme}) => theme.fonts.regular};
  line-height: ${RFValue(16)}px;
  color: ${({theme}) => theme.colors.title};
  margin-right: ${RFValue(10)}px;
`;

export const Separator = styled.View`
  width: 100%;
  background-color: ${({theme}) => theme.colors.separator};
  height: ${RFValue(1)}px;
  margin-bottom: ${RFValue(30)}px;
`;

export const ContainerPlots = styled.View`
  flex-direction: row;
  align-items: center;
  margin-bottom: ${RFValue(30)}px;
`;

export const LabelPlots = styled.Text`
  flex: 1;
  text-align: right;
  margin-right: ${RFValue(10)}px;
  font-family: ${({theme}) => theme.fonts.regular};
  font-size: ${RFValue(12)}px;
  color: ${({theme}) => theme.colors.text};
`;

export const BoxSelectPlots = styled.TouchableOpacity.attrs({
  activeOpacity: 0.7,
})`
  flex: 1;
  border: ${RFValue(1)}px solid ${({theme}) => theme.colors.text};
  border-radius: ${RFValue(4)}px;
  height: ${RFValue(48)}px;
  justify-content: center;
  align-items: center;
  flex-direction: row;
`;

export const TextSelectPlots = styled.Text`
  font-size: ${RFValue(12)}px;
  font-family: ${({theme}) => theme.fonts.medium};
  color: ${({theme}) => theme.colors.title};
  margin-right: ${RFValue(5)}px;
`;

export const ContainerButton = styled.View`
  margin-bottom: ${RFValue(30)}px;
  width: 100%;
`;

export const ButtonForm = styled(Button).attrs({
  fontBold: true,
  uppercase: true,
  background: themeGlobal.colors.success,
  color: themeGlobal.colors.shape,
  fontSize: 14,
  borderRadius: 5,
})``;

export const TextStatus = styled.Text`
  font-family: ${({theme}) => theme.fonts.medium};
  color: ${({theme}) => theme.colors.success};
  text-transform: uppercase;
  line-height: ${RFValue(34)}px;
  letter-spacing: ${RFValue(2)}px;
  margin-bottom: ${RFValue(15)}px;
`;

export const InfoStatus = styled.Text`
  font-family: ${({theme}) => theme.fonts.medium};
  font-size: ${RFValue(16)}px;
  line-height: ${RFValue(21)}px;
  letter-spacing: ${RFValue(1)}px;
  color: ${({theme}) => theme.colors.title};
  margin-bottom: ${RFValue(10)}px;
  padding: 0 ${RFValue(10)}px;
`;

export const Preview = styled.Text`
  color: ${({theme}) => theme.colors.text};
  font-size: ${RFValue(14)}px;
  font-family: ${({theme}) => theme.fonts.regular};
  line-height: ${RFValue(19)}px;
  margin-bottom: ${RFValue(20)}px;
`;

export const ImagePayment = styled.Image`
  width: ${RFValue(36)}px;
  height: ${RFValue(22)}px;
  margin-right: ${RFValue(8)}px;
`;
