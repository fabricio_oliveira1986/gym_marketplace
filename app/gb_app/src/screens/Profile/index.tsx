import React, {useCallback, useState} from 'react';
import {Alert} from 'react-native';
import {useNavigation, useFocusEffect} from '@react-navigation/native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {useTheme} from 'styled-components/native';
import {Icon} from 'react-native-elements';
import Header from '~/components/Header';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import ImageIcon from '~/assets/images/Icons';
import {useAuth} from '~/hooks/auth';

import {
  Container,
  HeaderProfile,
  HeaderProfileText,
  InfoProfile,
  ListButton,
  ItemButton,
  ContentItemButton,
  ItemTitle,
} from './style';

const Profile: React.FC = () => {
  const navigation = useNavigation();
  const [dataUser, setDataUser] = useState({});
  const {signOut} = useAuth();
  const theme = useTheme();

  const getDataUser = async () => {
    const data = JSON.parse(await AsyncStorage.getItem('@GB:user'));
    setDataUser(data);
  };

  const handleSignOut = useCallback(() => {
    Alert.alert('Sair', 'Você deseja sair?', [
      {
        text: 'Sim',
        onPress: signOut,
      },
      {
        text: 'Não',
        style: 'cancel',
      },
    ]);
  }, [signOut]);

  useFocusEffect(
    React.useCallback(() => {
      getDataUser();
    }, []),
  );

  return (
    <Container>
      <Header />
      <HeaderProfile>
        <ImageIcon.UserLine height={38} width={29} fill={theme.colors.title} />
        <HeaderProfileText>{dataUser.name}</HeaderProfileText>
      </HeaderProfile>
      <InfoProfile>Adicione ou atualize seus dados cadastrais.</InfoProfile>
      <ListButton>
        <ItemButton onPress={() => navigation.navigate('InfoProfile')}>
          <ContentItemButton>
            <ImageIcon.User height={18} width={16} fill={theme.colors.title} />
            <ItemTitle>Dados Pessoais</ItemTitle>
          </ContentItemButton>
        </ItemButton>
        <ItemButton onPress={() => navigation.navigate('ListAddressProfile')}>
          <ContentItemButton>
            <ImageIcon.Location
              height={18}
              width={16}
              fill={theme.colors.title}
            />
            <ItemTitle>Endereços de Entrega</ItemTitle>
          </ContentItemButton>
        </ItemButton>
        <ItemButton onPress={() => navigation.navigate('FormPayment')}>
          <ContentItemButton>
            <ImageIcon.Wallet
              height={18}
              width={16}
              fill={theme.colors.title}
            />
            <ItemTitle>Formas de Pagamento</ItemTitle>
          </ContentItemButton>
        </ItemButton>
        <ItemButton onPress={() => navigation.navigate('MyPurchases')}>
          <ContentItemButton>
            <ImageIcon.ShoppingCart
              height={18}
              width={16}
              fill={theme.colors.title}
            />
            <ItemTitle>Minhas Compras</ItemTitle>
          </ContentItemButton>
        </ItemButton>
        <ItemButton onPress={() => navigation.navigate('ToWithdraw')}>
          <ContentItemButton>
            <Icon
              type="ionicon"
              name={'cash'}
              color={theme.colors.title}
              size={18}
            />
            <ItemTitle>Saque</ItemTitle>
          </ContentItemButton>
        </ItemButton>
        <ItemButton onPress={handleSignOut}>
          <ContentItemButton>
            <ImageIcon.UserLine
              height={18}
              width={16}
              fill={theme.colors.title}
            />
            <ItemTitle>Sair</ItemTitle>
          </ContentItemButton>
        </ItemButton>
      </ListButton>
    </Container>
  );
};

export default Profile;
