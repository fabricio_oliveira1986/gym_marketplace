import styled from 'styled-components/native';
import {RFValue} from 'react-native-responsive-fontsize';

export const Container = styled.View`
  flex: 1;
`;

export const HeaderProfile = styled.View`
  flex-direction: row;
  align-items: center;
  margin: ${RFValue(40)}px ${RFValue(20)}px 0;
`;

export const HeaderProfileText = styled.Text`
  font-family: ${({theme}) => theme.fonts.regular};
  margin-left: ${RFValue(12)}px;
  font-size: ${RFValue(26)}px;
  line-height: ${RFValue(34)}px;
  flex: 1;
`;

export const InfoProfile = styled.Text`
  font-family: ${({theme}) => theme.fonts.regular};
  font-size: ${RFValue(12)}px;
  line-height: ${RFValue(16)}px;
  color: ${({theme}) => theme.colors.text};
  margin: ${RFValue(20)}px ${RFValue(20)}px 0;
`;

export const ListButton = styled.ScrollView`
  margin: ${RFValue(25)}px ${RFValue(20)}px 0;
  flex: 1;
`;

export const ItemButton = styled.TouchableOpacity.attrs({
  activeOpacity: 0.7,
})`
  width: 100%;
  background-color: ${({theme}) => theme.colors.shape};
  padding: ${RFValue(15)}px;
  border-radius: ${RFValue(6)}px;
  margin-bottom: ${RFValue(15)}px;
`;

export const ContentItemButton = styled.View`
  flex-direction: row;
  align-items: center;
`;

export const ItemTitle = styled.Text`
  flex: 1;
  margin-left: ${RFValue(15)}px;
  font-family: ${({theme}) => theme.fonts.regular};
  font-size: ${RFValue(16)}px;
  line-height: ${RFValue(21)}px;
`;
