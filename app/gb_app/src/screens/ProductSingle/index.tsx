import React, {useCallback, useEffect, useRef, useState} from 'react';
import RNFetchBlob from 'rn-fetch-blob';
import {useRoute, useNavigation} from '@react-navigation/native';
import Share from 'react-native-share';
import {Alert, useWindowDimensions, Platform} from 'react-native';
import {useTheme} from 'styled-components';
import HTML from 'react-native-render-html';
import {Modalize} from 'react-native-modalize';
import {useCart} from '~/hooks/cart';
import Counter from '~/components/Form/Counter';
import Swiper from '~/components/Swiper';
import {
  BoxTouchablePercent,
  ModalVideo,
  ModalImage,
  ItemSliderVideo,
  ItemSliderImg,
} from '~/components/index';
import Header from '~/components/Header';
import ListAsks from '~/components/ListAsks';
import {Modal} from '~/components';
import ListAvaliation from '~/screens/ListAvaliation';
import ImagesIcon from '~/assets/images/Icons';
import {Product} from '~/models/Product';
import {
  numberReal,
  numberDecimal,
  numberFormat,
  adaptObjVideo,
} from '~/global/utils/functions';
import {getFreights} from '~/services/order';
import {Freight} from '~/models/Freight';
import * as productService from '~/services/product';

import {
  Container,
  ContainerSingle,
  Item,
  BoxSingle,
  ImageSingle,
  ContentHeader,
  ContainerInfoProduct,
  ContainerInfoValue,
  InfoValue,
  InfoValuePlots,
  ContainerInfoCashback,
  InfoCashback,
  ContainerInfoShipping,
  ButtonShipping,
  TextShipping,
  ContainerInfoTop,
  InfoTop,
  TextInfoTop,
  TextInfoTopBold,
  ButtonAvaliation,
  TextTotalAvaliation,
  BoxRating,
  TextRating,
  SingleBody,
  ContainerTitle,
  Title,
  SubTitle,
  DescriptionSingle,
  BoxButtonAditional,
  ContainerCounter,
  CounterText,
  ButtonAditional,
  ButtonAditionalText,
  ButtonPurchase,
  TextButtonPurchase,
  TitleFreight,
  ListFreights,
  ItemFreight,
  ItemDescriptionFreight,
  ItemValueFreight,
} from './style';

const ProductSingle: React.FC = () => {
  const theme = useTheme();
  const modalizeRef = useRef<Modalize>(null);
  const [product, setProduct] = useState<Product>({} as Product);
  const {params} = useRoute<any>();
  const [isVisibleModalVideo, setIsVisibleModalVideo] = useState(false);
  const [isVisibleModalImg, setIsVisibleModalImg] = useState<boolean>(false);
  const [linkImg, setLinkImg] = useState('');
  const [linkVideo, setLinkVideo] = useState('');

  useEffect(() => {
    if (params?.data) {
      const data = adaptObjVideo(params.data);
      setProduct(data as Product);
    } else if (params?.id) {
      productService
        .getProduct('46')
        .then(item => {
          const data = adaptObjVideo(item);
          setProduct(data as Product);
        })
        .catch(error => {
          console.log(error);
        });
    }
  }, [params]);

  const openVideo = (link: string) => {
    setIsVisibleModalVideo(true);
    setLinkVideo(link);
  };

  const openImage = (link: string) => {
    setIsVisibleModalImg(true);
    setLinkImg(link);
  };

  return (
    <Container>
      <Header />
      {product && (
        <>
          <ContainerSingle>
            {product?.newObjVideo && (
              <Swiper
                data={product.newObjVideo}
                renderItem={({item}) =>
                  item.isVoid ? (
                    <ItemSliderVideo
                      openVideo={() => openVideo(item.linkVideo)}
                      theme={theme}
                      linkVideo={linkVideo}
                      img={item.img}
                    />
                  ) : (
                    <ItemSliderImg
                      img={item.img}
                      openImg={() => openImage(item.img)}
                    />
                  )
                }
              />
            )}
            <Box product={product} modalizeRef={modalizeRef} theme={theme} />
            <ListAsks product={product} />
          </ContainerSingle>
          <ListAvaliation product={product} modalizeRef={modalizeRef} />
          <ModalVideo
            visible={isVisibleModalVideo}
            uri={linkVideo}
            setVisivelModalVod={setIsVisibleModalVideo}
          />
          <ModalImage
            visible={isVisibleModalImg}
            uri={linkImg}
            setVisivelModalImg={setIsVisibleModalImg}
          />
        </>
      )}
    </Container>
  );
};

interface BoxProps {
  product: Product;
  modalizeRef: React.RefObject<Modalize>;
}

const Box: React.FC<BoxProps> = ({product, modalizeRef, theme}) => {
  const {width: contentWidth} = useWindowDimensions();
  const navigation = useNavigation();

  const [quantity, setQuantity] = useState(1);
  const [visibleFreight, setVisibleFreight] = useState(false);
  const {addToCart, checkAddCart} = useCart();
  const [freights, setFreights] = useState<Freight[]>([]);

  const openAvaliation = () => {
    modalizeRef.current?.open();
  };

  const addCart = useCallback(
    async (product: Product) => {
      if (checkAddCart(product)) {
        await addToCart({
          product,
          quantity,
          value: Number(product.value),
        });
        return true;
      } else {
        Alert.alert(
          'Atenção',
          'Você não pode adicionar no carrinho produtos de vendedores diferente.',
        );
        return false;
      }
    },
    [quantity, addToCart, checkAddCart],
  );

  const onShare = async data => {
    try {
      const imageBase64 = await RNFetchBlob.config({
        fileCache: true,
      })
        .fetch('GET', data.image)
        .then(resp => {
          return resp.readFile('base64');
        })
        .then(base64Data => {
          return base64Data;
        });
      const image = `data:image/jpeg;base64,${imageBase64}`;
      const title = `${data.title} \n${data.subTitle} \nR$: ${data.value} \n${data.cashBack}% de CashBack`;
      const options = Platform.select({
        ios: {
          activityItemSources: [
            {
              placeholderItem: {type: 'url', content: image},
              item: {
                default: {
                  type: 'text',
                  content:
                    title +
                    `\n https://gymbrother.com.br/Home/Bussiness/ProductSingle?id=${data.id}`,
                },
              },
              subject: {
                default:
                  title +
                  `\n https://gymbrother.com.br/Home/Bussiness/ProductSingle?id=${data.id}`,
              },
              linkMetadata: {originalUrl: image, url: image, title},
            },
          ],
        },
        default: {
          url: image,
          title,
          message:
            title +
            `\n https://gymbrother.com.br/Home/Bussiness/ProductSingle?id=${data.id}`,
          excludedActivityTypes: [],
        },
      });
      await Share.open(options);
    } catch (error) {}
  };

  const handleAddCart = useCallback(async () => {
    if (await addCart(product)) {
      if (product.bussiness) {
        navigation.navigate('BussinessSingle', {data: product.bussiness});
      }
    }
  }, [product, navigation, addCart]);

  const handleCheckout = useCallback(async () => {
    if (await addCart(product)) {
      navigation.navigate('Order', {
        screen: 'Order',
      });
    }
  }, [product, navigation, addCart]);

  useEffect(() => {
    if (product.type === '1') {
      getFreights({
        idProducts: [product.id],
      }).then(items => setFreights(items));
    }
  }, [product]);

  return (
    <>
      <BoxSingle isMargin={!!product?.images?.length}>
        <ContentHeader>
          <ImageSingle resizeMode="contain" source={{uri: product.image}} />
          <ContainerInfoProduct>
            <ContainerTitle>
              <BoxTouchablePercent
                position="absolute"
                right={1}
                top={0}
                zIndex={999}
                onPress={() => {
                  onShare(product);
                }}>
                <ImagesIcon.Share
                  height={16}
                  width={16}
                  fill={theme.colors.error}
                />
              </BoxTouchablePercent>
              <Title>{product.title}</Title>
              <SubTitle>{product.subTitle}</SubTitle>
            </ContainerTitle>
            <ContainerInfoValue>
              <InfoValue>{numberReal(product.value)}</InfoValue>
              {product.plots && (
                <InfoValuePlots>
                  {product.plots.qtd}x {numberReal(product.plots.value)} sem
                  juros
                </InfoValuePlots>
              )}
            </ContainerInfoValue>
            <ContainerInfoCashback>
              <ImagesIcon.MoneyFlow
                height={16}
                width={16}
                fill={theme.colors.error}
              />
              {product.cashBack && (
                <InfoCashback>
                  {numberDecimal(product.cashBack)}% de cashback
                </InfoCashback>
              )}
            </ContainerInfoCashback>
            {product.type === '1' && (
              <ContainerInfoShipping>
                <ButtonShipping onPress={() => setVisibleFreight(true)}>
                  <>
                    <ImagesIcon.Shipping
                      fill={theme.colors.success}
                      width={24}
                      height={12}
                    />
                    <TextShipping>Ver frete</TextShipping>
                  </>
                </ButtonShipping>
              </ContainerInfoShipping>
            )}
          </ContainerInfoProduct>
        </ContentHeader>
        <ContainerInfoTop>
          <InfoTop>
            {product.bussiness && (
              <>
                <TextInfoTop>Vendido e entregue por</TextInfoTop>
                <TextInfoTopBold>{product.bussiness.name}</TextInfoTopBold>
              </>
            )}
          </InfoTop>
          <ButtonAvaliation onPress={openAvaliation}>
            <BoxRating>
              <ImagesIcon.Star
                height={18}
                width={17}
                fill={theme.colors.warning}
              />
              <TextRating>{numberDecimal(product.stars)}</TextRating>
            </BoxRating>
            <TextTotalAvaliation>
              Total: {product.starsTotal}
            </TextTotalAvaliation>
          </ButtonAvaliation>
        </ContainerInfoTop>
        <SingleBody>
          <DescriptionSingle>
            <HTML
              source={{
                html: product.description || '<div></div>',
              }}
              contentWidth={contentWidth}
            />
          </DescriptionSingle>
        </SingleBody>
        <BoxButtonAditional>
          <ContainerCounter>
            <CounterText>Quantidade:</CounterText>
            <Counter setValueCounter={setQuantity} value={quantity} />
          </ContainerCounter>
          <ButtonAditional onPress={handleAddCart}>
            <ImagesIcon.Cart fill={theme.colors.shape} width={23} height={20} />
            <ButtonAditionalText>Adicionar ao carrinho</ButtonAditionalText>
          </ButtonAditional>
        </BoxButtonAditional>
        <ButtonPurchase onPress={handleCheckout}>
          <TextButtonPurchase>Compre já</TextButtonPurchase>
        </ButtonPurchase>
      </BoxSingle>
      <Modal
        modalVisible={visibleFreight}
        onRequestClose={() => setVisibleFreight(false)}
        width={80}
        align="center">
        <TitleFreight>As opções de fretes</TitleFreight>
        <ListFreights>
          {freights.map(freight => (
            <ItemFreight key={freight.id}>
              <ItemDescriptionFreight>{freight.title}</ItemDescriptionFreight>
              <ItemValueFreight>
                {numberFormat(Number(freight.value), 1)}
              </ItemValueFreight>
            </ItemFreight>
          ))}
        </ListFreights>
      </Modal>
    </>
  );
};

export default ProductSingle;
