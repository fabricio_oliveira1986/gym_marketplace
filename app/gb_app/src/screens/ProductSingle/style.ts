import styled, {css} from 'styled-components/native';
import {RFValue} from 'react-native-responsive-fontsize';
import {Dimensions} from 'react-native';

const {width} = Dimensions.get('window');

interface BoxSingleProps {
  isMargin: boolean;
}

export const Container = styled.View`
  flex: 1;
`;

export const ContainerSingle = styled.ScrollView.attrs({
  showsVerticalScrollIndicator: false,
})`
  flex: 1;
`;

export const Item = styled.Image.attrs({
  resizeMode: 'cover',
})`
  width: ${width}px;
  height: ${RFValue(250)}px;
`;

export const BoxSingle = styled.View<BoxSingleProps>`
  background-color: ${({theme}) => theme.colors.shape};
  ${({isMargin}) =>
    isMargin &&
    css`
      margin-top: -${RFValue(40)}px;
      border-top-left-radius: ${RFValue(30)}px;
      border-top-right-radius: ${RFValue(30)}px;
    `}
  width: 100%;
  flex: 1;
  padding: ${RFValue(30)}px ${RFValue(15)}px;
`;

export const ImageSingle = styled.Image.attrs({
  resizeMode: 'contain',
})`
  width: ${RFValue(70)}px;
  height: ${RFValue(70)}px;
  border-radius: ${RFValue(8)}px;
`;

export const ContentHeader = styled.View`
  flex-direction: row;
  margin-bottom: ${RFValue(15)}px;
`;

export const ContainerInfoProduct = styled.View`
  margin-left: ${RFValue(10)}px;
  justify-content: flex-start;
  flex: 1;
`;

export const ContainerInfoValue = styled.View`
  flex-direction: column;
  align-items: flex-start;
`;

export const InfoValue = styled.Text`
  font-family: ${({theme}) => theme.fonts.medium};
  font-size: ${RFValue(24)}px;
  line-height: ${RFValue(32)}px;
  color: ${({theme}) => theme.colors.title};
`;

export const InfoValuePlots = styled.Text`
  font-family: ${({theme}) => theme.fonts.medium};
  font-size: ${RFValue(14)}px;
  line-height: ${RFValue(16)}px;
  color: ${({theme}) => theme.colors.success};
  margin-bottom: ${RFValue(5)}px;
  margin-left: ${RFValue(5)}px;
  text-align: right;
`;

export const ContainerInfoCashback = styled.View`
  flex-direction: row;
  align-items: center;
  margin-bottom: ${RFValue(3)}px;
`;

export const InfoCashback = styled.Text`
  margin-left: ${RFValue(5)}px;
  font-family: ${({theme}) => theme.fonts.medium};
  font-size: ${RFValue(14)}px;
  line-height: ${RFValue(14)}px;
  color: ${({theme}) => theme.colors.error};
`;

export const ContainerInfoShipping = styled.View`
  margin-top: ${RFValue(5)}px;
`;

export const ButtonShipping = styled.TouchableOpacity.attrs({
  activeOpacity: 0.7,
})`
  flex-direction: row;
  align-items: center;
`;

export const TextShipping = styled.Text`
  margin-left: ${RFValue(7)}px;
  color: ${({theme}) => theme.colors.success};
  font-family: ${({theme}) => theme.fonts.medium};
  font-size: ${RFValue(10)}px;
  line-height: ${RFValue(13)}px;
  text-transform: uppercase;
`;

export const ContainerTitle = styled.View`
  width: 100%;
  position: relative;
`;

export const Title = styled.Text`
  font-family: ${({theme}) => theme.fonts.medium};
  font-size: ${RFValue(16)}px;
  line-height: ${RFValue(18)}px;
  color: ${({theme}) => theme.colors.title};
`;

export const SubTitle = styled.Text`
  font-size: ${RFValue(12)}px;
  font-family: ${({theme}) => theme.fonts.regular};
  line-height: ${RFValue(18)}px;
  color: ${({theme}) => theme.colors.title};
`;

export const ContainerInfoTop = styled.View`
  flex-direction: row;
  justify-content: space-between;
  margin-bottom: ${RFValue(30)}px;
`;

export const InfoTop = styled.View`
  margin-top: ${RFValue(10)}px;
`;

export const TextInfoTop = styled.Text`
  font-size: ${RFValue(12)}px;
  color: ${({theme}) => theme.colors.text};
  font-family: ${({theme}) => theme.fonts.regular};
  margin-bottom: ${RFValue(3)}px;
`;

export const TextInfoTopBold = styled.Text`
  font-size: ${RFValue(12)}px;
  color: ${({theme}) => theme.colors.text};
  font-family: ${({theme}) => theme.fonts.medium};
  margin-bottom: ${RFValue(3)}px;
`;

export const ButtonAvaliation = styled.TouchableOpacity.attrs({
  activeOpacity: 0.7,
})``;

export const TextTotalAvaliation = styled.Text`
  font-size: ${RFValue(12)}px;
  text-transform: uppercase;
  font-family: ${({theme}) => theme.fonts.medium};
  color: ${({theme}) => theme.colors.text};
  margin-top: ${RFValue(3)}px;
  text-align: right;
`;

export const BoxRating = styled.View`
  flex-direction: row;
  align-items: center;
`;

export const TextRating = styled.Text`
  color: ${({theme}) => theme.colors.title};
  font-size: ${RFValue(16)}px;
  font-family: ${({theme}) => theme.fonts.regular};
  margin-left: ${RFValue(5)}px;
`;

export const SingleBody = styled.View`
  padding: ${RFValue(10)}px 0;
  border-bottom-color: ${({theme}) => theme.colors.muted};
  border-top-color: ${({theme}) => theme.colors.muted};
  border-bottom-width: ${RFValue(1)}px;
  border-top-width: ${RFValue(1)}px;
  margin-bottom: ${RFValue(30)}px;
`;

export const BoxTitle = styled.View``;

export const BoxCashBack = styled.View`
  flex-direction: row;
  align-items: center;
`;

export const TextCashBack = styled.Text`
  color: ${({theme}) => theme.colors.error};
  font-family: ${({theme}) => theme.fonts.medium};
  font-size: ${RFValue(14)}px;
  margin-left: ${RFValue(5)}px;
`;

export const DescriptionSingle = styled.View``;

export const TextItem = styled.Text`
  font-size: ${RFValue(14)}px;
  font-family: ${({theme}) => theme.fonts.regular};
  color: ${({theme}) => theme.colors.title};
  margin-bottom: ${RFValue(2)}px;
`;

export const BoxButtonAditional = styled.View`
  flex-direction: row;
  margin-bottom: ${RFValue(30)}px;
  justify-content: space-between;
  align-items: flex-end;
`;

export const ContainerCounter = styled.View`
  align-items: flex-start;
  justify-content: center;
`;

export const CounterText = styled.Text`
  font-family: ${({theme}) => theme.fonts.medium};
  font-size: ${RFValue(10)}px;
  line-height: ${RFValue(12)}px;
  margin-bottom: ${RFValue(5)}px;
  color: ${({theme}) => theme.colors.text};
  text-transform: uppercase;
`;

export const ButtonAditional = styled.TouchableOpacity.attrs({
  activeOpacity: 0.7,
})`
  border-radius: ${RFValue(5)}px;
  background: ${({theme}) => theme.colors.error};
  border-width: ${RFValue(1)}px;
  padding: ${RFValue(10)}px ${RFValue(10)}px;
  height: ${RFValue(40)}px;
  flex-direction: row;
  align-items: center;
`;

export const ButtonAditionalText = styled.Text`
  color: ${({theme}) => theme.colors.shape};
  font-family: ${({theme}) => theme.fonts.medium};
  font-size: ${RFValue(10)}px;
  text-transform: uppercase;
  margin-left: ${RFValue(8)}px;
`;

export const ButtonPurchase = styled.TouchableOpacity.attrs({
  activeOpacity: 0.7,
})`
  height: ${RFValue(40)}px;
  width: 100%;
  background-color: ${({theme}) => theme.colors.success};
  justify-content: center;
  align-items: center;
`;

export const TextButtonPurchase = styled.Text`
  color: ${({theme}) => theme.colors.shape};
  font-family: ${({theme}) => theme.fonts.medium};
  text-transform: uppercase;
  font-size: ${RFValue(14)}px;
`;

export const TitleFreight = styled.Text`
  font-family: ${({theme}) => theme.fonts.medium};
  color: ${({theme}) => theme.colors.title};
  font-size: ${RFValue(16)}px;
  margin-top: ${RFValue(20)}px;
  text-transform: uppercase;
`;

export const ListFreights = styled.View`
  width: 100%;
  margin-top: 20px;
`;

export const ItemFreight = styled.View`
  flex-direction: row;
  justify-content: space-between;
  margin-bottom: ${RFValue(10)}px;
  padding-bottom: ${RFValue(10)}px;
  border-bottom-width: 1px;
  border-bottom-color: ${({theme}) => theme.colors.separator};
`;

export const ItemDescriptionFreight = styled.Text`
  font-family: ${({theme}) => theme.fonts.regular};
  font-size: ${RFValue(14)}px;
`;

export const ItemValueFreight = styled.Text`
  font-family: ${({theme}) => theme.fonts.medium};
  font-size: ${RFValue(14)}px;
`;
