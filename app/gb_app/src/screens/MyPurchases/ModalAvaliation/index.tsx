import React, {useState} from 'react';
import {parseAvaliationStars} from '~/global/utils/functions';
import Star from '~/components/Star';
import {Modal, Button, BoxPercent, Text} from '~/components';

interface ModalProps {
  isVisibleModal: boolean;
  setAvaliationPurchase: () => void;
  setIsVisibleModal: () => void;
}

const ModalAddress: React.FC<ModalProps> = ({
  isVisibleModal,
  setIsVisibleModal,
  setAvaliationPurchase,
  theme,
  data,
}) => {
  const [start, setStart] = useState([]);

  let dataStar;
  if (data.avaliacao > 0) {
    dataStar = {
      starNumber: data.avaliacao - 1,
    };
  } else {
    dataStar = {};
  }

  return (
    <Modal
      modalVisible={isVisibleModal}
      onRequestClose={() => setIsVisibleModal(false)}
      width={90}
      align="center">
      <Text bold fontSize={16} fontFamily={theme.fonts.text_regular}>
        Que nota você dá para essa compra?
      </Text>

      <BoxPercent mt={30} mb={30}>
        <Star
          color={theme.colors.warning}
          {...dataStar}
          size={30}
          editable
          onChangeStar={value => {
            setStart(value);
          }}
        />
      </BoxPercent>

      <Button
        onPress={() => {
          setAvaliationPurchase({
            txtVenda: data.id,
            txtAvaliacao: parseAvaliationStars(start),
          });
        }}
        block
        fontBold
        uppercase
        background={theme.colors.success}
        color={theme.colors.shape}
        fontSize={14}
        borderRadius={5}>
        Avaliar
      </Button>
    </Modal>
  );
};

export default ModalAddress;
