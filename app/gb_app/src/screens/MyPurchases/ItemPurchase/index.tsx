import React from 'react';
import {useTheme} from 'styled-components/native';
import {RFValue} from 'react-native-responsive-fontsize';
import {Text, BoxPercent} from '~/components/index';
import {Badge} from 'react-native-elements';

interface ItemPurchaseTypes {
  label: string;
  data: string | undefined;
  mb?: number;
  badge?: boolean;
  colorBadge?: string;
}

const ItemPurchase: React.FC<ItemPurchaseTypes> = ({
  label,
  data,
  mb,
  badge,
  colorBadge,
}) => {
  const theme = useTheme();
  return (
    <BoxPercent mb={mb || 10} direction width="100%">
      <Text color={theme.colors.title} bold mr={10}>
        {label} -
      </Text>
      {badge ? (
        <BoxPercent width="100%">
          <Badge
            value={data}
            badgeStyle={{
              backgroundColor: colorBadge,
              paddingLeft: RFValue(10),
              paddingRight: RFValue(10),
            }}
            containerStyle={{}}
          />
        </BoxPercent>
      ) : (
        <Text color={theme.colors.title}>{data}</Text>
      )}
    </BoxPercent>
  );
};

export default ItemPurchase;
