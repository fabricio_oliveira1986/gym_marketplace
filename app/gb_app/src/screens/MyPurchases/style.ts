import styled from 'styled-components/native';
import { RFValue } from 'react-native-responsive-fontsize';

export const Container = styled.View`
  flex: 1;
`;

export const Body = styled.ScrollView`
  flex: 1;
  padding: ${RFValue(30)}px ${RFValue(15)}px;
`;

export const HeaderTitle = styled.Text`
  color: ${({ theme }) => theme.colors.text};
  font-family: ${({ theme }) => theme.fonts.medium};
  font-size: ${RFValue(16)}px;
  line-height: ${RFValue(21)}px;
  margin: ${RFValue(25)}px ${RFValue(20)}px 0;
`;

