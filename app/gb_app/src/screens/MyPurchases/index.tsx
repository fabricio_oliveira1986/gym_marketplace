import React, {useState} from 'react';
import {Alert} from 'react-native';
import {useTheme} from 'styled-components/native';
import {useNavigation, useFocusEffect} from '@react-navigation/native';
import CardPurchases from './CardPurchases/index';
import Header from '~/components/Header';
import {Container, Body, HeaderTitle} from './style';
import {Load, Box} from '~/components/index';
import * as PurchasesService from '~/services/purchases';
import * as AvaliationService from '~/services/avaliation';
import ModalAvaliation from './ModalAvaliation/index';

const MyPurchases: React.FC = () => {
  const theme = useTheme();
  const [isLoading, setIsLoading] = useState(false);
  const [isVisibleModal, setIsVisibleModal] = useState(false);
  const [listPurchases, setListPurchases] = useState([]);
  const [dataPurchaseAvaliation, setDataPurchaseAvaliation] = useState([]);
  const navigation = useNavigation();

  const getListPurchases = async () => {
    try {
      setIsLoading(true);
      const data = await PurchasesService.getMyPurchases();
      setListPurchases(data.reverse());
      setIsLoading(false);
    } catch (error) {
      setIsLoading(false);
    }
  };

  useFocusEffect(
    React.useCallback(() => {
      getListPurchases();
    }, []),
  );

  const setAvaliationPurchase = async body => {
    try {
      setIsLoading(true);
      await AvaliationService.setAvaliationPurchase(body);
      await Alert.alert('', 'Sua Avaliação foi salva com sucesso');
      getListPurchases();
      setIsLoading(false);
      setIsVisibleModal(false);
    } catch (error) {
      Alert.alert('', 'Não foi possível salvar sua avaliação');
      setIsLoading(false);
    }
  };

  return (
    <Container>
      <Header />
      <HeaderTitle>Minhas Compras</HeaderTitle>
      <Body>
        {isLoading && <Load />}

        {listPurchases.map((item, index) => {
          return (
            <Box mb={listPurchases.length - 1 === index ? 70 : 0} key={index}>
              <CardPurchases
                navigation={navigation}
                data={item}
                theme={theme}
                setDataPurchaseAvaliation={setDataPurchaseAvaliation}
                setIsVisibleModal={setIsVisibleModal}
              />
            </Box>
          );
        })}
      </Body>
      <ModalAvaliation
        isVisibleModal={isVisibleModal}
        theme={theme}
        data={dataPurchaseAvaliation}
        setAvaliationPurchase={setAvaliationPurchase}
        setIsVisibleModal={setIsVisibleModal}
      />
    </Container>
  );
};

export default MyPurchases;
