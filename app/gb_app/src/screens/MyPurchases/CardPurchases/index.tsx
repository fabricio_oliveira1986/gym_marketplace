import React from 'react';
import {useTheme} from 'styled-components/native';
import Card from '~/components/Card';
import {BoxPercent, Button, Text, Box} from '~/components/index';
import ItemPurchase from '../ItemPurchase/index';
import {
  dateBr,
  getTypeStatusPayment,
  getTypePayment,
  getTypeStatusDelivery,
} from '~/global/utils/functions';

const CardPurchases: React.FC = ({
  navigation,
  data,
  setIsVisibleModal,
  setDataPurchaseAvaliation,
}) => {
  const theme = useTheme();
  return (
    <Card>
      <BoxPercent
        direction
        width="100%"
        pt={10}
        borderBottomWidth={1}
        borderColor={theme.colors.muted}
        pb={14}>
        <BoxPercent pl={12} width="100%">
          <ItemPurchase
            label="Tipo de Pagamento"
            data={getTypePayment(data.tipo_documento).description}
          />

          {data.data_pagamento && (
            <ItemPurchase
              label="Data Pagamento"
              data={dateBr(data.data_pagamento)}
            />
          )}

          {data.data_entrega && (
            <ItemPurchase
              label="Data da Entrega"
              data={dateBr(data.data_entrega)}
            />
          )}

          <ItemPurchase
            badge
            label="Status Pagamento"
            colorBadge={getTypeStatusPayment(data.status).color}
            data={getTypeStatusPayment(data.status).description}
          />
          <ItemPurchase
            badge
            label="Status da Entrega"
            colorBadge={getTypeStatusDelivery(data.entrega).color}
            data={getTypeStatusDelivery(data.entrega).description}
          />
        </BoxPercent>
      </BoxPercent>

      <BoxPercent direction width="100%" justify="space-between" pt={8}>
        {data.status === 'P' && (
          <BoxPercent width="49%">
            <Button
              onPress={() => {
                setDataPurchaseAvaliation(data);
                setIsVisibleModal(true);
              }}
              block
              fontBold
              uppercase
              color={theme.colors.text}
              fontSize={12}
              borderRadius={5}>
              AVALIAR
            </Button>
          </BoxPercent>
        )}

        <BoxPercent
          width={data.status !== 'P' ? '100%' : '49%'}
          borderLeftWidth={1}
          borderColor={theme.colors.muted}>
          <Button
            onPress={() => {
              navigation.navigate('MyPurchaseDetail', {data});
            }}
            block
            fontBold
            uppercase
            color={theme.colors.text}
            fontSize={12}
            borderRadius={5}>
            VER DETALHES
          </Button>
        </BoxPercent>
      </BoxPercent>
    </Card>
  );
};

export default CardPurchases;
