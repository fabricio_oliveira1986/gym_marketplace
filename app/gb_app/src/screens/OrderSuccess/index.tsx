import React, {useState, useEffect} from 'react';
import {BoxPercent, Header, Card, LoadFull} from '~/components/index';
import {BackHandler} from 'react-native';

import {useTheme} from 'styled-components';
import ImageIcon from '~/assets/images/Icons';
import {useCart} from '~/hooks/cart';
import {
  TextStatus,
  InfoStatus,
  Preview,
  ContainerButton,
  ButtonForm,
  Container,
} from './style';
import * as purchasesService from '~/services/purchases';

const OrderSuccess: React.FC = props => {
  const theme = useTheme();
  const {clearCart} = useCart();
  const [isLoading, setIsLoading] = useState(false);
  const {
    params: {id_venda, typePayment},
  } = props.route;

  useEffect(() => {
    clearCart();
    BackHandler.addEventListener('hardwareBackPress', () => true);
  }, []);
  const getDataOrder = async () => {
    try {
      setIsLoading(true);
      const data = await purchasesService.getOnlyPurchases(id_venda);
      props.navigation.navigate('MyPurchaseDetail', {data});
      setIsLoading(false);
    } catch (error) {
      setIsLoading(false);
      console.log('valor de erro =>', error);
    }
  };

  return (
    <Container bgColor={theme.colors.background}>
      <Header />
      {isLoading && <LoadFull />}

      <BoxPercent
        pl={17}
        pr={17}
        align="center"
        justify="center"
        style={{flex: 1}}>
        <Card align="center" style={{width: '100%'}}>
          <ImageIcon.CheckCircle
            fill={theme.colors.shape}
            width={45}
            height={45}
            style={{marginTop: 20}}
          />
          <TextStatus>Sucesso!</TextStatus>
          {typePayment === 'B' && (
            <>
              <InfoStatus>
                O boleto tem prazo de compensação de 1 a 2 dias úteis. Nós não
                reservamos estoque até que seja aprovado. Não perca tempo!
              </InfoStatus>
              {/* <Preview>Previsao de chegada: 18h.</Preview> */}
            </>
          )}

          {typePayment === 'P' && (
            <>
              <InfoStatus>
                Nós não reservamos os itens da compra até que o PIX seja
                efetuado. Não perca tempo!
              </InfoStatus>
              {/* <Preview>Previsao de chegada: 18h.</Preview> */}
            </>
          )}

          {(typePayment === 'C' || typePayment === 'T') && (
            <>
              <InfoStatus>Pagamento realizado com sucessso!</InfoStatus>
              {/* <Preview>Previsao de chegada: 18h.</Preview> */}
            </>
          )}

          <ContainerButton>
            <ButtonForm
              width={190}
              onPress={() => {
                getDataOrder();
              }}>
              Ver detalhes da Compra
            </ButtonForm>
          </ContainerButton>
        </Card>
      </BoxPercent>
    </Container>
  );
};

export default OrderSuccess;
