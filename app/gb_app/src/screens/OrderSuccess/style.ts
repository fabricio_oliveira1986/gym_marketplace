import styled from 'styled-components/native';
import { RFValue } from 'react-native-responsive-fontsize';
import Button from '~/components/Form/Button';
import { theme as themeGlobal } from '~/global/styles/theme';

export const TextStatus = styled.Text`
font-family: ${({ theme }) => theme.fonts.medium};
color: ${({ theme }) => theme.colors.success};
text-transform: uppercase;
line-height: ${RFValue(34)}px;
letter-spacing: ${RFValue(2)}px;
margin-bottom: ${RFValue(15)}px;
`;

export const InfoStatus = styled.Text`
font-family: ${({ theme }) => theme.fonts.medium};
font-size: ${RFValue(16)}px;
line-height: ${RFValue(21)}px;
letter-spacing: ${RFValue(1)}px;
color: ${({ theme }) => theme.colors.title};
margin-bottom: ${RFValue(10)}px;
padding: 0 ${RFValue(10)}px;
text-align: center;
`;

export const Preview = styled.Text`
color: ${({ theme }) => theme.colors.text};
font-size: ${RFValue(14)}px;
font-family: ${({ theme }) => theme.fonts.regular};
line-height: ${RFValue(19)}px;
margin-bottom: ${RFValue(20)}px;
`;

export const ContainerButton = styled.View`
  margin-top: ${({ mt }) => mt ? `${RFValue(mt)}px` : `0px`};
  margin-bottom: ${({ mb }) => mb ? `${RFValue(mb)}px` : `0px`};
  width: 100%;
`;

export const Container = styled.View`
    flex: 1;
`;

export const ButtonForm = styled(Button).attrs({
    fontBold: true,
    uppercase: true,
    background: themeGlobal.colors.success,
    color: themeGlobal.colors.shape,
    fontSize: 14,
    borderRadius: 5,
})``;