import React, {useState, useCallback, useEffect} from 'react';
import Search from '~/components/Search';
import Header from '~/components/Header';
import ListCategories from '~/components/ListCategories';
import ListOferts from '~/components/ListOferts';
import ListProducts from '~/components/ListProducts';
import Load from '~/components/Load';
import * as productService from '~/services/product';
import {Product} from '~/models/Product';

import {Container, ContainerBody, ContentLoad, TitleLoad} from './style';

import {DataValue, useFilter} from '~/hooks/filter';
import ModalLocation from '~/components/ModalLocation';
import {useAuth} from '~/hooks/auth';

interface FilterData {
  [key: string]: string;
}

const Home: React.FC = props => {
  const {user} = useAuth();
  const {productFilter} = useFilter();
  const [products, setProducts] = useState<Product[]>([]);
  const [loading, setLoading] = useState(true);
  const [search, setSearch] = useState('');
  const [order, setOrder] = useState('');
  const [category, setCategory] = useState('');
  const [visibleModalLocation, setVisibleModalLocation] = useState(
    !user?.id ? true : false,
  );
  const [citySelected, setCitySelected] = useState('');

  const {params} = props.route;

  useEffect(() => {
    setTimeout(() => {
      params?.callback && params.callback();
    }, 2000);
  }, []);

  const handleOrder = useCallback(
    (item: string) => {
      order === item ? setOrder('') : setOrder(item);
    },
    [order],
  );

  const handleSearch = useCallback((value: string) => {
    setSearch(value);
  }, []);

  const getCitySelected = useCallback(value => {
    setCitySelected(value);
  }, []);

  const handleCategory = useCallback(
    (item: string) => {
      if (category === item) {
        setCategory('');
        if (
          productFilter?.filters &&
          productFilter.filters.hasOwnProperty('category')
        ) {
          productFilter.filters.category = {} as DataValue;
        }
      } else {
        setCategory(item);
      }
    },
    [category, productFilter.filters],
  );

  useEffect(() => {
    setLoading(true);
    let data = {};
    if (productFilter?.filters) {
      data = Object.keys(productFilter.filters).reduce((acc, key) => {
        acc[key] = productFilter?.filters[key].id;
        return acc;
      }, {} as FilterData);
    }

    const options = productService.transformFilterServer({
      ...(category !== '' ? data : null),
      order,
      search,
      id_usuario: '',
      cidade: citySelected,
    });
    productService
      .getProducts(options)
      .then(item => {
        setProducts(item);
        setLoading(false);
      })
      .catch(error => {
        console.log(error);
        setLoading(false);
      });
  }, [category, citySelected, order, productFilter.filters, search]);

  return (
    <Container>
      <Header />
      <Search value={search} setValue={handleSearch} type="Product" />
      <ContainerBody>
        <ListCategories
          category={
            productFilter.filters && productFilter.filters.category
              ? productFilter.filters.category.id
              : ''
          }
          setCategory={handleCategory}
        />
        <ListOferts onPress={item => handleOrder(item)} value={order} />
        {loading ? (
          <ContentLoad>
            <TitleLoad>Aguarde, carregando anuncios...</TitleLoad>
            <Load />
          </ContentLoad>
        ) : (
          <ListProducts
            data={products}
            title="Ofertas do dia"
            showLogo={true}
            hasPadding={true}
          />
        )}

        <ModalLocation
          visible={visibleModalLocation}
          setVisivelModalLocation={setVisibleModalLocation}
          getCitySelected={getCitySelected}
        />
      </ContainerBody>
    </Container>
  );
};

export default Home;
