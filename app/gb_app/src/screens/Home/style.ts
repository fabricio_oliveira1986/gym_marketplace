import styled from 'styled-components/native';
import {RFValue} from 'react-native-responsive-fontsize';

export const Container = styled.View`
  flex: 1;
  background: ${({theme}) => theme.colors.shape};
`;

export const ContainerBody = styled.ScrollView`
  flex: 1;
`;

export const ContentLoad = styled.View`
  flex: 1;
  width: 100%;
  justify-content: center;
  align-items: center;
`;

export const TitleLoad = styled.Text`
  font-size: ${RFValue(16)}px;
  font-family: ${({theme}) => theme.fonts.medium};
  margin-bottom: ${RFValue(20)}px;
`;
