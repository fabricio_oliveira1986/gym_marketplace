import React from 'react';
import {useTheme} from 'styled-components/native';
import Images from '~/assets/images/Logos';

import {Container, Image} from './style';

const Splash: React.FC = () => {
  const theme = useTheme();
  return (
    <Container colors={[theme.colors.linear1, theme.colors.linear2]}>
      <Image source={Images.LogoSplash} />
    </Container>
  );
};

export default Splash;
