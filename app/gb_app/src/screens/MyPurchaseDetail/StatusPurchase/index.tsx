import React from 'react';
import {useTheme} from 'styled-components/native';
import {Text, Box, BoxPercent} from '~/components/index';
import {
  dateBr,
  getTypeStatusPayment,
  getTypePayment,
  getTypeStatusDelivery,
} from '~/global/utils/functions';
import DataPurchase from '../DataPurchase';
import TitleItemPurchased from '../TitleItemPurchased';

const StatusPurchase: React.FC = ({data}) => {
  const theme = useTheme();
  return (
    <>
      <TitleItemPurchased title="Status da Compra" theme={theme} />

      <BoxPercent width="100%" pb={10} mt={10}>
        <DataPurchase label="Data da Compra" data={dateBr(data.data)} mb={5} />

        <DataPurchase
          mb={5}
          label="Tipo de Pagamento"
          data={getTypePayment(data.tipo_documento).description}
        />

        {data.data_entrega && (
          <DataPurchase
            mb={5}
            label="Data da Entrega"
            data={dateBr(data.data_entrega)}
          />
        )}

        <DataPurchase
          label="Data do Pagamento"
          data={dateBr(data.data_pagamento)}
          mb={5}
        />
        <DataPurchase
          mb={5}
          badge
          label="Status Pagamento"
          colorBadge={getTypeStatusPayment(data.status).color}
          data={getTypeStatusPayment(data.status).description}
        />
        <DataPurchase
          mb={5}
          badge
          label="Status da Entrega"
          colorBadge={getTypeStatusDelivery(data.entrega).color}
          data={getTypeStatusDelivery(data.entrega).description}
        />
      </BoxPercent>
    </>
  );
};

export default StatusPurchase;
