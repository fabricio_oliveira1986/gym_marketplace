import React from 'react';
import {BoxPercent} from '~/components/index';
import TitleItemPurchased from '../TitleItemPurchased';
import DataPurchase from '../DataPurchase';

const DataFinance: React.FC = ({data, theme}) => {
  return (
    <>
      <TitleItemPurchased title="Dados Financeiro" theme={theme} />
      <DataPurchase
        label="Nome"
        data={data.id_cartao.nome_cartao}
        mt={10}
        mb={8}
      />
      <DataPurchase label="Cartão" data={data.id_cartao.cartao} />
      <DataPurchase label="Bandeira" data={data.id_cartao.bandeira} />
    </>
  );
};

export default DataFinance;
