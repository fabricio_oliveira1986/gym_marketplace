import React, {useState} from 'react';
import {useTheme} from 'styled-components/native';
import {useNavigation} from '@react-navigation/native';
import Clipboard from '@react-native-clipboard/clipboard';
import {Alert, Linking} from 'react-native';
import {Icon} from 'react-native-elements';
import Header from '~/components/Header';
import {
  HeaderTitle,
  Card,
  ContentScroll,
  Text,
  BoxTouchablePercent,
  Button,
  BoxPercent,
  Load,
  Box,
} from '~/components/index';
import ItensPurchased from './ItensPurchased';
import StatusPurchase from './StatusPurchase';
import DataFinance from './DataFinance';
import * as MyPurchaseServices from '~/services/purchases';
import {diffTwoDates} from '~/global/utils/functions';
import {CommonActions} from '@react-navigation/native';
import moment from 'moment';

const MyPurchaseDetail: React.FC = props => {
  const [isLoading, setIsLoading] = useState(false);
  const navigation = useNavigation();
  const theme = useTheme();
  const {
    params: {data},
  } = props.route;

  const cancelMyPurchase = async id => {
    try {
      setIsLoading(true);
      await MyPurchaseServices.cancelMyPurchases(id);
      await Alert.alert('', 'Venda excluida com sucesso!');
      navigation.dispatch(
        CommonActions.reset({
          index: 0,
          routes: [{name: 'Home'}],
        }),
      );
      setIsLoading(false);
    } catch (error) {
      Alert.alert('', 'Não foi possível cancelar sua venda');
      setIsLoading(false);
    }
  };

  const copyTextClipBoard = async (text: string, msgSuccess: string) => {
    try {
      Clipboard.setString(text);
      Alert.alert('', msgSuccess);
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <ContentScroll>
      <Header />
      <HeaderTitle>Detalhes da Compra</HeaderTitle>
      <BoxTouchablePercent
        direction
        mt={20}
        mb={5}
        ml={15}
        align="center"
        onPress={() => navigation.pop()}>
        <Icon
          type="ionicon"
          name={'chevron-back-outline'}
          color={theme.colors.text}
          size={20}
        />
        <Text bold color={theme.colors.text} fontSize={10}>
          VOLTAR
        </Text>
      </BoxTouchablePercent>

      {isLoading ? (
        <Load />
      ) : (
        <Box ml={25} mr={25} mb={20}>
          <Card>
            <StatusPurchase data={data} />
            {data.id_cartao && <DataFinance data={data} theme={theme} />}
            <ItensPurchased data={data} theme={theme} />

            {data.tipo_documento === 'B' && data.status === 'A' && (
              <>
                <BoxPercent
                  width="100%"
                  borderTopWidth={1}
                  pt={7}
                  borderColor={theme.colors.text}>
                  <Button
                    onPress={() => {
                      copyTextClipBoard(
                        data.charge_linha,
                        'Linha digitável copiado com sucesso!',
                      );
                    }}
                    block
                    fontBold
                    uppercase
                    color={theme.colors.dark}
                    fontSize={12}
                    borderRadius={5}>
                    COPIAR LINHA DIGITÁVEL BOLETO
                  </Button>
                </BoxPercent>
                <BoxPercent
                  width="100%"
                  borderTopWidth={1}
                  pt={7}
                  borderColor={theme.colors.text}>
                  <Button
                    onPress={() => {
                      Linking.openURL(data.charge_link);
                    }}
                    block
                    fontBold
                    uppercase
                    color={theme.colors.dark}
                    fontSize={12}
                    borderRadius={5}>
                    SEGUNDA VIA BOLETO
                  </Button>
                </BoxPercent>
              </>
            )}

            {data.tipo_documento === 'P' && data.status === 'A' && (
              <BoxPercent
                width="100%"
                borderTopWidth={1}
                pt={7}
                borderColor={theme.colors.edit}>
                <Button
                  onPress={() => {
                    copyTextClipBoard(
                      data.charge_linha,
                      'Código PIX copiado com sucesso!',
                    );
                  }}
                  block
                  fontBold
                  uppercase
                  color={theme.colors.dark}
                  fontSize={12}
                  borderRadius={5}>
                  COPIAR PIX
                </Button>
              </BoxPercent>
            )}

            {data.tipo_documento === 'C' &&
              data.status === 'P' &&
              diffTwoDates(
                new Date(),
                new Date(
                  moment(data.data_pagamento).year(),
                  moment(data.data_pagamento).month(),
                  moment(data.data_pagamento).date(),
                ),
              ) <= 7 &&
              data.entrega === 'P' && (
                <BoxPercent
                  width="100%"
                  borderTopWidth={1}
                  pt={7}
                  borderColor={theme.colors.text}>
                  <Button
                    onPress={() => {
                      Alert.alert('', 'Deseja realmente cancelar a compra?', [
                        {
                          text: 'Não',
                          onPress: () => console.log('Cancel Pressed'),
                          style: 'cancel',
                        },
                        {
                          text: 'Sim',
                          onPress: () => cancelMyPurchase({txtVenda: data.id}),
                        },
                      ]);
                    }}
                    block
                    fontBold
                    uppercase
                    color={theme.colors.dark}
                    fontSize={12}
                    borderRadius={5}>
                    CANCELAR COMPRA
                  </Button>
                </BoxPercent>
              )}
          </Card>
        </Box>
      )}
    </ContentScroll>
  );
};

export default MyPurchaseDetail;
