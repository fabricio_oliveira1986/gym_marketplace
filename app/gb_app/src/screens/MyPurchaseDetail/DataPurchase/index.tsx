import React from 'react';
import {useTheme} from 'styled-components/native';
import {RFValue} from 'react-native-responsive-fontsize';
import {Text, Box, Bagde, BoxPercent} from '~/components/index';
import {Avatar, Badge, Icon, withBadge} from 'react-native-elements';

const DataPurchase: React.FC = ({
  label,
  data,
  mb,
  badge,
  colorBadge,
  mt,
  ml,
}) => {
  const theme = useTheme();
  return (
    <BoxPercent mb={mb || 8} mt={mt} ml={ml} direction width="100%">
      <Text color={theme.colors.title} bold>
        {label}:{' '}
      </Text>
      {badge ? (
        <BoxPercent width="100%">
          <Badge
            value={data}
            badgeStyle={{
              backgroundColor: colorBadge,
              paddingLeft: RFValue(10),
              paddingRight: RFValue(10),
            }}
            containerStyle={{}}
          />
        </BoxPercent>
      ) : (
        <Text color={theme.colors.title}>{data}</Text>
      )}
    </BoxPercent>
  );
};

export default DataPurchase;
