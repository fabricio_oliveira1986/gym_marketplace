import React from 'react';
import {Box, Text, HR} from '~/components/index';

const TitleItemPurchase: React.FC = ({theme, title}) => {
  return (
    <>
      <Box pt={10} pb={10}>
        <Text bold color={theme.colors.success} transform="uppercase">
          {title}
        </Text>
      </Box>
      <HR />
    </>
  );
};

export default TitleItemPurchase;
