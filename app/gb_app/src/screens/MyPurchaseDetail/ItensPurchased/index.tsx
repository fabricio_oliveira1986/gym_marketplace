import React from 'react';
import TitleItemPurchased from '../TitleItemPurchased';
import ItemPurchase from '../ItemPurchased/index';

const ItensPurchase: React.FC = ({data, theme}) => {
  return (
    <>
      <TitleItemPurchased theme={theme} title="Itens Comprado" />
      {data.itens.map((item, index) => (
        <ItemPurchase key={index} data={item} theme={theme} />
      ))}
    </>
  );
};

export default ItensPurchase;
