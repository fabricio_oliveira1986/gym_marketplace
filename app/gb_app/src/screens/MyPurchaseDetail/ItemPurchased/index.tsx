import React from 'react';
import {Linking} from 'react-native';
import {BoxPercent, Text, BoxTouchablePercent} from '~/components/index';
import {ItemImage} from './style';
import {numberReal} from '~/global/utils/functions';
import DataPurchase from '../DataPurchase';
import TitleItemPurchased from '../TitleItemPurchased';

const ItemPurchase: React.FC = ({data, theme}) => {
  return (
    <BoxPercent
      pt={10}
      pb={10}
      direction
      width="100%"
      borderBottomWidth={1}
      borderColor={theme.colors.text}>
      <BoxPercent>
        <ItemImage
          source={{
            uri: data.image,
          }}
        />
      </BoxPercent>
      <BoxPercent style={{flex: 1}} pl={10}>
        <DataPurchase label="Nome" data={data.nome} />
        <DataPurchase label="Quantidade" data={data.quantidade} />
        <DataPurchase label="Valor" data={numberReal(data.valor)} />
        <DataPurchase label="Cash Back" data={numberReal(data.cashback)} />
        <DataPurchase label="Descrição" data={data.descricao} />

        <TitleItemPurchased theme={theme} title=" Dados do Vendedor" />
        <DataPurchase label="Nome" data={data.nome_fantasia} mt={8} />
        <BoxTouchablePercent
          onPress={() => {
            Linking.openURL(`tel:${data.celular}`);
          }}>
          <DataPurchase label="Telefone" data={data.celular} />
        </BoxTouchablePercent>

        <DataPurchase label="E-mail" data={data.email} />
      </BoxPercent>
    </BoxPercent>
  );
};

export default ItemPurchase;
