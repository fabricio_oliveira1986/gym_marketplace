import React, { useState, useEffect} from 'react';
import {useNavigation} from '@react-navigation/native';
import {useTheme} from 'styled-components';
import Header from '~/components/Header';
import Load from '~/components/Load';
import * as bussinessService from '~/services/bussiness';
import {
  Container as ContainerSearchWrapper,
  ContainerSearch,
  TextSearch,
} from '../../components/Search/style';
import {Bussiness as BussinessModel} from '~/models/Bussiness';
import ImagesIcon from '~/assets/images/Icons';
import {useFilter} from '~/hooks/filter';

import {
  Container,
  ContainerBody,
  InfoResult,
  TextResult,
  ListBussiness,
  Item,
  ItemImage,
  ContainerInfo,
  TitleBussiness,
  InfoBussiness,
  RatingBussiness,
  TextRating,
} from './style';

interface FilterData {
  [key: string]: string;
}

const Bussiness: React.FC = () => {
  const theme = useTheme();
  const [bussinesses, setBussinesses] = useState<BussinessModel[]>([]);
  const [bussinessesFiltered, setBussinessesFiltered] = useState<
    BussinessModel[]
  >([]);
  const [loading, setLoading] = useState(true);
  const [search, setSearch] = useState('');

  const {bussinessFilter} = useFilter();

  useEffect(() => {
    const result = bussinesses.filter(item => item.name.includes(search));
    setBussinessesFiltered(result);
  }, [bussinesses, search]);

  useEffect(() => {
    let data = {};
    const options = bussinessService.transformFilterServer({...data, search});
    bussinessService.getBussinesses(options).then(item => {
      setBussinesses(item);
      setLoading(false);
    });
  }, [bussinessFilter.filters, search]);

  return (
    <Container>
      <Header />
      <ContainerSearchWrapper>
        <ContainerSearch>
          <ImagesIcon.Search width={15} height={15} fill={theme.colors.text} />
          <TextSearch
            placeholder="Buscar"
            placeholderTextColor={theme.colors.text}
            value={search}
            onChangeText={setSearch}
          />
        </ContainerSearch>
      </ContainerSearchWrapper>
      <ContainerBody>
        {bussinessesFiltered.length === 0 && (
          <InfoResult>
            <ImagesIcon.Search
              width={12}
              height={12}
              fill={theme.colors.text}
            />
            <TextResult>
              {bussinessesFiltered.length} resultados encontrados{' '}
              {search && "para '" + search + "'"}
            </TextResult>
          </InfoResult>
        )}

        {loading ? (
          <Load />
        ) : (
          bussinesses && (
            <ListBussiness
              data={bussinessesFiltered}
              keyExtractor={bussiness => bussiness.id_address}
              renderItem={({item: bussiness}) => (
                <ItemBussiness data={bussiness} />
              )}
            />
          )
        )}
      </ContainerBody>
    </Container>
  );
};

interface BussinessProps {
  data: BussinessModel;
}

const ItemBussiness: React.FC<BussinessProps> = ({data: bussiness}) => {
  const navigation = useNavigation();
  const theme = useTheme();
  return (
    <Item
      onPress={() =>
        navigation.navigate('BussinessSingle', {
          data: bussiness,
        })
      }>
      <ItemImage source={{uri: bussiness.image}} />
      <ContainerInfo>
        <TitleBussiness>{bussiness.name}</TitleBussiness>
        <InfoBussiness>
          {bussiness.neighborhood +
            (bussiness.distance ? ' | ' + bussiness.distance + 'KM' : '')}
        </InfoBussiness>
        <RatingBussiness>
          <ImagesIcon.Star
            height={20}
            width={18}
            color={theme.colors.warning}
          />
          <TextRating>{bussiness.rating}</TextRating>
        </RatingBussiness>
      </ContainerInfo>
    </Item>
  );
};

export default Bussiness;
