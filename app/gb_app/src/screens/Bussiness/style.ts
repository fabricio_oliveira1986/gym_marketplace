import styled from 'styled-components/native';

import {FlatList} from 'react-native';
import {RFValue} from 'react-native-responsive-fontsize';

import Bussiness from '~/models/Bussiness';

export const Container = styled.View`
  flex: 1;
  background-color: ${({theme}) => theme.colors.shape};
`;

export const ContainerBody = styled.View`
  flex: 1;
  margin-top: ${RFValue(10)}px;
`;

export const InfoResult = styled.View`
  flex-direction: row;
  align-items: center;
  margin: ${RFValue(10)}px ${RFValue(15)}px;
`;

export const TextResult = styled.Text`
  font-family: ${({theme}) => theme.fonts.regular};
  font-size: ${RFValue(12)}px;
  color: ${({theme}) => theme.colors.text};
  margin-left: ${RFValue(10)}px;
`;

export const ListBussiness = styled(
  FlatList as new () => FlatList<Bussiness>,
).attrs({
  showsVerticalScrollIndicator: false,
})``;

export const Item = styled.TouchableOpacity.attrs({
  activeOpacity: 0.7,
})`
  margin: ${RFValue(10)}px ${RFValue(15)}px;
  background-color: ${({theme}) => theme.colors.background};
  border-radius: ${RFValue(10)}px;
  flex-direction: row;
  align-items: center;
  padding: ${RFValue(15)}px;
`;

export const ItemImage = styled.Image.attrs({
  resizeMode: 'cover',
})`
  width: ${RFValue(85)}px;
  height: ${RFValue(85)}px;
`;

export const ContainerInfo = styled.View`
  flex: 1;
  padding-left: ${RFValue(15)}px;
`;

export const TitleBussiness = styled.Text`
  font-family: ${({theme}) => theme.fonts.medium};
  font-size: ${RFValue(14)}px;
  color: ${({theme}) => theme.colors.title};
  text-transform: uppercase;
`;

export const InfoBussiness = styled.Text`
  font-family: ${({theme}) => theme.fonts.medium};
  font-size: ${RFValue(14)}px;
  color: ${({theme}) => theme.colors.title};
  margin-top: ${RFValue(3)}px;
`;

export const RatingBussiness = styled.View`
  flex-direction: row;
  align-items: center;
  margin-top: ${RFValue(3)}px;
`;

export const TextRating = styled.Text`
  font-family: ${({theme}) => theme.fonts.medium};
  font-size: ${RFValue(16)}px;
  color: ${({theme}) => theme.colors.success};
  margin-left: ${RFValue(5)}px;
`;
