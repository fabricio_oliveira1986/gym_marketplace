import React, {useState} from 'react';
import {Alert} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {useTheme} from 'styled-components/native';
import * as Yup from 'yup';
import {yupResolver} from '@hookform/resolvers/yup';
import {useForm} from 'react-hook-form';
import {Background, LoadFull, Box} from '~/components/index';
import * as userServices from '~/services/user';
import {UserPasswordResetData} from '~/models/User';
import {
  Container,
  Card,
  CardTitle,
  BoxForm,
  InputForm,
  ButtonForm,
  TextButtonForm,
  BoxLink,
  ButtonLink,
  TextButtonLink,
} from './style';

const ResetPassword: React.FC = () => {
  const [isLoading, setIsLoading] = useState(false);
  const navigation = useNavigation();
  const theme = useTheme();

  const schema = Yup.object().shape({
    txtEmail: Yup.string()
      .required('E-mail é obrigatório')
      .email('Digite um e-mail válido'),
  });

  const {
    control,
    handleSubmit,
    formState: {errors},
  } = useForm({
    resolver: yupResolver(schema),
  });

  const resetPassword = async (form: UserPasswordResetData) => {
    try {
      setIsLoading(true);
      const response = await userServices.resetPassword(form);
      if (response.send) {
        Alert.alert(
          '',
          'E-mail enviado com os dados para recuperação de senha!',
        );
      }
      setIsLoading(false);
    } catch (error) {
      Alert.alert(
        '',
        'Não foi possível recuperar sua senha! Entre em contato com o administrador',
      );
      setIsLoading(false);
    }
  };

  const goToBack = () => {
    navigation.goBack();
  };

  return (
    <Container>
      <Background>
        <Card>
          <Box>
            {isLoading && <LoadFull />}
            <CardTitle>Esqueci Minha Senha</CardTitle>
            <BoxForm>
              <InputForm
                control={control}
                name="txtEmail"
                placeholder="EMAIL"
                keyboardType="email-address"
                placeholderTextColor={theme.colors.placeholder}
                color={theme.colors.text}
                error={errors.email && errors.email.message}
              />

              <ButtonForm
                onPress={handleSubmit(resetPassword)}
                activeOpacity={0.4}>
                <TextButtonForm>Enviar</TextButtonForm>
              </ButtonForm>
            </BoxForm>
            <BoxLink>
              <ButtonLink onPress={goToBack}>
                <TextButtonLink>Voltar</TextButtonLink>
              </ButtonLink>
            </BoxLink>
          </Box>
        </Card>
      </Background>
    </Container>
  );
};

export default ResetPassword;
