import styled from 'styled-components/native';
import { RFValue } from 'react-native-responsive-fontsize';
import InputFormComponent from '~/components/Form/InputForm';

export const Container = styled.KeyboardAvoidingView`
  flex: 1;
`;

export const Card = styled.View`
  margin: ${RFValue(30)}px;
  background: ${({ theme }) => theme.colors.shape};
  padding: ${RFValue(40)}px ${RFValue(30)}px;
  border-radius: ${RFValue(5)}px;
  box-shadow: 0px 0px ${RFValue(2)}px ${({ theme }) => theme.colors.shadow};
`;

export const CardTitle = styled.Text`
  font-size: ${RFValue(20)}px;
  text-align: center;
  margin-bottom: ${RFValue(30)}px;
  text-transform: uppercase;
`;

export const Logo = styled.Image.attrs({
  resizeMode: 'contain',
})`
  width: ${RFValue(90)}px;
  align-self: center;
  margin-bottom: ${RFValue(20)}px;
`;

export const BoxForm = styled.View``;

export const InputForm = styled(InputFormComponent).attrs({
  height: 40,
  fontSize: 14,
  typeInput: 'line',
  marginBottom: 10,
})``;

export const ButtonForm = styled.TouchableOpacity.attrs({
  activeOpacity: 0.7,
})`
  height: ${RFValue(40)}px;
  margin-top: ${RFValue(10)}px;
  border-color: ${({ theme }) => theme.colors.dark};
  border-width: ${RFValue(1)}px;
  border-radius: ${RFValue(5)}px;
  justify-content: center;
  align-items: center;
`;

export const TextButtonForm = styled.Text`
  font-size: ${RFValue(14)}px;
  font-weight: bold;
  color: ${({ theme }) => theme.colors.dark};
  text-transform: uppercase;
`;

export const BoxLink = styled.View`
  margin-top: ${RFValue(40)}px;
  flex-direction: row;
`;

export const ButtonLink = styled.TouchableOpacity.attrs({
  activeOpacity: 0.7,
})`
  border-bottom-color: ${({ theme }) => theme.colors.title};
  border-bottom-width: ${RFValue(1)}px;
`;

export const TextButtonLink = styled.Text`
  font-family: ${({ theme }) => theme.fonts.regular};
  color: ${({ theme }) => theme.colors.title};
  text-transform: uppercase;
  font-size: ${RFValue(10)}px;
`;
